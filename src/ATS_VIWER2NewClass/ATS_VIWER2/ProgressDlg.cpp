// ProgressDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ATS_VIWER2.h"
#include "ProgressDlg.h"

CRect mRect;
CString gText("处理中,请稍后...");
// CProgressDlg 对话框

IMPLEMENT_DYNAMIC(CProgressDlg, CDialog)

CProgressDlg::CProgressDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDlg::IDD, pParent)
{	
	m_flag = 0;
}

CProgressDlg::~CProgressDlg()
{
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CProgressDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CProgressDlg 消息处理程序

BOOL CProgressDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_bkBrush.CreateSolidBrush(RGB(28,33,38)); 
	//m_bkBrush.CreateSolidBrush(RGB(255,255,0));
	GetClientRect(mRect);
	mRect.bottom = 300;
	mRect.right = 400;
	if(mPic.Create("sql_pic_state",WS_CHILD|WS_VISIBLE,mRect,this,1111) == FALSE) 
	{
		MessageBox("create PictrueBox faild");
	}

	if(mPic.Load("res/wait.gif"))
		mPic.Draw();
	::SetWindowLong( m_hWnd, GWL_EXSTYLE, GetWindowLong(m_hWnd, GWL_EXSTYLE) | WS_EX_LAYERED);  
	::SetLayeredWindowAttributes( m_hWnd, 0, 200, LWA_ALPHA); // 120是透明度，范围是0～255
	SetWindowPos (&CWnd::wndTopMost, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

HBRUSH CProgressDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if(nCtlColor == CTLCOLOR_DLG)   // 判断是否是对话框  
	{  
		return   m_bkBrush; // 返回刚才创建的背景刷子  
	}
	return hbr;
}

void CProgressDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	dc.SetBkMode(TRANSPARENT);
	CRect re;
	GetClientRect(re);

	CRect client;
	client.SetRect(re.left,re.top + 300, re.right,re.bottom);
	dc.SetTextColor(RGB(255,255,0));
	dc.DrawText(gText,client,DT_VCENTER|DT_CENTER|DT_SINGLELINE);
}


void CProgressDlg::pro_set_text(CString text)
{
	gText = "";
	gText =  text;
}
void CProgressDlg::OnOK()
{
	// TODO: 在此添加专用代码和/或调用基类
	
	CDialog::OnOK();
}

void CProgressDlg::close()
{
	gText = "玩命处理中,请稍后...";
	OnOK();
}

void CProgressDlg::m_show(char flag, int timeout /* = 3000 */)
{
	if (flag)
	{
		ShowWindow(SW_SHOW);
		if(0 == m_flag)
		{
			m_flag = 1;

			if(mPic.Load("res/wait.gif"))
				mPic.Draw();
		}

	}
	else
	{
		ShowWindow(SW_HIDE);
		m_flag = 0;
		mPic.UnLoad();
		KillTimer(1);
	}

	if(timeout > 0)
	{
		KillTimer(1);
		SetTimer(1,timeout,NULL);
	}
}

bool CProgressDlg::is_show()
{
	if(m_flag)return true;

	return false;
}
void CProgressDlg::OnTimer(UINT_PTR nIDEvent)
{
	
	if(1 == nIDEvent)
	{
		SendMessage(WM_CLOSE);
		m_flag = 0;
	}

	CDialog::OnTimer(nIDEvent);
}
