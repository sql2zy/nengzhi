// CMinDisUnitDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "CMinDisUnitDlg.h"


// CCMinDisUnitDlg 对话框

IMPLEMENT_DYNAMIC(CCMinDisUnitDlg, CDialog)

CCMinDisUnitDlg::CCMinDisUnitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCMinDisUnitDlg::IDD, pParent)
{

}

CCMinDisUnitDlg::~CCMinDisUnitDlg()
{
}

void CCMinDisUnitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COB_MIN_DIS_UNIT, m_combox_min);
}


BEGIN_MESSAGE_MAP(CCMinDisUnitDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCMinDisUnitDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CCMinDisUnitDlg 消息处理程序

BOOL CCMinDisUnitDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString tmp("");

	for (int i=0; i<7; i++)
	{
		tmp.Format("%d",i+1);
		m_combox_min.AddString(tmp);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CCMinDisUnitDlg::OnBnClickedOk()
{
	CString tmp("");
	m_combox_min.GetWindowText(tmp);
	m_min_dis = atoi((LPTSTR)(LPCTSTR)tmp);
	OnOK();
}
