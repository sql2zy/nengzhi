// CPingBaodlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ATS_VIWER2.h"
#include "CPingBaodlg.h"

extern	CCBatteryGIF gBatGIF;
// CCPingBaodlg 对话框

IMPLEMENT_DYNAMIC(CCPingBaodlg, CDialog)

CCPingBaodlg::CCPingBaodlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCPingBaodlg::IDD, pParent)
{

}

CCPingBaodlg::~CCPingBaodlg()
{
}

void CCPingBaodlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCPingBaodlg, CDialog)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONUP()
	ON_WM_KEYDOWN()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// CCPingBaodlg 消息处理程序

BOOL CCPingBaodlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_first_flag = 0;

	//删除窗口的标题栏
	LONG   style  = GetWindowLong(m_hWnd,GWL_STYLE);   
	style &= ~WS_CAPTION;   
	SetWindowLong(m_hWnd,GWL_STYLE,style); //设置显示窗口状态  
	//获得显示器屏幕的宽度  
	m_cx  =  GetSystemMetrics(SM_CXSCREEN);   
	m_cy  =  GetSystemMetrics(SM_CYSCREEN);   
	//show        window   
	SetWindowPos(NULL,0,0,m_cx,m_cy,SWP_NOZORDER);
	//获得鼠标的位置
	::GetCursorPos(&w_point);
	//隐藏鼠标
	//ShowCursor(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CCPingBaodlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CBitmap  * oldbmp;
	CRect rect,client;
	GetClientRect(client);
	GetClientRect(rect);
 
	rect.SetRect(0,0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN));

	if(0 == m_first_flag)
	{
		m_memDC.CreateCompatibleDC(NULL);

		//m_memBmp.CreateCompatibleBitmap(&dc,rect.Width(),rect.Height());
		m_memBmp.CreateCompatibleBitmap(&dc,m_cx,m_cy);
		m_memDC.SelectObject(m_memBmp);
		m_memDC.SetBkMode(TRANSPARENT);
		m_first_flag = 1;

	}

	gBatGIF.batEx_set_rect(rect,rect);
	gBatGIF.batEx_get_memDC(&m_memDC ,rect,&dc);

	//dc.BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&m_memDC,0,0,SRCCOPY);
	dc.BitBlt(0,0,m_cx,m_cy,&m_memDC,0,0,SRCCOPY);
	/*
	CDC dcMemory;
	CBitmap bitmap;
	dcMemory.CreateCompatibleDC(&dc);
	bitmap.CreateCompatibleBitmap(&dc,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN));
	dcMemory.SelectObject(&bitmap);
	
	gBatGIF.batEx_set_rect(rect,rect);
	gBatGIF.batEx_get_memDC(&dcMemory ,rect,&dc);

	dc.BitBlt(0,0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN),&dcMemory,0,0,SRCCOPY);
	*/

}


//开始
void CCPingBaodlg::start()
{
	CRect rect;      
	GetClientRect(&rect);     //取客户区大小    
	//mOld.x=rect.right-rect.left;  
	//mOld.y=rect.bottom-rect.top;  
	//m_cx = GetSystemMetrics(SM_CXFULLSCREEN);  
	//m_cy = GetSystemMetrics(SM_CYFULLSCREEN);  
	ShowWindow(SW_SHOW);
	//CRect rt;  
	//SystemParametersInfo(SPI_GETWORKAREA,0,&rt,0);  
	//m_cy  = rt.bottom;  
	//MoveWindow(0, 0, m_cx, m_cy ); 
	
	SetTimer(1,55,NULL);

}

//停止
void CCPingBaodlg::stop()
{
	KillTimer(1);
	ShowWindow(SW_HIDE);
}
void CCPingBaodlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(1 == nIDEvent)
	{
		Invalidate(TRUE);
	}
	CDialog::OnTimer(nIDEvent);
}

void CCPingBaodlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CRect client;
	GetClientRect(client);
	gBatGIF.batEx_set_rect(client,client);
}

void CCPingBaodlg::OnMouseMove(UINT nFlags, CPoint point)
{
	//ShowCursor(TRUE);
	//stop();
	CDialog::OnMouseMove(nFlags, point);
}

void CCPingBaodlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialog::OnRButtonUp(nFlags, point);
}

void CCPingBaodlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	//ShowCursor(TRUE);
	stop();

	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CCPingBaodlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	return TRUE;

	return CDialog::OnEraseBkgnd(pDC);
}
