
// ATS_VIWER2View.h : CATS_VIWER2View 类的接口
//


#pragma once
#include "afxext.h"
#include "BtnST.h"
#include "include\batteryEX.h"
#include "include\comm.h"
#include "CurvePlotDlg.h"
#include "gdipbutton.h"
#include "cla\CCObject.h"
#include "ATS_VIWER2Doc.h"


#define BATTERY_MAX 16
#define BATTERY_ID0 2001

#define PINGBAO_TIMEOUT 10


typedef struct __text_information
{
	CRect rect;	//位置
	CString title;

}sys_text_info_t;


class CATS_VIWER2View : public CFormView
{
protected: // 仅从序列化创建
	CATS_VIWER2View();
	DECLARE_DYNCREATE(CATS_VIWER2View)

public:
	enum{ IDD = IDD_ATS_VIWER2_FORM };

// 属性
public:
	CATS_VIWER2Doc* GetDocument() const;

// 操作
public:

// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual void OnInitialUpdate(); // 构造后第一次调用

// 实现
public:
	virtual ~CATS_VIWER2View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:

	/***
     * @brief: 更新界面
	 * @param obj:
	 */
	void show_message(CBatObject* obj);

	//改变显示风格
	void change_view_style();

	/***
	 * @brief: 更换当前模块
	 * @param id: 模块序号
	 */
	void v_update_moduel_selected(char id);


	/***
	 * @brief: 更新模块信息
	 * @param mid:
	 */
	void v_update_module_info(char mid);


	/***
	 * @brief: 获取工作模式
	 */
	char v_get_system_status();

private:

	CDC* m_pDC;
	CDC			m_memDC;		//动画显示DC

	CBitmap		m_memBmp;		//动画显示画布

	int			m_first_flag;	//动画环境十分初始化

	CString		m_uid;

	void sql_update_mod_btn(char selected, ATS_sys_info_t* asit = NULL);


	//初始化电池信息
	void sql_init_battery();


	//更新内容
	void sql_update_ui();


	//更新电池状态
	void sql_udpate_bat(ATS_sys_info_t* asit, uchar id = 0);


	//处理电池的动作
	void sql_battery_action(uint message, WPARAM wParam, LPARAM lParam);

	//添加数据
	void sql_curve_add_data(ATS_ident_t* ait, DATA_TYPE_E dte, float value);

	void net_system_control(unsigned char str);

	//sql add
	HBITMAP m_hBitmap;


	//背景
	CBitmap mBACK;



	/***
	 * @brief: 清空
	 */
	void BatteryReset();

public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);

protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	char sql_is_net_mode();


	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	//////////////////////////////////////////////////////////////////////////
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);


	void ats_show_progress(char flag = 1, unsigned int timeout = 3000);

	/***
	 * @brief: 设置最小放电单元数
	 * @param obj:
	 * @return 
	 */
	void ats_set_min_dis_unit(CBatObject* obj);

};

#ifndef _DEBUG  // ATS_VIWER2View.cpp 中的调试版本
inline CATS_VIWER2Doc* CATS_VIWER2View::GetDocument() const
   { return reinterpret_cast<CATS_VIWER2Doc*>(m_pDocument); }
#endif

