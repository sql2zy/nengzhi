// CMainLog.cpp : 实现文件
//

#include "stdafx.h"
#include "resource.h"
#include "CMainLog.h"
#include <stdio.h>

BOOL g_desc = TRUE;

typedef struct __DATA_T   
{  
	int subitem;// 点击表头的列数  
	CListCtrl* plist; //listctrl的指针  
}DATA_T;  

CCMainLog *g_main_log = NULL;

COLORREF g_log_color[4] = {RGB(97,97,97),RGB(45,45,200),RGB(234,145,4),RGB(255,0,0)};

// CCMainLog

IMPLEMENT_DYNCREATE(CCMainLog, CFormView)

CCriticalSection g_main_lock;
CCMainLog::CCMainLog()
	: CFormView(CCMainLog::IDD)
{
	g_main_log = this;
}

CCMainLog::~CCMainLog()
{
}

void CCMainLog::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_listCtrl);
}

BEGIN_MESSAGE_MAP(CCMainLog, CFormView)
	ON_WM_SIZE()
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST1, &CCMainLog::OnLvnColumnclickList1)
END_MESSAGE_MAP()


// CCMainLog 诊断

#ifdef _DEBUG
void CCMainLog::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CCMainLog::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CCMainLog 消息处理程序

void CCMainLog::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	//初始化列表控件
	//m_listCtrl.SetHeaderHeight(1.5);          //设置头部高度
	m_listCtrl.SetHeaderFontHW(0,0);         //设置头部字体高度,和宽度,0表示缺省，自适应 
	//m_listCtrl.SetFontHW(0,0);               //设置字体高度，和宽度,0表示缺省宽度
	m_listCtrl.SetHeaderBKColor(255,255,255,8); //设置头部背景色

	m_listCtrl.InsertColumn(0,_T("时间"),LVCFMT_CENTER,100);
	m_listCtrl.InsertColumn(1,_T("机架"),LVCFMT_CENTER,60);
	m_listCtrl.InsertColumn(2,_T("单元"),LVCFMT_CENTER,60);
	m_listCtrl.InsertColumn(3,_T("消息"),LVCFMT_LEFT,1000);

	SetWindowLong(m_listCtrl.m_hWnd ,GWL_EXSTYLE,WS_EX_CLIENTEDGE);
	m_listCtrl.SetExtendedStyle(LVS_EX_GRIDLINES);                     //设置扩展风格为网格
	::SendMessage(m_listCtrl.m_hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE,
		LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
}


//改变大小
void CCMainLog::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	CRect rect;
	GetClientRect(rect);
	if(IsWindow(m_listCtrl.m_hWnd))
	{
		m_listCtrl.MoveWindow(rect);
	}
}


//添加日志
char CCMainLog::add_log(LL_LEVEL_E ll, char mac_id, char brick_id, char* format,...)
{
	SYSTEMTIME st;
	unsigned int index = 0;
	CString tmp("");
	char buf[1024] = {0};
	va_list vl;

	if (IsWindow(m_listCtrl.m_hWnd))
	{
		GetLocalTime(&st);

		g_main_lock.Lock();
		index = m_listCtrl.GetItemCount();

		//写入时间
		tmp.Format("%02d:%02d:%02d:%03d",st.wHour,st.wMinute,st.wSecond,st.wMilliseconds);
		m_listCtrl.InsertItem(index,tmp);
		
		//写入机架号与单元号
		tmp.Format("%d",mac_id);
		m_listCtrl.SetItemText(index,1,tmp);
		tmp.Format("%d",brick_id);
		m_listCtrl.SetItemText(index,2,tmp);

		//写入消息
		va_start(vl,format);
		vsprintf_s(buf,format,vl);
		m_listCtrl.SetItemText(index,3,buf);
		va_end(vl);

		//设置颜色
		m_listCtrl.SetItemTextColor(0,index,g_log_color[(int)ll]);
		m_listCtrl.SetItemTextColor(1,index,g_log_color[(int)ll]);
		m_listCtrl.SetItemTextColor(2,index,g_log_color[(int)ll]);
		m_listCtrl.SetItemTextColor(3,index,g_log_color[(int)ll]);

		//自动翻滚到最后一条
		if(m_listCtrl.GetItemCount() > 0)  
			m_listCtrl.EnsureVisible(index,FALSE); 

		g_main_lock.Unlock();
	}
	return 1;
}

/***
 * 保存日志
 */
void CCMainLog::save_log()
{
	CString filename("sql.txt");
	FILE* fd = NULL;
	SYSTEMTIME st;
	char tim[31] = {0}, mid[10] = {0},uid[10] = {0},buf[1024];

	g_main_lock.Lock();

	int count = m_listCtrl.GetItemCount();
	if(0 == count)
		MessageBox("没有日志产生");
	else
	{
		GetLocalTime(&st);

		filename.Format("log/%04d年%02d月%02d日 %02d-%02d-%02d.txt",
			st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond);

		fd = fopen(filename,"a");
		if(!fd)
			fd = fopen(filename,"a");

		if(!fd)
		{
			MessageBox("保存日志失败");
		}
		else 
		{
			for (int i=0; i<count; i++)
			{
				memset(tim,0,sizeof(tim));
				memset(mid,0,sizeof(mid));
				memset(uid,0,sizeof(uid));
				memset(buf,0,sizeof(buf));
				m_listCtrl.GetItemText(i,0,tim,sizeof(tim));
				m_listCtrl.GetItemText(i,1,mid,sizeof(mid));
				m_listCtrl.GetItemText(i,2,uid,sizeof(uid));
				m_listCtrl.GetItemText(i,3,buf,sizeof(buf));

				fprintf_s(fd,"[%s](机架:%02s 储能单元:%02s): %s\n",tim,mid,uid,buf);
			}
		}
		fclose(fd);
		MessageBox(filename,"日志保存");
	}

	g_main_lock.Unlock();
}


/***
 * 清除日志
 */
void CCMainLog::clear_log()
{
	g_main_lock.Lock();
	m_listCtrl.DeleteItems();
	g_main_lock.Unlock();
}


void CCMainLog::OnLvnColumnclickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	DATA_T data;
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	g_desc = !g_desc;
	if(1 == pNMLV->iSubItem || 2 == pNMLV->iSubItem)
	{
		for (int i=0; i< m_listCtrl.GetItemCount(); i++)
		{
			m_listCtrl.SetItemData(i,i);
		}

		data.subitem = pNMLV->iSubItem;
		data.plist = &m_listCtrl;

		 m_listCtrl.SortItems(listCompare,(LPARAM)&data); 
	}

	*pResult = 0;
}


int CALLBACK CCMainLog::listCompare(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort) 
{
	DATA_T* pListCtrl = (DATA_T*)lParamSort;   
	int col = pListCtrl->subitem;//点击的列项传递给col，用来判断点击了第几列 

	CString str1 = (pListCtrl->plist)->GetItemText(lParam1, col);
	CString str2 = (pListCtrl->plist)->GetItemText(lParam2, col);

	LPCTSTR s1=(LPCTSTR)str1;   
	LPCTSTR s2=(LPCTSTR)str2; 

	int num1 = atoi(s1);
	int num2 = atoi(s2);

	//升序
	if(g_desc)
	{
		if(num1<num2)
			return -1;
		else
			return 1;
	}
	else
	{
		if(num1<num2)
			return 1;
		else
			return -1;
	}
}