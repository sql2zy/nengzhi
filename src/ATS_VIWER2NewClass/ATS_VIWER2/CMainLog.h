#pragma once
#include "ListCtrlCl.h"
#include "resource.h"


typedef enum __LOG_LEVEL{
	LL_NORMAL =0,
	LL_NOTICE,
	LL_WARNING,
	LL_ERROR
}LL_LEVEL_E;

// CCMainLog 窗体视图

class CCMainLog : public CFormView
{
	DECLARE_DYNCREATE(CCMainLog)

protected:
	CCMainLog();           // 动态创建所使用的受保护的构造函数
	virtual ~CCMainLog();

public:
	enum { IDD = IDD_DLG_LOG_MAIN };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CListCtrlCl m_listCtrl;
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);


	/***
	 * @brief: 添加消息
	 * @param ll: 消息类型
	 * @param mac_id:
	 * @param brick_id:
	 * @param format:
	 */
	char add_log(LL_LEVEL_E ll, char mac_id, char brick_id, char* format,...);


	/***
	 * @brief: 保存日志
	 * @param null
	 * @return null
	 */
	void save_log();


	/***
	 * @brief: 清除日志
	 * @param null
	 * @return null
	 */
	void clear_log();
	afx_msg void OnLvnColumnclickList1(NMHDR *pNMHDR, LRESULT *pResult);

	static int CALLBACK listCompare(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
};


