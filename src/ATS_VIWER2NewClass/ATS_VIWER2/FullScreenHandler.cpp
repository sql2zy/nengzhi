#include "stdafx.h"    
#include "FullScreenHandler.h"  
      
    // Global object handles full-screen mode  
    CFullScreenHandler FullScreenHandler;  
      
    CFullScreenHandler::CFullScreenHandler()  
    {   
        m_rcRestore.SetRectEmpty();  
    }  
      
    CFullScreenHandler::~CFullScreenHandler()  
    {   
    }  
      
    //////////////////  
    // Resize frame so view��s client area fills the entire screen. Use  
    // GetSystemMetrics to get the screen size -- the rest is pixel  
    // arithmetic.  
    //  
    void CFullScreenHandler::Maximize(CFrameWnd* pFrame, CWnd* pView)  
    {   
        // get view rectangle  
        if (pView) {   
            CRect rcv;  
            pView->GetWindowRect(&rcv);  
            // get frame rectangle  
            pFrame->GetWindowRect(m_rcRestore); // save for restore  
            const CRect& rcf = m_rcRestore;             // frame rect  
            // now compute new rect  
            CRect rc(0,0, GetSystemMetrics(SM_CXSCREEN),  
                GetSystemMetrics(SM_CYSCREEN));  
            rc.left  += rcf.left  - rcv.left - 5;  
            rc.top   += rcf.top   - rcv.top;  
            rc.right += rcf.right - rcv.right;  
            rc.bottom+= rcf.bottom- rcv.bottom;  
            // move frame!  
            pFrame->SetWindowPos(NULL, rc.left, rc.top,  
                rc.Width(), rc.Height(), SWP_NOZORDER);  
        }  
    }  
      
    void CFullScreenHandler::Restore(CFrameWnd* pFrame)  
    {   
        const CRect& rc = m_rcRestore;  
        pFrame->SetWindowPos(NULL, rc.left, rc.top,  
            rc.Width(), rc.Height(), SWP_NOZORDER);  
        m_rcRestore.SetRectEmpty();  
    }  
      
    CSize CFullScreenHandler::GetMaxSize()  
    {   
        CRect rc(0,0,  
            GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN));  
        rc.InflateRect(10,50);  
        return rc.Size();  
    }  