#pragma once
#include "battery\PictureEx.h"
#include "resource.h"


// CProgressDlg 对话框

class CProgressDlg : public CDialog
{
	DECLARE_DYNAMIC(CProgressDlg)

public:
	CProgressDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CProgressDlg();

// 对话框数据
	enum { IDD = IDD_DLG_PROGRESS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

	CPictureEx mPic;
	CBrush m_bkBrush; 

	char m_flag;
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnPaint();
	void pro_set_text(CString text);
	void close();

	void m_show(char flag, int timeout = 0);
	bool is_show();
protected:
	virtual void OnOK();
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
