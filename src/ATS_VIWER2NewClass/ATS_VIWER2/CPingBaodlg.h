#pragma once
#include "battery\CBatBallEX.h"


// CCPingBaodlg 对话框

class CCPingBaodlg : public CDialog
{
	DECLARE_DYNAMIC(CCPingBaodlg)

public:
	CCPingBaodlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCPingBaodlg();

// 对话框数据
	enum { IDD = IDD_DLG_PINGBAO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

protected:
	CDC			m_memDC;		//动画显示DC

	CBitmap		m_memBmp;		//动画显示画布
	int			m_first_flag;	//动画环境十分初始化

	int m_cx;
	int m_cy;

	CPoint w_point;
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();



	//开始
	void start();

	//停止
	void stop();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};
