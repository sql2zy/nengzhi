#include "stdafx.h"
#include "CNameplate.h"
#include "include\comm.h"

//////////////////////////////////////////////////////////////////////////

//构造函数
CCTopHead::CCTopHead(void)
{
	m_create_flag = 0;

	m_mac_id = m_ups_id = 0;
	m_current = m_voltage = 0;

	m_bms_mode = -1;
	m_status = -1;
	m_debug = -1;

	m_min_dis_nuit = 0;
}


//析构函数
CCTopHead::~CCTopHead(void)
{

}

BEGIN_MESSAGE_MAP(CCTopHead, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

//刷新
void CCTopHead::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CString cap("");
	CRect rect, recCap, recSta, recCur;
	CFont t_CAP, t_info,* old_font = NULL;

	CDC memDC;
	CBitmap memBmp,*old_bmp = NULL;

	
	GetClientRect(rect);

	//初始化
	memDC.CreateCompatibleDC(NULL);
	memBmp.CreateCompatibleBitmap(&dc,rect.Width(),rect.Height());
	old_bmp = memDC.SelectObject(&memBmp);
	memDC.FillSolidRect(rect,RGB(255,255,255));	//绘制背景色
	memDC.SetBkMode(TRANSPARENT);	//设置透明格式
	memDC.RoundRect(rect,CPoint(30,30));
	//绘制背景
	CBatcolor m(&memDC,CTH_COLOR_BK_DEBUG_0,CTH_COLOR_BK_DEBUG_0);
	if(1 == m_debug)
		m.update(CTH_COLOR_BK_DEBUG_1,CTH_COLOR_BK_DEBUG_1);

	memDC.RoundRect(rect,CPoint(30,30));
	int sub_right = 150;
	
	//先绘制机架号
	t_CAP.CreateFont(16,               // 以逻辑单位方式指定字体的高度
		0,                               // 以逻辑单位方式指定字体中字符的平均宽度
		0,                               // 指定偏离垂线和X轴在显示面上的夹角(单位:0.1度)
		0,                               // 指定符串基线和X轴之间的夹角(单位:0.1度)
		FW_HEAVY,						 // 指定字体镑数
		FALSE,                           // 是不是斜体
		FALSE,                           // 加不加下划线
		0,                               // 指定是否是字体字符突出
		ANSI_CHARSET,                    // 指定字体的字符集
		OUT_DEFAULT_PRECIS,              // 指定所需的输出精度
		CLIP_DEFAULT_PRECIS,             // 指定所需的剪贴精度
		DEFAULT_QUALITY,                 // 指示字体的输出质量
		DEFAULT_PITCH | FF_SWISS,        // 指定字体的间距和家族
		_T("宋体")                       // 指定字体字样的名称
		);
	
	memDC.SetTextColor(RGB(137,199,137));
	old_font = memDC.SelectObject(&t_CAP);

	if(BMS_MODE_NET_GFKD == m_bms_mode)
	{
		cap.Format("机架:%02d-单元:%02d",m_mac_id,m_ups_id);
		recCap.SetRect(rect.right - sub_right,rect.top,rect.right,rect.bottom);
		memDC.DrawText(cap,recCap,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	}
	else
	{
		cap.Format("机架:%02d-单元:%02d",m_mac_id,m_ups_id);
		recCap.SetRect(rect.right - sub_right,rect.top,rect.right,rect.bottom - rect.Height()/2 + 2);
		memDC.DrawText(cap,recCap,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

		cap.Format("最小放电单元: %d",m_min_dis_nuit);
		recCap.SetRect(rect.right - sub_right,rect.top + rect.Height()/2 - 2,rect.right,rect.bottom);
		memDC.DrawText(cap,recCap,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	}


// 	m.update(CTH_COLOR_INFO,CTH_COLOR_INFO);
// 	recCur.SetRect(rect.left + rect.Width()/3,rect.top,rect.right,rect.bottom);
// 	memDC.RoundRect(recCur,CPoint(30,30));

	//绘制工作模式与状态
	t_info.CreateFont(25,				 // 以逻辑单位方式指定字体的高度
		0,                               // 以逻辑单位方式指定字体中字符的平均宽度
		0,                               // 指定偏离垂线和X轴在显示面上的夹角(单位:0.1度)
		0,                               // 指定符串基线和X轴之间的夹角(单位:0.1度)
		FW_HEAVY,						 // 指定字体镑数
		FALSE,                           // 是不是斜体
		FALSE,                           // 加不加下划线
		0,                               // 指定是否是字体字符突出
		ANSI_CHARSET,                    // 指定字体的字符集
		OUT_DEFAULT_PRECIS,              // 指定所需的输出精度
		CLIP_DEFAULT_PRECIS,             // 指定所需的剪贴精度
		DEFAULT_QUALITY,                 // 指示字体的输出质量
		DEFAULT_PITCH | FF_SWISS,        // 指定字体的间距和家族
		_T("宋体")                       // 指定字体字样的名称 方正姚体
		);

	memDC.SetTextColor(RGB(0,255,0));
	old_font = memDC.SelectObject(&t_info);
	int sub_w = (rect.Width() - sub_right)/4;

	//设置工作模式
	cap.Format("模式:%s",
		(m_bms_mode == BMS_MODE_UPS_GFKD)?"分布式UPS":(m_bms_mode == BMS_MODE_NET_GFKD?"能源互联网":"未知"));
	recCur.SetRect(rect.left,rect.top,rect.left + sub_w, rect.bottom);
	memDC.DrawText(cap,recCur,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	char tVal[31] = {0}, tCur[31] = {0};

	//设置系统状态
	CString tmp("");
	cth_get_system_status(tmp);
	cap.Format("状态:%s",tmp);
	recCur.SetRect(rect.left+sub_w,rect.top,rect.left+sub_w + sub_w, rect.bottom);
	memDC.DrawText(cap,recCur,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	
	//设置电流
	sprintf_s(tCur,sizeof(tCur),"电流:%.2fA",(float)m_current/1000);
	recCur.SetRect(rect.left+sub_w*2,rect.top,rect.left+sub_w*3, rect.bottom);
	memDC.DrawText(tCur,recCur,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	//设置电压
	sprintf_s(tVal,sizeof(tVal),"电压:%.2fV",(float)m_voltage/1000);
	recCur.SetRect(rect.left+sub_w*3,rect.top,rect.left+sub_w*4, rect.bottom);
	memDC.DrawText(tVal,recCur,DT_CENTER|DT_VCENTER|DT_SINGLELINE);


	dc.TransparentBlt(0,0,rect.Width(),rect.Height(),&memDC,0,0,rect.Width(),rect.Height(),RGB(255,255,255));

	if(memDC.m_hDC && old_bmp)
		memDC.SelectObject(old_bmp);

	m.mRelease();

	t_CAP.DeleteObject();
	t_info.DeleteObject();

	memDC.DeleteDC();
	memBmp.DeleteObject();

	DeleteDC(memDC);
}



//获取系统状态
void  CCTopHead::cth_get_system_status(CString& sta)
{
	/*
	0-空闲 
	1-放电 
	2-充电  
	3-报警 
	4-超级电容充电 
	5-UPS停机前等待
	6-UPS停机状态
	*/
	if(0 == m_status)
		sta.Format("空闲");

	else if(1 == m_status)
		sta.Format("放电");

	else if(2 == m_status)
		sta.Format("充电");

	else if(3 == m_status)
		sta.Format("报警");

	else if(4 == m_status)
		sta.Format("超级电容充电");

	else if(5 == m_status)
		sta.Format("UPS停机前等待");

	else if(6 == m_status)
		sta.Format("UPS停机状态");

	else
		sta.Format("未知");
}


//创建
int CCTopHead::mCreate(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID)
{
	if(1 == m_create_flag)
	{
		this->MoveWindow(&rect);
		return 1;
	}

	BOOL ret = CWnd::Create(NULL, NULL, dwStyle, rect,ParentWnd, nID);
	if(ret == TRUE){
		m_create_flag = 1;

		return 1;
	}

	return 0;
}