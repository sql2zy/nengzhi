#include <libs_version.h>

#include <Entity.h>


//构造
MAC_ENTITY::MAC_ENTITY()
{

}

//析构
MAC_ENTITY::~MAC_ENTITY()
{

}

//添加
bool MAC_ENTITY::mac_Add(int mac_id, int upsNum, char state /* = 0 */)
{
	bool ret = false;
	MAC_INFO_T mit;
	memset(&mit,0,sizeof(mit));
	map<int,MAC_INFO_T>::iterator iter;

	mit.mUpsNum = upsNum;
	mit.mMacID = mac_id;
	mit.mMask = state;

	g_macLock.do_Lock();
	try
	{
	
		{
			iter = g_mac_map.find(mac_id);
			if(iter == g_mac_map.end())
			{
				g_mac_map.insert(pair<int,MAC_INFO_T>(mac_id,mit));
			}
			else
			{
				iter->second.mMask = state;
			}
		}

		ret = true;
	}
	catch(...)
	{
		;
	}
	g_macLock.do_Unlock();

	return true;
}

//更新
void MAC_ENTITY::mac_Update(int mac_id, char state)
{
	map<int,MAC_INFO_T>::iterator iter;

	g_macLock.do_Lock();

	iter = g_mac_map.find(mac_id);
	if(iter != g_mac_map.end())
	{
		iter->second.mMask = state;
	}
	g_macLock.do_Unlock();
}

//获取
char MAC_ENTITY::mac_Get(int mac_id)
{
	char ret = 0;
	map<int,MAC_INFO_T>::iterator iter;

	g_macLock.do_Lock();

	iter = g_mac_map.find(mac_id);
	if(iter != g_mac_map.end())
	{
		ret = iter->second.mMask;
	}
	g_macLock.do_Unlock();

	return ret;
}

//
char MAC_ENTITY::mac_Get(char* dst, int len /*= 4*/)
{
	map<int,MAC_INFO_T>::iterator iter;;

	g_macLock.do_Lock();
	for (int i=0; i<len; i++)
	{
		iter = g_mac_map.find(i);
		
		if(iter != g_mac_map.end())
			dst[i] = iter->second.mMask;

		else
			dst[i] = 0;
	}
	g_macLock.do_Unlock();

	return 1;
}