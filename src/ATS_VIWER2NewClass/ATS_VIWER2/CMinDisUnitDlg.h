#pragma once
#include "afxwin.h"
#include "resource.h"


// CCMinDisUnitDlg 对话框

class CCMinDisUnitDlg : public CDialog
{
	DECLARE_DYNAMIC(CCMinDisUnitDlg)

public:
	CCMinDisUnitDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCMinDisUnitDlg();

// 对话框数据
	enum { IDD = IDD_DLG_MIN_DIS_UNIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_combox_min;
	virtual BOOL OnInitDialog();
	char m_min_dis;
	afx_msg void OnBnClickedOk();
};
