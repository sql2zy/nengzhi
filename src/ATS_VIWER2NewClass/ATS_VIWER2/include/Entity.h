#ifndef HEADER_ENTITY_SHIQINGLIANG__
#define HEADER_ENTITY_SHIQINGLIANG__
#include <iostream>
#include <Windows.h>
#include <map>
#include <CLock.h>
using namespace std;

typedef struct __MAC_INFO
{
	int mMacID;
	char mMask;
	int mUpsNum;

}MAC_INFO_T;



class MAC_ENTITY
{
private:

	MAC_ENTITY();
	~MAC_ENTITY();

	MAC_ENTITY(const MAC_ENTITY&);
	MAC_ENTITY& operator=(const MAC_ENTITY&);

	CMyLock  g_macLock;
	map<int, MAC_INFO_T> g_mac_map;

public:

	/***
	 * @brief: 接口函数
	 */
	static MAC_ENTITY* GetInstance()
	{
		static MAC_ENTITY m_entity;

		return &m_entity;
	}


	/***
	 * @brief: 添加
	 * @param mac_id:
	 * @param upsNum:
	 * @param state:
	 */
	bool mac_Add(int mac_id, int upsNum, char state = 0);

	/***
	 * @brief: 更新
	 * @param mac_id:
	 * @param state:
	 */
	void mac_Update(int mac_id, char state);

	/***
	 * @brief: 获取
	 */
	char mac_Get(int mac_id);

	char mac_Get(char* dst, int len = 4);


};

#endif