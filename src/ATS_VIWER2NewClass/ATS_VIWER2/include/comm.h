
//#include <windef.h>

#ifndef HEADER_COMM_SHIQINGLIANG_20141123
#define HEADER_COMM_SHIQINGLIANG_20141123
#define _BIND_TO_CURRENT_VCLIBS_VERSION 1
#include "batteryEX.h"
#include <Windows.h>
#ifndef uint
#define uint unsigned int
#endif

#ifndef uchar
#define uchar unsigned char
#endif


#define CS2CHARP(var) (LPSTR)(LPCTSTR)var

#define F_PARAM(var) &(var[0]),sizeof(var)

//IP地址
typedef struct __net_address{

	uchar ip1;
	uchar ip2;
	uchar ip3;
	uchar ip4;

	uint port;

}net_addr_t;


typedef struct __ATS_message_t
{
	uchar pbuf[1024];
	uint len;

}ATS_message_t;


typedef struct __ATS_identy_t
{
	uchar shelf_id;	//机架号
	uchar ups_id;	//储能单元ID

}ATS_ident_t;

//曲线图
typedef struct __ATS_CURVE_PLOT
{
	ATS_ident_t ait;
	float curr;
	float volt;

}ATS_CURVE_PLOT_T;


//电池位置信息
typedef struct __ATS_BAT_T{

	uchar mode_id;
	uchar unit_id;
	uchar batt_id;

}ATS_BAT_T;


typedef struct __ATS_send_msg_t
{
	uchar mac_id;
	uchar ups_id;
	LPARAM lParam;
	WPARAM wParam;

}ATS_send_msg_t;


typedef struct __ATS_CMD_INFO
{
	ATS_ident_t ait;
	ATS_message_t amt;

}ATS_cmd_info_t;

typedef enum __ups_type_t{ROUTE=0,UPS}ups_type_t;
typedef struct __ATS_vector_t
{
	ups_type_t mType;

	ATS_ident_t ait;
	net_addr_t nat;
	ATS_sys_info_t sys;

}ATS_vector_t;

#define C_NETADDR(a1,a2) ((a1).ip1==(a2).ip1 && (a1).ip2==(a2).ip2 && (a1).ip3==(a2).ip3 && (a1).ip4==(a2).ip4 && (a1).port==(a2).port) ? 1:0
#define C_IDENTY(a1,a2) ((a1).shelf_id==(a2).shelf_id && (a1).ups_id==(a2).ups_id) ? 1:0

//标准信息
#define STD_BUF_LEN 100

typedef struct __cmd_param
{
	uchar	cmd;
	uint	parameter;
	uchar	buf[STD_BUF_LEN];
	uint	len_buf;
}cmd_param_t;

typedef struct __std_data_t
{
	ATS_ident_t ait;

	cmd_param_t ori;
	cmd_param_t res;

	uint	wparam;
	uint	lparam;
	uchar	data[STD_BUF_LEN];

}std_data_t;




/// 主机 --> 硬件 cmd 
#define CMD_DOWN_SPCMD_CMD_NO_DATA		0x01	//对硬件发出无数据指令
#define CMD_DOWN_WRITE_BYTE				0x02	//对硬件写入BYTE(单字节)数据，用于模式0和模式1
#define CMD_DOWN_WRITE_WORD				0x03	//对硬件写入WORD(双字节)数据，用于模式0和模式1
#define CMD_DOWN_WRITE_DWORD			0x04	//对硬件写入 DWORD(4字节)数据，用于模式0和模式1
#define CMD_DOWN_READ_BYTE				0x05	//从硬件读取BYTE(单字节)数据，只可用于模式0
#define CMD_DOWN_READ_WORD				0x06	//从硬件读取WORD(双字节)数据，只可用于模式0
#define CMD_DOWN_READ_DWORD				0x07	//从硬件读取DWORD(4字节)数据，只可用于模式0
#define CMD_DOWN_CHANGE_TO_OUT_MODE_0	0x08	//命令硬件转入输出模式0
#define CMD_DOWN_CHANGE_TO_OUT_MODE_1	0x09	//命令硬件转入输出模式1
#define CMD_DOWN_HOST_ALIVE_NOTFICATION	0x0B	//主机定时发送,硬件由此可知软件在正常接收数据,只用于模式1

/// 主机 <--- 硬件 cmd 
#define CMD_UP_MODE_CMD_STATUS			0x01	//在模式0硬件接收到指令执行后，返回执行状态
#define CMD_UP_MODE_0_READ_BYTE			0x02	//在模式0硬件接收到READ_BYTE指令执行后，返回数据
#define CMD_UP_MODE_0_READ_WORD			0x03	//在模式0硬件接收到READ_WORD指令执行后，返回数据
#define CMD_UP_MODE_0_READ_DWORD		0x04	//在模式0硬件接收到READ_DWORD指令执行后，返回数据
#define CMD_UP_MODE_1_BYTE				0x05	//在模式1硬件向主机发送BYTE数据
#define CMD_UP_MODE_1_WORD				0x06	//在模式1硬件向主机发送WORD数据
#define CMD_UP_MODE_1_DWORD				0x07	//在模式1硬件向主机发送DWORD数据


/// 
#define OBJ_ID_SYS_VERSION					0x102	//系统版本号
#define OBJ_ID_COMMAND_STATUS				0x103	//指令执行状态
#define OBJ_ID_HOST_ALIVE_NOTIFICATION		0x104	//主机状态正常通报
#define OBJ_ID_OUTPUT_MODE_CHANGE			0x105	//改变输出模式
#define OBJ_ID_PRODUCT_NAME					0x106	//产品名称
#define OBJ_ID_CMD_COMPLETE					0x107	//命令执行完成通报
#define OBJ_ID_STOP_DISCHARGING_ALL			0x108	//同时停止放电
#define OBJ_ID_SW_VERSION					0x109	//系统软件版本号
#define OBJ_ID_FLASH_SAVE_CONFIG			0x10A	//配置参数写入FLASH
#define OBJ_ID_FLASH_READ_CONFIG			0x10B	//配置参数FLASH读出

#define OBJ_ID_DATA_UP_MODE_GFKD			0x10C	//国防科大改变输出模式

#define OBJ_ID_BATTERY_MODULE_ENABLE		0x201	//电池模块使能控制
#define OBJ_ID_CHARGING_CURRENT				0x202	//设置充电电流电压
#define OBJ_ID_BMS_MODE						0x203	//系统工作模式


#define OBJ_ID_SYSTEM_CONTROL			0x207	//系统控制

//电池屏蔽控制
#define OBJ_ID_BATTERY_MASK_0              0x210 //new define  1DWORD
#define OBJ_ID_BATTERY_MASK_1              0x211 //new define  1DWORD
#define OBJ_ID_BATTERY_MASK_2              0x212 //new define  1DWORD
#define OBJ_ID_BATTERY_MASK_3              0x213 //new define  1DWORD


#define OBJ_ID_MIN_DISCHARGE_UNIT		0x214	//最少放电单元数

//////////////////////////////////////////////////////////////////////////


//电池单元当前充/放电电流
#define OBJ_ID_CURRENT_0_EX				0x300
#define OBJ_ID_CURRENT_1_EX				0x301
#define OBJ_ID_CURRENT_2_EX				0x302
#define OBJ_ID_CURRENT_3_EX				0x303
#define OBJ_ID_CURRENT_4_EX				0x304
#define OBJ_ID_CURRENT_5_EX				0x305
#define OBJ_ID_CURRENT_6_EX				0x306
#define OBJ_ID_CURRENT_7_EX				0x307
#define OBJ_ID_CURRENT_8_EX				0x308
#define OBJ_ID_CURRENT_9_EX				0x309
#define OBJ_ID_CURRENT_10_EX			0x30A
#define OBJ_ID_CURRENT_11_EX			0x30B
#define OBJ_ID_CURRENT_12_EX				0x30C
#define OBJ_ID_CURRENT_13_EX				0x30D
#define OBJ_ID_CURRENT_14_EX				0x30E
#define OBJ_ID_CURRENT_15_EX				0x30F
#define OBJ_ID_CURRENT_16_EX				0x310
#define OBJ_ID_CURRENT_17_EX				0x311
#define OBJ_ID_CURRENT_18_EX				0x312
#define OBJ_ID_CURRENT_19_EX				0x313
#define OBJ_ID_CURRENT_20_EX				0x314
#define OBJ_ID_CURRENT_21_EX				0x315
#define OBJ_ID_CURRENT_22_EX				0x316
#define OBJ_ID_CURRENT_23_EX				0x317
#define OBJ_ID_CURRENT_24_EX				0x318
#define OBJ_ID_CURRENT_25_EX				0x319
#define OBJ_ID_CURRENT_26_EX				0x31A
#define OBJ_ID_CURRENT_27_EX				0x31B
#define OBJ_ID_CURRENT_28_EX				0x31C
#define OBJ_ID_CURRENT_29_EX				0x31D
#define OBJ_ID_CURRENT_30_EX				0x31E
#define OBJ_ID_CURRENT_31_EX				0x31F

//电池单元当前的电压
#define OBJ_ID_BATTERY_VOLTAGE_0_EX		0x320 //0x33F
#define OBJ_ID_BATTERY_VOLTAGE_1_EX		0x321
#define OBJ_ID_BATTERY_VOLTAGE_2_EX		0x322
#define OBJ_ID_BATTERY_VOLTAGE_3_EX		0x323
#define OBJ_ID_BATTERY_VOLTAGE_4_EX		0x324
#define OBJ_ID_BATTERY_VOLTAGE_5_EX		0x325
#define OBJ_ID_BATTERY_VOLTAGE_6_EX		0x326
#define OBJ_ID_BATTERY_VOLTAGE_7_EX		0x327
#define OBJ_ID_BATTERY_VOLTAGE_8_EX		0x328
#define OBJ_ID_BATTERY_VOLTAGE_9_EX		0x329
#define OBJ_ID_BATTERY_VOLTAGE_10_EX		0x32A
#define OBJ_ID_BATTERY_VOLTAGE_11_EX	0x32B
#define OBJ_ID_BATTERY_VOLTAGE_12_EX		0x32C
#define OBJ_ID_BATTERY_VOLTAGE_13_EX		0x32D
#define OBJ_ID_BATTERY_VOLTAGE_14_EX		0x32E
#define OBJ_ID_BATTERY_VOLTAGE_15_EX		0x32F
#define OBJ_ID_BATTERY_VOLTAGE_16_EX		0x330
#define OBJ_ID_BATTERY_VOLTAGE_17_EX		0x331
#define OBJ_ID_BATTERY_VOLTAGE_18_EX		0x332
#define OBJ_ID_BATTERY_VOLTAGE_19_EX		0x333
#define OBJ_ID_BATTERY_VOLTAGE_20_EX		0x334
#define OBJ_ID_BATTERY_VOLTAGE_21_EX		0x335
#define OBJ_ID_BATTERY_VOLTAGE_22_EX		0x336
#define OBJ_ID_BATTERY_VOLTAGE_23_EX		0x337
#define OBJ_ID_BATTERY_VOLTAGE_24_EX		0x338
#define OBJ_ID_BATTERY_VOLTAGE_25_EX		0x339
#define OBJ_ID_BATTERY_VOLTAGE_26_EX		0x33A
#define OBJ_ID_BATTERY_VOLTAGE_27_EX		0x33B
#define OBJ_ID_BATTERY_VOLTAGE_28_EX		0x33C
#define OBJ_ID_BATTERY_VOLTAGE_29_EX		0x33D
#define OBJ_ID_BATTERY_VOLTAGE_30_EX		0x33E
#define OBJ_ID_BATTERY_VOLTAGE_31_EX		0x33F

//电池单元当前的SOC
#define OBJ_ID_BATTERY_SOC_0_EX			0x340//0x34F
#define OBJ_ID_BATTERY_SOC_1_EX			0x341
#define OBJ_ID_BATTERY_SOC_2_EX			0x342
#define OBJ_ID_BATTERY_SOC_3_EX			0x343
#define OBJ_ID_BATTERY_SOC_4_EX			0x344
#define OBJ_ID_BATTERY_SOC_5_EX			0x345
#define OBJ_ID_BATTERY_SOC_6_EX			0x346
#define OBJ_ID_BATTERY_SOC_7_EX			0x347
#define OBJ_ID_BATTERY_SOC_8_EX			0x348
#define OBJ_ID_BATTERY_SOC_9_EX			0x349
#define OBJ_ID_BATTERY_SOC_10_EX			0x34A
#define OBJ_ID_BATTERY_SOC_11_EX			0x34B
#define OBJ_ID_BATTERY_SOC_12_EX			0x34C
#define OBJ_ID_BATTERY_SOC_13_EX			0x34D
#define OBJ_ID_BATTERY_SOC_14_EX			0x34E
#define OBJ_ID_BATTERY_SOC_15_EX			0x34F

//电池状态
#define OBJ_ID_BATTERY_STATUS_0_EX			0x350 
#define OBJ_ID_BATTERY_STATUS_1_EX			0x351
#define OBJ_ID_BATTERY_STATUS_2_EX			0x352
#define OBJ_ID_BATTERY_STATUS_3_EX			0x353

#define OBJ_ID_SWITCH_STATUS_0_EX			0x354		//电池开关状态
#define OBJ_ID_SWITCH_STATUS_1_EX			0x355		//电池开关状态
#define OBJ_ID_MODULE_TEMP_EX				0x356		//温度
#define OBJ_ID_SYSTEM_STATUS_EX				0x357		//系统状态
#define OBJ_ID_SYSTEM_ERROR_EX				0x358

//电池充电状态
#define OBJ_ID_BATTERY_STATUS_SET_0				0x359	
#define OBJ_ID_BATTERY_STATUS_SET_1				0x35A
//////////////////////////////////////////////////////////////////////////


//#define OBJ_ID_DATA_PACKET	0x4XX
#define   OBJ_ID_DATA_PACKET			0x438
#define   OBJ_ID_SOC_PACKET				0x440


//////////////////////////////////////////////////////////////////////////
//电池状态
#define BAT_STATUS_NORMAL			0	//正常
#define BAT_STATUS_FULL				1	//满电压
#define BAT_STATUS_EMPTY			2	//低电压
#define BAT_STATUS_ERROR			3	//电池故障


//////////////////////////////// GFKD主要是符合国防科大项目
#define		BMS_MODE_NET_GFKD     0
#define		BMS_MODE_UPS_GFKD     1

#define		MSG_UPDATE				WM_USER+0x01
#define		MSG_UPDATE_LV_UI		WM_USER+0x02	//更新
#define		MSG_BATTERY_MASK		WM_USER+0x03
#define		MSG_MAC_UPS_CHANGED		WM_USER+0x04
#define		MSG_BATTERY_ACT			WM_USER+0x05
#define		MSG_UPS_CURVE_PLOT		WM_USER+0x06	//显示曲线
#define		MSG_UPS_CURVE_DATA		WM_USER+0x07	//显示曲线

#define		MSG_UPS_ERROR			WM_USER+0x08	//UPS报警

#define		MSG_UI_RESET			WM_USER+0x09
#define		MSG_SHOW_PROGRESS		WM_USER+0x0A

#define		MSG_MIN_DIS_UNIT_SET	WM_USER+0x0B


//////////////////////////////////////////////////////////////////////////
//自定义消息
#define NET_CALLBACK_QUERY_CHARGING				0x01	//互联网模式下，查询充电充电电流
#define NET_CALLBACK_SET_CHARGING				0x02	//互联网模式下，设置充电充电电流


#define WPARAM_INIT								0x03	//初始化
#define WPARAM_BAT_MASK							0x04	//屏蔽
#define WPARAM_BAT_UNMASK						0x05	//取消屏蔽

#define UPS_CALLBACK_SET_CHARGING				0x06	//互联网模式下，设置充电充电电流

#define NET_CALLBACK_QUERY_CHARGING_2			0x07	//互联网模式下，查询充电充电电流
#define NET_CALLBACK_SET_CHARGING_2				0x08	//互联网模式下，设置充电充电电流

#define WPARAM_NET_SYSTEM_CONTROL				0x09	//互联网模式下，设置充电充电电流
#define WPARAM_NET_SYSTEM_CONTROL_2				0x0A	//互联网模式下，设置充电充电电流
#define WPARAM_BATTER_ENABLE					0x0B	//使能控制

#define WPARAM_BAT_MASK_EX						0x0C	//屏蔽

#define WPARAM_DEBUG_MODE						0x0D	//
#define WPARAM_HOST_ALIVE_NOTIFICATION			0x0E	//ping

#define WPAPAM_NET_STOP_CHAR_1					0x10	//停止充电，先设置电池状态
#define WPARAM_NET_STOP_CHAR_2					0x11	//停止充电，选中

#define WPARAM_MODE_SWITCH						0x12	//模式转换

#define WPARAM_MODE_SWITCH_STATE_CHANGE			0x13	//模式转换

#define WPARAM_DISCHARGING_STATE_CHANGE			0x14	//模式转换




#endif //HEADER_COMM_SHIQINGLIANG_20141123