#ifndef HEADER_LCT_MYSQL_SQL_20120903
#define HEADER_LCT_MYSQL_SQL_20120903

#if defined DLL_LCT_MYSQL_SQL
#define DECLDIR __declspec(dllexport)//导出
#else
#define DECLDIR __declspec(dllimport)//导入
#endif

#if defined(_WIN32) || defined (_WIN64)
#include <Windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>



#ifdef __cplusplus
extern "C"
{
#endif




struct __lm_variable{
	char* name;
	char* value;
	struct __lm_variable* next;
	char stuff[0];
};
typedef struct __lm_variable lm_variable;

struct __lm_category{
	char		 name[20];
	lm_variable* root;
	lm_variable* last;
	struct __lm_category* next;	
};
typedef struct __lm_category lm_category;

typedef struct{
	int category_level;
	lm_category* root;
	lm_category* curr;
	lm_category* last;
}lm_config;

/***
* 初始化数据库模块
***/
DECLDIR int lm_initialize(char* dbhost,char* user,char* password,char* database, unsigned int port);

/***
* 选择数据库
***/
DECLDIR int lm_select_database(char* database);

/***
* 日志输出
***/
DECLDIR void lm_set_verbose(void func(char*));

/***
* 销毁数据
***/
DECLDIR void lm_variable_destroy(lm_variable* var);

/***
* 将变量添加到目录中
***/
DECLDIR void lm_variable_append(lm_category *category, lm_variable *variable);

/***
* 获取值
*
***/
DECLDIR char* lm_variable_retrieve(lm_variable* variable,const char* name);

/***
/* 和获取新的变量结构
***/
DECLDIR lm_variable* lm_variable_new(char* name,char* value);

/***
* 获取新的目录结构
***/
DECLDIR lm_category* lm_category_new(char* name);

/***
* 获取新的config结构
***/
DECLDIR lm_config* lm_config_new();

/***
* 
***/
DECLDIR void lm_category_append(lm_config* config,lm_category* category);

/***
* 销毁目录
***/
DECLDIR void lm_category_destroy(lm_category* category);

/***
* 销毁
***/
DECLDIR void lm_config_destroy(lm_config* config);

/***
* 更新操作
* param sqlstr:		更新语句
* param len_sqlstr:	更新语句长度
* 返回值: 执行成功返回更新影响的行数,失败返回-1
* sql add
***/
DECLDIR int lm_update_exec(char* sqlstr,unsigned long len_sqlstr);

/***
* 插入新数据操作
* param sqlstr:
* param len_sqlstr:
* 返回值:
* sql add
***/
DECLDIR int lm_insert_exec(char* sqlstr,unsigned long len_sqlstr);

/***
* 查询一条数据操作
* param sqlstr:
* param len_sqlstr:
* 返回值:  lm_variable指针(如果有多条数据,将返回第一条数据记录)
* sql add
* 注意:  最后要调用lm_variable_destroy释放空间
***/
DECLDIR lm_variable * lm_select_exec(char* sqlstr, unsigned long len_sqlstr);

/***
* 查询多条数据操作
* param sqlstr:
* param len_sqlstr:
* 返回值:  lm_config指针
* sql add
* 注意:  最后要调用lm_config_destroy释放空间
***/
DECLDIR lm_config * lm_select_multi_exec(char* sqlstr, unsigned long len_sqlstr);


/***
* 调用存储过程
* param sqlstr:
* param len_sqlstr:
* 返回值:  lm_config指针
* sql add
* 注意:  最后要调用lm_config_destroy释放空间
***/
DECLDIR lm_config * lm_procedure_exec(char* sqlstr, unsigned long len_sqlstr);

/***
* 调用无返回值存储过程
* param sqlstr:
* param len_sqlstr:
* 返回值:  lm_config指针
* sql add
* 注意:  最后要调用lm_config_destroy释放空间
***/
DECLDIR int lm_proc_without_res_exec(char* sqlstr, unsigned long len_sqlstr);


/***
* 退出
***/
DECLDIR void lm_quit();


#ifdef __cplusplus
};
#endif

#endif  //HEADER_LCT_MYSQL_SQL_20120903