#ifndef HEADER_BATTERY_SHIQINGLIANG_20141021

#define HEADER_BATTERY_SHIQINGLIANG_20141021

#define MAX_MODUE		4
#define MAX_UNIT		4
#define MAX_BAT			4


#define SYS_CHARGE_STATE       1	//充电
#define SYS_DISCHARGE_STATE    2	//放电
#define SYS_IDLE_STATE         0	//空闲
#define SYS_SUPCAP_CHAR_STATE  3	//

#define B_CHARE_STATE			1	//充电
#define B_DISCHARGE_STATE       2	//放电
#define B_IDLE_STATE			0	//空闲
#define B_ERROR_STATE           3
#define B_WORKING_STATE			4

#ifndef uchar
#define uchar unsigned char 
#endif

#ifndef uint
#define uint unsigned int 
#endif


//充、放电值
struct __value_t
{
	uint charge;	//充电
	uint discharge;	//放电
};
typedef struct __value_t val_t;


//soc数值
struct __value_soc_t
{
	uint fcc;	//峰值
	uint rm;	//内存值
	uint soc;
};
typedef struct __value_soc_t val_soc_t;


//共有属性
struct __attribute_t
{
	uchar	dc_diode_bypass;
	uchar	sw;				//开关
	uchar   bypass_sw;		//旁路开关
	val_t	current;		//电流
	val_t	voltage;		//电压
	uchar	state;			//0:空闲,1:充电，2:放电,3:屏蔽
	char   isMask;			//是否屏蔽0未屏蔽，1屏蔽
};
typedef struct __attribute_t attr_t;


//电池属性
struct __battery_t
{
	attr_t		attr;	//基本属性
};
typedef struct __battery_t battery_t;


//储能单元属性
struct __bat_unit_t
{
	attr_t		attribute;	//当前储能单元属性
	battery_t	battery[MAX_BAT];	//四块电池
};
typedef struct __bat_unit_t bat_unit_t; 


//模块属性
struct __bat_module_t
{
	attr_t		attr;			//当前模块属性
	uint		temperature;	//温度
	char		join_discha;		//是否参与充、放电
	bat_unit_t	bat_unit[MAX_UNIT];	//四个储能单元
};
typedef struct __bat_module_t module_t;

//整个系统
struct __sys_info_t
{
	uchar		addr[4];		//ip地址	例：192.168.0.110， 0-3分别代表192,168,0,1110
	uchar		timestamp[4];	//时间	0-3分别代表天，时，分，秒
	uchar		state;			//系统状态
	uint		T_UP;
	uint		I_DisCha;
	uint		T_Scap;
	uint		V_dcbus;
	uint		T_Chg;
	uint		T_Piggy;
	uint		V_REF_CHA;
	uint		V_19VPWR;
	module_t	mod[MAX_MODUE];			//四个模块
};
typedef struct __sys_info_t sys_info_t;


struct __soc_info
{
	uchar addr[4];
	val_soc_t soc[MAX_MODUE][MAX_UNIT][MAX_BAT];
};
typedef struct __soc_info soc_info_t;


#endif //HEADER_BATTERY_SHIQINGLIANG_20141021