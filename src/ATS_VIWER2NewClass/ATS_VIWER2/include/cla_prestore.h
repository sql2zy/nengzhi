#ifndef HEADER_CLA_PRESTORE_SHIQINGLIANG__
#define HEADER_CLA_PRESTORE_SHIQINGLIANG__

#include <iostream>
#include <string>
using namespace std;

#define BATTERY_CNT 64

typedef struct __battery_data
{
	unsigned short current;	//电流
	unsigned short voltage;	//电压
	unsigned char  soc;		//soc值
	char		   sw;		//开关
}battery_data_t;

typedef struct __header_list
{


}header_list_t;

class CCPreStore
{
public:

	/***
	 * @brief: 构造、析构
	 */
	CCPreStore(unsigned char macid, unsigned char upsid,char sta = 0);
	~CCPreStore(void);

	/***
	 * @brief: 初始化
	 */
	void cps_Zero();


	string			datetimeStr;		//时间字符串
	unsigned char	mac_id;	//
	unsigned char	ups_id;
	char			state;

	//64节电池数据
	battery_data_t battery[BATTERY_CNT];

	/***
	 * @brief: 设置ID
	 */
	void cps_SetID(unsigned char macid, unsigned char upsid);

	void cps_SetState(char sta){
		state = sta;
	}

	/***
	 * @brief: 设置电池信息
	 */
	void cps_SetBatInfo(int id, unsigned short cur,unsigned short vol,unsigned char soc, char sw);

	/***
	 * @brief: 生成字符串
	 */
	string cps_toStr();

	/***
	 * #brief: 保存数据
	 */
	bool cps_Store();

private:
	
	/***
	 * @brief: 获取路径
	 */
	void cps_GetPath(string* str);

};

#endif