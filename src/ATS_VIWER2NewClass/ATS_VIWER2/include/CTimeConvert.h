#ifndef HEADER_CTIMECONVERT_SHIQINGLIANG__
#define HEADER_CTIMECONVERT_SHIQINGLIANG__
#include <Windows.h>
#include <time.h>


/***
 * @brief: 时间转换
 */
SYSTEMTIME TimetToSystemTime(time_t t);

/***
 * @brief: 时间转换
 */
time_t SystemTimeToTimet(SYSTEMTIME st);

/***
 * @brief: 获取当前时间
 */
time_t GetCurrentTimet();

/***
 * @brief: 获取时间差
 */
double GetDiffTime(time_t t1, time_t t2);

#endif