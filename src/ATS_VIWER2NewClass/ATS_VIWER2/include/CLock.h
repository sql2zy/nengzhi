#ifndef HADER_CLOCK_SHIQINGLIANG__
#define HADER_CLOCK_SHIQINGLIANG__


#include <Windows.h>
class CMyLock
{
public:
	CMyLock(void)
	{
		InitializeCriticalSection(&critical_lock);
	}


	~CMyLock(void)
	{
		DeleteCriticalSection(&critical_lock);
	}

	/***
	 * @brief: lock
	 */
	void do_Lock()
	{
		EnterCriticalSection(&critical_lock);
	}


	/***
	 * @brief: unlock
	 */
	void do_Unlock()
	{
		LeaveCriticalSection(&critical_lock);
	}


private:
	//
	CRITICAL_SECTION critical_lock;
};

#endif //HADER_CLOCK_SHIQINGLIANG__