#ifndef HEADER_BATTERYEX_SHIQINGLIANG_20141021
#define HEADER_BATTERYEX_SHIQINGLIANG_20141021
#include "battery.h"
#include <time.h>
#include <windef.h>



#define B_EX_STATE_NORMAL		0x00	//正常
#define B_EX_STATE_CUR_HI		0x01	//满电压
#define B_EX_STATE_CUR_LO		0x02	//低电压
#define B_EX_STATE_ERROR		0x03	//故障

/*
0-空闲 
1-放电 
2-充电  
3-报警 
4-超级电容充电 
5-UPS停机前等待
6-UPS停机状态

*/
#define S_EX_STATE_NORMAL		0x00	//空闲
#define S_EX_STATE_DISCHARGE	0x01	//放电
#define S_EX_STATE_CHARG		0x02	//充电
#define S_EX_STATE_ERROR		0x03	//报警
#define S_EX_STATE_CJDR_CHARGE	0x04	//超级电容充电
#define S_EX_STATE_UPS_WAIT_DOWN 0x05	//UPS停机前等待
#define S_EX_STATE_UPS_DOWN		0x06	//UPS停机状态

#define S_EX_ERROR_BREAK		//电池故障
#define S_EX_ERROR_LOW_CUR		//电池低压报警
#define S_EX_ERROR_SHORT		//短路报警
#define S_EX_ERROR_HI_TMP		//高温报警
#define S_EX_ERROR_OVERHEAT		//过温保护

typedef struct __batteryEX_t
{
	val_soc_t	vst;			//soc值
	char		batEX_state;	//电池状态

	time_t		charging_start;	//开始充电时间
	//
	uint last_current;		//
	char bat_state;

}batteryEX_t;


typedef struct __range_value_t
{
	uint mMax;	//最大值
	uint mMin;	//最小值
	uint mCurr;	//当前值

}range_value_t;

typedef struct __bat_unitEX_t
{

	DWORD		uinEX_system_control;	//
	batteryEX_t battery[MAX_BAT];

}bat_unitEX_t;

//模块属性
typedef struct __bat_moduleEX_t
{
	uchar battery_state[4];	//电池状态缓存
	bat_unitEX_t	bat_unit[MAX_UNIT];	//四个储能单元

	//sql add
	uint modCurrent;	//充放电电流
	uint modVoltage;	//充放电电压

	//测试用
	char mod_flag;
	char mod_flag_count;

}moduleEX_t;



typedef struct __sys_infoEX_t
{

	uchar sysEX_version[4];				//系统版本号
	uchar sysEX_product_name[4];		//产品名称
	uchar sysEX_soft_version[4];		//系统软件版本号
	uchar sysEX_mem_addr[4];			//串口访问地址
	uchar sysEX_state;					//系统状态

	uint  sysEX_charge_current;			//充电电流
	uint  sysEX_charge_voltage;			//充电电压
	uchar  sysEX_error;					//报警状态
	char  sysEX_bms_mode;				//工作模式
	char  sysEX_bms_output_mode;		//信息输出模式
	range_value_t sysEX_current_trk;	//放电电流均衡控制
	uchar sysEX_min_discha_unit;		//最小放电单元
	uchar sysEX_can_addr;				//CAN地址

	uchar sysEX_mode_control[4];		//系统控制

	moduleEX_t mod[MAX_MODUE];

}sys_infoEX_t;


typedef struct __ATS_sys_info
{
	//uchar sys_mode;
	sys_info_t sit;
	sys_infoEX_t sitEX;
	
}ATS_sys_info_t;



#endif //HEADER_BATTERYEX_SHIQINGLIANG_20141021