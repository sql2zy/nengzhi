    #ifndef __FullScreenHandler_h__  
    #define __FullScreenHandler_h__  
      
    class CFullScreenHandler  
    {   
    public:  
        CFullScreenHandler();  
        ~CFullScreenHandler();  
        void Maximize(CFrameWnd* pFrame, CWnd* pView);  
        void Restore(CFrameWnd* pFrame);  
        BOOL InFullScreenMode() {  return !m_rcRestore.IsRectEmpty();  }  
        CSize GetMaxSize();  
      
    protected:  
        CRect m_rcRestore;  
    };  
      
    #endif  //__FullScrennHandler_h__  
      
    // Global instance  
    extern CFullScreenHandler FullScreenHandler;  