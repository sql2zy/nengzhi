#pragma once
#include "afxwin.h"


// CChargingSet 对话框

class CChargingSet : public CDialog
{
	DECLARE_DYNAMIC(CChargingSet)

public:
	CChargingSet(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CChargingSet();

// 对话框数据
	enum { IDD = IDD_CHARGING_SET };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	char mMask;
	char mMode;

	DECLARE_MESSAGE_MAP()
public:
	unsigned int mCurrent;	//电流
	unsigned int mVoltage;	//电压

	unsigned int mMaxVol, mMinVol, mMaxCur, mMinCur;

	virtual BOOL OnInitDialog();

	//选择模块
	void cs_set_mask(char mask);
	int cs_get_mask();

	void cs_set_current(unsigned int current);
	void cs_set_voltage(unsigned int voltage);
	void cs_set_params(unsigned int current, unsigned int voltage);
	void cs_set_mode(char mode);

	//设置范围值
	void cs_set_range(unsigned int MaxVol, unsigned int MinVol,unsigned int MaxCur, unsigned int MinCur);

	afx_msg void OnBnClickedOk();
	CButton mCheck0;
	CButton mCheck1;
	CButton mCheck2;
	CButton mCheck3;
};
