#include <libs_version.h>
#include <cla_prestore.h>
#include <Windows.h>
#include <WinDef.h>
#include <time.h>
#include <io.h>
#include <fstream>


/***
* @brief: 写入头
*/
void cps_WriteHeader(fstream& file);

//构造
CCPreStore::CCPreStore(unsigned char macid, unsigned char upsid,char sta/* = 0*/)
{
	//sql 于长沙修改,时间仓促，需要优化
	string tmp = "";
	CreateDirectory("data",NULL);
	cps_GetPath(&tmp);
	CreateDirectory(tmp.c_str(),NULL);
	
	cps_Zero();
	cps_SetID(macid,upsid);
	state = sta;
}

//析构
CCPreStore::~CCPreStore(void)
{

}

//初始化
void CCPreStore::cps_Zero()
{
	SYSTEMTIME st;
	char tmp[31] = {0};
	//string			datetimeStr;		//时间字符串

	GetLocalTime(&st);
	sprintf_s(tmp,sizeof(tmp),"%04d-%02d-%02d %02d:%02d:%02d:%03d",
		st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond,st.wMilliseconds);
	datetimeStr.clear();
	datetimeStr.append(tmp);
	mac_id = 0;
	ups_id = 0;
	state = 0;
	for (int i=0; i<BATTERY_CNT;i++)
	{
		battery[i].current	= 0;
		battery[i].voltage	= 0;
		battery[i].soc		= 0;
		battery[i].sw		= 0;
	}
}

//设置ID
void CCPreStore::cps_SetID(unsigned char macid, unsigned char upsid)
{
	mac_id = macid;
	ups_id = upsid;
}

//设置电池信息
void CCPreStore::cps_SetBatInfo(int id, unsigned short cur, unsigned short vol,unsigned char soc, char sw)
{
	if (id>= 0 && id<BATTERY_CNT)
	{
		battery[id].current = cur;
		battery[id].voltage = vol;
		battery[id].soc = soc;
		battery[id].sw = sw;
	}
}

//生成字符串
string CCPreStore::cps_toStr()
{
	char tmp[21] = {0};
	string str = "";
	
	//时间
	str.append(datetimeStr);
	str.append(",");

	//mac
	str.append(itoa((int)mac_id,tmp,10));
	str.append(",");

	//ups
	str.append(itoa((int)ups_id,tmp,10));
	str.append(",");

	//state
	str.append(itoa((int)state,tmp,10));
	str.append(",");

	for (int i=0; i<BATTERY_CNT;i++)
	{
		str.append(itoa((int)battery[i].current,tmp,10));
		str.append(",");

		str.append(itoa((int)battery[i].voltage,tmp,10));
		str.append(",");

		str.append(itoa((int)battery[i].soc,tmp,10));
		str.append(",");

		str.append(itoa((int)battery[i].sw,tmp,10));
		str.append(",");
	}

	return str;
}

//保存
bool CCPreStore::cps_Store()
{
	char tmp[31] = {0};
	bool flag = false;	//文件是否存在
	bool ret = false;
	string str = "";
	fstream file;

	try
	{
		cps_GetPath(&str);
		sprintf_s(tmp,sizeof(tmp),"/%02d-%02d.csv",mac_id,ups_id);
		str.append(tmp);

		if(0 == _access(str.c_str(),0))
			flag = true;

		file.open(str.c_str(),ios::app);
		if(false == flag)
		{
			cps_WriteHeader(file);
		}

		str = "";
		str = cps_toStr();
		file<<str.c_str()<<endl;
		file.close();

	}
	catch(...)
	{
		return false;
	}

	return ret;
}

//写入头
void cps_WriteHeader(fstream& file)
{
	char tmp[51] = {0};
	file<<"datetime,mac_id,ups_id,state,";
	for (int i=0; i<BATTERY_CNT; i++)
	{
		memset(tmp,0,sizeof(tmp));
		sprintf_s(tmp,sizeof(tmp),"%02d-current,%02d-voltage,%02d-soc,%02d-sw,",
			i+1,i+1,i+1,i+1);
		file<<tmp;
	}
	file<<endl;
}

//获取路径
void CCPreStore::cps_GetPath(string* str)
{
	char tmp[MAX_PATH] = {0};
	SYSTEMTIME st;
	GetLocalTime(&st);

	sprintf_s(tmp,sizeof(tmp),"data/%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);

	str->append(tmp);
}