
// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "ATS_VIWER2.h"

#include "MainFrm.h"
#include "LeftView.h"
#include "ATS_VIWER2View.h"
#include "cla\ats_main.h"
#include "ProgressDlg.h"
#include "FullScreenHandler.h"
#include "CControlPanel.h"
#include "CMainLog.h"
#include "CMinDisUnitDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
extern ats_main theATS; 

extern CATS_VIWER2View* theVIEWER;
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWndEx)

const int  iMaxUserToolbars = 10;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWndEx)
	ON_WM_CREATE()
	ON_COMMAND(ID_VIEW_CUSTOMIZE, &CMainFrame::OnViewCustomize)
	ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
	ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_OFF_2007_AQUA, &CMainFrame::OnApplicationLook)
	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_OFF_2007_AQUA, &CMainFrame::OnUpdateApplicationLook)
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_MENU_MODE_UPS, &CMainFrame::OnMenuModeUps)
	ON_COMMAND(ID_MENU_MODE_NET, &CMainFrame::OnMenuModeNet)
	ON_UPDATE_COMMAND_UI(ID_MENU_MODE_UPS, &CMainFrame::OnUpdateMenuModeUps)
	ON_UPDATE_COMMAND_UI(ID_MENU_MODE_NET, &CMainFrame::OnUpdateMenuModeNet)
	ON_COMMAND(ID_MENU_START, &CMainFrame::OnMenuStart)
	ON_COMMAND(ID_MENU_DATA_AUTO, &CMainFrame::OnMenuDataAuto)
	ON_COMMAND(ID_MENU_DATA_ACT, &CMainFrame::OnMenuDataAct)
	ON_UPDATE_COMMAND_UI(ID_MENU_DATA_AUTO, &CMainFrame::OnUpdateMenuDataAuto)
	ON_UPDATE_COMMAND_UI(ID_MENU_DATA_ACT, &CMainFrame::OnUpdateMenuDataAct)
	ON_WM_GETMINMAXINFO()
	ON_COMMAND(ID_MENU_PLAY_GIF, &CMainFrame::OnMenuPlayGif)
	ON_COMMAND(ID_UPS_STOP, &CMainFrame::OnUpsStop)
	ON_COMMAND(ID_HELP, &CMainFrame::OnHelp)
	ON_WM_CLOSE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 状态行指示器
	ID_SEPARATOR,
	ID_SEPARATOR,
	/*
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
	*/
	
};

// CMainFrame 构造/析构

CMainFrame::CMainFrame()
{
	// TODO: 在此添加成员初始化代码
	theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_OFF_2007_BLUE);

	//sql add
	theATS.bat_viewStyle = VS_NOMAL;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	BOOL bNameValid;
	// 基于持久值设置视觉管理器和样式
	OnApplicationLook(theApp.m_nAppLook);

	if (!m_wndMenuBar.Create(this))
	{
		TRACE0("未能创建菜单栏\n");
		return -1;      // 未能创建
	}

	m_wndMenuBar.SetPaneStyle(m_wndMenuBar.GetPaneStyle() | CBRS_SIZE_DYNAMIC | CBRS_TOOLTIPS | CBRS_FLYBY);

	// 防止菜单栏在激活时获得焦点
	CMFCPopupMenu::SetForceMenuFocus(FALSE);
// 

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(theApp.m_bHiColorIcons ? IDR_MAINFRAME_256 : IDR_MAINFRAME))
	{
		TRACE0("未能创建工具栏\n");
		return -1;      // 未能创建
	}

	CString strToolBarName;
	bNameValid = strToolBarName.LoadString(IDS_TOOLBAR_STANDARD);
	ASSERT(bNameValid);
	m_wndToolBar.SetWindowText(strToolBarName);

	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);
	m_wndToolBar.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);

	// 允许用户定义的工具栏操作:
	InitUserToolbars(NULL, uiFirstUserToolBarId, uiLastUserToolBarId);

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("未能创建状态栏\n");
		return -1;      // 未能创建
	}
	
	char tmp[100] = {0};
	net_addr_t* nat = theATS.ats_get_addr();
	sprintf(tmp,"能量路由器[%d.%d.%d.%d:%d]",nat->ip1,nat->ip2,nat->ip3,nat->ip4,nat->port);
	m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));
	m_wndStatusBar.SetPaneText(1,tmp);
	m_wndStatusBar.SetPaneWidth(1,200);
	/*m_bmpStatusBarIcon.LoadBitmap(IDB_BMP_CON_BAD);
	m_wndStatusBar.SetPaneIcon(2,m_bmpStatusBarIcon);
	m_wndStatusBar.SetPaneWidth(2,17);
	*/
	//

	// TODO: 如果您不希望工具栏和菜单栏可停靠，请删除这五行
	//m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);
	//m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_BOTTOM);
	DockPane(&m_wndMenuBar);
	//DockPane(&m_wndToolBar);


	// 启用 Visual Studio 2005 样式停靠窗口行为
	CDockingManager::SetDockingMode(DT_SMART);
	// 启用 Visual Studio 2005 样式停靠窗口自动隐藏行为
	EnableAutoHidePanes(CBRS_ALIGN_ANY);

	// 启用工具栏和停靠窗口菜单替换 sql mod
	EnablePaneMenu(TRUE, ID_VIEW_CUSTOMIZE, strCustomize, ID_VIEW_TOOLBAR);

	// 启用快速(按住 Alt 拖动)工具栏自定义
	CMFCToolBar::EnableQuickCustomization();

	if (CMFCToolBar::GetUserImages() == NULL)
	{
		// 加载用户定义的工具栏图像
		if (m_UserImages.Load(_T(".\\UserImages.bmp")))
		{
			m_UserImages.SetImageSize(CSize(16, 16), FALSE);
			CMFCToolBar::SetUserImages(&m_UserImages);
		}
	}

	// 启用菜单个性化(最近使用的命令)
	// TODO: 定义您自己的基本命令，确保每个下拉菜单至少有一个基本命令。
	CList<UINT, UINT> lstBasicCommands;

	lstBasicCommands.AddTail(ID_FILE_NEW);
	lstBasicCommands.AddTail(ID_FILE_OPEN);
	lstBasicCommands.AddTail(ID_FILE_SAVE);
	lstBasicCommands.AddTail(ID_FILE_PRINT);
	lstBasicCommands.AddTail(ID_APP_EXIT);
	lstBasicCommands.AddTail(ID_EDIT_CUT);
	lstBasicCommands.AddTail(ID_EDIT_PASTE);
	lstBasicCommands.AddTail(ID_EDIT_UNDO);
	lstBasicCommands.AddTail(ID_APP_ABOUT);
	lstBasicCommands.AddTail(ID_VIEW_STATUS_BAR);
	lstBasicCommands.AddTail(ID_VIEW_TOOLBAR);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2003);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_VS_2005);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLUE);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_SILVER);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLACK);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_AQUA);

	CMFCToolBar::SetBasicCommands(lstBasicCommands);

	CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
	return 0;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
	CCreateContext* pContext)
{
	// 创建拆分窗口CCControlPanel
	if (!m_wndSplitter.CreateStatic(this, 1, 3))
		return FALSE;

	//禁止移动
	//m_wndSplitter.csw_set_draw(0);

	if(!m_wndSplitterMid.CreateStatic(&m_wndSplitter,2,1,WS_CHILD|WS_VISIBLE,m_wndSplitter.IdFromRowCol(0,1)))
		return FALSE;

		CRect rect;
		GetClientRect(rect);
		if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CLeftView), CSize(120, rect.Height()), pContext))
		{
			return FALSE;
		}

		if (!m_wndSplitterMid.CreateView(0, 0, RUNTIME_CLASS(CATS_VIWER2View), CSize(1000, rect.Height() - 145), pContext))
		{
			return FALSE;
		}

		if (!m_wndSplitterMid.CreateView(1, 0, RUNTIME_CLASS(CCMainLog), CSize(1000, 145), pContext))
		{
			return FALSE;
		}

		if (!m_wndSplitter.CreateView(0, 2, RUNTIME_CLASS(CCControlPanel), CSize(150, rect.Height()), pContext))
		{
			return FALSE;
		}
		m_wndSplitter.SetColumnInfo(0,110,100);
		m_wndSplitter.SetColumnInfo(1,rect.Width()-260,100);

	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{

	cs.style &= ~(LONG) FWS_ADDTOTITLE;
	cs.lpszName = _T("能智科技能源互联网监控平台");
	//cs.cx = 970;
	//cs.cy = 650;
	//cs.hMenu=NULL;
	if( !CFrameWndEx::PreCreateWindow(cs) )
		return FALSE;



	// TODO: 在此处通过修改

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style &= ~WS_MAXIMIZEBOX; //禁止窗口最大化
	//cs.style &= ~WS_MINIMIZEBOX; //禁止窗口最小化
	//cs.style &= ~WS_SYSMENU; //取消Title上的按钮
	//cs.style &= ~WS_THICKFRAME;//使窗口不能用鼠标改变大小
	cs.lpszClass = AfxRegisterWndClass(0);


	//
	return TRUE;
}

// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 消息处理程序

void CMainFrame::OnViewCustomize()
{
	CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog(this, TRUE /* 扫描菜单*/);
	pDlgCust->EnableUserDefinedToolbars();
	pDlgCust->Create();
}

LRESULT CMainFrame::OnToolbarCreateNew(WPARAM wp,LPARAM lp)
{
	LRESULT lres = CFrameWndEx::OnToolbarCreateNew(wp,lp);
	if (lres == 0)
	{
		return 0;
	}

	CMFCToolBar* pUserToolbar = (CMFCToolBar*)lres;
	ASSERT_VALID(pUserToolbar);

	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);

	return lres;
}

void CMainFrame::OnApplicationLook(UINT id)
{
	CWaitCursor wait;

	theApp.m_nAppLook = id;

	switch (theApp.m_nAppLook)
	{
	case ID_VIEW_APPLOOK_WIN_2000:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
		break;

	case ID_VIEW_APPLOOK_OFF_XP:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
		break;

	case ID_VIEW_APPLOOK_WIN_XP:
		CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
		break;

	case ID_VIEW_APPLOOK_OFF_2003:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_VS_2005:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	default:
		switch (theApp.m_nAppLook)
		{
		case ID_VIEW_APPLOOK_OFF_2007_BLUE:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_BLACK:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_SILVER:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_AQUA:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
			break;
		}

		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
		CDockingManager::SetDockingMode(DT_SMART);
	}

	RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

	theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}

BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
	// 基类将执行真正的工作

	if (!CFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
	{
		return FALSE;
	}


	// 为所有用户工具栏启用自定义按钮
	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	for (int i = 0; i < iMaxUserToolbars; i ++)
	{
		CMFCToolBar* pUserToolbar = GetUserToolBarByIndex(i);
		if (pUserToolbar != NULL)
		{
			pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
		}
	}

	return TRUE;
}


void CMainFrame::ActivateFrame(int nCmdShow)
{
	// TODO: 在此添加专用代码和/或调用基类
	//nCmdShow = SW_SHOWMAXIMIZED;
	CFrameWndEx::ActivateFrame(nCmdShow);
}

void CMainFrame::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	//CFrameWndEx::OnRButtonDown(nFlags, point);
}

void CMainFrame::OnMenuModeUps()
{
	theATS.ats_set_bms_mode(BMS_MODE_UPS_GFKD,NZ_ACT_WRITE);
}

void CMainFrame::OnMenuModeNet()
{

	theATS.ats_set_bms_mode(BMS_MODE_NET_GFKD,NZ_ACT_WRITE);
}

void CMainFrame::OnUpdateMenuModeUps(CCmdUI *pCmdUI)
{
	//pCmdUI->SetCheck(theATS.ats_check_sys_mode_ups());
}

void CMainFrame::OnUpdateMenuModeNet(CCmdUI *pCmdUI)
{
	//pCmdUI->SetCheck(theATS.ats_check_sys_mode_net());
}

/***
 * 开始
 */
void CMainFrame::OnMenuStart()
{
	theATS.ats_start();
}

void CMainFrame::OnMenuDataAuto()
{
	theATS.ats_set_output_mode(1,NZ_ACT_WRITE);
}

void CMainFrame::OnMenuDataAct()
{
	theATS.ats_set_output_mode(0,NZ_ACT_WRITE);
}

void CMainFrame::OnUpdateMenuDataAuto(CCmdUI *pCmdUI)
{//
	//pCmdUI->SetCheck(theATS.ats_check_data_output(1));
}

void CMainFrame::OnUpdateMenuDataAct(CCmdUI *pCmdUI)
{
	//pCmdUI->SetCheck(theATS.ats_check_data_output(0));		
}


void CMainFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	CSize sz = FullScreenHandler.GetMaxSize();  
	lpMMI->ptMaxSize = CPoint(sz);  
	lpMMI->ptMaxTrackSize = CPoint(sz); 
}


void CMainFrame::FullScreen(char state)
{
	if(state)
	{
		m_wndSplitter.SetColumnInfo(0,0,0);
		m_wndSplitter.RecalcLayout();
		FullScreenHandler.Maximize(this, GetActiveView()); 
	}
	else
	{
		m_wndSplitter.SetColumnInfo(0,100,0);
		m_wndSplitter.RecalcLayout();
		if (FullScreenHandler.InFullScreenMode())  
			FullScreenHandler.Restore(this);
	}
}
void CMainFrame::OnMenuPlayGif()
{
	theATS.bat_viewStyle = VS_NOMAL;
	theVIEWER->change_view_style();
}

void CMainFrame::OnUpsStop()
{
	theATS.ats_ups_contrl(1);
}

LRESULT CMainFrame::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	return CFrameWndEx::DefWindowProc(message, wParam, lParam);
}

void CMainFrame::OnHelp()
{
	char path[MAX_PATH] = {0};

	GetModuleFileName(NULL,path,MAX_PATH); 
	(_tcsrchr(path,'\\'))[1] = 0; 
	lstrcat(path,_T("Manual.CHM"));

	::ShellExecute(NULL,"open",path,NULL,NULL,SW_SHOW);
}

void CMainFrame::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	if(false == theATS.ats_quit_check()) return ;

	CFrameWndEx::OnClose();
}
