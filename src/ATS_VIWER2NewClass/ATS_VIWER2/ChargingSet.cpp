// ChargingSet.cpp : 实现文件
//

#include "stdafx.h"
#include "ATS_VIWER2.h"
#include "ChargingSet.h"


// CChargingSet 对话框

IMPLEMENT_DYNAMIC(CChargingSet, CDialog)

CChargingSet::CChargingSet(CWnd* pParent /*=NULL*/)
	: CDialog(CChargingSet::IDD, pParent)
{
	mMask = 0;
	mCurrent = 0;
	mVoltage = 0;
	mMode = -1;
}

CChargingSet::~CChargingSet()
{
}

void CChargingSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_MODE0, mCheck0);
	DDX_Control(pDX, IDC_CHECK_MODE1, mCheck1);
	DDX_Control(pDX, IDC_CHECK_MODE2, mCheck2);
	DDX_Control(pDX, IDC_CHECK_MODE3, mCheck3);
}


BEGIN_MESSAGE_MAP(CChargingSet, CDialog)
	ON_BN_CLICKED(IDOK, &CChargingSet::OnBnClickedOk)
END_MESSAGE_MAP()


// CChargingSet 消息处理程序

BOOL CChargingSet::OnInitDialog()
{
	CDialog::OnInitDialog();
	/*
	if(0 == mMode)
	{
		if(mMask &(1<<0))
			mCheck0.SetCheck(1);

		if(mMask &(1<<1))
			mCheck1.SetCheck(1);

		if(mMask &(1<<2))
			mCheck2.SetCheck(1);

		if(mMask &(1<<3))
			mCheck3.SetCheck(1);
	}
	else*/
	{
		mCheck0.ShowWindow(0);
		mCheck1.ShowWindow(0);
		mCheck2.ShowWindow(0);
		mCheck3.ShowWindow(0);
	}

	if (0 == mMode)
	{
		CString tmp("");
		tmp.Format("%d",mCurrent);
		GetDlgItem(IDC_E_CURRENT)->SetWindowText(tmp);

		tmp.Format("%d",mVoltage);
		GetDlgItem(IDC_E_VOLTAGE)->SetWindowText(tmp);

		tmp.Format("%d-%d (mA)",mMinCur,mMaxCur);
		GetDlgItem(IDC_STA_CUR_RANG)->SetWindowText(tmp);

		tmp.Format("%d-%d (mV)",mMinVol,mMaxVol);
		GetDlgItem(IDC_STA_VOL_RANG)->SetWindowText(tmp);

	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

//选择模块
void CChargingSet::cs_set_mask(char mask)
{
	if(mask>=0 && mask<=3)
	{
		mMask = mMask|(1<<mask);
	}
}

int CChargingSet::cs_get_mask()
{
	return mMask;
}

void CChargingSet::cs_set_current(unsigned int current)
{
	mCurrent = current;
}
void CChargingSet::cs_set_voltage(unsigned int voltage)
{
	mVoltage = voltage;
}
void CChargingSet::cs_set_params(unsigned int current, unsigned int voltage)
{
	mCurrent = current;
	mVoltage = voltage;
}
void CChargingSet::cs_set_mode(char mode)
{
	mMode = mode;
}

/***
 * 设置范围值
 */
void CChargingSet::cs_set_range(unsigned int MaxVol, unsigned int MinVol,unsigned int MaxCur, unsigned int MinCur)
{
	mMaxVol = MaxVol; mMinVol = MinVol;
	mMaxCur = MaxCur; mMinCur = MinCur;
}

void CChargingSet::OnBnClickedOk()
{
	CString tmp("");
	unsigned int current=0,voltage=0;
	if(mCheck0.GetCheck())
		mMask = mMask|(1);
	if(mCheck1.GetCheck())
		mMask = mMask|(1<<1);
	if(mCheck2.GetCheck())
		mMask = mMask|(1<<2);
	if(mCheck3.GetCheck())
		mMask = mMask|(1<<3);
	
	GetDlgItem(IDC_E_CURRENT)->GetWindowText(tmp);
	sscanf((LPTSTR)(LPCTSTR)tmp,"%d",&current);

	GetDlgItem(IDC_E_VOLTAGE)->GetWindowText(tmp);
	sscanf((LPTSTR)(LPCTSTR)tmp,"%d",&voltage);

	if(0 == mMode)
	{
		if (current< mMinCur || current > mMaxCur)
		{
			MessageBox("充电电流不符合要求");
			return ;
		}

		if (voltage< mMinVol || voltage > mMaxVol)
		{
			MessageBox("充电电压不符合要求");
			return ;
		}
	}

	mCurrent = current;
	mVoltage = voltage;

	OnOK();
}
