#pragma once


// CCBatteryEnable 对话框

class CCBatteryEnable : public CDialog
{
	DECLARE_DYNAMIC(CCBatteryEnable)

public:
	CCBatteryEnable(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCBatteryEnable();

// 对话框数据
	enum { IDD = IDD_BATTERY_ENABLE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

	char mEnable;
	char mResult;

	CString mTitle;
	CString mInfo;
	CString mYT;
	CString mNT;

public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	void cbe_set_enable(char mask);

	char cbe_get_eanble();

	void cbe_set_text(CString title, CString information, CString yT="参与", CString nT="不参与");

	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
protected:
	virtual void PreInitDialog();
};
