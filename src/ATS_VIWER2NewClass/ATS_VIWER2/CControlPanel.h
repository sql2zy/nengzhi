#pragma once
#include "gdipbutton.h"


typedef enum __char_ctrl_e
{
	CTRL_START_CHARGING=0,
	CTRL_STOP_CHARGING,
	CTRL_START_DISCHARGING,
	CTRL_STOP_DISCHARGING

}charging_ctrl_e;

// CCControlPanel 窗体视图

class CCControlPanel : public CFormView
{
	DECLARE_DYNCREATE(CCControlPanel)

protected:
	CCControlPanel();           // 动态创建所使用的受保护的构造函数
	virtual ~CCControlPanel();

public:
	enum { IDD = IDD_DLG_RIGHT };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CGdipButton m_btn_module_1;
	CGdipButton m_btn_module_2;
	CGdipButton m_btn_module_3;
	// 模块4
	CGdipButton m_btn_module_4;
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);


	/***
	 * 用户点击模块切换按钮
	 */
	afx_msg void OnModuleClick(UINT nID);

	/***
	 * 用户点击控制按钮
	 */
	afx_msg void OnCtrlClick(UINT nID);


	/***
	 * 设置使能
	 * @param id:
	 * @param enable:
	 */
	void ctl_set_enable(char id, char enable, bool update = true);


	/***
	 * 更新按钮状态
	 */
	void ctrl_update_btn();


	/***
	 * 充、放电控制
	 */
	void ctrl_dis_charging(charging_ctrl_e ctrl);

protected:

	/***
	 * 加载图片
	 */
	void Load_Image(CImage* ima,char* png);
	char img_flag;


	/***
	 * 绘制模块选择状态
	 */
	void ShowImage();

	
	/***
	 * 显示图片
	 */
	void ctrl_update_select();


	/*** 
	 * 更新使能图片
	 */
	void ctrl_update_enable();


	/***
	 * 更新按钮位置
	 */
	void ctrl_update_btn_pos();

	/***
	 * 移动按钮位置
	 */
	void ctrl_move_btn(CGdipButton& btn, CRect rect, BOOL show = TRUE);


public:
	afx_msg void OnPaint();

	// 使能
	CGdipButton m_btn_enable;
	CGdipButton m_btn_enable_query;
	CGdipButton m_btn_enable_set;
	CGdipButton m_btn_params;
	CGdipButton m_btn_params_query;
	CGdipButton m_btn_params_set;
	CGdipButton m_btn_switch;
	CGdipButton m_btn_switch_query;
	CGdipButton m_btn_switch_set;
	CGdipButton m_btn_charging;
	CGdipButton m_btn_charging_start;
	CGdipButton m_btn_charging_stop;
	CGdipButton m_btn_discharging;
	CGdipButton m_btn_discharging_start;
	CGdipButton m_btn_discharging_stop;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CGdipButton m_btn_log;
	CGdipButton m_btn_log_save;
	CGdipButton m_btn_log_clear;
	CGdipButton m_btn_min_dis_unit;
	CGdipButton m_btn_min_dis_query;
	CGdipButton m_btn_min_dis_set;
	CGdipButton m_btn_ups_ctrl;
	CGdipButton m_btn_ups_ctrl_start;
	CGdipButton m_btn_ups_ctrl_stop;
};


