#include "stdafx.h"
#include "MySplitterWnd.h"

CMySplitterWnd::CMySplitterWnd(void)
{
	m_own_draw = 1;
	/*
	m_cxSplitter=0;
	m_cySplitter=0;
	m_cxBorderShare=1;
	m_cyBorderShare=1;
	m_cxSplitterGap=1;
	m_cySplitterGap=1;
	m_cxBorder=2;
	m_cyBorder=2;
*/
	m_cxSplitterGap = m_cySplitterGap = 3;  
	m_cxBorder      = m_cyBorder      = 2;  
	m_cxBorderShare = m_cyBorderShare = 0;  
	m_cxSplitter    = m_cySplitter    = 4;
}

CMySplitterWnd::~CMySplitterWnd(void)
{
}
BEGIN_MESSAGE_MAP(CMySplitterWnd, CSplitterWnd)
	ON_WM_LBUTTONDOWN()
	ON_WM_SETCURSOR()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void CMySplitterWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(1 == m_own_draw)
		CSplitterWnd::OnLButtonDown(nFlags, point);
}

BOOL CMySplitterWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(1 == m_own_draw)
		return CSplitterWnd::OnSetCursor(pWnd, nHitTest, message);

	else 
		return FALSE;
}

void CMySplitterWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	if(1 == m_own_draw)
		CSplitterWnd::OnMouseMove(nFlags, point);
	else
		CWnd::OnNcMouseMove(nFlags,point);
	//CSplitterWnd::OnMouseMove(nFlags, point);
}
