#pragma once


// CCBatSW_Set 对话框

class CCBatSW_Set : public CDialog
{
	DECLARE_DYNAMIC(CCBatSW_Set)

public:
	CCBatSW_Set(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCBatSW_Set();

// 对话框数据
	enum { IDD = IDD_DLG_SW_SET };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBoxChanged(UINT uID);

protected:
	WORD SW_STATE;

public:
	//设置\获取开关
	void cbs_sw_state_set(WORD sw);
	WORD cbs_sw_state_get();
	virtual BOOL OnInitDialog();
};
