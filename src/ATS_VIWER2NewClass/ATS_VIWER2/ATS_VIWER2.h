
// ATS_VIWER2.h : ATS_VIWER2 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"       // 主符号


// CATS_VIWER2App:
// 有关此类的实现，请参阅 ATS_VIWER2.cpp
//

class CATS_VIWER2App : public CWinAppEx
{
public:
	CATS_VIWER2App();


// 重写
public:
	virtual BOOL InitInstance();

	GdiplusStartupInput   m_gdiplusStartupInput;    
	ULONG_PTR   m_gdiplusToken; 
// 实现
	UINT  m_nAppLook;
	BOOL  m_bHiColorIcons;

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();

	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};

extern CATS_VIWER2App theApp;
