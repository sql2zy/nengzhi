
// LeftView.h : CLeftView 类的接口
//


#pragma once
#include "include\batteryEX.h"

class CATS_VIWER2Doc;
/*
0：独立UPS工作模式
1：储能单元
2：UPS主机工作模式
3：UPS从机工作模式
4：能源互联网模拟与实验平台模式

*/
#define LV_MENU_QUERY_SYS_MODE				10001	//查询工作模式
#define LV_MENU_SET_SYS_MODE_NET			10002	//能源互联网模拟与实验平台模式
#define LV_MENU_SET_SYS_MODE_UPS_DULI		10003	//独立UPS工作模式
#define LV_MENU_SET_SYS_MODE_UPS			10004	//储能单元
#define LV_MENU_SET_SYS_MODE_UPS_ZHUJU		10005	//UPS主机工作模式
#define LV_MENU_SET_SYS_MODE_UPS_CHONGJI	10006	//UPS从机工作模式
#define LV_MENU_SET_SYS_MODE_SUBITEM		10007

#define LV_MENU_UPS_CONNECT					10008	//连接

#define LV_MENU_UPS_PING					10009	//ping
#define LV_MENU_UPS_QUERY_RUNINGTIME		10010	//查询时间

#define LV_MENU_QUERY_INFO_OUT				10011	//查询日志输出模式
#define LV_MENU_INFO_OUT_SUBITEM			10012	//
#define LV_MENU_INFO_OUT_MODE0				10013	//交互模式
#define LV_MENU_INFO_OUT_MODE1				10014	//自动输出模式

#define LV_MENU_STOP_DISCHARGE_ALL			10015	//停止同时放电

#define LV_MENU_QUERY_SYS_STATUS			10016	//查询系统状态
#define LV_MENU_QUERY_SYS_ERROR_STA			10017	//查询报警类别

#define LV_MENU_SAVE_PARAMS					10018	//保存系统参数

#define LV_MENU_VAL_CUR_SUBITEM				10019	//电流、电压查询设置
#define LV_MENU_VAL_CUR_QUERY				10020	//电流、电压查询
#define LV_MENU_VAL_CUR_SET					10021	//电流、电压设置

#define LV_MENU_SYS_CONTRAL_SUBITEM			10022	//
#define LV_MENU_SYS_CONTRAL_START			10023	//
#define LV_MENU_SYS_CONTRAL_STOP			10024	//

#define LV_MENU_SYS_CURVEPLOT_REALTIME		10025	//显示曲线
#define LV_MENU_SYS_CURVEPLOT_HISTORY		10026	//显示曲线

#define LV_MENU_MIN_DIS_UNITS_ITEMS			10027	//最小放电单元数
#define LV_MENU_MIN_DIS_UNITS_ITEM_QUERY	10028	//最小放电单元数
#define LV_MENU_MIN_DIS_UNITS_ITEM_SET		10029	//最小放电单元数


class CLeftView : public CTreeView
{
protected: // 仅从序列化创建
	CLeftView();
	DECLARE_DYNCREATE(CLeftView)

// 属性
public:
	CATS_VIWER2Doc* GetDocument();

// 操作
public:

// 重写
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // 构造后第一次调用

// 实现
public:
	virtual ~CLeftView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CImageList gIList1,gIList2;
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	//添加
	void lv_con_add(char mac_id, char ups_id, char con = -1);

	//更新
	void lv_con_update(char mac_id, char ups_id, char con);

	void lv_con_update_ui(char mac_id, char ups_id, char con);

	//查询
	char lv_con_check(char mac_id, char ups_id);

protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	
	//添加工作模式菜单
	void lv_menu_add_sys_mode(CMenu* pMenu, CMenu* subMenu, ATS_sys_info_t* asit, char sys_mode);

public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
public:
	
};

#ifndef _DEBUG  // LeftView.cpp 中的调试版本
inline CATS_VIWER2Doc* CLeftView::GetDocument()
   { return reinterpret_cast<CATS_VIWER2Doc*>(m_pDocument); }
#endif

