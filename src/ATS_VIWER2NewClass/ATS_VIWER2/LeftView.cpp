
// LeftView.cpp : CLeftView 类的实现
//

#include "stdafx.h"
#include "ATS_VIWER2.h"

#include "ATS_VIWER2Doc.h"
#include "LeftView.h"
#include <afxcmn.h>
#include "MainFrm.h"
#include "ATS_VIWER2View.h"
#include "cla\ats_main.h"
#include "include\comm.h"
#include "ChargingSet.h"
#include "cla\CCObject.h"
#include "cla\CNotice.h"
#include "CMinDisUnitDlg.h"
#include "Entity.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif

HTREEITEM mItem;
CTreeCtrl *pTreeCtrl;
extern ats_main theATS; 
extern CATS_VIWER2View* theVIEWER;

typedef struct __ATS_conn_stat{
	ATS_ident_t ait;
	char con;
}ATS_con_sta_t;


typedef struct __ATS_selected_t{
	ATS_ident_t ait;
	char sta;
}ATS_selected_t;
ATS_selected_t gSelectAIT;

CList<ATS_con_sta_t> gAtsConn;

// CLeftView

IMPLEMENT_DYNCREATE(CLeftView, CTreeView)

BEGIN_MESSAGE_MAP(CLeftView, CTreeView)
	ON_WM_LBUTTONUP()
	ON_WM_CONTEXTMENU()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CLeftView 构造/析构

CLeftView::CLeftView()
{
	// TODO: 在此处添加构造代码
	pTreeCtrl = &GetTreeCtrl();
}

CLeftView::~CLeftView()
{
}

BOOL CLeftView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改 CREATESTRUCT cs 来修改窗口类或样式

	return CTreeView::PreCreateWindow(cs);
}

void CLeftView::OnInitialUpdate()
{
	CTreeView::OnInitialUpdate();

	// TODO: 调用 GetTreeCtrl() 直接访问 TreeView 的树控件，
	//  从而可以用项填充 TreeView。
	CCNotice* n = CCNotice::GetInstance();
	n->add_hand(this->GetSafeHwnd());
	
	gIList1.Create(16,16,ILC_COLOR16|ILC_MASK,0,4);

	HICON icon[4];
	icon[3]=AfxGetApp()->LoadIcon(IDI_SERVER01);
	icon[2]=AfxGetApp()->LoadIcon(IDI_RED);
	icon[1]=AfxGetApp()->LoadIcon(IDI_GREEN);
	icon[0]=AfxGetApp()->LoadIcon(IDI_GRAY);

	gIList1.Add(icon[0]);
	gIList1.Add(icon[1]);
	gIList1.Add(icon[2]);
	gIList1.Add(icon[3]);

	pTreeCtrl->SetImageList(&gIList1,LVSIL_NORMAL);
	 
	pTreeCtrl->ModifyStyle(0,TVS_HASBUTTONS|TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS);
	
	//sql add
	int count = theATS.ats_get_mach_num();
	mItem = pTreeCtrl->GetRootItem();
	//pTreeCtrl->SetItemImage(mItem,2,2);
	HTREEITEM tmpTree;
	if(0 == count)
	{
		MessageBox("配置文件解析失败，请检查config,并重新启动");
	}
	else
	{
		CString tmp("");
		for (int i=0; i<count; i++)
		{
			tmpTree = NULL;
			tmp.Format("机架-%02d",i);
			tmpTree = pTreeCtrl->InsertItem(tmp,0,0,TVI_ROOT);
			//pTreeCtrl->SetItemImage(tmpTree,3,3);
			
		
			device_t* amt = NULL;
			if((amt = theATS.ats_get_mach_info(i)))
			{
				for(int j=0; j<amt->upsnum; j++)
				{
					CString sub("");
					sub.Format("单元-%02d",j);
					lv_con_add(i,j);
					pTreeCtrl->InsertItem(sub,0,0,tmpTree);
				}
			}
			pTreeCtrl->Expand(tmpTree,TVIS_EX_ALL);
		}
	}
	pTreeCtrl->SetBkColor(RGB(223,223,223));
}


// CLeftView 诊断

#ifdef _DEBUG
void CLeftView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CLeftView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}

CATS_VIWER2Doc* CLeftView::GetDocument() // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CATS_VIWER2Doc)));
	return (CATS_VIWER2Doc*)m_pDocument;
}
#endif //_DEBUG


// CLeftView 消息处理程序

void CLeftView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	int ups_id=0, mac_id=0;

	HTREEITEM tmp = (&GetTreeCtrl())->HitTest(point);
	if(tmp)
	{
		CString tt = (&GetTreeCtrl())->GetItemText(tmp);
		CString dd = 	(&GetTreeCtrl())->GetItemText((&GetTreeCtrl())->GetParentItem(tmp));
		sscanf((LPTSTR)(LPCTSTR)tt,"单元-%02d",&(ups_id));
		sscanf((LPTSTR)(LPCTSTR)dd,"机架-%02d",&(mac_id));
		CMainFrame* frm = (CMainFrame*)::AfxGetMainWnd();

		theVIEWER->SendMessage(MSG_MAC_UPS_CHANGED,(LPARAM)mac_id,(WPARAM)ups_id);
	}
	//CTreeView::OnLButtonUp(nFlags, point);
}

void CLeftView::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	// TODO: 在此处添加消息处理程序代码
	
	HTREEITEM tmp = (&GetTreeCtrl())->HitTest(point);
	if(tmp)
	{
		CString tt = (&GetTreeCtrl())->GetItemText(tmp);

		CMainFrame* frm = (CMainFrame*)::AfxGetMainWnd();

	
	}
}

void CLeftView::OnLButtonDown(UINT nFlags, CPoint point)
{
	int select = 0, unselected = 0, mac_id = 0;
	CString tt("");
	HTREEITEM parent = NULL;
	HTREEITEM tmp = (&GetTreeCtrl())->HitTest(point);

	if(tmp)
	{
		parent = (&GetTreeCtrl())->GetParentItem(tmp);
		if(NULL == parent)
		{
			MAC_ENTITY* mac = MAC_ENTITY::GetInstance();

			tt = (&GetTreeCtrl())->GetItemText(tmp);
			sscanf((LPTSTR)(LPCTSTR)tt,"机架-%02d",&(mac_id));
			(&GetTreeCtrl())->GetItemImage(tmp,select,unselected);

			if(0 == select)
			{
				mac->mac_Update(mac_id,1);
				(&GetTreeCtrl())->SetItemImage(tmp,1,1);
			}
			else if(1 == select)
			{
				mac->mac_Update(mac_id,0);
				(&GetTreeCtrl())->SetItemImage(tmp,0,0);
			}
			
		}
	}

	CTreeView::OnLButtonDown(nFlags, point);
}


/***
 * 添加
 */
void CLeftView::lv_con_add(char mac_id, char ups_id,char con)
{
	POSITION p;
	int i=0;
	ATS_con_sta_t acst;
	int c = gAtsConn.GetCount();
	for (i=0; i<c; i++)
	{
		p = gAtsConn.FindIndex(i);
		acst = gAtsConn.GetAt(p);
		if(acst.ait.shelf_id == mac_id && acst.ait.ups_id==ups_id)
			return;
	}
	
	memset(&acst,0,sizeof(acst));
	acst.con = con;
	acst.ait.shelf_id = mac_id;
	acst.ait.ups_id = ups_id;
	gAtsConn.AddTail(acst);
}

/***
 * 更新
 */
void CLeftView::lv_con_update(char mac_id, char ups_id,char con)
{
	
	POSITION p;
	int i=0;
	ATS_con_sta_t acst;
	int c = gAtsConn.GetCount();
	for (i=0; i<c; i++)
	{
		p = gAtsConn.FindIndex(i);
		acst = gAtsConn.GetAt(p);
		if(acst.ait.shelf_id == mac_id && acst.ait.ups_id==ups_id)
		{
			if(con != acst.con){
				acst.con = con;
				lv_con_update_ui( mac_id,  ups_id,  con);
			}
		}
	}
}

/***
 * 更新
 */
void CLeftView::lv_con_update_ui(char mac_id, char ups_id, char con)
{
	int dc = 0,ddd = 0;
	HTREEITEM ht = pTreeCtrl->GetRootItem();
	HTREEITEM subitem;
	CString mac("");
	mac.Format("机架-%02d",mac_id);
	CString ups("");
	ups.Format("单元-%02d",ups_id);
	//2-red 
	while (ht)
	{
		CString tmp = pTreeCtrl->GetItemText(ht);
		if(0 == tmp.Compare(mac))
		{
			subitem = pTreeCtrl->GetChildItem(ht);
			while(subitem)
			{
				CString dd = pTreeCtrl->GetItemText(subitem);
				if(dd.Compare(ups)==0)
				{
					pTreeCtrl->GetItemImage(subitem,dc,ddd);
					
					if(2 == con)	//要设置成报警状态
					{
						pTreeCtrl->SetItemImage(subitem,1,1);
					}
					else if(1 == con)
					{
						//要设置成连接状态
						if(1 == dc && 1 == ddd) return ;
						pTreeCtrl->SetItemImage(subitem,2,2);
					}
					else
						pTreeCtrl->SetItemImage(subitem,0,0);
					return;
				}
				subitem = pTreeCtrl->GetNextItem(subitem,1);
			}
			Sleep(1);
		}
		ht = pTreeCtrl->GetNextItem(ht,1);
		Sleep(1);
	}
}


/***
 * 查询
 */
char CLeftView::lv_con_check(char mac_id, char ups_id)
{
	POSITION p;
	int i=0;
	ATS_con_sta_t acst;
	int c = gAtsConn.GetCount();
	for (i=0; i<c; i++)
	{
		p = gAtsConn.FindIndex(i);
		acst = gAtsConn.GetAt(p);
		if(acst.ait.shelf_id == mac_id && acst.ait.ups_id==ups_id)
			return acst.con;
	}

	return 0;
}

/***
 * 消息处理循环
 */
LRESULT CLeftView::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	ATS_send_msg_t asmt;
	if(message == MSG_UPDATE_LV_UI)
	{
		memset(&asmt,0,sizeof(asmt));
		memcpy(&asmt,(ATS_send_msg_t*)wParam,sizeof(ATS_send_msg_t));
		
		uchar mac_id = asmt.mac_id;
		uchar ups_id = asmt.ups_id;

		char sta = (char)asmt.lParam;
		lv_con_update(mac_id,ups_id,sta);
	}
	else if (message == MSG_MAC_UPS_CHANGED)
	{
		MessageBox("ddddddd");
	}
	return CTreeView::DefWindowProc(message, wParam, lParam);
}

void CLeftView::OnRButtonDown(UINT nFlags, CPoint point)
{	CMenu mMenu;
	int mac_id=0, ups_id=0; 
	HTREEITEM tmp = (&GetTreeCtrl())->HitTest(point);

	CBatObject* obj = NULL;
	ATS_sys_info_t* asit = NULL;
	
	if(!(pTreeCtrl->GetParentItem(tmp))) {
		gSelectAIT.sta = 0;
		return ;
	}

	if(tmp)
	{
		CString tt = (&GetTreeCtrl())->GetItemText(tmp);
		CString dd = 	(&GetTreeCtrl())->GetItemText((&GetTreeCtrl())->GetParentItem(tmp));
		sscanf((LPTSTR)(LPCTSTR)tt,"单元-%02d",&(ups_id));
		sscanf((LPTSTR)(LPCTSTR)dd,"机架-%02d",&(mac_id));

		gSelectAIT.sta = 1; gSelectAIT.ait.shelf_id = mac_id;gSelectAIT.ait.ups_id = ups_id;
		obj = theATS.ats_get_obj(mac_id,ups_id);
		asit = &(obj->m_asit);
		mMenu.CreatePopupMenu();

		//未连接
		if(1 == lv_con_check(mac_id,ups_id))
		{
			mMenu.AppendMenu(MF_STRING,LV_MENU_UPS_CONNECT,"连接");
		}
		else
		{
			//关于工作模式
			CMenu tSysmode;
			CPoint pt;
			GetCursorPos(&pt);
			tSysmode.CreatePopupMenu();
			lv_menu_add_sys_mode(&mMenu,&tSysmode,asit,asit->sitEX.sysEX_bms_mode);
			mMenu.AppendMenu(MF_SEPARATOR);

			//输出模式
			CMenu menu_debug_out;
			menu_debug_out.CreatePopupMenu();
			if(0 == asit->sitEX.sysEX_bms_output_mode || -1 == asit->sitEX.sysEX_bms_output_mode){

				menu_debug_out.AppendMenu(MF_STRING|MF_CHECKED,LV_MENU_INFO_OUT_MODE0,"交互模式");
				menu_debug_out.AppendMenu(MF_STRING,LV_MENU_INFO_OUT_MODE1,"自动模式");
			}
			else if(1 == asit->sitEX.sysEX_bms_output_mode)
			{
				menu_debug_out.AppendMenu(MF_STRING,LV_MENU_INFO_OUT_MODE0,"交互模式");
				menu_debug_out.AppendMenu(MF_STRING|MF_CHECKED,LV_MENU_INFO_OUT_MODE1,"自动模式");
			}
			mMenu.AppendMenu(MF_POPUP,(UINT_PTR)menu_debug_out.operator HMENU(), "输出模式");
			mMenu.AppendMenu(MF_SEPARATOR);

			//查询系统状态
			mMenu.AppendMenu(MF_STRING,LV_MENU_QUERY_SYS_STATUS,"查询系统状态");
			if(S_EX_STATE_ERROR == asit->sitEX.sysEX_state)
				mMenu.AppendMenu(MF_STRING,LV_MENU_QUERY_SYS_ERROR_STA,"查询报警状态");
			else mMenu.AppendMenu(MF_STRING|MF_GRAYED,LV_MENU_QUERY_SYS_ERROR_STA,"查询报警状态");
			mMenu.AppendMenu(MF_SEPARATOR);


			if(S_EX_STATE_CHARG == asit->sitEX.sysEX_state || S_EX_STATE_CJDR_CHARGE == asit->sitEX.sysEX_state)
				mMenu.AppendMenu(MF_STRING,LV_MENU_SYS_CURVEPLOT_REALTIME,"充电曲线");
			else if(S_EX_STATE_DISCHARGE== asit->sitEX.sysEX_state)
				mMenu.AppendMenu(MF_STRING,LV_MENU_SYS_CURVEPLOT_REALTIME,"放电曲线");

			//mMenu.AppendMenu(MF_STRING,LV_MENU_SYS_CURVEPLOT_HISTORY,"历史数据曲线");
			

			mMenu.TrackPopupMenu(TPM_RIGHTBUTTON, pt.x, pt.y,this);
		}

		mMenu.DestroyMenu();
		CWnd::OnRButtonUp(nFlags, point);
	}
	CTreeView::OnRButtonDown(nFlags, point);
}


//添加工作模式菜单
void CLeftView::lv_menu_add_sys_mode(CMenu* pMenu, CMenu* subMenu, ATS_sys_info_t* asit, char sys_mode)
{
	if(!pMenu || !subMenu) return ;

	pMenu->AppendMenu(MF_STRING,LV_MENU_QUERY_SYS_MODE,"查询工作模式");
}
void CLeftView::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CTreeView::OnRButtonUp(nFlags, point);
}


BOOL CLeftView::OnCommand(WPARAM wParam, LPARAM lParam)
{

	CBatObject* obj = NULL;
	ATS_sys_info_t* asit = NULL;


	int menuID = LOWORD(wParam);
	if(menuID > 10000 && 1 == gSelectAIT.sta)
	{
		obj = theATS.ats_get_obj(gSelectAIT.ait.shelf_id,gSelectAIT.ait.ups_id);
		asit = &(obj->m_asit); 

		if(LV_MENU_QUERY_SYS_MODE == menuID)			//查询工作模式
		{
			obj->ATS_bms_mode(NULL,NZ_ACT_READ,0,NULL);
		}
		else if(LV_MENU_UPS_PING == menuID)	{		//pingATS_host_alive_notification
		
			obj->ATS_host_alive_notification(NULL,NULL);
		}
		else if (LV_MENU_INFO_OUT_MODE0 == menuID){	//交互模式

				obj->ATS_output_mode_change_GFKD(NULL,0,NULL);
		}
		else if (LV_MENU_INFO_OUT_MODE1 == menuID){	//自动模式

				obj->ATS_output_mode_change_GFKD(NULL,1);
		}											//停止同时放电

		else if(LV_MENU_QUERY_SYS_STATUS == menuID){// 查询系统状态
		
				obj->ATS_sys_status_Ex(NULL);
		}											//查询报警类别
		else if(LV_MENU_QUERY_SYS_ERROR_STA == menuID){

				obj->ATS_sys_error_Ex(NULL);
		}
													//保存系统参数
		else if(LV_MENU_SAVE_PARAMS == menuID){

				obj->ATS_flash_save_config(NULL);
		}

		else if(LV_MENU_VAL_CUR_QUERY == menuID){//电流、电压查询

				obj->ATS_charging_current(NULL,NZ_ACT_READ,0);
		}
		else if (LV_MENU_VAL_CUR_SET == menuID)////电流、电压查询
		{
			char tmp =0;
			CChargingSet dlg;
	
			uchar data[4]={0};
			if(asit)
				dlg.cs_set_params(asit->sitEX.sysEX_charge_current,asit->sitEX.sysEX_charge_voltage);

			uint res = dlg.DoModal();
			if (res = IDOK)
			{
				data[0] = ((dlg.mVoltage) & 0xff00)>>8;	data[1] = (dlg.mVoltage) & 0x00ff;
				data[2] = ((dlg.mCurrent) & 0xff00)>>8;	data[3] = (dlg.mCurrent) & 0x00ff;
				
				obj->ATS_charging_current(NULL,NZ_ACT_WRITE,data,NULL,UPS_CALLBACK_SET_CHARGING);
			}
		}
		else if(LV_MENU_SYS_CONTRAL_START == menuID){

				obj->ATS_system_control_GFKD(NULL,0);
		}
		else if(LV_MENU_SYS_CONTRAL_STOP == menuID){

				obj->ATS_system_control_GFKD(NULL,1);
		}

		else if(LV_MENU_SYS_CURVEPLOT_REALTIME == menuID){
			
			theVIEWER->SendMessage(MSG_UPS_CURVE_PLOT,(WPARAM)1,(LPARAM)&gSelectAIT);
		}
		else if(LV_MENU_SYS_CURVEPLOT_HISTORY == menuID){

			theVIEWER->SendMessage(MSG_UPS_CURVE_PLOT,(WPARAM)0,(LPARAM)&gSelectAIT);
		}

		//最小放电单元数查询
		else if (LV_MENU_MIN_DIS_UNITS_ITEM_QUERY == menuID)
		{
			obj->ATS_min_discharge_unit(NULL,NULL,NZ_ACT_READ,0);
		}

		//最小放电单元数设置
		else if (LV_MENU_MIN_DIS_UNITS_ITEM_SET == menuID)
		{
			//theVIEWER->PostMessage(MSG_MIN_DIS_UNIT_SET,(LPARAM)obj);

			CCMinDisUnitDlg dlg;
			dlg.DoModal();
		}
	}


	return CTreeView::OnCommand(wParam, lParam);
}

