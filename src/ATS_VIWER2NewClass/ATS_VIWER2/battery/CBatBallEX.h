#pragma once
#include "afxwin.h"
#include <afxcmn.h>
#include <atlimage.h>
#include "..\include\batteryEX.h"

#define PNG_X 50
#define PNG_Y 50
#define MSPACE 20

#define BAT_SPEED 1

#ifndef uint
#define uint unsigned int 
#endif

#ifndef uchar
#define uchar unsigned char 
#endif

typedef struct __state_t
{
	CImage ima_1;
	CImage ima_2;
	CImage ima_3;
	CImage ima_4;

}state_t;

typedef struct __data_range{

	int mMax;
	int mMin;

}data_range_t;

typedef struct __bat_info_t
{
	CImage* ima;

	int ima_index;

	uint speed;		//速度
	uint direction;	//移动方向

	int mspace;
	CRect rect;

	data_range_t m_x;
	data_range_t m_y;

	bool isOut;	//是否出界

}bat_info_t;


class CCBatteryGIF :
	public CWnd
{
	
public:
	CCBatteryGIF();
	~CCBatteryGIF();

private:

	CImage m_backpng;	//背景

	CDC * m_parentPDC;	//

	CDC m_bufferMenDC;

	CBitmap m_bkbmp;

	CRect m_windowRect;
	CRect m_clientRect;

	state_t m_image[4];

	char m_flag;	//是否

	ATS_sys_info_t* m_asit;	//电池信息


	/***
	 * @brief: 加载位图
	 * @param ima: 
	 * @param png:
	 * @return null
	 */
	void batEx_load(CImage* ima,char* png);


	/***
	 * @brief: 获取soc
	 * @param index: 电池序号
	 * @param memDC:
	 * @return CImage pointer
	 */
	CImage* batEx_get_image(int index, CDC* memDC);

	
	/***
	 * @brief: 初始化
	 * @param index:
	 * @param rect:
	 * @return null
	 */
	void batEx_initlize_ball(int index, CRect rect);


	/***
	 * @brief 获取soc
	 * @param index 序号
	 * @return int SOC值
	 */
	unsigned int batEx_get_soc(int index);


	/***
	 * @brief: 获取区域
	 * @param i:
	 * @param rect:
	 * @return int 
	 */
	int batEx_get_rect(int i, CRect& rect);

public:

	/***
	 * @brief: 创建
	 * @param dwStyle:
	 * @param rect:
	 * @param ParentWnd:
	 * @param nID:
	 * @return int 
	 */
	int Create(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID);


	/***
	 * @brief: 获取随机值
	 * @param p:
	 * @param x:
	 * @param y:
	 * @return char
	 */
	char batEx_rand_point(POINT* p, data_range_t x, data_range_t y);


	/***
	 * @brief: 获取随机值
	 * @param drt:
	 * @param mMin:
	 * @param mMax:
	 * @return int 
	 */
	int batEx_rand(data_range_t drt);
	int batEx_rand(int mMin, int mMax);


	/***
	 * @brief: 设置CDC
	 * @param pDC
	 * @return null
	 */
	void batEx_attch_cdc(CDC* pDC){m_parentPDC = pDC;};


	/***
	 * @brief: 初始化
	 * @param pDC
	 * @return null
	 */
	void batEx_initlize(CDC* pDC);


	/***
	 * @brief: 设置区域
	 * @param wr:
	 * @param cr:
	 * @return null
	 */
	void batEx_set_rect(CRect wr, CRect cr);

	
	/***
	 * @brief: 
	 * @param rect
	 * @param src
	 * @param memDC:
	 * @return CDC Pointer
	 */
	CDC* batEx_get_memDC(CRect rect, CDC* src = NULL);
	CDC* batEx_get_memDC(CDC* memDC, CRect rect, CDC* src = NULL);


	/***
	 * @brief: 贴背景
	 * @param memDC:
	 * @return null
	 */
	void batEx_StickBk(CDC* memDC = NULL);


	/***
	 * @brief: 贴单元
	 * @param memDC:
	 * @return null
	 */
	void batEx_StickBattery(CDC* memDC = NULL);


	/***
	 * @brief: 获取单元的下一步
	 * @param i:
	 * @return char
	 */
	char batEx_GetDestination(int i);


	/***
	 * @brief:  设置区域
	 * @param rect:
	 * @param l
	 * @param t
	 * @param r
	 * @param b
	 * @param i
	 * @return null
	 */
	void batEx_setRect(CRect* rect, int l, int t, int r, int b);
	void batEx_setRect(int i, int l, int t, int r, int b);

	void batEx_set_asit(ATS_sys_info_t* asit){m_asit = asit;};
};
