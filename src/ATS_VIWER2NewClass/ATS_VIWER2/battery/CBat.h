#pragma once
#include "afxwin.h"
#include "PictureEx.h"
#include <afxcmn.h>

#ifndef uint
#define uint unsigned int 
#endif

#ifndef uchar
#define uchar unsigned char 
#endif

#define ID_PIC_STATE	1001
#define ID_PIC_ID		1002
#define ID_PIC_BP		1003

#define BAT_HIGHT		80
#define BAT_WITH		90

#define BAT_MENU_MASK				10001
#define BAT_MENU_UNMASK				10002

#define BAT_MENU_QUERY_STA			10003	//查询状态
#define BAT_MENU_QUERY_SOC			10004	//查询SOC
#define BAT_MENU_QUERY_VAL			10005	//查询电压
#define BAT_MENU_QEURY_CURRENT		10006	//查询电流

#define BAT_MENU_QUERY_TEMPRATURE	10007	//查询温度
#define BAT_MENU_QUERY_SWITCH		10008	//查询开关

#define BAT_MENU_SHOW_CURVE			10009	//显示曲线图

#define BAT_MENU_MASK_QUERY			10010	//屏蔽查询

class CCBat :
	public CWnd
{
	//DECLARE_DYNAMIC(CCBat)
public:
	CCBat();
	~CCBat();


	//电池状态
	typedef enum __BAT_STATE{

		BS_UNKOWN=0,
		BS_NORMAL,		//正常
		BS_VOL_HI,		//高电压
		BS_VOL_LO,		//低电压
		BS_BAD,			//电池故障

		BS_CHARING,
		BS_DISCHARGING

	}BAT_STATE_T;

	//电池状态
	typedef enum __BAT_SYS_STATE{

		S_IDLE=0,			//空闲
		S_DISCHARGING,		//放电
		S_CHARGING,			//充电
		S_ERROR,			//报警
		S_SUPERCARGING,		//超级电容充电
		S_UPS_WAIT_FOR_STOP,//UPS停机前等待
		S_UPS_STOP				//UPS停机状态

	}BAT_SYS_STATE_T;

	typedef enum __BAT_MODE{
		BAT_MODE_UPS=0,
		BAT_MODE_NET
	}BAT_MODE_E;

	int Create(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID);

protected:

	CWnd* mParent;		//父窗口
	CPictureEx mPic;	//图标

	CImage m_uid;		//编号
	CImage m_sw_on;		//开
	CImage m_sw_off;	//关

	CRect gPicRect;		//图像位置
	CRect gRect;		//整个图像位置

	uchar mID;			//电池序号
	uint mVoltage;		//电压
	uint mSoc;			//电荷
	uint mCurrent;		//电流
	float mTmperature;	//温度
	char mSW;			//开关

	char			m_participation;	//是否参与
	char			mMask;			//屏蔽

	BOOL			_bMouseTrack;
	CToolTipCtrl	m_ctrlTT;



	BAT_STATE_T		mState;	//电池状态
	BAT_SYS_STATE_T mPSTA;

	char gCreateFlag ;
	BAT_MODE_E mMode;

	/***
	 * @brief: 更新状态
	 * @param bst:状态
	 * @return null
	 */
	void bat_update_pic(BAT_STATE_T bst);


	/***
	 * @brief: 更新文字
	 * @param memcdc: 绘图句柄
	 * @param rect: 文字区域
	 * @return null
	 */
	void bat_update_text(CDC& memcdc,CRect rect);


	/***
	 * @brief: 加载序号
	 * @param null
	 * @return null
	 */
	void bat_load_id();

	/***
	 * @brief: 加载开关
	 * @param null
	 * @return null
	 */
	void bat_load_sw();


	/***
	 * @brief: 更新开关
	 * @param bp
	 * @return null
	 */
	void bat_update_sw(uchar bp) {
	
		if(bp != mSW)
		{
			mSW = bp; 
			bat_update_pic(mState);
		}
	};
	

	/***
	 * @brief: 获取开关
	 * @param null
	 * @return CIamge pointer
	 */
	CImage* bat_get_swtich();


	/***
	 * @brief: 更新TIP内容
	 * @param null
	 * @return null
	 */
	void bat_update_tips();


	/***
	 * @brief: 获取三个图片的位置
	 * @param recID:
	 * @param recBat:
	 * @param recSW:
	 */
	char bat_get_pic_rect(CRect& recID,CRect& recBat, CRect& recSW, CRect& recText);

	
public:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

public:


	/***
	 * @brief: 设置变量
	 * @param vol:		电压
	 * @param soc:		SOC
	 * @param current:	电流
	 * @param bst:		状态
	 */
	void bat_set_params(uint vol, uint soc, uint current, BAT_STATE_T bst);


	/***
	 * @brief: 设置电池序号
	 * @param id: 序号
	 * @return null
	 */
	void bat_set_identy(uchar id) {mID = id;};


	/***
	 * @brief: 设置电压
	 * @param vol: 电压
	 * @return null
	 */
	void bat_set_voltage(uint vol);


	/***
	 * @brief: 设置soc
	 * @param soc：
	 * @return null
	 */
	void bat_set_soc(uint soc);


	/***
	 * @brief: 设置电流
	 * @param current: 电流
	 * @return null
	 */
	void bat_set_current(uint current);


	/***
	 * @brief: 设置状态
	 * @param bst: 
	 * @return null
	 */
	void bat_set_state(BAT_STATE_T bst);
	CString bat_get_state();


	/***
	 * @brief: 设置系统状态
	 * @param bsst: 
	 * @return null
	 */
	void bat_set_sys_state(BAT_SYS_STATE_T bsst);


	/***
	 * @brief: 设置温度
	 * @param tmp: 
	 * @return null
	 */
	void bat_set_temprature(float tmp){
		mTmperature = tmp;
	};


	/***
	 * @brief: 设置开关
	 * @param bp: 开关状态
	 * @return null
	 */
	void bat_set_sw(uchar bp);


	/***
	 * @brief: 设置屏蔽
	 * @param mask: 屏蔽状态
	 * @return null
	 */
	void bat_set_mask(uchar mask){ mMask = mask; };


	/***
	 * @brief: 设置模式
	 * @param bme:工作模式
	 * @return null
	 */
	void bat_set_mode(BAT_MODE_E bme) {mMode = bme;};


	/***
	 * @brief: 更新界面
	 * @param null
	 * @return null
	 */
	void bat_update_ui() { 

		Invalidate();
	};


	/***
	 * @brief: 初始化信息
	 */
	void bat_init();

	char bat_CheckState(){
		return gCreateFlag;
	}


	/***
	 * @brief: 是否参与
	 * @param p: 
	 * @return null
	 */
	void bat_participation(char p);

	afx_msg void OnPaint();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	void batEx_load(CImage* ima,char* png);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
