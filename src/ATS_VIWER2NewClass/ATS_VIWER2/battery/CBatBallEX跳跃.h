#pragma once
#include "afxwin.h"
#include <afxcmn.h>
#include <atlimage.h>
#include "..\include\batteryEX.h"

#define PNG_X 50
#define PNG_Y 50
#define MSPACE 50

#define BAT_SPEED 2

#ifndef uint
#define uint unsigned int 
#endif

#ifndef uchar
#define uchar unsigned char 
#endif

typedef struct __state_t
{
	CImage ima_1;
	CImage ima_2;
	CImage ima_3;
	CImage ima_4;

}state_t;

typedef struct __data_range{

	int mMax;
	int mMin;

}data_range_t;

typedef struct __bat_info_t
{
	CImage* ima;

	int ima_index;

	uint speed;		//速度
	uint direction;	//移动方向

	int mspace;
	CRect rect;

	data_range_t m_x;
	data_range_t m_y;

	bool isOut;	//是否出界

}bat_info_t;





class CCBatteryGIF :
	public CWnd
{
	
public:
	CCBatteryGIF();
	~CCBatteryGIF();

private:

	CImage m_backpng;	//背景

	CDC * m_parentPDC;	//

	CDC m_bufferMenDC;

	CBitmap m_bkbmp;

	CRect m_windowRect;
	CRect m_clientRect;

	state_t m_image[4];

	char m_flag;	//是否

	//设置
	void batEx_set_color(int i);

	//加载位图
	void batEx_load(CImage* ima,char* png);


	ATS_sys_info_t* m_asit;


	//获取soc
	CImage* batEx_get_image(int index, CDC* memDC);

	void batEx_initlize_ball(int index, CRect rect);

	unsigned int batEx_get_soc(int index);

	int batEx_get_rect(int i, CRect& rect);

public:
	int Create(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID);

	//获取随机值
	char batEx_rand_point(POINT* p, data_range_t x, data_range_t y);

	//获取随机值
	int batEx_rand(data_range_t drt);
	int batEx_rand(int mMin, int mMax);

	//设置CDC
	void batEx_attch_cdc(CDC* pDC){m_parentPDC = pDC;};

	//初始化
	void batEx_initlize(CDC* pDC);

	//设置区域
	void batEx_set_rect(CRect wr, CRect cr);

	CDC* batEx_get_memDC(CRect rect, CDC* src = NULL);
	CDC* batEx_get_memDC(CDC* memDC, CRect rect, CDC* src = NULL);

	//贴背景
	void batEx_StickBk(CDC* memDC = NULL);

	//贴单元
	void batEx_StickBattery(CDC* memDC = NULL);

	//获取单元的下一步
	char batEx_GetDestination(int i);

	//设置区域
	void batEx_setRect(CRect* rect, int l, int t, int r, int b);

	void batEx_setRect(int i, int l, int t, int r, int b);

	void batEx_set_asit(ATS_sys_info_t* asit){m_asit = asit;};
};
