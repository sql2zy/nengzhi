#include "../StdAfx.h"
#include "CBatBallEX.h"
#include "../include/comm.h"
#include <math.h>

#define BATEX_MAX  64

//sql test
int g_soc[64];

bat_info_t gBATEX[BATEX_MAX];

CCBatteryGIF::CCBatteryGIF()
{
	m_asit = NULL;
	for (int i=0; i<64; i++)
		g_soc[i] = batEx_rand(0,100);

}

CCBatteryGIF::~CCBatteryGIF()
{
}


int CCBatteryGIF::Create(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID)
{
	return 1;
}


/***
 * 获取随机数
 */
int CCBatteryGIF::batEx_rand(data_range_t drt)
{
	return batEx_rand(drt.mMin, drt.mMax);
}

int CCBatteryGIF::batEx_rand(int mMin, int mMax)
{
	//srand((uint)time(NULL));
	return rand()%(mMax - mMin+1) + mMin;
}


/***
 * 获取随机数
 */
char CCBatteryGIF::batEx_rand_point(POINT* p, data_range_t x, data_range_t y)
{
	if(!p)return 0;

	p->x = batEx_rand(x);
	p->y = batEx_rand(y);

	return 1;
}


void CCBatteryGIF::batEx_load(CImage* ima,char* png)
{
	if(!ima || !png)return ;
	if (ima->IsNull())
	{
		ima->Load(_T(png));
		if (ima->GetBPP() == 32) //确认该图像包含Alpha通道
		{
			int i;
			int j;
			for (i=0; i<ima->GetWidth(); i++)
			{
				for (j=0; j<ima->GetHeight(); j++)
				{
					byte *pByte = (byte *)ima->GetPixelAddress(i, j);
					pByte[0] = pByte[0] * pByte[3] / 255;
					pByte[1] = pByte[1] * pByte[3] / 255;
					pByte[2] = pByte[2] * pByte[3] / 255;
				}
			}
		}
	}
}


/***
 * 初始化
 */
void CCBatteryGIF::batEx_initlize(CDC* pDC)
{
	m_parentPDC =  pDC;
	data_range_t drt_x, drt_y;

	drt_x.mMin = m_windowRect.left;
	drt_x.mMax = m_windowRect.right - PNG_X;

	drt_y.mMin = m_windowRect.top;
	drt_y.mMax = m_windowRect.bottom - PNG_Y;


	batEx_load(&(m_image[0].ima_1),"res\\ball\\red_1.png");
	batEx_load(&(m_image[0].ima_2),"res\\ball\\red_2.png");
	batEx_load(&(m_image[0].ima_3),"res\\ball\\red_3.png");
	batEx_load(&(m_image[0].ima_4),"res\\ball\\red_4.png");

	batEx_load(&(m_image[1].ima_1),"res\\ball\\blue_1.png");
	batEx_load(&(m_image[1].ima_2),"res\\ball\\blue_2.png");
	batEx_load(&(m_image[1].ima_3),"res\\ball\\blue_3.png");
	batEx_load(&(m_image[1].ima_4),"res\\ball\\blue_4.png");

	batEx_load(&(m_image[2].ima_1),"res\\ball\\green_1.png");
	batEx_load(&(m_image[2].ima_2),"res\\ball\\green_2.png");
	batEx_load(&(m_image[2].ima_3),"res\\ball\\green_3.png");
	batEx_load(&(m_image[2].ima_4),"res\\ball\\green_4.png");

	batEx_load(&(m_backpng),"res\\ball\\back.png");


	for (int i=0; i< BATEX_MAX; i++)
	{
		gBATEX[i].rect.left = batEx_rand(drt_x);
		gBATEX[i].rect.top = batEx_rand(drt_y);

		gBATEX[i].rect.right = gBATEX[i].rect.left+ PNG_X;
		gBATEX[i].rect.bottom = gBATEX[i].rect.top+ PNG_Y;

		gBATEX[i].direction = batEx_rand(0,360);

		if(i>=0 && i<= 21)
			gBATEX[i].ima_index = 0;
		else if (i>21 && i<= 42)
		{
			gBATEX[i].ima_index = 1;
		}
		else 
			gBATEX[i].ima_index = 2;

		gBATEX[i].speed = 3;
	
	}

}


void CCBatteryGIF::batEx_set_color(int i)
{
	
}

/***
 * 初始化
 */
void CCBatteryGIF::batEx_set_rect(CRect wr, CRect cr)
{
	m_windowRect = wr;
	m_clientRect = cr;
}


/***
 *
 */
CDC* CCBatteryGIF::batEx_get_memDC(CRect rect, CDC * src)
{
	if(src)
		m_parentPDC = src;

	m_bufferMenDC.CreateCompatibleDC(NULL);
	m_bkbmp.CreateCompatibleBitmap(m_parentPDC,rect.Width(),rect.Height());
	m_bufferMenDC.SelectObject(&m_bkbmp);
	m_bufferMenDC.FillSolidRect(rect.left,rect.top,rect.Width(),rect.Height(),RGB(255,255,255));
	
	batEx_StickBk();	//背景
	batEx_StickBattery();	//单元
	return &m_bufferMenDC;
}

CDC* CCBatteryGIF::batEx_get_memDC(CDC* memDC, CRect rect, CDC* src)
{
	if(!memDC) return NULL;

	m_clientRect = rect;

	batEx_StickBk(memDC);	//背景
	batEx_StickBattery(memDC);	//单元

	return memDC;
}


/***
 * 贴背景
 */
void CCBatteryGIF::batEx_StickBk(CDC* memDC)
{
	CDC* tmpDC = NULL;

	if(memDC)
		tmpDC = memDC;
	else
		tmpDC = &m_bufferMenDC;

	m_backpng.TransparentBlt(tmpDC->m_hDC,m_windowRect,RGB(0,0,0));
}

/***
 *贴单元
 * 
 */
void CCBatteryGIF::batEx_StickBattery(CDC* memDC)
{


	int i= 0;
	CDC* tmpDC = NULL;

	if(memDC)
		tmpDC = memDC;
	else
		tmpDC = &m_bufferMenDC;

	//先计算位置
	for (i=0; i<BATEX_MAX; i++)
	{
		batEx_GetDestination(i);
	}

	COLORREF old = memDC->SetTextColor(RGB(238,244,78));
	
	for (i=0; i<BATEX_MAX; i++)
	{
		batEx_get_image(i,tmpDC);

	}
}

CImage* CCBatteryGIF::batEx_get_image(int index, CDC* memDC)
{
	CImage* ima = NULL;
	if(index <0 || index >64)return NULL;

	if(!m_asit)return NULL;

	int mid = index/16;
	int tmp = index%16;

	int uid = tmp/4;
	int bid = tmp%4;

	unsigned int soc = m_asit->sitEX.mod[mid].bat_unit[uid].battery[bid].vst.soc;

	//sql 测试
	soc = g_soc[index];

	if(soc <= 25)
		ima = &(m_image[gBATEX[index].ima_index].ima_1);

	else if (soc> 25 && soc <= 50)
		ima = &(m_image[gBATEX[index].ima_index].ima_2);

	else if (soc> 50 && soc <= 75)
		ima = &(m_image[gBATEX[index].ima_index].ima_3);

	else 
		ima = &(m_image[gBATEX[index].ima_index].ima_4);

	ima->TransparentBlt(memDC->m_hDC,gBATEX[index].rect,RGB(91, 41, 207));

	 /*
	 char txt[100] = {0};
	 sprintf_s(txt,sizeof(txt),"%d %%",soc);
	 CRect TxtRect(gBATEX[index].rect);
	 TxtRect.top += 15;
	memDC->DrawText(txt,strlen(txt),TxtRect,DT_VCENTER|DT_CENTER);
	*/
	return NULL;

}

char CCBatteryGIF::batEx_GetDestination(int i)
{
	int dstX = 0, dstY = 0, dstT = 0, dstB = 0;
	char res = 0;
	CRect dstRect, srcRect;

	srcRect.SetRect(gBATEX[i].rect.left,gBATEX[i].rect.top,gBATEX[i].rect.right,gBATEX[i].rect.bottom);

	if(i<0 || i >= BATEX_MAX) return res;

	//向左移动
	if(0 == gBATEX[i].direction)
	{
		dstX = gBATEX[i].rect.left + gBATEX[i].speed;
		dstY = gBATEX[i].rect.right + gBATEX[i].speed;

		batEx_setRect(i,dstX,srcRect.top,dstY,srcRect.bottom);

	}

	//向右移动
	else if (180 == gBATEX[i].direction)
	{
		dstX = gBATEX[i].rect.left - gBATEX[i].speed;
		dstY = gBATEX[i].rect.right - gBATEX[i].speed;

		batEx_setRect(i,dstX,srcRect.top,	dstY,srcRect.bottom);
	}

	//向上移动
	else if(90 == gBATEX[i].direction)
	{
		dstT = srcRect.top - gBATEX[i].speed;
		dstB = srcRect.bottom - gBATEX[i].speed;

		batEx_setRect(i,srcRect.left, dstT,srcRect.right,dstB);
	}
	
	//向下移动
	else if (270 == gBATEX[i].direction)
	{
		dstT = srcRect.top + gBATEX[i].speed;
		dstB = srcRect.bottom + gBATEX[i].speed;

		batEx_setRect(i,srcRect.left, dstT,srcRect.right,dstB);
	}

	
	else 
	{
		int tmpX = 0, tmpY =0;
	
		//第一象限
		if ((gBATEX[i].direction > 0) && (gBATEX[i].direction < 90))
		{
			tmpX = (int)gBATEX[i].speed * fabs(cos((double)gBATEX[i].direction));
			tmpY = (int)gBATEX[i].speed * fabs(sin((double)gBATEX[i].direction));
			
			dstX = srcRect.left + tmpX;
			dstY  = srcRect.top - tmpY; 
		}
		//第二象限
		else if ((gBATEX[i].direction > 90) && (gBATEX[i].direction < 180))
		{
			tmpX = (int)gBATEX[i].speed * fabs(cos((double)(180 - gBATEX[i].direction)));
			tmpY = (int)gBATEX[i].speed * fabs(sin((double)(180 - gBATEX[i].direction)));

			dstX = srcRect.left - tmpX;
			dstY  = srcRect.top - tmpY; 
		}
		
		else if ((gBATEX[i].direction > 180) && (gBATEX[i].direction < 270))
		{
			tmpX = (int)gBATEX[i].speed * fabs(cos((double)(gBATEX[i].direction - 180)));
			tmpY = (int)gBATEX[i].speed * fabs(sin((double)(gBATEX[i].direction - 180)));

			dstX = srcRect.left - tmpX;
			dstY  = srcRect.top + tmpY; 
		}

		else
		{
			tmpX = (int)gBATEX[i].speed * fabs(cos((double)(360 - gBATEX[i].direction)));
			tmpY = (int)gBATEX[i].speed *fabs( sin((double)(360 - gBATEX[i].direction)));

			dstX = srcRect.left + tmpX;
			dstY  = srcRect.top + tmpY; 
		}

		batEx_setRect(i,dstX, dstY,dstX+PNG_X, dstY + PNG_Y);
	}

	return 1;
}


void CCBatteryGIF::batEx_setRect(CRect* rect, int l, int t, int r, int b)
{
	//TRACE(" --> l: %04d  t: %04d  r: %04d  b:%04d\n",l,t,r,b);
	if(!rect) return ;
	rect->left = l;
	rect->top  = t;
	rect->right = r;
	rect->bottom  = b;
}

void CCBatteryGIF::batEx_setRect(int i, int l, int t, int r, int b)
{
	if(i<0 || i >= BATEX_MAX) return ;

	//左边出界
	if(l <= m_windowRect.left)
	{
		batEx_setRect(&(gBATEX[i].rect),m_windowRect.left,t,m_windowRect.left + PNG_X, b);
		gBATEX[i].direction = batEx_rand(0,360);
		//TRACE("改变方向: %d\n",gBATEX[i].direction);
	}

	//上边出界
	else if(t <= m_windowRect.top)
	{
		batEx_setRect(&(gBATEX[i].rect),l,m_windowRect.top,r, m_windowRect.top + PNG_Y);
		gBATEX[i].direction = batEx_rand(0,360);
		//TRACE("改变方向: %d\n",gBATEX[i].direction);
	}

	//右边出界
	else if (r >= m_windowRect.right)
	{
		batEx_setRect(&(gBATEX[i].rect),m_windowRect.right - PNG_X, t,m_windowRect.right, b);
		gBATEX[i].direction = batEx_rand(0,360);
		//TRACE("改变方向: %d\n",gBATEX[i].direction);
	}

	else if (b >= m_windowRect.bottom)
	{
		batEx_setRect(&(gBATEX[i].rect),l, m_windowRect.bottom - PNG_X, r, m_windowRect.bottom);
		gBATEX[i].direction = batEx_rand(0,360);
		//TRACE("改变方向: %d\n",gBATEX[i].direction);
	}
	else 
		batEx_setRect(&(gBATEX[i].rect),l, t, r, b);
}