#pragma once
#include "afxwin.h"
#include "PictureEx.h"
#include <afxcmn.h>

#define COLOR_WARNING RGB(255,0,0)

#define COLOR_SW_ON RGB(37,242,15)
#define COLOR_SW_OFF RGB(84,84,84)

#define COLOR_BODY_NOMAL RGB(110,146,116)
#define COLOR_CHARGING	RGB(0,255,0)
#define COLOR_DISCHARGING RGB(215,234,84)

#define COLOR_BODY_SOC RGB(0,200,0)

#define ROUND_ANGLE CPoint(10,10)

#define DRAW_UP		0x01
#define DRAW_DOWN	0x01<<1
#define DRAW_LEFT	0x01<<2
#define DRAW_RIGHT	0x01<<3

#define TIMER_FRESH  1
#define TIMER_INTERVAL 1000

class CBatcolor
{
private:
	char m_pen_flag; 
	char m_brush_flag;

	CPen* m_old_pen;
	CBrush * m_old_brush;

	int m_pen_with;
	CDC* m_pDC;

public:
	CPen m_pen;
	CBrush m_brush;

	void update(COLORREF pen_color, COLORREF brush_color, int pen_with =  1)
	{
		if(0 != pen_color)
		{
			if(m_pen_flag)
				m_pen.Detach();
			m_pen_flag = 1;
			m_pen.CreatePen(PS_SOLID,1,pen_color);
			m_old_pen = (CPen*)m_pDC->SelectObject(&m_pen);

		}

		if(0 != brush_color)
		{
			if(m_brush_flag)
				m_brush.Detach();
			m_brush_flag = 1;
			m_brush.CreateSolidBrush(brush_color);
			m_old_brush = (CBrush *)m_pDC->SelectObject(&m_brush);
		}
	}

public:
	CBatcolor(CDC* pDC, COLORREF pen_color, COLORREF brush_color, int pen_with =  1)
	{
		m_pen_flag = 0;
		m_brush_flag = 0;
		m_pen_with = pen_with;
		m_pDC = pDC;

		if(0 != pen_color)
		{
			m_pen_flag = 1;
			m_pen.CreatePen(PS_SOLID,1,pen_color);
			m_old_pen = (CPen*)pDC->SelectObject(&m_pen);
		}

		if(0 != brush_color)
		{
			m_brush_flag = 1;
			m_brush.CreateSolidBrush(brush_color);
			m_old_brush = (CBrush *)pDC->SelectObject(&m_brush);
		}
	}
	~CBatcolor(void)
	{
		if(1 == m_pen_flag)
			m_pDC->SelectObject(m_old_pen);

		if(1 == m_brush_flag)
			m_pDC->SelectObject(m_old_brush);
	}

};

typedef enum __VIEW_STYLE_E
{
	VS_HORIZONTAL = 0,
	VS_VERITCAL

}VIEW_STYLE_E;



#ifndef uint
#define uint unsigned int 
#endif

#ifndef uchar
#define uchar unsigned char 
#endif

#define ID_PIC_STATE	1001
#define ID_PIC_ID		1002
#define ID_PIC_BP		1003

#define BAT_HIGHT		80
#define BAT_WITH		90

#define BAT_MENU_MASK				10001
#define BAT_MENU_UNMASK				10002

#define BAT_MENU_QUERY_STA			10003	//查询状态
#define BAT_MENU_QUERY_SOC			10004	//查询SOC
#define BAT_MENU_QUERY_VAL			10005	//查询电压
#define BAT_MENU_QEURY_CURRENT		10006	//查询电流

#define BAT_MENU_QUERY_TEMPRATURE	10007	//查询温度
#define BAT_MENU_QUERY_SWITCH		10008	//查询开关

#define BAT_MENU_MASK_QUERY			10010	//屏蔽查询

class CCBat :
	public CWnd
{
	//DECLARE_DYNAMIC(CCBat)
public:
	CCBat();
	~CCBat();


	//电池状态
	typedef enum __BAT_STATE{

		BS_UNKOWN=0,
		BS_NORMAL,		//正常
		BS_VOL_HI,		//高电压
		BS_VOL_LO,		//低电压
		BS_BAD,			//电池故障

		BS_CHARING,
		BS_DISCHARGING

	}BAT_STATE_T;

	//电池状态
	typedef enum __BAT_SYS_STATE{

		S_IDLE=0,			//空闲
		S_DISCHARGING,		//放电
		S_CHARGING,			//充电
		S_ERROR,			//报警
		S_SUPERCARGING,		//超级电容充电
		S_UPS_WAIT_FOR_STOP,//UPS停机前等待
		S_UPS_STOP				//UPS停机状态

	}BAT_SYS_STATE_T;

	typedef enum __BAT_MODE{
		BAT_MODE_UPS=0,
		BAT_MODE_NET
	}BAT_MODE_E;

	int Create(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID);

protected:

	CWnd* mParent;	//父窗口

	//sql add
	CRect m_client;	//全局
	CRect m_head;	//头部
	CRect m_body;	//身体
	CRect m_high;	//高压
	CRect m_low;	//低压
	CRect m_txt_ID;
	CRect m_txt_soc;

	//字体
	CFont m_ft_id, *m_ft_id_old;
	CFont m_ft_soc, *m_ft_soc_old;

	VIEW_STYLE_E m_view_stylee;

	CDC			m_memDC;	//画刷
	CBitmap		m_menBMP;	//画布
	CBitmap*	m_oldbmp;

	char		m_gif_pos;	//充放电位置 

	BOOL		m_bat_bad_pos;	//报警显示

	uchar mID;		//电池序号
	CString m_UID;
	uint mVoltage;	//电压
	uint mSoc;		//电荷
	uint mCurrent;	//电流
	float mTmperature;//温度
	char mSW;		//开关

	char mMask;		//屏蔽

	BOOL _bMouseTrack;
	CToolTipCtrl m_ctrlTT;

	BAT_STATE_T mState;	//电池状态
	BAT_SYS_STATE_T mPSTA;

	char gCreateFlag ;

	BAT_MODE_E mMode;

	//更新文字
	void bat_update_text(CDC& memcdc);
	//
	void bat_text_out(CDC *cdc, CRect rect, char soc[], char val[], char cur[], float tmpe, char bypass);


	//更新TIP内容
	void bat_update_tips();


	//画图
	void bat_draw();

	//显示文字
	void bat_draw_text();

	//画矩形
	void bat_draw_rect(CRect rect, int postion, COLORREF color=RGB(0,0,0));
	
public:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

public:
	//设置变量
	void bat_set_params(uint vol, uint soc, uint current, BAT_STATE_T bst);

	//设置标识
	void bat_set_identy(uchar id)	{mID = id; m_gif_pos = 0;m_UID.Format("%02d",id);};

	//设置电压
	void bat_set_voltage(uint vol)	{mVoltage = vol; m_gif_pos = 0;};

	//设置soc
	void bat_set_soc(uint soc)	{mSoc = soc; m_gif_pos = 0;};

	//设置电流
	void bat_set_current(uint current)	{mCurrent = current;  m_gif_pos = 0;};

	//设置状态
	void bat_set_state(BAT_STATE_T bst)	{
	
		if(BS_BAD == bst)
			SetTimer(TIMER_FRESH,TIMER_INTERVAL,NULL);
		else
			KillTimer(TIMER_FRESH);
		
		mState = bst; m_gif_pos = 0;
	};
	CString bat_get_state();
	void bat_set_sys_state(BAT_SYS_STATE_T bsst)	{
		
		mPSTA = bsst;
		if((S_CHARGING == bsst || S_DISCHARGING == bsst || S_ERROR == bsst) && 0 == m_gif_pos)
		{
			SetTimer(TIMER_FRESH,TIMER_INTERVAL,NULL);
			m_gif_pos = 1;
		}
	};

	//设置温度
	void bat_set_temprature(float tmp){mTmperature = tmp; m_gif_pos = 0;};

	//设置开关
	void bat_set_sw(uchar bp);

	//设置屏蔽
	void bat_set_mask(uchar mask){
		mMask = mask; m_gif_pos = 0;

		if((S_CHARGING == mPSTA || S_DISCHARGING == mPSTA) && 0 == mMask)
			SetTimer(TIMER_FRESH,TIMER_INTERVAL,NULL);
	};

	//设置模式
	void bat_set_mode(BAT_MODE_E bme){mMode = bme; m_gif_pos = 0;};

	//刷新
	void bat_update_ui();

	void bat_change();


	afx_msg void OnPaint();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
