#include "../StdAfx.h"
#include "CBat.h"
#include "../include/comm.h"



#define NORMAL_COLOR RGB(176,176,176)
#define SELECTED_COLOR RGB(49,97,233)

char gBatSelected = 0;

extern void sql_trace(char* format,...);

CBrush batBrush(RGB(221,227,228));

BOOL g_bat_bad_pos = FALSE;


CCBat::CCBat()
{
	mID = 0;
	gCreateFlag = 0;
	mSoc = 0;
	mVoltage = 0;
	mCurrent = 0;
	mTmperature = 0;

	_bMouseTrack = TRUE;
	mMask = -1;

	m_view_stylee = VS_HORIZONTAL;
}

CCBat::~CCBat()
{
}
BEGIN_MESSAGE_MAP(CCBat, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_CTLCOLOR()
	ON_WM_SIZE()
	ON_WM_TIMER()
END_MESSAGE_MAP()

int CCBat::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码

	return 0;
}



int CCBat::Create(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID)
{
	if(1 == gCreateFlag)
	{
		this->MoveWindow(&rect);
		return 1;
	}

	BOOL ret = CWnd::Create(NULL, NULL, dwStyle, rect,ParentWnd, nID);
	if(ret == TRUE){
	
		ModifyStyleEx(0, WS_EX_LAYERED);
		SetLayeredWindowAttributes(RGB(255,0,255),100,LWA_ALPHA);//LWA_ALPHA | LWA_COLORKEY
		//bat_update_text();	//文字

		//提示控件
		EnableToolTips(TRUE);//enable use it
		m_ctrlTT.Create(this);
		m_ctrlTT.Activate(TRUE);
		m_ctrlTT.SetTipTextColor(RGB(0,0,255));//font color
		//m_ctrlTT.SetDelayTime(150);//delay time
		m_ctrlTT.SetMaxTipWidth(100);
		m_ctrlTT.AddTool(this,(LPCTSTR)"");

		//
		m_ft_id.CreateFont(18,               // 以逻辑单位方式指定字体的高度
			0,                               // 以逻辑单位方式指定字体中字符的平均宽度
			0,                               // 指定偏离垂线和X轴在显示面上的夹角(单位:0.1度)
			0,                               // 指定符串基线和X轴之间的夹角(单位:0.1度)
			FW_BOLD,						 // 指定字体镑数
			FALSE,                           // 是不是斜体
			FALSE,                           // 加不加下划线
			0,                               // 指定是否是字体字符突出
			ANSI_CHARSET,                    // 指定字体的字符集
			OUT_DEFAULT_PRECIS,              // 指定所需的输出精度
			CLIP_DEFAULT_PRECIS,             // 指定所需的剪贴精度
			DEFAULT_QUALITY,                 // 指示字体的输出质量
			DEFAULT_PITCH | FF_SWISS,        // 指定字体的间距和家族
			_T("宋体")                       // 指定字体字样的名称
			);

		m_ft_soc.CreateFont(12,               // 以逻辑单位方式指定字体的高度
			0,                               // 以逻辑单位方式指定字体中字符的平均宽度
			0,                               // 指定偏离垂线和X轴在显示面上的夹角(单位:0.1度)
			0,                               // 指定符串基线和X轴之间的夹角(单位:0.1度)
			FW_NORMAL,						 // 指定字体镑数
			FALSE,                           // 是不是斜体
			FALSE,                           // 加不加下划线
			0,                               // 指定是否是字体字符突出
			ANSI_CHARSET,                    // 指定字体的字符集
			OUT_DEFAULT_PRECIS,              // 指定所需的输出精度
			CLIP_DEFAULT_PRECIS,             // 指定所需的剪贴精度
			DEFAULT_QUALITY,                 // 指示字体的输出质量
			DEFAULT_PITCH | FF_SWISS,        // 指定字体的间距和家族
			_T("宋体")                       // 指定字体字样的名称
			);
	}

	mParent = ParentWnd;
	gCreateFlag = 1;
	return 1;
}



/***
 * 设置变量
 * @param vol: 电压
 *		  soc: 电荷
 *		  current:电流
 *		  bst:	状态
 * @author: Shi Qingliang
 * @date: 20141204
 */
void CCBat::bat_set_params(uint vol, uint soc, uint current, BAT_STATE_T bst)
{
	mVoltage = vol;
	mSoc = soc;
	mCurrent = current;

}


void CCBat::bat_update_ui()
{
	Invalidate(TRUE);
}


CString CCBat::bat_get_state()
{

			switch(mState)
			{
			case BS_NORMAL:
				return "正常";
				break;
			case BS_VOL_HI:
				return "高电压";
				break;
			case BS_VOL_LO:
				return "低电压";
				break;
			case BS_BAD:
				return "故障";
				break;
			case BS_CHARING:
				return "充电";
				break;;
			case BS_DISCHARGING:
				return "放电";
				break;;
			default:
				return "未知";
			}
}


/***
 * 更新文字 
 * @author: Shi Qingliang
 * @date: 20141204
 */
void CCBat::bat_update_text(CDC& memcdc)
{

	char tSOC[16]={0},tVal[16] = {0},tCur[16] = {0};
	CRect rect;
	GetClientRect(rect);

	rect.left = 18;
	rect.top = 4;
	rect.right -= 1;
	rect.bottom -= 3;

	float per = 0.00;
	if(0 == mSoc)
		sprintf_s(tVal,sizeof(tVal),"   0 %%");
	else{

		sprintf_s(tVal,sizeof(tVal),"%5d %%",mSoc);
	}
	sprintf_s(tSOC,sizeof(tSOC),"%5.2f V",(float)mVoltage/1000);
	sprintf_s(tCur,sizeof(tCur),"%5.2f A",(float)mCurrent/1000);

	this->bat_text_out(&memcdc,rect,tVal,tSOC,tCur,mTmperature,mSW);
}


/***
 * 输出文字
 * @param	cdc:
 *			rect:
 *			soc:
 *			val:
 *			tmpe:
 *			bypass:
 */
void CCBat::bat_text_out(CDC *cdc, CRect rect, char soc[], char val[], char cur[], float tmpe, char bypass)
{
	char tmp[11] = {0};
	CFont * m_font,* oldfont;
	rect.top += 1;
	m_font = new CFont();
	m_font->CreateFont(12,               // 以逻辑单位方式指定字体的高度
		0,                               // 以逻辑单位方式指定字体中字符的平均宽度
		0,                               // 指定偏离垂线和X轴在显示面上的夹角(单位:0.1度)
		0,                               // 指定符串基线和X轴之间的夹角(单位:0.1度)
		FW_NORMAL,                       // 指定字体镑数
		FALSE,                           // 是不是斜体
		FALSE,                           // 加不加下划线
		0,                               // 指定是否是字体字符突出
		ANSI_CHARSET,                    // 指定字体的字符集
		OUT_DEFAULT_PRECIS,              // 指定所需的输出精度
		CLIP_DEFAULT_PRECIS,             // 指定所需的剪贴精度
		DEFAULT_QUALITY,                 // 指示字体的输出质量
		DEFAULT_PITCH | FF_SWISS,        // 指定字体的间距和家族
		_T("宋体")                       // 指定字体字样的名称
		);
	oldfont = (CFont*)cdc->SelectObject(m_font);


	cdc->SetTextAlign(TA_CENTER|TA_CENTER);
	cdc->SetBkMode(TRANSPARENT);
	cdc->SetTextColor(RGB(105,105,105));

	//电压
	int y = rect.top + 3;
	int x = rect.left + 15;// (int)(rect.Width()/2) -(int)(strlen(val)/2);
	cdc->TextOut(x+9,y+3,val);

	//soc
	y = rect.top+3+12+2;
	x = rect.left + 15; //(int)(rect.Width()/2) -(int)(strlen(soc)/2);
	cdc->TextOut(x+9,y+3,soc);

	//温度
	memset(tmp,0,sizeof(tmp));
	sprintf_s(tmp,sizeof(tmp),"%4.1f ℃",tmpe);
	y = rect.top+3+12*2+2;
	cdc->TextOut(x+9,y+3,tmp);

	//电流
	y = rect.top+3+12*3+2;
	cdc->TextOut(x+9,y+3,cur);

	cdc->SelectObject(oldfont);
	delete m_font;
}

void CCBat::bat_set_sw(uchar bp)
{
	mSW = bp; 
	m_gif_pos = 0;
	if((S_CHARGING == mPSTA || S_DISCHARGING == mPSTA) && 1 == mSW)
		SetTimer(TIMER_FRESH,TIMER_INTERVAL,NULL);
}


void CCBat::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	m_memDC.CreateCompatibleDC(NULL);
	if(!m_menBMP.CreateCompatibleBitmap(&dc,m_client.Width(),m_client.Height()))
	{
		TRACE("errr\n");
	}
	m_oldbmp = (CBitmap*)m_memDC.SelectObject(&m_menBMP);
	m_memDC.FillSolidRect(0,0,m_client.Width(),m_client.Height(),RGB(255,255,255));
	bat_draw();
	bat_draw_text();
	dc.TransparentBlt(0,0,m_client.Width(),m_client.Height(),&m_memDC,0,0,m_client.Width(),m_client.Height(),RGB(255,255,255));

	m_menBMP.DeleteObject();
	//m_memDC.SelectObject(m_oldbmp);
	m_memDC.DeleteDC();

}


/***
 * 画图
 */
void CCBat::bat_draw()
{

	CBatcolor m(&m_memDC,COLOR_SW_ON,COLOR_SW_ON);

	//如果是报警，
	if(BS_BAD == mState)
	{
		COLORREF cur;
		if(! g_bat_bad_pos)
			cur =COLOR_WARNING;
		else
			cur =COLOR_BODY_NOMAL;
		m.update(cur,cur);

		g_bat_bad_pos = !g_bat_bad_pos;
		m_memDC.RoundRect(m_head,ROUND_ANGLE);
		m_memDC.RoundRect(m_high,ROUND_ANGLE);
		m_memDC.RoundRect(m_low,ROUND_ANGLE);
		m_memDC.FillSolidRect(m_body,cur);

		return ;
	}
	
	//先画开关

	//如果正在冲放电，显示实际的状态
	if (S_CHARGING == mPSTA || S_DISCHARGING == mPSTA)
	{
		if(0 == mSW) //如果关闭，切换颜色
			m.update(COLOR_SW_OFF,COLOR_SW_OFF);
		else
			m.update(COLOR_SW_ON,COLOR_SW_ON);
	}
	else	//空闲状态，则显示屏蔽
	{
		if(0 == mMask)
			m.update(COLOR_SW_ON,COLOR_SW_ON);
		else
		m.update(COLOR_SW_OFF,COLOR_SW_OFF);
		
	}
	m_memDC.RoundRect(m_head,ROUND_ANGLE);

/*
	//画高压状态 COLOR_WARNING
	if(BS_VOL_HI == mState)
		m.update(COLOR_WARNING,COLOR_WARNING);
	else 
	{
		m.update(COLOR_BODY_NOMAL,COLOR_BODY_NOMAL);
	}

	m_memDC.RoundRect(m_high,ROUND_ANGLE);


	//画低压状态
	if(BS_VOL_LO == mState)
		m.update(COLOR_WARNING,COLOR_WARNING);
	else 
	{
		if(mSoc > 0)
			m.update(COLOR_BODY_SOC,COLOR_BODY_SOC);
		else
			m.update(COLOR_BODY_NOMAL,COLOR_BODY_NOMAL);
	}
	m_memDC.RoundRect(m_low,ROUND_ANGLE);
*/
	m.update(COLOR_BODY_NOMAL,COLOR_BODY_NOMAL);
	m_memDC.RoundRect(m_body,ROUND_ANGLE);
	
	CRect ttt = m_body;
	ttt.bottom+=2;

	CRect tmpR;
	tmpR.SetRect(m_body.left,m_body.top,m_body.right,m_body.bottom);
	if(S_DISCHARGING == mPSTA && 1 == mSW)
	{
	
		if(VS_HORIZONTAL == m_view_stylee)
		{
			tmpR.left = m_body.left + m_body.Width()*m_gif_pos/5;
		}
		else
			tmpR.top = m_body.top + m_body.Height()*m_gif_pos/5;

		m.update(COLOR_BODY_SOC,COLOR_BODY_SOC);
		m_memDC.RoundRect(tmpR,ROUND_ANGLE);
		//m_memDC.FillSolidRect(tmpR,COLOR_BODY_SOC);


		m_gif_pos++;
		if(m_gif_pos > 5)	m_gif_pos = 1;

		return ;
	}

	if(S_CHARGING == mPSTA && 1 == mSW)
	{

		if(VS_HORIZONTAL == m_view_stylee)
		{
			tmpR.left = m_body.right - m_body.Width()*m_gif_pos/5;
		}
		else
			tmpR.top = m_body.bottom - m_body.Height()*m_gif_pos/5;

		m.update(COLOR_BODY_SOC,COLOR_BODY_SOC);
		m_memDC.RoundRect(tmpR,ROUND_ANGLE);
		
		m_gif_pos++;
		if(m_gif_pos > 5)	m_gif_pos = 1;

		return;
	}

	//空闲状态

	if(BS_NORMAL == mState || BS_UNKOWN == mState)
	{
		if(VS_HORIZONTAL == m_view_stylee)
		{
			tmpR.left = m_body.right - m_body.Width()*mSoc/100;
		}
		else
			tmpR.top = tmpR.bottom - m_body.Height()*mSoc/100;
		m.update(COLOR_BODY_SOC,COLOR_BODY_SOC);
		m_memDC.RoundRect(tmpR,ROUND_ANGLE);
		return ;
	}

}

//显示文字
void CCBat::bat_draw_text()
{
	CString t_soc("");
	char tmp[11] = {0};

	m_ft_id_old = (CFont*)m_memDC.SelectObject(m_ft_id);

	m_memDC.SetBkMode(TRANSPARENT);
	m_memDC.SetTextColor(RGB(0,0,255));
	m_memDC.DrawText(m_UID,m_txt_ID,DT_LEFT|DT_VCENTER|DT_SINGLELINE);

	//m_memDC.SelectObject(m_ft_id_old);

	t_soc.Format("%5.2f V\n%5.2f A\n%d %%\n%4.1f ℃",(float)mVoltage/1000,(float)mCurrent/1000,mSoc,mTmperature);

	m_ft_soc_old = (CFont*)m_memDC.SelectObject(m_ft_soc);
	m_memDC.SetTextColor(RGB(71,81,210));
	m_memDC.DrawText(t_soc,m_txt_soc,DT_CENTER);
	//m_memDC.SelectObject(m_ft_soc_old);

}


void CCBat::OnLButtonUp(UINT nFlags, CPoint point)
{
	CWnd::OnLButtonUp(nFlags, point);
}


void CCBat::OnRButtonDown(UINT nFlags, CPoint point)
{

	CWnd::OnRButtonDown(nFlags, point);
}


void CCBat::OnRButtonUp(UINT nFlags, CPoint point)
{

	CMenu mMenu, subMenu;
	mMenu.CreatePopupMenu();
	if(BAT_MODE_NET == mMode){

		subMenu.CreatePopupMenu();
		subMenu.AppendMenu(MF_STRING,BAT_MENU_MASK_QUERY,"查询");
		subMenu.AppendMenu(MF_STRING,BAT_MENU_MASK,"屏蔽");
		subMenu.AppendMenu(MF_STRING,BAT_MENU_UNMASK,"取消屏蔽");

		if(0 == mMask)	//
			subMenu.CheckMenuItem(BAT_MENU_MASK,MF_CHECKED);
		else if(1 == mMask)
			subMenu.CheckMenuItem(BAT_MENU_UNMASK,MF_CHECKED);
			
		mMenu.AppendMenu(MF_POPUP,(UINT_PTR)subMenu.operator HMENU(), "电池屏蔽");
		mMenu.AppendMenu(MF_SEPARATOR);
	}


	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_STA,"查询状态");
	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_SOC,"查询SOC");
	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_VAL,"查询电压");
	
 	if(BS_CHARING == mState || BS_DISCHARGING == mState)
 		mMenu.AppendMenu(MF_STRING,BAT_MENU_QEURY_CURRENT,"查询电流");
 	else 
 		mMenu.AppendMenu(MF_STRING|MF_GRAYED,BAT_MENU_QEURY_CURRENT,"查询电流");

	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_TEMPRATURE,"查询温度");
	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_SWITCH,"查询开关");

	CPoint pt;
	GetCursorPos(&pt);

	mMenu.TrackPopupMenu(TPM_RIGHTBUTTON, pt.x, pt.y,this);
	mMenu.DestroyMenu();
	mMenu.Detach();
	subMenu.Detach();

	CWnd::OnRButtonUp(nFlags, point);
}


BOOL CCBat::OnCommand(WPARAM wParam, LPARAM lParam)
{
	int menuID = LOWORD(wParam);
	if(menuID > 10000)
	{
		
		if(BAT_MENU_MASK == menuID)		//屏蔽
			mParent->SendMessage(MSG_BATTERY_MASK,(WPARAM)(mID-1),(LPARAM)1);
		else if(BAT_MENU_UNMASK == menuID)//取消屏蔽
			mParent->SendMessage(MSG_BATTERY_MASK,(WPARAM)(mID-1),(LPARAM)0);

		else
			mParent->SendMessage(MSG_BATTERY_ACT,(WPARAM)(menuID),(LPARAM)(mID-1));

	}

	return CWnd::OnCommand(wParam, lParam);
}



BOOL CCBat::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
	return CWnd::OnEraseBkgnd(pDC);
}


void CCBat::OnMouseMove(UINT nFlags, CPoint point)
{
	if (_bMouseTrack )//若容许追踪，则。
	{
		bat_update_tips();
		TRACKMOUSEEVENT csTME;
		csTME.cbSize = sizeof(csTME);
		csTME.dwFlags = TME_LEAVE|TME_HOVER;
		csTME.hwndTrack = m_hWnd;	//指定要追踪的窗口
		csTME.dwHoverTime = 10;		//鼠标在按钮上逗留跨越10ms，才认为状况为HOVER
		::_TrackMouseEvent(&csTME); //开启Windows的WM_MOUSELEAVE，WM_MOUSEHOVER事务支撑
		_bMouseTrack=FALSE;			//若已经追踪，则停止追踪
	}

	CWnd::OnMouseMove(nFlags, point);
}



/***
 *
 */
void CCBat::OnMouseLeave()
{
	_bMouseTrack=TRUE;
	CWnd::OnMouseLeave();
}


/***
 *
 */
void CCBat::bat_update_tips()
{
	char mask[10] = {0};
	char ddd[100] = {0};
	
	CString tmp("");

	if(0 == mMask)
		sprintf_s(mask,sizeof(mask),"%s","否");
	else if(1 == mMask)
		sprintf_s(mask,sizeof(mask),"%s","是");
	else 
		sprintf_s(mask,sizeof(mask),"%s","未知");

	sprintf_s(ddd,sizeof(ddd),_T("ID:%-10d\n"
		"电压:%-5.2f V\n"
		"电流:%-5.2f A\n"
		"SOC:%3d %% \n"
		"开关:%-5s\n"
		"状态:%-5s\n"
		"温度:%.1f℃\n"
		"屏蔽:%s\n"/**/
		),this->mID ,(float)mVoltage/1000,(float)mCurrent/1000,mSoc/**/,mSW==1?"开":"关",bat_get_state(),
		mTmperature,mask);

	m_ctrlTT.UpdateTipText(ddd,this);
}


/***
 *
 */
BOOL CCBat::PreTranslateMessage(MSG* pMsg)
{
	m_ctrlTT.RelayEvent(pMsg); 
	return CWnd::PreTranslateMessage(pMsg);
}



/***
 *
 */
HBRUSH CCBat::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CWnd::OnCtlColor(pDC, pWnd, nCtlColor);
	return hbr;
}


/***
 * 划分区域
 */
void CCBat::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	CRect tmp;
	GetClientRect(m_client);

	//m_client.SetRect(tmp.left+10, tmp.top+10, tmp.right-10, tmp.bottom -10);

	int sub_head = 0;

	if(VS_HORIZONTAL == m_view_stylee)
	{
		sub_head = m_client.Height()/4;

		m_head.SetRect(m_client.left,m_client.top + sub_head, m_client.left+10+ m_client.Width()/2,m_client.bottom-sub_head);

		m_body.SetRect(m_client.left+10,m_client.top,m_client.right ,m_client.bottom);

		//m_high.SetRect(m_client.left+10,m_client.top,m_client.left+ m_client.Width()/2,m_client.bottom);
		//m_low.SetRect(m_client.right-m_client.Width()/2,m_client.top,m_client.right,m_client.bottom);

		m_txt_ID.SetRect(m_body.left,m_body.top,m_body.right,m_body.bottom);
		m_txt_soc.SetRect(m_txt_ID.left +10,m_txt_ID.top+4,m_body.right,m_body.bottom);
	}
	else
	{
		sub_head = m_client.Width()/4;

		m_head.SetRect(m_client.left + sub_head,m_client.top,m_client.right - sub_head,m_client.top + 10+10);

		m_body.SetRect(m_client.left,m_client.top + 10,m_client.right,m_client.bottom);

		m_high.SetRect(m_client.left,m_client.top + 10 ,m_client.right, m_client.top+10+20);
		m_low.SetRect(m_client.left,m_client.bottom-20 ,m_client.right,m_client.bottom);
	}

}

void CCBat::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	if(TIMER_FRESH == nIDEvent)
	{
		Invalidate();
	}
	CWnd::OnTimer(nIDEvent);
}
