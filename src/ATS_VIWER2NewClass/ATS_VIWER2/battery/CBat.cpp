#include "../StdAfx.h"
#include "CBat.h"
#include "../include/comm.h"



#define NORMAL_COLOR RGB(176,176,176)
#define SELECTED_COLOR RGB(49,97,233)

char gBatSelected = 0;

extern void sql_trace(char* format,...);

CBrush batBrush(RGB(221,227,228));


//构造函数
CCBat::CCBat()
{
	mID = 0;
	gCreateFlag = 0;

	mSoc = 0;
	mVoltage = 0;
	mCurrent = 0;
	mTmperature = 0;

	_bMouseTrack = TRUE;
	mMask = -1;
	mSW = -1;
	mState = BS_UNKOWN;
	mPSTA = S_IDLE;
	mParent = NULL;		//父窗口
	gPicRect.SetRect(0,0,0,0);		//图像位置
	gRect.SetRect(0,0,0,0);		//整个图像位置
	m_participation = 0;	//是否参与
	char gCreateFlag ;
	mMode =BAT_MODE_UPS;
}


//析构函数
CCBat::~CCBat()
{
}

BEGIN_MESSAGE_MAP(CCBat, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


//创建
int CCBat::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}


//初始化电池数据
void CCBat::bat_init()
{
	mSoc = 0;
	mVoltage = 0;
	mCurrent = 0;
	mTmperature = 0;

	_bMouseTrack = TRUE;
	mMask = -1;
	mSW = -1;
	mState = BS_UNKOWN;
	mPSTA = S_IDLE;

	bat_update_pic(mState);
}


//创建对象
int CCBat::Create(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID)
{
	//如果已经创建，只需移动位置
	if(1 == gCreateFlag)
	{
		this->MoveWindow(&rect);
		return 1;
	}

	gRect = rect;
	BOOL ret = CWnd::Create(NULL, NULL, dwStyle, rect,ParentWnd, nID);
	if(ret == TRUE){

		ShowWindow(SW_HIDE);

		gPicRect.top = 20+1;
		gPicRect.bottom = 37+1;
		gPicRect.left = 2;
		gPicRect.right =16+2;

		//电池图片
		if(mPic.Create("sql_pic_state",WS_CHILD|WS_VISIBLE,gPicRect,this,ID_PIC_STATE) == FALSE) 
		{
			MessageBox("create PictrueBox faild");
		}

		if(mPic.Load("res/电池黑.bmp"))
			mPic.Draw();

		bat_load_id();	//序号
		bat_load_sw();	//开关

		//提示控件
		EnableToolTips(TRUE);
		m_ctrlTT.Create(this);
		m_ctrlTT.Activate(TRUE);
		m_ctrlTT.SetTipTextColor(RGB(0,0,255));//font color
		m_ctrlTT.SetMaxTipWidth(100);
		m_ctrlTT.AddTool(this,(LPCTSTR)"");

	}

	mParent = ParentWnd;
	gCreateFlag = 1;
	return 1;
}


//加载序号
void CCBat::bat_load_id()
{
	CString dd("");
	dd.Format("res/d_%d.png",mID);
	batEx_load(&m_uid,(LPTSTR)(LPCTSTR)dd);
}


//加载图片
void CCBat::batEx_load(CImage* ima,char* png)
{
	if(!ima || !png)return ;

	if (ima->IsNull())
	{
		ima->Load(_T(png));
		if(ima->IsNull()) return;
		if (ima->GetBPP() == 32) //确认该图像包含Alpha通道
		{
			int i; 
			int j;
			for (i=0; i<ima->GetWidth(); i++)
			{
				for (j=0; j<ima->GetHeight(); j++)
				{
					byte *pByte = (byte *)ima->GetPixelAddress(i, j);
					pByte[0] = pByte[0] * pByte[3] / 255;
					pByte[1] = pByte[1] * pByte[3] / 255;
					pByte[2] = pByte[2] * pByte[3] / 255;
				}
			} //for (i=0; i<ima->GetWidth(); i++)
		} //if (ima->GetBPP() == 32)
	} //if (ima->IsNull())
}


//加载开关
void CCBat::bat_load_sw()
{
	batEx_load(&m_sw_on,"res\\switch_on.png");
	batEx_load(&m_sw_off,"res\\switch_off.png");
}


//设置变量
void CCBat::bat_set_params(uint vol, uint soc, uint current, BAT_STATE_T bst)
{
	mVoltage = vol;
	mSoc = soc;
	mCurrent = current;
}


//设置电压
void CCBat::bat_set_voltage(uint vol)
{
	bat_set_params(vol,mSoc,mCurrent,mState);
}


//设置soc
void CCBat::bat_set_soc(uint soc)
{
	bat_set_params(mVoltage,soc,mCurrent,mState);
}


// 设置电流
void CCBat::bat_set_current(uint current)
{
	bat_set_params(mVoltage,mSoc,current,mState);
}


//设置状态
void CCBat::bat_set_state(BAT_STATE_T bst)
{
	if(bst != mState)
	{
		mState = bst;
		bat_update_pic(mState);
	}
}


// 是否参与
void CCBat::bat_participation(char p)
{
	m_participation = p;
}


//设置系统状态
void CCBat::bat_set_sys_state(BAT_SYS_STATE_T bsst)
{
	if (bsst != mPSTA)
	{
		mPSTA = bsst;
		bat_update_pic(mState);
	}
}


//状态文字描述
CString CCBat::bat_get_state()
{

			switch(mState)
			{
			case BS_NORMAL:
				return "正常";
				break;
			case BS_VOL_HI:
				return "满电压";
				break;
			case BS_VOL_LO:
				return "低电压";
				break;
			case BS_BAD:
				return "故障";
				break;
			default:
				;
			}

			if(mPSTA == S_CHARGING &&(1== mSW || 0 == mMask))
				return "充电";

			else if (mPSTA == S_DISCHARGING &&(1== mSW || 0 == mMask))
			{
				return "放电";
			}

			return "未知";
}


//设置开关
void CCBat::bat_set_sw(uchar bp)
{
	bat_update_sw(bp);
}


// 更新文字 
void CCBat::bat_update_text(CDC& memcdc,CRect rect)
{
	char tmp[11] = {0};
	CFont m_font, m_id_font ,* oldfont=NULL;
	char tSOC[16]={0},tVal[16] = {0},tCur[16] = {0};
	CRect tmpRec;
	int sub_h = 0;
	CString t_id("");

	float per = 0.00;

	if(0 == mSoc)
		sprintf_s(tSOC,sizeof(tSOC),"   0 %%");
	else
		sprintf_s(tSOC,sizeof(tSOC),"%d %%",mSoc);

	sprintf_s(tVal,sizeof(tVal),"%-.2f V",(float)mVoltage/1000);
	sprintf_s(tCur,sizeof(tCur),"%-.2f A",(float)mCurrent/1000);
	
	m_font.CreateFont(14,               
		0,                              
		0,                              
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		0,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS,        
		_T("宋体")                       
		);

	m_id_font.CreateFont(rect.Height() - 20,
		0,
		0,
		0,
		FW_NORMAL,
		FALSE,
		FALSE,
		0,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS,
		_T("宋体")                      
		);

	memcdc.SetBkMode(TRANSPARENT);

	//先书写编号
	/*	
	t_id.Format("%02d",mID);
	memcdc.SetTextColor(RGB(255,255,255));
	oldfont = (CFont*)memcdc.SelectObject(&m_id_font);
	memcdc.DrawText(t_id,rect,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	*/

	//书写各种参数
	rect.top += 5;
	rect.bottom -= 5;
	sub_h = rect.Height()/4;
	memcdc.SetTextColor(RGB(0,0,255));
	oldfont = (CFont*)memcdc.SelectObject(&m_font);

	//电压
	tmpRec.SetRect(rect.left,rect.top,rect.right,rect.top+sub_h);
	memcdc.DrawText((LPCTSTR)(LPTSTR)tVal,tmpRec,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	//电流
	tmpRec.SetRect(rect.left,rect.top+sub_h,rect.right,rect.top+2*sub_h);
	memcdc.DrawText((LPCTSTR)(LPTSTR)tCur,tmpRec,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	//SOC
	tmpRec.SetRect(rect.left,rect.top+2*sub_h,rect.right,rect.bottom - sub_h);
	memcdc.DrawText((LPCTSTR)(LPTSTR)tSOC,tmpRec,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	//温度
	t_id.Format("%4.1f ℃",mTmperature);
	tmpRec.SetRect(rect.left,rect.bottom-sub_h,rect.right,rect.bottom);
	memcdc.DrawText(t_id,tmpRec,DT_CENTER|DT_VCENTER|DT_SINGLELINE);

	m_font.DeleteObject();
	m_id_font.DeleteObject();
}


//更新状态
void CCBat::bat_update_pic(BAT_STATE_T bst)
{
	CString tmp = bat_get_state();

	mPic.UnLoad();
	if(BS_BAD == bst)
	{
		mPic.Load("res/error.gif");
		goto UPDATE_END;
	}

	if(BS_VOL_HI == bst)
	{
		mPic.Load("res/电池绿100.bmp");
		goto UPDATE_END;
	}
	if(BS_VOL_LO == bst)
	{
		mPic.Load("res/电池绿20.bmp");
		goto UPDATE_END;
	}

	if(S_CHARGING == mPSTA && (1 ==  mSW || 0 == mMask))
	{                                                                                      
		mPic.Load("res/chargine.gif");
		goto UPDATE_END;
	}
	
	if(S_DISCHARGING== mPSTA && (1 ==  mSW || 0 == mMask))
	{
			TRACE("更新状态 %d S_DISCHARGING\n",mID);
		mPic.Load("res/discharge.gif");
		goto UPDATE_END;
	}

	if(BS_NORMAL == bst)
	{
		if(mSoc < 30)
			mPic.Load("res/电池绿20.bmp");

		else if(mSoc >= 30 && mSoc < 50)
			mPic.Load("res/电池绿40.bmp");

		else if(mSoc >= 50 && mSoc < 70)
			mPic.Load("res/电池绿60.bmp");

		else if(mSoc >= 70 && mSoc < 90)
			mPic.Load("res/电池绿80.bmp");

		else 
			mPic.Load("res/电池绿100.bmp");

	}

	else if (BS_UNKOWN == bst)
	{
		mPic.Load("res/电池黑.bmp");
	}

	
UPDATE_END:
	mPic.Draw();
}


// 获取三个图片的位置
char CCBat::bat_get_pic_rect(CRect& recID,CRect& recBat, CRect& recSW, CRect& recText)
{
	// 16 * 29 * 16
	int sub_l = 0;
	CRect rect;
	GetClientRect(rect);

	if(rect.Height() < 32)
	{
		sub_l = rect.left + (rect.Width()-32)/2;
		recID.SetRect(sub_l,rect.top,sub_l+16,rect.top + 16);
		recSW.SetRect(recID.right + 2,rect.top,recID.right + 2+16,rect.top + 16);
	}
	else if(rect.Height() < 64)
	{
		int sub = rect.left + (rect.Width()-48)/2;
		recID.SetRect(sub,rect.bottom-16,sub+16,rect.bottom);
		recBat.SetRect(recID.right,rect.bottom - 29 ,recID.right+16,rect.bottom);
		recSW.SetRect(recBat.right,rect.bottom-16,recBat.right+16,rect.bottom);
	}
	else
	{
		int sub = (rect.Height()-40 - 28)/2;
		recID.SetRect(rect.left+3,rect.top+3,rect.left+16+3,rect.top+16+3);
		recBat.SetRect(rect.left+3,rect.top+16 + sub + 3,rect.left+16+3,rect.top+16+sub + 28+ 3);
		recSW.SetRect(rect.left,rect.bottom-18-2,rect.left+18+2,rect.bottom);
		recText.SetRect(rect.left+17,rect.top,rect.right,rect.bottom);
	}

	return 1;
}


//绘制
void CCBat::OnPaint()
{
	
	CPaintDC dc(this); // device context for painting

	CRect rect, recText, recID, recBat,recSW;
	GetClientRect(rect);

	CBrush m_bk_brush(RGB(255,255,255)), * old_brush=NULL;//200,245,245
	CPen m_bk_pen(PS_SOLID,1,RGB(255,255,255)), * old_pen=NULL;


	CDC MemDC; 
	CBitmap MemBitmap, *old_bmp = NULL;

	MemDC.CreateCompatibleDC(NULL);
	MemBitmap.CreateCompatibleBitmap(&dc,rect.Width(),rect.Height());
	CBitmap *pOldBit=MemDC.SelectObject(&MemBitmap);
	
	MemDC.FillSolidRect(0,0,rect.Width(),rect.Height(),RGB(0,0,0));
	bat_get_pic_rect(recID,recBat,recSW,recText);


	old_brush	= MemDC.SelectObject(&m_bk_brush);
	old_pen		= MemDC.SelectObject(&m_bk_pen);

	dc.SetBkMode(TRANSPARENT);
	//绘制背景
	MemDC.RoundRect(recText,CPoint(20,20));
	MemDC.SelectObject(old_pen);
	MemDC.SelectObject(old_brush);

	//书写文字信息
	bat_update_text(MemDC,recText);


	dc.TransparentBlt(recText.left,recText.top,recText.Width(),recText.Height(),&MemDC,0,0,
		rect.Width(),rect.Height(),RGB(0,0,0));

	//显示UID
	if(!m_uid.IsNull())
		m_uid.TransparentBlt(dc.m_hDC,recID,RGB(255,255,255));

	//显示开关
	CImage* ima = bat_get_swtich();
	if(ima)
		ima->TransparentBlt(dc.m_hDC,recSW,RGB(255,0,0));

	mPic.MoveWindow(&recBat,TRUE);
	
	if(MemDC.m_hDC && old_bmp)
		MemDC.SelectObject(old_bmp);

	m_bk_brush.DeleteObject();
	m_bk_pen.DeleteObject();

	//绘图完成后的清理
	MemBitmap.DeleteObject();
	MemDC.DeleteDC();
	
}

//更新开关
CImage* CCBat::bat_get_swtich()
{
	//正在充、放电，显示真实的开关状态
	if(S_CHARGING == mPSTA || S_DISCHARGING == mPSTA)
	{
		if(1 == mSW){
			return &m_sw_on;
		}
		else{

			return &m_sw_off;
		}
	}
	//不在充、放电状态，则显示屏蔽状态
	else
	{
		if(0 == mMask){

			return &m_sw_on;
		}
		else{

			return &m_sw_off;
		}
	}
}


void CCBat::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(0 == gBatSelected)
		gBatSelected = 1;
	else
		gBatSelected = 0;

	CWnd::OnLButtonUp(nFlags, point);
}


void CCBat::OnRButtonDown(UINT nFlags, CPoint point)
{
	
	CWnd::OnRButtonDown(nFlags, point);
}

void CCBat::OnRButtonUp(UINT nFlags, CPoint point)
{
	/*
	#define BAT_MENU_QUERY_STA			10003	//查询状态
	#define BAT_MENU_QUERY_SOC_VAL		10004	//查询SOC和电压
	#define BAT_MENU_QEURY_CURRENT		10005	//查询充电电流
	#define BAT_MENU_QUERY_TEMPRATURE	10006	//查询温度
	#define BAT_MENU_QUERY_SWITCH		10007	//查询开关
	*/
	CMenu mMenu, subMenu;
	mMenu.CreatePopupMenu();
	if(BAT_MODE_NET == mMode){

		subMenu.CreatePopupMenu();
		subMenu.AppendMenu(MF_STRING,BAT_MENU_MASK_QUERY,"查询");
		subMenu.AppendMenu(MF_STRING,BAT_MENU_MASK,"屏蔽");
		subMenu.AppendMenu(MF_STRING,BAT_MENU_UNMASK,"取消屏蔽");

		if(0 == mMask)	//
			subMenu.CheckMenuItem(BAT_MENU_MASK,MF_CHECKED);
		else if(1 == mMask)
			subMenu.CheckMenuItem(BAT_MENU_UNMASK,MF_CHECKED);
			
		mMenu.AppendMenu(MF_POPUP,(UINT_PTR)subMenu.operator HMENU(), "电池屏蔽");
		mMenu.AppendMenu(MF_SEPARATOR);
	}


	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_STA,"查询状态");
	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_SOC,"查询SOC");
	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_VAL,"查询电压");
	
 	if((S_CHARGING == mPSTA || S_DISCHARGING == mPSTA) && (1 ==  mSW || 0 == mMask))
 		mMenu.AppendMenu(MF_STRING,BAT_MENU_QEURY_CURRENT,"查询电流");
 	else 
 		mMenu.AppendMenu(MF_STRING|MF_GRAYED,BAT_MENU_QEURY_CURRENT,"查询电流");

	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_TEMPRATURE,"查询温度");
	mMenu.AppendMenu(MF_STRING,BAT_MENU_QUERY_SWITCH,"查询开关");


	CPoint pt;
	GetCursorPos(&pt);

	mMenu.TrackPopupMenu(TPM_RIGHTBUTTON, pt.x, pt.y,this);
	mMenu.DestroyMenu();
	mMenu.Detach();
	subMenu.Detach();

	CWnd::OnRButtonUp(nFlags, point);
}

BOOL CCBat::OnCommand(WPARAM wParam, LPARAM lParam)
{
	int menuID = LOWORD(wParam);
	if(menuID > 10000)
	{
		
		if(BAT_MENU_MASK == menuID)			//屏蔽
			mParent->SendMessage(MSG_BATTERY_MASK,(WPARAM)(mID-1),(LPARAM)1);
		else if(BAT_MENU_UNMASK == menuID)	//取消屏蔽
			mParent->SendMessage(MSG_BATTERY_MASK,(WPARAM)(mID-1),(LPARAM)0);

		else
			mParent->SendMessage(MSG_BATTERY_ACT,(WPARAM)(menuID),(LPARAM)(mID-1));

	}

	return CWnd::OnCommand(wParam, lParam);
}

BOOL CCBat::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
	return CWnd::OnEraseBkgnd(pDC);
}

void CCBat::OnMouseMove(UINT nFlags, CPoint point)
{
	if (_bMouseTrack )
	{
		bat_update_tips();
		TRACKMOUSEEVENT csTME;
		csTME.cbSize = sizeof(csTME);
		csTME.dwFlags = TME_LEAVE|TME_HOVER;
		csTME.hwndTrack = m_hWnd;	
		csTME.dwHoverTime = 10;
		::_TrackMouseEvent(&csTME); 
		_bMouseTrack=FALSE;	
	}

	CWnd::OnMouseMove(nFlags, point);
}

void CCBat::OnMouseLeave()
{
	_bMouseTrack=TRUE;
	CWnd::OnMouseLeave();
}


//更新提示
void CCBat::bat_update_tips()
{
	char mask[10] = {0};
	char ddd[100] = {0};
	
	CString tmp("");

	if(0 == mMask)
		sprintf_s(mask,sizeof(mask),"%s","否");
	else if(1 == mMask)
		sprintf_s(mask,sizeof(mask),"%s","是");
	else 
		sprintf_s(mask,sizeof(mask),"%s","未知");

	sprintf_s(ddd,sizeof(ddd),_T("ID:%-10d\n"
		"电压:%-5.2f V\n"
		"电流:%-5.2f A\n"
		"SOC:%3d %% \n"
		"开关:%-5s\n"
		"状态:%-5s\n"
		"温度:%.1f℃\n"
		"屏蔽:%s\n"/**/
		),this->mID ,(float)mVoltage/1000,(float)mCurrent/1000,mSoc/**/,mSW==1?"开":"关",bat_get_state(),
		mTmperature,mask);

	m_ctrlTT.UpdateTipText(ddd,this);
}
BOOL CCBat::PreTranslateMessage(MSG* pMsg)
{
	m_ctrlTT.RelayEvent(pMsg); 
	return CWnd::PreTranslateMessage(pMsg);
}

HBRUSH CCBat::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CWnd::OnCtlColor(pDC, pWnd, nCtlColor);
	return hbr;
}
