#include "../StdAfx.h"
#include "CBatBallEX.h"
#include "../include/comm.h"
#include <math.h>

#define BATEX_MAX  64

//sql test
int g_soc[64];

bat_info_t gBATEX[BATEX_MAX];


//构造函数
CCBatteryGIF::CCBatteryGIF()
{
	m_asit = NULL;
	for (int i=0; i<64; i++)
		g_soc[i] = batEx_rand(0,100);

}


//析构
CCBatteryGIF::~CCBatteryGIF()
{
}


int CCBatteryGIF::Create(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID)
{
	return 1;
}


/***
 * 获取随机数
 */
int CCBatteryGIF::batEx_rand(data_range_t drt)
{
	return batEx_rand(drt.mMin, drt.mMax);
}

int CCBatteryGIF::batEx_rand(int mMin, int mMax)
{
	//srand((uint)time(NULL));
	return rand()%(mMax - mMin+1) + mMin;
}


/***
 * 获取随机数
 */
char CCBatteryGIF::batEx_rand_point(POINT* p, data_range_t x, data_range_t y)
{
	if(!p)return 0;

	p->x = batEx_rand(x);
	p->y = batEx_rand(y);

	return 1;
}


//加载图片
void CCBatteryGIF::batEx_load(CImage* ima,char* png)
{
	if(!ima || !png)return ;
	if (ima->IsNull())
	{
		ima->Load(_T(png));
		if (ima->GetBPP() == 32) 
		{
			int i;
			int j;
			for (i=0; i<ima->GetWidth(); i++)
			{
				for (j=0; j<ima->GetHeight(); j++)
				{
					byte *pByte = (byte *)ima->GetPixelAddress(i, j);
					pByte[0] = pByte[0] * pByte[3] / 255;
					pByte[1] = pByte[1] * pByte[3] / 255;
					pByte[2] = pByte[2] * pByte[3] / 255;
				}
			} //for (i=0; i<ima->GetWidth(); i++)
		}//if (ima->GetBPP() == 32) 
	}
}


//初始化
void CCBatteryGIF::batEx_initlize(CDC* pDC)
{
	m_parentPDC =  pDC;
	CRect tmp;

	int sub_w = m_windowRect.Width()/4;

	batEx_load(&(m_image[0].ima_1),"res\\ball\\module_11.png");
	batEx_load(&(m_image[0].ima_2),"res\\ball\\module_12.png");
	batEx_load(&(m_image[0].ima_3),"res\\ball\\module_13.png");
	batEx_load(&(m_image[0].ima_4),"res\\ball\\module_14.png");

	batEx_load(&(m_image[1].ima_1),"res\\ball\\module_21.png");
	batEx_load(&(m_image[1].ima_2),"res\\ball\\module_22.png");
	batEx_load(&(m_image[1].ima_3),"res\\ball\\module_23.png");
	batEx_load(&(m_image[1].ima_4),"res\\ball\\module_24.png");

	batEx_load(&(m_image[2].ima_1),"res\\ball\\module_31.png");
	batEx_load(&(m_image[2].ima_2),"res\\ball\\module_32.png");
	batEx_load(&(m_image[2].ima_3),"res\\ball\\module_33.png");
	batEx_load(&(m_image[2].ima_4),"res\\ball\\module_34.png");


	batEx_load(&(m_image[3].ima_1),"res\\ball\\module_41.png");
	batEx_load(&(m_image[3].ima_2),"res\\ball\\module_42.png");
	batEx_load(&(m_image[3].ima_3),"res\\ball\\module_43.png");
	batEx_load(&(m_image[3].ima_4),"res\\ball\\module_44.png");
	batEx_load(&(m_backpng),"res\\ball\\back.png");


	for (int i=0; i< BATEX_MAX; i++)
	{
		gBATEX[i].ima_index = i/16;

		batEx_initlize_ball(i,tmp);
		gBATEX[i].speed = 3;
	}

}


// 初始化
void CCBatteryGIF::batEx_initlize_ball(int index, CRect rect)
{
	CRect area;

	data_range_t drt_x, drt_y;

	batEx_get_rect(index, area);

	gBATEX[index].m_x.mMax = area.right-PNG_X;
	gBATEX[index].m_x.mMin = area.left;

	drt_x.mMin = area.left;
	drt_x.mMax = area.right - PNG_X;

	drt_y.mMin = area.top;
	drt_y.mMax = area.bottom - PNG_Y;


	gBATEX[index].rect.left = batEx_rand(drt_x);
	gBATEX[index].rect.top = batEx_rand(drt_y);

	gBATEX[index].rect.right = gBATEX[index].rect.left+ PNG_X;
	gBATEX[index].rect.bottom = gBATEX[index].rect.top+ PNG_Y;

	gBATEX[index].direction = batEx_rand(0,360);

	
}


//初始化
void CCBatteryGIF::batEx_set_rect(CRect wr, CRect cr)
{
	m_windowRect = wr;
	m_clientRect = cr;
}


//
CDC* CCBatteryGIF::batEx_get_memDC(CRect rect, CDC * src)
{
	if(src)
		m_parentPDC = src;

	m_bufferMenDC.CreateCompatibleDC(NULL);
	m_bkbmp.CreateCompatibleBitmap(m_parentPDC,rect.Width(),rect.Height());
	m_bufferMenDC.SelectObject(&m_bkbmp);
	m_bufferMenDC.FillSolidRect(rect.left,rect.top,rect.Width(),rect.Height(),RGB(255,255,255));
	
	batEx_StickBk();		//背景
	batEx_StickBattery();	//单元
	return &m_bufferMenDC;
}

CDC* CCBatteryGIF::batEx_get_memDC(CDC* memDC, CRect rect, CDC* src)
{
	if(!memDC) return NULL;

	m_clientRect = rect;

	batEx_StickBk(memDC);		//背景
	batEx_StickBattery(memDC);	//单元

	return memDC;
}


//贴背景
void CCBatteryGIF::batEx_StickBk(CDC* memDC)
{
	CDC* tmpDC = NULL;

	if(memDC)
		tmpDC = memDC;
	else
		tmpDC = &m_bufferMenDC;

	m_backpng.TransparentBlt(tmpDC->m_hDC,m_windowRect,RGB(0,0,0));
}


//贴单元
void CCBatteryGIF::batEx_StickBattery(CDC* memDC)
{
	int i= 0;
	CDC* tmpDC = NULL;

	if(memDC)
		tmpDC = memDC;
	else
		tmpDC = &m_bufferMenDC;

	//先计算位置
	for (i=0; i<BATEX_MAX; i++)
	{
		batEx_GetDestination(i);
	}

	COLORREF old = memDC->SetTextColor(RGB(238,244,78));
	
	for (i=0; i<BATEX_MAX; i++)
	{
		batEx_get_image(i,tmpDC);
	}
}


//获取SOC
unsigned int CCBatteryGIF::batEx_get_soc(int index)
{
	if(!m_asit)return NULL;

	int mid = index/16;
	int tmp = index%16;

	int uid = tmp/4;
	int bid = tmp%4;

	return  m_asit->sitEX.mod[mid].bat_unit[uid].battery[bid].vst.soc;
}


//
CImage* CCBatteryGIF::batEx_get_image(int index, CDC* memDC)
{
	CImage* ima = NULL;
	if(index <0 || index >64)return NULL;

	//sql 测试
	unsigned int soc = batEx_get_soc(index);


	if(soc <= 25)
		ima = &(m_image[gBATEX[index].ima_index].ima_1);

	else if (soc> 25 && soc <= 50)
		ima = &(m_image[gBATEX[index].ima_index].ima_2);

	else if (soc> 50 && soc <= 75)
		ima = &(m_image[gBATEX[index].ima_index].ima_3);

	else 
		ima = &(m_image[gBATEX[index].ima_index].ima_4);

	ima->TransparentBlt(memDC->m_hDC,gBATEX[index].rect,RGB(0,0,0));

	return NULL;

}


//计算位置
char CCBatteryGIF::batEx_GetDestination(int i)
{
	int dstX = 0, dstY = 0, dstT = 0, dstB = 0;
	char res = 0;
	CRect dstRect, srcRect;

	srcRect.SetRect(gBATEX[i].rect.left,gBATEX[i].rect.top,gBATEX[i].rect.right,gBATEX[i].rect.bottom);

	if(i<0 || i >= BATEX_MAX) return res;

	//向左移动
	if(0 == gBATEX[i].direction)
	{
		dstX = gBATEX[i].rect.left + gBATEX[i].speed;
		dstY = gBATEX[i].rect.right + gBATEX[i].speed;

		batEx_setRect(i,dstX,srcRect.top,dstY,srcRect.bottom);

	}

	//向右移动
	else if (180 == gBATEX[i].direction)
	{
		dstX = gBATEX[i].rect.left - gBATEX[i].speed;
		dstY = gBATEX[i].rect.right - gBATEX[i].speed;

		batEx_setRect(i,dstX,srcRect.top,	dstY,srcRect.bottom);
	}

	//向上移动
	else if(90 == gBATEX[i].direction)
	{
		dstT = srcRect.top - gBATEX[i].speed;
		dstB = srcRect.bottom - gBATEX[i].speed;

		batEx_setRect(i,srcRect.left, dstT,srcRect.right,dstB);
	}
	
	//向下移动
	else if (270 == gBATEX[i].direction)
	{
		dstT = srcRect.top + gBATEX[i].speed;
		dstB = srcRect.bottom + gBATEX[i].speed;

		batEx_setRect(i,srcRect.left, dstT,srcRect.right,dstB);
	}

	
	else 
	{
		int tmpX = 0, tmpY =0;
	
		//第一象限
		if ((gBATEX[i].direction > 0) && (gBATEX[i].direction < 90))
		{
			tmpX = (int)gBATEX[i].speed * fabs(cos((double)gBATEX[i].direction));
			tmpY = (int)gBATEX[i].speed * fabs(sin((double)gBATEX[i].direction));
			
			dstX = srcRect.left + tmpX;
			dstY  = srcRect.top - tmpY; 
		}
		//第二象限
		else if ((gBATEX[i].direction > 90) && (gBATEX[i].direction < 180))
		{
			tmpX = (int)gBATEX[i].speed * fabs(cos((double)(180 - gBATEX[i].direction)));
			tmpY = (int)gBATEX[i].speed * fabs(sin((double)(180 - gBATEX[i].direction)));

			dstX = srcRect.left - tmpX;
			dstY  = srcRect.top - tmpY; 
		}
		
		else if ((gBATEX[i].direction > 180) && (gBATEX[i].direction < 270))
		{
			tmpX = (int)gBATEX[i].speed * fabs(cos((double)(gBATEX[i].direction - 180)));
			tmpY = (int)gBATEX[i].speed * fabs(sin((double)(gBATEX[i].direction - 180)));

			dstX = srcRect.left - tmpX;
			dstY  = srcRect.top + tmpY; 
		}

		else
		{
			tmpX = (int)gBATEX[i].speed * fabs(cos((double)(360 - gBATEX[i].direction)));
			tmpY = (int)gBATEX[i].speed *fabs( sin((double)(360 - gBATEX[i].direction)));

			dstX = srcRect.left + tmpX;
			dstY  = srcRect.top + tmpY; 
		}

		batEx_setRect(i,dstX, dstY,dstX+PNG_X, dstY + PNG_Y);
	}

	return 1;
}


//
void CCBatteryGIF::batEx_setRect(CRect* rect, int l, int t, int r, int b)
{
	if(!rect) return ;
	rect->left = l;
	rect->top  = t;
	rect->right = r;
	rect->bottom  = b;
}


//
int CCBatteryGIF::batEx_get_rect(int i, CRect& rect)
{
	
	unsigned int soc = batEx_get_soc(i);

	int sub_rank = (m_clientRect.Height() - PNG_Y) * soc/100;

	int sub_w = (m_clientRect.Width())/4;
	int index = i/16;
	rect.left = m_clientRect.left+ index*sub_w;
	rect.right = m_clientRect.right - (3-index)*sub_w;

	rect.top = m_clientRect.Height() - PNG_Y - sub_rank - MSPACE;
	rect.bottom = rect.top + PNG_Y + MSPACE;

	if(rect.top < m_clientRect.top)
		rect.top = m_clientRect.top;

	if (rect.bottom > m_clientRect.bottom)
		rect.bottom = m_clientRect.bottom;

	return 1;

}


//
void CCBatteryGIF::batEx_setRect(int i, int l, int t, int r, int b)
{
	CRect tmp;

	if(i<0 || i >= BATEX_MAX) return ;

	//sql test根据不同的soc获取不同的空间 
	batEx_get_rect(i,tmp);


	//左边出界
	if(l <= tmp.left)
	{
		batEx_setRect(&(gBATEX[i].rect),tmp.left,t,tmp.left + PNG_X, b);
		gBATEX[i].direction = batEx_rand(0,360);
	}

	//上边出界
	else if(t <= tmp.top)
	{
		batEx_setRect(&(gBATEX[i].rect),l,tmp.top,r, tmp.top + PNG_Y);
		gBATEX[i].direction = batEx_rand(0,360);
	}

	//右边出界
	else if (r >= tmp.right)
	{
		batEx_setRect(&(gBATEX[i].rect),tmp.right - PNG_X, t,tmp.right, b);
		gBATEX[i].direction = batEx_rand(0,360);
	}

	else if (b >= tmp.bottom)
	{
		batEx_setRect(&(gBATEX[i].rect),l, tmp.bottom - PNG_X, r, tmp.bottom);
		gBATEX[i].direction = batEx_rand(0,360);
	}
	else 
		batEx_setRect(&(gBATEX[i].rect),l, t, r, b);
}