#pragma once
#include "afxwin.h"
#include <atlimage.h>

// 
class CBatcolor
{

private:
	char m_pen_flag; 
	char m_brush_flag;

	CPen* m_old_pen;
	CBrush * m_old_brush;

	int m_pen_with;
	CDC* m_pDC;

public:
	CPen m_pen;
	CBrush m_brush;

	void update(COLORREF pen_color, COLORREF brush_color, int pen_with =  1)
	{
		if(0 != pen_color)
		{
			if(m_pen_flag){
				m_pen.DeleteObject();
				m_pen.Detach();
			}
				
			m_pen_flag = 1;
			m_pen.CreatePen(PS_SOLID,1,pen_color);
			m_old_pen = (CPen*)m_pDC->SelectObject(&m_pen);

		}

		if(0 != brush_color)
		{
			if(m_brush_flag)
			{
				m_brush.DeleteObject();
				m_brush.Detach();
			}
			m_brush_flag = 1;
			m_brush.CreateSolidBrush(brush_color);
			m_old_brush = (CBrush *)m_pDC->SelectObject(&m_brush);
		}
	}


	void mRelease()
	{
		if(m_pDC && m_pDC->m_hDC)
		{
			if(1 == m_pen_flag && m_old_pen )
				m_pDC->SelectObject(m_old_pen);

			if(1 == m_brush_flag && m_old_brush)
				m_pDC->SelectObject(m_old_brush);

			m_pDC = NULL;
		}

		m_brush.DeleteObject();
		m_pen.DeleteObject();

	}

public:
	CBatcolor(CDC* pDC, COLORREF pen_color, COLORREF brush_color, int pen_with =  1)
	{
		m_pen_flag = 0;
		m_brush_flag = 0;
		m_pen_with = pen_with;
		m_pDC = pDC;

		if(0 != pen_color)
		{
			m_pen_flag = 1;
			m_pen.CreatePen(PS_SOLID,1,pen_color);
			m_old_pen = (CPen*)pDC->SelectObject(&m_pen);
		}

		if(0 != brush_color)
		{
			m_brush_flag = 1;
			m_brush.CreateSolidBrush(brush_color);
			m_old_brush = (CBrush *)pDC->SelectObject(&m_brush);
		}
	}
	~CBatcolor(void)
	{
		mRelease();
	}

};


//////////////////////////////////////////////////////////////////////////

#define CTH_COLOR_BACKGROUND	RGB(135,135,135)	//背景颜色
#define CTH_COLOR_BK_DEBUG_0	CTH_COLOR_BACKGROUND
#define CTH_COLOR_BK_DEBUG_1	RGB(117,115,200)	
#define CTH_COLOR_INFO			RGB(221,221,221)

class CCTopHead : public CWnd
{
public:
	CCTopHead(void);
	~CCTopHead(void);

private:

	unsigned char m_mac_id;		//机架号
	unsigned char m_ups_id;		//brick号

	char m_status;		//当前状态
	char m_bms_mode;	//当前模式 

	int m_current;		//电流
	int m_voltage;		//电压

	char m_debug;		//消息模式

	char m_create_flag;

	char m_min_dis_nuit;	//最小放电单元数


	/***
	 * @brief: 获取系统状态
	 */
	void cth_get_system_status(CString& sta);

public:

	/***
	 * @brief: 设置序号
	 * @param mac_id:
	 * @param ups_id:
	 */
	void cth_set_id(unsigned char mac_id, unsigned char ups_id) {m_mac_id = mac_id; m_ups_id = ups_id;};


	/***
	 * @brief: 设置状态
	 * @param status:
	 */
	void cth_set_status(char status) {m_status = status;};


	/***
	 * @brief: 设置工作模式
	 * @param bms_mode:
	 */
	void cth_set_bms_mode(char bms_mode) {m_bms_mode = bms_mode;};


	/***
	 * @brief: 设置电流、电压
	 * @param current:	电流
	 * @param voltage:	电压
	 */
	void cth_set_params(int current = -1, int voltage = -1){

		if(current > -1) m_current = current;
		if(voltage > -1) m_voltage = voltage;
	};


	/***
	 * @brief: 设置消息模式
	 * @param debug:
	 */
	void cth_set_debug(char debug) {m_debug = debug;};


	/***
	 * @brief: 设置最小放电单元数
	 */
	void cth_set_min_dis(char min_dis) {m_min_dis_nuit = min_dis;};


	/***
	 * @brief: 刷新
	 */
	void cth_fresh() { if(IsWindow(m_hWnd)) Invalidate(TRUE);};


	/***
	 * @brief: 创建
	 */
	int mCreate(DWORD dwStyle, const RECT &rect, CWnd *ParentWnd, UINT nID);


	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};