#pragma once
#include "afxext.h"

class CMySplitterWnd :
	public CSplitterWnd
{
public:
	CMySplitterWnd(void);
	~CMySplitterWnd(void);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

protected:
	char m_own_draw;

public:

	/***
	 * �Ƿ������ƶ�
	 */
	void csw_set_draw(char draw){m_own_draw = draw;};

	/***
	 *
	 */
	char csw_get_draw(){return m_own_draw;};
};
