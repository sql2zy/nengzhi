// CControlPanel.cpp : 实现文件
//

#include "stdafx.h"
#include "resource.h"
#include "CControlPanel.h"
#include "CMainLog.h"
#include "ATS_VIWER2View.h"
#include "cla\CCObject.h"
#include "CBatteryEnable.h"
#include "ChargingSet.h"
#include "CBatSW_Set.h"
#include "include/batteryEX.h"
#include "CMinDisUnitDlg.h"


extern CCMainLog *g_main_log;		//日志
extern CATS_VIWER2View* theVIEWER;	//主要显示界面
extern CBatObject* g_obj;
CCControlPanel * theCtrlPanel = NULL;

/***
 * 所有的模块信息
 */
#define MODULE_MAX 4
typedef struct __module_info_t
{
	CImage img_select;		//模块使能选择按钮
	CImage img_unselect;	//模块使能未选择按钮

	char curr_id;			//当前按钮序号
	CRect rect[MODULE_MAX];	//四个图片的位置 
}moduel_info_t;

moduel_info_t g_modules;

char g_enable[MODULE_MAX] = {0};
// CCControlPanel

IMPLEMENT_DYNCREATE(CCControlPanel, CFormView)

CBrush g_ctrl_brush;

CCControlPanel::CCControlPanel()
	: CFormView(CCControlPanel::IDD)
{
	theCtrlPanel = this;

	//初始化使能控制
	for (int i=0; i<MODULE_MAX; i++)
	{
		g_enable[i] = 0;
	}

	img_flag = 0;

	g_ctrl_brush.CreateSolidBrush(RGB(223,223,223));
}

CCControlPanel::~CCControlPanel()
{
}

void CCControlPanel::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_MODE_1, m_btn_module_1);
	DDX_Control(pDX, IDC_BTN_MODE_2, m_btn_module_2);
	DDX_Control(pDX, IDC_BTN_MODE_3, m_btn_module_3);
	DDX_Control(pDX, IDC_BTN_MODE_4, m_btn_module_4);
	DDX_Control(pDX, IDC_BTN_ENABLE, m_btn_enable);
	DDX_Control(pDX, IDC_BTN_ENABLE_QUERY, m_btn_enable_query);
	DDX_Control(pDX, IDC_BTN_ENABLE_SET, m_btn_enable_set);
	DDX_Control(pDX, IDC_BTN_PARAM, m_btn_params);
	DDX_Control(pDX, IDC_BTN_PARAM_QUERY, m_btn_params_query);
	DDX_Control(pDX, IDC_BTN_PARAM_SET, m_btn_params_set);
	DDX_Control(pDX, IDC_BTN_SWITCH, m_btn_switch);
	DDX_Control(pDX, IDC_BTN_SWITCH_QUERY, m_btn_switch_query);
	DDX_Control(pDX, IDC_BTN_SWITCH_SET, m_btn_switch_set);
	DDX_Control(pDX, IDC_BTN_CHARGING, m_btn_charging);
	DDX_Control(pDX, IDC_BTN_CHARGING_START, m_btn_charging_start);
	DDX_Control(pDX, IDC_BTN_CHARGING_STOP, m_btn_charging_stop);
	DDX_Control(pDX, IDC_BTN_DISCHARGING, m_btn_discharging);
	DDX_Control(pDX, IDC_BTN_DISCHARGING_START, m_btn_discharging_start);
	DDX_Control(pDX, IDC_BTN_DISCHARGING_STOP, m_btn_discharging_stop);
	DDX_Control(pDX, IDC_BTN_LOG, m_btn_log);
	DDX_Control(pDX, IDC_BTN_LOG_SAVE, m_btn_log_save);
	DDX_Control(pDX, IDC_BTN_LOG_CLEAR, m_btn_log_clear);
	DDX_Control(pDX, IDC_BTN_MIN_DIS_UNIT, m_btn_min_dis_unit);
	DDX_Control(pDX, IDC_BTN_MIN_DIS_QUERY, m_btn_min_dis_query);
	DDX_Control(pDX, IDC_BTN_MIN_DIS_SET, m_btn_min_dis_set);
	DDX_Control(pDX, IDC_BTN_UPS_CTRL, m_btn_ups_ctrl);
	DDX_Control(pDX, IDC_BTN_UPS_CTRL_START, m_btn_ups_ctrl_start);
	DDX_Control(pDX, IDC_BTN_UPS_CTRL_STOP, m_btn_ups_ctrl_stop);
}

BEGIN_MESSAGE_MAP(CCControlPanel, CFormView)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_CONTROL_RANGE(BN_CLICKED,IDC_BTN_MODE_1,IDC_BTN_MODE_4,OnModuleClick)
	ON_CONTROL_RANGE(BN_CLICKED,IDC_BTN_ENABLE,IDC_BTN_UPS_CTRL_STOP,OnCtrlClick)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CCControlPanel 诊断

#ifdef _DEBUG
void CCControlPanel::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CCControlPanel::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CCControlPanel 消息处理程序
void CCControlPanel::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	g_modules.curr_id = 0;

	//模块0
	m_btn_module_1.LoadStdImage(IDB_PNG_UNSELECTED_1,"PNG");
	m_btn_module_1.LoadAltImage(IDB_PNG_SELECTED_1,"PNG");
	m_btn_module_1.SetToolTipText("切换至模块-1");


	//模块1
	m_btn_module_2.LoadStdImage(IDB_PNG_UNSELECTED_2,"PNG");
	m_btn_module_2.LoadAltImage(IDB_PNG_SELECTED_2,"PNG");
	m_btn_module_2.SetToolTipText("切换至模块-2");


	//模块2
	m_btn_module_3.LoadStdImage(IDB_PNG_UNSELECTED_3,"PNG");
	m_btn_module_3.LoadAltImage(IDB_PNG_SELECTED_3,"PNG");
	m_btn_module_3.SetToolTipText("切换至模块-3");


	//模块3
	m_btn_module_4.LoadStdImage(IDB_PNG_UNSELECTED_4,"PNG");
	m_btn_module_4.LoadAltImage(IDB_PNG_SELECTED_4,"PNG");
	m_btn_module_4.SetToolTipText("切换至模块-4");

	img_flag = 1;

	//加载图片
	Load_Image(&g_modules.img_select,"res/select.png");
	Load_Image(&g_modules.img_unselect,"res/unselect.png");

	//各个按钮
	
	//使能
	m_btn_enable.LoadStdImage(IDB_PNG_NEW_ENABLE,"PNG");	m_btn_enable.EnableButton(FALSE);
	m_btn_enable.SetToolTipText("选择模块是否参与充、放电");
	m_btn_enable_query.LoadStdImage(IDB_PNG_NEW_QUERY,"PNG");
	m_btn_enable_query.SetToolTipText("使能查询");
	m_btn_enable_set.LoadStdImage(IDB_PNG_NEW_SET,"PNG");
	m_btn_enable_set.SetToolTipText("使能控制");

	//电流
	m_btn_params.LoadStdImage(IDB_PNG_NEW_PARAMS,"PNG");		m_btn_params.EnableButton(FALSE);
	m_btn_params.SetToolTipText("充、放电时，电流、电压的配置");
	m_btn_params_query.LoadStdImage(IDB_PNG_NEW_QUERY,"PNG");
	m_btn_params_query.SetToolTipText("电流、电压查询");
	m_btn_params_set.LoadStdImage(IDB_PNG_NEW_SET,"PNG");
	m_btn_params_set.SetToolTipText("电流、电压设置");

	//开关
	m_btn_switch.LoadStdImage(IDB_PNG_SWITCH,"PNG");	m_btn_switch.EnableButton(FALSE);
	m_btn_switch.SetToolTipText("开关控制");
	m_btn_switch_query.LoadStdImage(IDB_PNG_QUERY,"PNG");
	m_btn_switch_query.SetToolTipText("电池开关查询");
	m_btn_switch_set.LoadStdImage(IDB_PNG_SET,"PNG");
	m_btn_switch_set.SetToolTipText("电池开关设置");

	//充电
	m_btn_charging.LoadStdImage(IDB_PNG_CHARGING,"PNG");	m_btn_charging.EnableButton(FALSE);
	m_btn_charging.SetToolTipText("充电控制");
	m_btn_charging_start.LoadStdImage(IDB_PNG_START,"PNG");
	m_btn_charging_start.SetToolTipText("开始充电");
	m_btn_charging_stop.LoadStdImage(IDB_PNG_STOP,"PNG");
	m_btn_charging_stop.SetToolTipText("结束充电");

	//放电
	m_btn_discharging.LoadStdImage(IDB_PNG_DISCHARGING,"PNG");	m_btn_discharging.EnableButton(FALSE);
	m_btn_discharging.SetToolTipText("放电控制");
	m_btn_discharging_start.LoadStdImage(IDB_PNG_START,"PNG");
	m_btn_discharging_start.SetToolTipText("开始放电");
	m_btn_discharging_stop.LoadStdImage(IDB_PNG_STOP,"PNG");
	m_btn_discharging_stop.SetToolTipText("结束放电");

	//日志
	m_btn_log.LoadStdImage(IDB_PNG_LOG,"PNG");
	m_btn_log.EnableButton(FALSE);
	m_btn_log_save.LoadStdImage(IDB_PNG_LOG_SAVE,"PNG");
	m_btn_log_save.SetToolTipText("保存日志");
	m_btn_log_clear.LoadStdImage(IDB_PNG_LOG_CLEAR,"PNG");
	m_btn_log_clear.SetToolTipText("删除日志");


	//最小放电单元数
	m_btn_min_dis_unit.LoadStdImage(IDB_PNG_NEW_MIN_DIS_UNIT,"PNG");
	m_btn_min_dis_unit.EnableButton(FALSE);
	m_btn_min_dis_unit.SetToolTipText("设置最小放电单元数,仅对主机有效");
	m_btn_min_dis_set.LoadStdImage(IDB_PNG_NEW_SET,"PNG");
	m_btn_min_dis_set.SetToolTipText("设置最小放电单元数");
	m_btn_min_dis_query.LoadStdImage(IDB_PNG_NEW_QUERY,"PNG");
	m_btn_min_dis_query.SetToolTipText("查询最小放电单元数");

	//UPS系统控制
	m_btn_ups_ctrl.LoadStdImage(IDB_PNG_UPS_CTRL,"PNG");
	m_btn_ups_ctrl.EnableButton(FALSE);
	m_btn_ups_ctrl.SetToolTipText("控制储能单元运行或停机");
	m_btn_ups_ctrl_start.LoadStdImage(IDB_PNG_UPS_CTRL_START,"PNG");
	m_btn_ups_ctrl_start.SetToolTipText("启动储能单元");
	m_btn_ups_ctrl_stop.LoadStdImage(IDB_PNG_UPS_CTRL_STOP,"PNG");
	m_btn_ups_ctrl_stop.SetToolTipText("关闭储能单元");

}


void CCControlPanel::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);
	CRect rect;

	if (IsWindow(m_hWnd) && IsWindow(m_btn_module_1.m_hWnd))
	{
		GetClientRect(rect);
		int lef =0, top = 0;

		lef = rect.left+5;
		
		top = rect.top + 5+5;

		//四个模块按钮
		m_btn_module_1.MoveWindow(lef,top,80,34);
		g_modules.rect[0].SetRect(lef+80,top,lef+80+34,top+34);

		m_btn_module_2.MoveWindow(lef,top + 34 + 5,80,34);
		g_modules.rect[1].SetRect(lef+80,top + 34 + 5,
			lef+80+34,top+34 + 34 + 5);

		m_btn_module_3.MoveWindow(lef,top + 34 + 5 + 34 + 5,80,34);
		g_modules.rect[2].SetRect(lef+80,top + 34 + 5 + 34 + 5,
			lef+80+34,top+34 + 34 + 5 + 34 + 5);

		m_btn_module_4.MoveWindow(lef,top + 34 + 5 + 34 + 5 + 34 + 5,80,34);
		g_modules.rect[3].SetRect(lef+80,top + 34 + 5 + 34 + 5 + 34 + 5,
			lef+80+34,top+34 + 34 + 5 + 34 + 5 + 34 + 5);

		ctrl_update_btn_pos();
	}
}


//用户点击模块切换按钮
void CCControlPanel::OnModuleClick(UINT nID)
{
	if(g_modules.curr_id == (nID - IDC_BTN_MODE_1))return ;

	g_modules.curr_id = nID - IDC_BTN_MODE_1;
	//更新模块按钮
	ctrl_update_select();
	
	//通知主界面
	theVIEWER->v_update_moduel_selected(g_modules.curr_id);
}


//用户点击控制按钮
void CCControlPanel::OnCtrlClick(UINT nID)
{
	CCBatteryEnable cbe;
	CCMinDisUnitDlg minDlg;
	char tmp = 0;
	uchar ddd = 0;
	uchar str[4] = {0},data[4]={0};

	CChargingSet dlg;
	range_value_t rvt_vur,rvt_vol;

	WORD wd = 0;
	CCBatSW_Set cbs;

	switch(nID)
	{
	case IDC_BTN_ENABLE_QUERY:	//查询使能
		{
			g_obj->ATS_bat_mod_enable(NULL,NZ_ACT_READ,g_modules.curr_id);
		}
		break;
	case IDC_BTN_ENABLE_SET:	//设置使能
		{
			if(1 == g_obj->m_asit.sit.mod[0].join_discha)
				tmp = tmp|1;
			if(1 == g_obj->m_asit.sit.mod[1].join_discha)
				tmp = tmp|2;
			if(1 == g_obj->m_asit.sit.mod[2].join_discha)
				tmp = tmp|4;
			if(1 == g_obj->m_asit.sit.mod[3].join_discha)
				tmp = tmp|8;
			cbe.cbe_set_enable(tmp);
			cbe.cbe_set_text("选通控制","参与充放电");

			if(IDOK == cbe.DoModal())
			{
				uchar tt = 0;
				tt = cbe.cbe_get_eanble();
				tt = tt&0x0f;

				g_obj->ATS_bat_mod_enable(NULL,NZ_ACT_WRITE,tt,NULL,WPARAM_BATTER_ENABLE);

			}
		}
		break;
	case IDC_BTN_PARAM_QUERY:	//电流查询
		{
			
			str[0] = g_modules.curr_id;
			g_obj->ATS_charging_current(NULL,NZ_ACT_READ,str);
		}
		break;
	case IDC_BTN_PARAM_SET:		//电流设置
		{

			if(BMS_MODE_NET_GFKD == g_obj->m_asit.sitEX.sysEX_bms_mode)
			{
				memset(&rvt_vur,0,sizeof(rvt_vur));
				g_obj->obj_calculation_current(&rvt_vur,g_modules.curr_id);

				memset(&rvt_vol,0,sizeof(rvt_vol));
				if(0 == g_obj->obj_calculation_voltage(&rvt_vol,g_modules.curr_id)) return ;

				if( BMS_MODE_NET_GFKD == g_obj->m_asit.sitEX.sysEX_bms_mode &&(0 == rvt_vol.mMax || 0 == rvt_vur.mMax))
				{
					MessageBox("请先配置电池开关","注意",MB_OK);
					return;
				}

				for (int i=0; i<4; i++){

					if(1 == g_obj->m_asit.sit.mod[i].join_discha)
						tmp = tmp|(1<<i);
				}

				dlg.cs_set_mode(g_obj->m_asit.sitEX.sysEX_bms_mode);
				dlg.cs_set_params(g_obj->m_asit.sit.mod[g_modules.curr_id].attr.current.charge,
					g_obj->m_asit.sit.mod[g_modules.curr_id].attr.voltage.charge);
				dlg.cs_set_mask(tmp);
				dlg.cs_set_range(rvt_vol.mMax,rvt_vol.mMin,rvt_vur.mMax,rvt_vur.mMin);
				uint res = dlg.DoModal();
				if(IDOK != res)	return ;

				data[0] = ((dlg.mVoltage) & 0xff00)>>8;	data[1] = (dlg.mVoltage) & 0x00ff;
				data[2] = ((dlg.mCurrent) & 0xff00)>>8;	data[3] = (dlg.mCurrent) & 0x00ff;

				g_obj->ATS_bat_mod_select(NULL,NZ_ACT_WRITE,1<<g_modules.curr_id,0,
					NET_CALLBACK_SET_CHARGING,(uint)g_modules.curr_id,(char*)data);
			}
			else
			{
				char tmp =0;
				uchar data[4]={0};
				if(g_obj)
					dlg.cs_set_params(g_obj->m_asit.sitEX.sysEX_charge_current,g_obj->m_asit.sitEX.sysEX_charge_voltage);

				uint res = dlg.DoModal();
				if (res = IDOK)
				{
					data[0] = ((dlg.mVoltage) & 0xff00)>>8;	data[1] = (dlg.mVoltage) & 0x00ff;
					data[2] = ((dlg.mCurrent) & 0xff00)>>8;	data[3] = (dlg.mCurrent) & 0x00ff;

					g_obj->ATS_charging_current(NULL,NZ_ACT_WRITE,data,NULL,UPS_CALLBACK_SET_CHARGING);
				}
			}
			
		}	
		break;
	case IDC_BTN_SWITCH_QUERY:	//开关查询
		{
			g_obj->ATS_battery_mask(NULL,NZ_ACT_READ,g_modules.curr_id,NULL);
		}
		break;
	case IDC_BTN_SWITCH_SET:	//开关设置
		{
			for (int i=0; i<4; i++)
				for (int j=0; j<4; j++)
					if(0 == g_obj->m_asit.sit.mod[g_modules.curr_id].bat_unit[i].battery[j].attr.isMask)
					{
						wd = wd | (1<<(i*4+j));
					}


			cbs.cbs_sw_state_set(wd);
			if(IDOK == cbs.DoModal())
			{
				uchar da[4] = {0};
				wd = cbs.cbs_sw_state_get();
				da[2] = (wd & 0xff00)>>8; da[3] = wd & 0x00ff;

				g_obj->ATS_battery_mask(NULL,NZ_ACT_WRITE,g_modules.curr_id,da,0,
					WPARAM_BAT_MASK_EX,(uint)wd);

			}
		}
		break;
	case IDC_BTN_CHARGING_START:	//开始充电
		{
			ctrl_dis_charging(CTRL_START_CHARGING);
		}
		break;
	case IDC_BTN_CHARGING_STOP:		//结束充电
		{
			ctrl_dis_charging(CTRL_STOP_CHARGING);
		}
		break;
	case IDC_BTN_DISCHARGING_START:	//开始充电
		{
			ctrl_dis_charging(CTRL_START_DISCHARGING);
		}
		break;
	case IDC_BTN_DISCHARGING_STOP:		//结束充电
		{
			ctrl_dis_charging(CTRL_STOP_DISCHARGING);
		}
		break;

	case IDC_BTN_LOG_CLEAR:	//清除日志
		{
			g_main_log->clear_log();
		}
		break;
	case IDC_BTN_LOG_SAVE:	//保存日志
		{
			g_main_log->save_log();
		}
		break;
	case IDC_BTN_UPS_CTRL_START:	//UPS启动
		{
			if(IDOK== ::MessageBox(NULL,"确定启动该储能单元?","提示",MB_OKCANCEL|MB_ICONQUESTION))
				g_obj->ATS_system_control_GFKD(NULL,0);
		}
		break;	
	case  IDC_BTN_UPS_CTRL_STOP:	//UPS关闭
		{
			if(IDOK== ::MessageBox(NULL,"确定关闭该储能单元?","提示",MB_OKCANCEL|MB_ICONQUESTION))
				g_obj->ATS_system_control_GFKD(NULL,1);
		}
		break;
	case IDC_BTN_MIN_DIS_QUERY:
		{
			g_obj->ATS_min_discharge_unit(NULL,NULL,NZ_ACT_READ,0);
		}
		break;
	case IDC_BTN_MIN_DIS_SET:
		{
			if(IDOK == minDlg.DoModal())
			{
				g_obj->ATS_min_discharge_unit(NULL,NULL,NZ_ACT_WRITE,minDlg.m_min_dis);
			}
		}
		break;
	}
}


//控制充、放电
void CCControlPanel::ctrl_dis_charging(charging_ctrl_e ctrl)
{
	unsigned char str = 0;
	//NET
	if(BMS_MODE_NET_GFKD == g_obj->m_asit.sitEX.sysEX_bms_mode){

		switch(ctrl)
		{
		case CTRL_START_CHARGING:
			{
				if(0 == g_obj->obj_check_paralleling_diff(g_modules.curr_id))return ;
				
				if(IDOK== ::MessageBox(NULL,"确定开始充电?","提示",MB_OKCANCEL|MB_ICONQUESTION))
					g_obj->ATS_system_control_GFKD(NULL,3,NULL,WPARAM_NET_SYSTEM_CONTROL,3);
			}
			
			break;

		case CTRL_START_DISCHARGING:
			
			if(IDOK== ::MessageBox(NULL,"确定开始放电?","提示",MB_OKCANCEL|MB_ICONQUESTION))
				g_obj->ATS_system_control_GFKD(NULL,1,NULL,WPARAM_NET_SYSTEM_CONTROL,1,"0");

			break;

		case CTRL_STOP_CHARGING:
			
			if(IDOK== ::MessageBox(NULL,"确定停止充电?","提示",MB_OKCANCEL|MB_ICONQUESTION))
				g_obj->ATS_system_control_GFKD(NULL,4,NULL,WPARAM_NET_SYSTEM_CONTROL,4);
			break;

		case CTRL_STOP_DISCHARGING:

			if(IDOK== ::MessageBox(NULL,"确定停止放电?","提示",MB_OKCANCEL|MB_ICONQUESTION))
				g_obj->ATS_system_control_GFKD(NULL,2,NULL,WPARAM_NET_SYSTEM_CONTROL,2);
			break;
		}
	}
}


//加载png图片
void CCControlPanel::Load_Image(CImage* ima,char* png)
{
	if(!ima || !png)return ;
	if (ima->IsNull())
	{
		ima->Load(_T(png));
		if(ima->IsNull()) return;
		if (ima->GetBPP() == 32) //确认该图像包含Alpha通道
		{
			int i; 
			int j;
			for (i=0; i<ima->GetWidth(); i++)
			{
				for (j=0; j<ima->GetHeight(); j++)
				{
					byte *pByte = (byte *)ima->GetPixelAddress(i, j);
					pByte[0] = pByte[0] * pByte[3] / 255;
					pByte[1] = pByte[1] * pByte[3] / 255;
					pByte[2] = pByte[2] * pByte[3] / 255;
				}
			}
		}
	}
}


//绘制模块选择状态
void CCControlPanel::ShowImage()
{
	
}


//重绘
void CCControlPanel::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	ctrl_update_select();
}


//显示图片
void CCControlPanel::ctrl_update_select()
{
	if (IsWindow(m_hWnd) && IsWindow(m_btn_module_1.m_hWnd))
	{
		CDC* pDC = GetDC();
		int i = 0;

		for (i=0;i<MODULE_MAX; i++)
		{
			if(i == g_modules.curr_id)
				g_modules.img_select.TransparentBlt(pDC->m_hDC,g_modules.rect[i],RGB(0,0,0));

			else	
				g_modules.img_unselect.TransparentBlt(pDC->m_hDC,g_modules.rect[i],RGB(0,0,0));
		}
	}
}


//设置使能
void CCControlPanel::ctl_set_enable(char id, char enable, bool update)
{
	if(id<MODULE_MAX)
		g_enable[id] = enable;

	if(update) 	ctrl_update_enable();
}


//更新使能图片
void CCControlPanel::ctrl_update_enable()
{
	if(IsWindow(m_hWnd) && img_flag)
	{
		if(g_enable[0] > 0)
			m_btn_module_1.SetImage(CGdipButton::ALT_TYPE);
		else
			m_btn_module_1.SetImage(CGdipButton::STD_TYPE);

		if(g_enable[1] > 0)
			m_btn_module_2.SetImage(CGdipButton::ALT_TYPE);
		else
			m_btn_module_2.SetImage(CGdipButton::STD_TYPE);

		if(g_enable[2] > 0)
			m_btn_module_3.SetImage(CGdipButton::ALT_TYPE);
		else
			m_btn_module_3.SetImage(CGdipButton::STD_TYPE);

		if(g_enable[3] > 0)
			m_btn_module_4.SetImage(CGdipButton::ALT_TYPE);
		else
			m_btn_module_4.SetImage(CGdipButton::STD_TYPE);
	}

}
HBRUSH CCControlPanel::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	if(nCtlColor == CTLCOLOR_DLG)   // 判断是否是对话框  
	{  
		return   g_ctrl_brush; // 返回刚才创建的背景刷子  
	}
	return hbr;
}


//更新按钮状态
void CCControlPanel::ctrl_update_btn()
{
	static char curr_sta = -1;
	BOOL stype = 0;

	if(!IsWindow(m_btn_module_1.m_hWnd))return ;
	
	char sta = theVIEWER->v_get_system_status();

	if(curr_sta == sta) return ;
	curr_sta = sta;

	ctrl_update_btn_pos();
}


// 更新按钮位置
void CCControlPanel::ctrl_update_btn_pos()
{
	CRect rect;
	CWnd* pWnd = NULL;
	
	if(!(IsWindow(m_hWnd) && IsWindow(m_btn_module_1.m_hWnd)))return ;

	GetClientRect(rect);
	int lef =0, top = 0;

	lef = rect.left+5;
	top = rect.top + 5+5;

	if(BMS_MODE_NET_GFKD == g_obj->m_asit.sitEX.sysEX_bms_mode)
	{
		/*调整各个控制按钮 */
		lef = rect.left+5;
		top = rect.top + 5 + 34 +5 + 34 + 5 + 34 + 5 + 34 + 10 + 5;

		ctrl_move_btn(m_btn_enable,CRect(lef,top,lef+123,top+26),TRUE);
		ctrl_move_btn(m_btn_enable_set,CRect(lef,top+26,lef+58,top+26+34),TRUE);
		ctrl_move_btn(m_btn_enable_query,CRect(lef+58,top+26,lef+58+65,top+26+34),TRUE);

		top += (5 + 60 + 5);
		ctrl_move_btn(m_btn_params,CRect(lef,top,lef+123,top+26),TRUE);
		ctrl_move_btn(m_btn_params_set,CRect(lef,top+26,lef+58,top+26+34),TRUE);
		ctrl_move_btn(m_btn_params_query,CRect(lef+58,top+26,lef+58+65,top+26+34),TRUE);

		top += (5 + 60 + 5);
		ctrl_move_btn(m_btn_switch,CRect(lef,top,lef+37,top+38),TRUE);
		ctrl_move_btn(m_btn_switch_query,CRect(lef+37,top,lef+37+43,top+38),TRUE);
		ctrl_move_btn(m_btn_switch_set,CRect(lef+37+43,top,lef+37+43+43,top+38),TRUE);

		top += (15+  5 + 38);
		pWnd = GetDlgItem(IDC_STA_CONTROL);
		if(pWnd)
		{
			pWnd->MoveWindow(lef,top,rect.Width()-10,1);
			pWnd->ShowWindow(SW_SHOW);
		}

		top += (5 + 15);
		ctrl_move_btn(m_btn_charging,CRect(lef,top,lef+37,top+38),TRUE);
		ctrl_move_btn(m_btn_charging_start,CRect(lef+37,top,lef+37+43,top+38),TRUE);
		ctrl_move_btn(m_btn_charging_stop,CRect(lef+37+43,top,lef+37+43+43,top+38),TRUE);

		top += (5 + 38);
		ctrl_move_btn(m_btn_discharging,CRect(lef,top,lef+37,top+38),TRUE);
		ctrl_move_btn(m_btn_discharging_start,CRect(lef+37,top,lef+37+43,top+38),TRUE);
		ctrl_move_btn(m_btn_discharging_stop,CRect(lef+37+43,top,lef+37+43+43,top+38),TRUE);

		top += (10+  5 + 38);
		pWnd = GetDlgItem(IDC_STA_LINE);
		if(pWnd)
		{
			pWnd->MoveWindow(lef,top,rect.Width()-10,1);
			pWnd->ShowWindow(SW_SHOW);
		}

		top += (5 + 15);
		ctrl_move_btn(m_btn_log,CRect(lef,top,lef+37,top+38),TRUE);
		ctrl_move_btn(m_btn_log_save,CRect(lef+37,top,lef+37+43,top+38),TRUE);
		ctrl_move_btn(m_btn_log_clear,CRect(lef+37+43,top,lef+37+43+43,top+38),TRUE);

		m_btn_min_dis_unit.ShowWindow(SW_HIDE);
		m_btn_min_dis_query.ShowWindow(SW_HIDE);
		m_btn_min_dis_set.ShowWindow(SW_HIDE);

		/*m_btn_ups_ctrl.ShowWindow(SW_HIDE);
		m_btn_ups_ctrl_start.ShowWindow(SW_HIDE);
		m_btn_ups_ctrl_stop.ShowWindow(SW_HIDE);

		
		m_btn_enable.MoveWindow(lef,top,37,38);
		m_btn_enable_query.MoveWindow(lef+37,top,43,38);
		m_btn_enable_set.MoveWindow(lef+37+43,top,43,38);
		
		top += (5 + 38);
		m_btn_params.MoveWindow(lef,top,37,38);
		m_btn_params_query.MoveWindow(lef+37,top,43,38);
		m_btn_params_set.MoveWindow(lef+37+43,top,43,38);

		top += (5 + 38);
		m_btn_switch.MoveWindow(lef,top,37,38);
		m_btn_switch_query.MoveWindow(lef+37,top,43,38);
		m_btn_switch_set.MoveWindow(lef+37+43,top,43,38);

		top += (15+  5 + 38);
		pWnd = GetDlgItem(IDC_STA_CONTROL);
		if(pWnd)
			pWnd->MoveWindow(lef,top,rect.Width()-10,1);

		top += (5 + 15);
		m_btn_charging.MoveWindow(lef,top,37,38);
		m_btn_charging_start.MoveWindow(lef+37,top,43,38);
		m_btn_charging_stop.MoveWindow(lef+37+43,top,43,38);

		top += (5 + 38);
		m_btn_discharging.MoveWindow(lef,top,37,38);
		m_btn_discharging_start.MoveWindow(lef+37,top,43,38);
		m_btn_discharging_stop.MoveWindow(lef+37+43,top,43,38);

		top += (10+  5 + 38);
		pWnd = GetDlgItem(IDC_STA_LINE);
		if(pWnd)
			pWnd->MoveWindow(lef,top,rect.Width()-10,1);

		top += (5 + 15);
		m_btn_log.MoveWindow(lef,top,37,38);
		m_btn_log_save.MoveWindow(lef+37,top,43,38);
		m_btn_log_clear.MoveWindow(lef+37+43,top,43,38);


		m_btn_min_dis_unit.ShowWindow(SW_HIDE);
		m_btn_min_dis_query.ShowWindow(SW_HIDE);
		m_btn_min_dis_set.ShowWindow(SW_HIDE);
		*/
	}
	else
	{
		lef = rect.left+5;
		top = rect.top + 5 + 34 +5 + 34 + 5 + 34 + 5 + 34 + 10+5;

		//隐藏使能控制
		m_btn_enable.ShowWindow(SW_HIDE);
		m_btn_enable_query.ShowWindow(SW_HIDE);
		m_btn_enable_set.ShowWindow(SW_HIDE);
	

		//充电电流、电压
		ctrl_move_btn(m_btn_params,CRect(lef,top,lef+123,top+26),TRUE);
		ctrl_move_btn(m_btn_params_set,CRect(lef,top+26,lef+58,top+26+34),TRUE);
		ctrl_move_btn(m_btn_params_query,CRect(lef+58,top+26,lef+58+65,top+26+34),TRUE);

		//最小放电单元数
		top += (5 + 60 + 5);
		ctrl_move_btn(m_btn_min_dis_unit,CRect(lef,top,lef+123,top+26),TRUE);
		ctrl_move_btn(m_btn_min_dis_set,CRect(lef,top+26,lef+58,top+26+34),TRUE);
		ctrl_move_btn(m_btn_min_dis_query,CRect(lef+58,top+26,lef+58+65,top+26+34),TRUE);

		//最小放电单元数
		/*
		top += (5 + 60 + 5);
		ctrl_move_btn(m_btn_ups_ctrl,CRect(lef,top,lef+123,top+26),TRUE);
		ctrl_move_btn(m_btn_ups_ctrl_start,CRect(lef,top+26,lef+58,top+26+34),TRUE);
		ctrl_move_btn(m_btn_ups_ctrl_stop,CRect(lef+58,top+26,lef+58+65,top+26+34),TRUE);
		*/

		//隐藏开关
		//top += (5 + 60);
		m_btn_switch.ShowWindow(SW_HIDE);
		m_btn_switch_query.ShowWindow(SW_HIDE);
		m_btn_switch_set.ShowWindow(SW_HIDE);

		top += (15+  5 + 60);
		pWnd = GetDlgItem(IDC_STA_CONTROL);
		if(pWnd)
		{
			pWnd->MoveWindow(lef,top,rect.Width()-10,1);
			pWnd->ShowWindow(SW_SHOW);
		}


		//隐藏充、放电
		m_btn_charging.ShowWindow(SW_HIDE);
		m_btn_charging_start.ShowWindow(SW_HIDE);
		m_btn_charging_stop.ShowWindow(SW_HIDE);

		m_btn_discharging.ShowWindow(SW_HIDE);
		m_btn_discharging_start.ShowWindow(SW_HIDE);
		m_btn_discharging_stop.ShowWindow(SW_HIDE);
		
		
		//top += (10+  5 + 38);
		pWnd = GetDlgItem(IDC_STA_LINE);
		if(pWnd)
		{
			pWnd->ShowWindow(SW_HIDE);
		}
	
		top += (5 + 15);
		ctrl_move_btn(m_btn_log,CRect(lef,top,lef+37,top+38),TRUE);
		ctrl_move_btn(m_btn_log_save,CRect(lef+37,top,lef+37+43,top+38),TRUE);
		ctrl_move_btn(m_btn_log_clear,CRect(lef+37+43,top,lef+37+43+43,top+38),TRUE);

	}
}


// 移动按钮位置
void CCControlPanel::ctrl_move_btn(CGdipButton& btn, CRect rect, BOOL show /*= TRUE*/)
{
	if(IsWindow(btn.m_hWnd))
	{
		btn.MoveWindow(rect);
		if(show)
			btn.ShowWindow(SW_SHOW);
		else
			btn.ShowWindow(SW_HIDE);

	}
}