#ifndef HEADER_CLA_LOG_SHIQINGLIAN
#define HEADER_CLA_LOG_SHIQINGLIAN 
#include <stdio.h>
#include <iostream>
#include <atlstr.h>
#include <time.h>
using namespace std;

typedef enum __DATA_TYPE{
	DT_READ=0,
	DT_WRITE,
	DT_AUTO
}DATA_TYPE_E;

typedef struct __log_buf
{
	char pbuf[100];
	int buf_len;
	DATA_TYPE_E dt;
	SYSTEMTIME st;

}log_buf_t;


class CNetLog
{
public:
	CNetLog(void);
	~CNetLog(void);

private:
	char flag_send;
	char flag_recv;
	char flag_auto;

	FILE* m_hand;

	/***
	 * @brief: 操作文件线程
	 */
	static char WINAPI log_route(LPVOID lpVoid);


	/***
	 * @brief: 打开文件
	 * @param file:
	 * @return:
	 */
	bool log_open(char* file);

	/***
	 * @brief: 获取日志文件
	 */
	void log_get_name(char* dst, int len, char mid, char uid);

	BOOL FolderExists(CString s) ;
	BOOL CreateMuliteDirectory(CString P);

public:

	/***
	 * @brief: 添加日志
	 * @param buf: 日志缓存
	 * @param buf_len: 日志长度
	 */
	void add_log(char* buf, int buf_len, DATA_TYPE_E dte ,SYSTEMTIME st);

	void add_logEx(char* buf, int buf_len, DATA_TYPE_E dte);


	void show_message(char* src, int src_len, char* dst, int dst_len);
};

#endif


