#include <libs_version.h>
#include "CTimeConvert.h"
#include <math.h>
/*

**time_tתSYSTEMTIME

*/

SYSTEMTIME TimetToSystemTime(time_t t)
{
	FILETIME ft;
	SYSTEMTIME pst;
	LONGLONG nLL = Int32x32To64(t, 10000000) + 116444736000000000;
	ft.dwLowDateTime = (DWORD)nLL;
	ft.dwHighDateTime = (DWORD)(nLL >> 32);
	FileTimeToSystemTime(&ft, &pst);
	return pst;
}

/*

**SYSTEMTIMEתtime_t

*/

time_t SystemTimeToTimet(SYSTEMTIME st)
{
	FILETIME ft;
	SystemTimeToFileTime( &st, &ft );
	LONGLONG nLL;
	ULARGE_INTEGER ui;
	ui.LowPart = ft.dwLowDateTime;
	ui.HighPart = ft.dwHighDateTime;
	nLL = (ft.dwHighDateTime << 32) + ft.dwLowDateTime;
	time_t pt = (long)((LONGLONG)(ui.QuadPart - 116444736000000000) / 10000000);
	return pt;
}

//
time_t GetCurrentTimet()
{
	SYSTEMTIME st;
	GetLocalTime(&st);

	return SystemTimeToTimet(st);
}

//
double GetDiffTime(time_t t1, time_t t2)
{
	double diff = difftime(t2,t1);

	return fabs(diff);
}