#include "../StdAfx.h"
#include "CLA_ATS.h"
#include "cla_net.h"
#include "../include/batteryEX.h"
#include <assert.h>

//至关重要的全局变量

typedef struct __ATS_vvv_t
{
	uchar pos;
	ATS_vector_t vect[MACH_MAX];

}ATS_vvv_t;

ATS_vvv_t gATS_vect;


CNET_CALLBACK gCallBack = NULL;


//日志输出
extern void sql_trace(char* format,...);


//构造函数
NZ_ATS::NZ_ATS()
{
	m_mac_id = 0;
	m_ups_id = 0;
	m_key = "";
	unsigned int id;

	m_ats_hwnd = 0;
	mNet = NULL;
	memset(&m_nat,0,sizeof(m_nat));
	mNet = CNet::GetInstance();

	memset(&m_curr_action,0,sizeof(m_curr_action));
	InitializeCriticalSection(&action_lock);
	
	//设置退出标志
	m_eixt = 0;
	m_timeout = 4000;	//指令超时时间；单位:毫秒
	m_time_puase = 0;

	//启动超时检测线程
	m_event_timeout = CreateEvent(NULL,TRUE,FALSE,NULL);
	m_hand_timeout = (HANDLE)_beginthreadex(NULL,0,timeout_route,this,0,&id);
}


//通知地址
char NZ_ATS::ATS_tell_addr(net_addr_t* nat)
{
	ATS_ident_t ait;
	uchar tmp[10] = {0};
	net_addr_t tmpnat;
	memset(&tmpnat,0,sizeof(tmpnat));
	mNet->CN_get_local_addr(&tmpnat);
	tmpnat.port=6000;

	tmp[0] = 0xAA;
	tmp[1] = 0x01;
	tmp[2] = tmpnat.ip1;
	tmp[3] = tmpnat.ip2;
	tmp[4] = tmpnat.ip3;
	tmp[5] = tmpnat.ip4;
	ait.shelf_id = 4;
	ait.ups_id = 0;
	
	__push_cmd(nat,tmp,6,&ait);
	return 1;
}


//设置IP地址
char NZ_ATS::NZ_set_addr(net_addr_t* nat)
{
	if(nat){
		memcpy(&m_nat,nat,sizeof(net_addr_t));
	}

	return 1;
}


// 初始化
char NZ_ATS::ATS_initlize()
{
	memset(&m_curr_action,0,sizeof(m_curr_action));
	InitializeCriticalSection(&action_lock);
	return 1;
}


//析构函数
NZ_ATS::~NZ_ATS()
{
	DeleteCriticalSection(&action_lock);
}


//添加
char NZ_ATS::ATS_add_ups(ups_type_t type, net_addr_t* nat, ATS_ident_t* ait)
{
	if(!nat && !ait) return 0;

	for (int i=0; i<MACH_MAX; i++)
	{
		if((gATS_vect.vect[i].mType == type) &&
			(C_NETADDR(*nat,gATS_vect.vect[i].nat)) && (C_IDENTY(*ait,gATS_vect.vect[i].ait)))
		{
			return 0;
		}
	}

	if(gATS_vect.pos<MACH_MAX)
	{
		gATS_vect.vect[gATS_vect.pos].mType = type;
		memcpy(&(gATS_vect.vect[gATS_vect.pos].ait),ait,sizeof(ATS_ident_t));
		memcpy(&(gATS_vect.vect[gATS_vect.pos].nat),nat,sizeof(net_addr_t));
		gATS_vect.pos++;
		return 1;
	}

	return -1;
}


// 组建消息头
uchar NZ_ATS::__init_header(uchar* str, uint len, ATS_ident_t* ait, uchar cmd, uint cmd_parameter)
{
	uchar i =0;
	if(!str)return 0;

	//if(ait)
	{
		sql_trace("组建消息头:%d %d\n",m_mac_id,m_ups_id);
		str[i++] = m_mac_id;
		str[i++] = m_ups_id;
	}

	str[i++] = ((cmd&0x0f)<<4) + ((cmd_parameter&0xf00)>>8);
	str[i++] = cmd_parameter&0xff;

	return i;
}


void NZ_ATS::ATS_time()
{
	//开始计时
	m_time_puase = 0;	
	SetEvent(m_event_timeout);

}


// 送入消息队列
char NZ_ATS::__push_cmd( net_addr_t* nat, uchar* message, uint len, ATS_ident_t* ait, uint wparam, uint lparam , char *data)
{

	char ret = 0;
	if(!message) return 0;

	if(!nat)
		nat = &m_nat;

	//sql add
	EnterCriticalSection(&action_lock);

	if(0 == m_curr_action.state)
	{
		NZ_ParseMessage(message,len,&(m_curr_action.action));
		m_curr_action.wparam = wparam;
		m_curr_action.lparam = lparam;
		if(data)
			memcpy(m_curr_action.data,data,sizeof(m_curr_action.data));
		m_curr_action.state = 1;
		m_curr_action.resend = 0;

		//开始计时
		//m_time_puase = 0;	
		//SetEvent(m_event_timeout);

		if(mNet)
		{
			__make_checksum(message, len);

			memcpy(m_curr_action.message,message,sizeof(m_curr_action.message));
			m_curr_action.message_len = len+1;

			//为适应国防科大项目
			ret =  mNet->CN_send(nat,(void*)message,len+1);
		}
	}
	else{
		::MessageBox(NULL,"指令正在执行，请稍后","提示",MB_OK);
		ret = 0;
	}

	LeaveCriticalSection(&action_lock);

	return ret;
}


//解析数据
char NZ_ATS::NZ_ParseMessage(uchar* pbuf, uint buf_len, curr_action_params_t* cur_action)
{
	uchar tmp[100] = {0};
	if(!pbuf || !cur_action) return 0;

	cur_action->cmd			= ((pbuf[2] & 0xF0) >> 4) & 0x0F;
	cur_action->parameter	= (((pbuf[2]) & 0x0f)<<8) + pbuf[3]; 
	memcpy(cur_action->buf,pbuf + 4,sizeof(cur_action->buf));

	return 1;
}


// 生成校验码
char NZ_ATS::__make_checksum(uchar* msg, uint len)
{
	uint i=0;
	uint checksum = 0;

	if(!msg) return 0;

	for (i=2;i<len; i++)
		checksum += msg[i];

	msg[i] = (~checksum)+1;

	return 1;
}


// 检测校验码
char NZ_ATS::__check_checksum(uchar* msg, uint len)
{
	return 1;
}


//系统版本号
char NZ_ATS::ATS_sys_version(net_addr_t* nat, ATS_ident_t* ait /* = NULL */, uint wparam, uint lparam , char *data)
{
	uchar tmp[50] = {0};
	uchar len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_DWORD,OBJ_ID_SYS_VERSION);

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


// 指令执行状态
char NZ_ATS::ATS_command_status(net_addr_t* nat, ATS_ident_t* ait /*= NULL*/, uint wparam, uint lparam , char *data)
{
	uchar tmp[50] = {0};
	uchar len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_BYTE,OBJ_ID_COMMAND_STATUS);

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//主机状态正常通报
char NZ_ATS::ATS_host_alive_notification(net_addr_t* nat, ATS_ident_t* ait/*=NULL*/, uint wparam, uint lparam , char *data)
{
	uchar tmp[50] = {0};
	uchar len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_HOST_ALIVE_NOTFICATION,OBJ_ID_HOST_ALIVE_NOTIFICATION);

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


// 改变输出模式
char NZ_ATS::ATS_output_mode_change_GFKD(net_addr_t* nat, uchar mod, ATS_ident_t* ait, uint wparam, uint lparam , char *data)
{
	uchar len = 0;
	uchar tmp[50] = {0};
	
	len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_BYTE,OBJ_ID_DATA_UP_MODE_GFKD);
	tmp[len++] = mod;

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


// 产品名称
char NZ_ATS::ATS_product_name(net_addr_t* nat, ATS_ident_t* ait, uint wparam, uint lparam , char *data)
{

	uchar tmp[50] = {0};
	uchar len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_DWORD,OBJ_ID_PRODUCT_NAME);

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//命令执行完成通报
char NZ_ATS::ATS_cmd_complete(net_addr_t* nat, ATS_ident_t* ait, uint wparam, uint lparam , char *data)
{
	uchar tmp[10] = {0};
	char len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_BYTE,OBJ_ID_CMD_COMPLETE);

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//同时停止放电
char NZ_ATS::ATS_stop_discharging_all(net_addr_t* nat, ATS_ident_t* ait, uint wparam, uint lparam , char *data)
{
	uchar tmp[10] = {0};
	char len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_SPCMD_CMD_NO_DATA,OBJ_ID_STOP_DISCHARGING_ALL);
	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


// 系统软件版本号
char NZ_ATS::ATS_sw_version(net_addr_t* nat, action_type_e ate, char ver[4], ATS_ident_t* ait, uint wparam, uint lparam , char *data)
{
	

	uchar len = 0;
	uchar tmp[20] = {0};
	if(NZ_ACT_READ == ate)
		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_DWORD,OBJ_ID_SW_VERSION);
	else{

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_DWORD,OBJ_ID_SW_VERSION);
		for (int i=0; i<4; i++)
			tmp[len++] = ver[i];
	}
	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


// 配置参数写入Flash
char NZ_ATS::ATS_flash_save_config(net_addr_t* nat, ATS_ident_t* ait, uint wparam, uint lparam , char *data)
{
	uchar tmp[10] = {0};
	char len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_SPCMD_CMD_NO_DATA,OBJ_ID_FLASH_SAVE_CONFIG);

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//配置参数写入Flash
char NZ_ATS::ATS_flash_read_config(net_addr_t* nat, ATS_ident_t* ait, uint wparam, uint lparam , char *data)
{
	uchar tmp[10] = {0};
	char len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_SPCMD_CMD_NO_DATA,OBJ_ID_FLASH_READ_CONFIG);

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//电池模块使能控制
char NZ_ATS::ATS_bat_mod_enable(net_addr_t* nat, action_type_e ate, uchar offset, ATS_ident_t*ait
								, uint wparam, uint lparam , char *data)
{
	uchar len = 0;
	uchar tmp[20] = {0};
	if(NZ_ACT_READ == ate){
		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_WORD,OBJ_ID_BATTERY_MODULE_ENABLE);
		tmp[len++] = 0;
		tmp[len++] = offset;
	}
	else{

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_WORD,OBJ_ID_BATTERY_MODULE_ENABLE);
	
		tmp[len++] = 0;
		tmp[len++] = 0x80 |(offset & 0x0F);
	}
	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}

char NZ_ATS::ATS_bat_mod_select(net_addr_t* nat, action_type_e ate, uchar offset, ATS_ident_t*ait, uint wparam, uint lparam , char *data)
{

	
	uchar len = 0;
	uchar tmp[20] = {0};
	if(NZ_ACT_READ == ate)

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_WORD,OBJ_ID_BATTERY_MODULE_ENABLE);
	else{

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_WORD,OBJ_ID_BATTERY_MODULE_ENABLE);
		tmp[len++] = 0x80 | (offset&0x0f);
		tmp[len++] = 0;
	}
	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


 //设置充电电流电压
char NZ_ATS::ATS_charging_current(net_addr_t* nat, action_type_e ate, uchar str[4], ATS_ident_t*ait
								  , uint wparam, uint lparam , char *data)
{
	
	uchar len = 0;
	uchar tmp[20] = {0};
	if(NZ_ACT_READ == ate){

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_DWORD,OBJ_ID_CHARGING_CURRENT);
		if(str)
			tmp[len++] = str[0];
	}
	else{

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_DWORD,OBJ_ID_CHARGING_CURRENT);

		for (int i=0; i<4; i++)
			tmp[len++] = str[i];
	}
	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//系统工作模式
char NZ_ATS::ATS_bms_mode(net_addr_t* nat, action_type_e ate, uchar str, ATS_ident_t*ait,
						   uint wparam, uint lparam , char *data)
{
	
	uchar len = 0;
	uchar tmp[20] = {0};
	if(NZ_ACT_READ == ate)

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_BYTE,OBJ_ID_BMS_MODE);
	else{

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_BYTE,OBJ_ID_BMS_MODE);
		tmp[len++] = str;
		
	}
	
	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


// 系统控制
char NZ_ATS::ATS_system_control(net_addr_t* nat, action_type_e ate, uchar str[4], ATS_ident_t*ait
								, uint wparam, uint lparam , char *data)
{
	
	uchar len = 0;
	uchar tmp[20] = {0};

	if(NZ_ACT_READ == ate)

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_BYTE,OBJ_ID_SYSTEM_CONTROL);
	else{

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_BYTE,OBJ_ID_SYSTEM_CONTROL);

		tmp[len++] = str[0];tmp[len++] = str[1];
		tmp[len++] = str[2];tmp[len++] = str[3];
	}
	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//系统控制
char NZ_ATS::ATS_system_control_GFKD(net_addr_t* nat, uchar str, ATS_ident_t*ait
									 , uint wparam, uint lparam , char *data)
{
	
	uchar len = 0;
	uchar tmp[20] = {0};

	len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_BYTE,OBJ_ID_SYSTEM_CONTROL);

	tmp[len++] = str;
	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//电池屏蔽
char NZ_ATS::ATS_battery_mask(net_addr_t* nat, action_type_e ate, uchar mod_id, uchar str[4], ATS_ident_t* ait
							  , uint wparam, uint lparam , char *data)
{
	uint id = 0, len =0;
	
	uchar tmp[10] = {0};
	
	id = OBJ_ID_BATTERY_MASK_0 + mod_id;

	if(NZ_ACT_READ == ate)
		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_DWORD,id);

	else{

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_DWORD,id);

		tmp[len++] = str[0];tmp[len++] = str[1];
		tmp[len++] = str[2];tmp[len++] = str[3];
	}

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//
char NZ_ATS::ATS_build_params(net_addr_t* nat, uchar cmd, uint cmd_param, uchar str[], uint str_len, ATS_ident_t* ait
							  , uint wparam, uint lparam , char *data)
{
	

	uchar len = 0;
	uchar tmp[30] = {0};
	len = __init_header(F_PARAM(tmp),ait,cmd,cmd_param);

	if(str_len>0 && str){
		for (uint i=0; i<str_len; i++)
			tmp[len++] = str[i];
	}

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


//////////////////////////////////////////////////////////////////////////

//电池单元当前充/放电电流
char NZ_ATS::ATS_bat_current_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait
								, uint wparam, uint lparam , char *data)
{
	
	if(uid>31)return 0;
	
	return ATS_build_params(nat,CMD_DOWN_READ_WORD,OBJ_ID_CURRENT_0_EX + uid,NULL,0,ait,wparam,lparam,data);
}


//电池单元当前的电压
char NZ_ATS::ATS_bat_voltage_Ex(net_addr_t* nat, uchar uid,  ATS_ident_t* ait
								, uint wparam, uint lparam , char *data)
{
	if(uid>31)return 0;
	
	return ATS_build_params(nat,CMD_DOWN_READ_WORD,OBJ_ID_BATTERY_VOLTAGE_0_EX + uid,NULL,0,ait,wparam,lparam,data);
}


// 查询电池单元当前的SOC
char NZ_ATS::ATS_bat_soc_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait
							, uint wparam, uint lparam , char *data)
{
	if(uid>16)return 0;
	
	return ATS_build_params(nat,CMD_DOWN_READ_BYTE,OBJ_ID_BATTERY_SOC_0_EX + uid,NULL,0,ait,wparam,lparam,data);
}


// 电池状态
char NZ_ATS::ATS_bat_status_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait
							   , uint wparam, uint lparam , char *data)
{
	if(uid>3)return 0;

	return ATS_build_params(nat,CMD_DOWN_READ_DWORD,OBJ_ID_BATTERY_STATUS_0_EX+uid, NULL, 0, ait,wparam,lparam,data);
}


//电池开关状态
char NZ_ATS::ATS_bat_switch_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait
							   , uint wparam, uint lparam , char *data)
{
	if(uid>1)return 0;
	
	return ATS_build_params(nat,CMD_DOWN_READ_DWORD,OBJ_ID_SWITCH_STATUS_0_EX + uid,NULL,0,ait,wparam,lparam,data);
}


//温度
char NZ_ATS::ATS_bat_module_temperature_Ex(net_addr_t* nat, ATS_ident_t* ait
										   , uint wparam, uint lparam , char *data)
{
	return ATS_build_params(nat,CMD_DOWN_READ_BYTE,OBJ_ID_MODULE_TEMP_EX, NULL, 0, ait,wparam,lparam,data);
}


//系统状态
char NZ_ATS::ATS_sys_status_Ex(net_addr_t* nat, ATS_ident_t* ait
							   , uint wparam, uint lparam , char *data)
{
	return ATS_build_params(nat,CMD_DOWN_READ_BYTE,OBJ_ID_SYSTEM_STATUS_EX, NULL, 0, ait,wparam,lparam,data);
}


//系统报警
char NZ_ATS::ATS_sys_error_Ex(net_addr_t* nat, ATS_ident_t* ait
							  , uint wparam, uint lparam , char *data)
{
	return ATS_build_params(nat,CMD_DOWN_READ_BYTE,OBJ_ID_SYSTEM_ERROR_EX, NULL, 0, ait,wparam,lparam,data);
}


//设置电池状态
char NZ_ATS::ATS_battery_charge_state(net_addr_t* nat, ATS_ident_t* ait, char id, uchar str[4],
									  uint wparam, uint lparam , char *data)
{

	return ATS_build_params(nat,CMD_DOWN_WRITE_DWORD,OBJ_ID_BATTERY_STATUS_0_EX + id,str,4,ait,wparam,lparam,data);
}


// 最小放电单元数
char NZ_ATS::ATS_min_discharge_unit(net_addr_t* nat, ATS_ident_t* ait, action_type_e ate,
							uchar ct,	uint wparam, uint lparam, char *data)
{
	uint len =0;
	uchar tmp[10] = {0};

	if(NZ_ACT_READ == ate)
		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_READ_BYTE,OBJ_ID_MIN_DISCHARGE_UNIT);

	else{

		len = __init_header(F_PARAM(tmp),ait,CMD_DOWN_WRITE_BYTE,OBJ_ID_MIN_DISCHARGE_UNIT);
		tmp[len++] = ct;
	}

	return __push_cmd(nat,tmp,len,ait,wparam,lparam,data);
}


// 获取缓存指针
char NZ_ATS::ATS_get_action_p(curr_action_buf_t** p)
{
	char ret = 0;

	EnterCriticalSection(&action_lock);

	if(1 == m_curr_action.state)
	{
		ret = 1;
		*p = &m_curr_action;
	}
	
	LeaveCriticalSection(&action_lock);

	return ret;
}


// 置位重发标志
void NZ_ATS::ATS_action_resend(char flag)
{
	EnterCriticalSection(&action_lock);
	m_curr_action.resend = flag;
	LeaveCriticalSection(&action_lock);
}


// 初始化
void NZ_ATS::ATS_action_reset()
{
	EnterCriticalSection(&action_lock);
	memset(&m_curr_action,0,sizeof(m_curr_action));
	m_curr_action.state = 0;
	m_time_puase = 1;
	m_curr_action.resend = 0;
	memset(m_curr_action.message,0,sizeof(m_curr_action.message));
	LeaveCriticalSection(&action_lock);
}


//超时检测线程
unsigned WINAPI NZ_ATS::timeout_route(void* lpvoid)
{
	ULARGE_INTEGER fTime_start;/*FILETIME*/
	ULARGE_INTEGER fTime_end;/*FILETIME*/ 
	SYSTEMTIME t_start,t_end;
	unsigned __int64 dft;
	NZ_ATS* obj = (NZ_ATS*)lpvoid;
	char tmp[21] = {0};

	GetPrivateProfileString("general","TIMEOUT","7000",tmp,sizeof(tmp),"conf/conf.ini");
	obj->m_timeout = atoi(tmp);

	while(1)
	{
		//注意：是否要退出
		if(1 == obj->m_eixt)break;
	
		//等待信号
		WaitForSingleObject(obj->m_event_timeout,INFINITE);
		ResetEvent(obj->m_event_timeout);	//释放信号

		GetLocalTime(&t_start);
		while (0 == obj->m_time_puase)	//现在开始计时
		{
			//注意：是否要退出
			if(1 == obj->m_eixt)break;

			Sleep(10);
			if(1 == obj->m_time_puase)break;

			GetLocalTime(&t_end);

			SystemTimeToFileTime(&(t_start),(FILETIME*)&fTime_start);
			SystemTimeToFileTime(&t_end,(FILETIME*)&fTime_end); 

			dft=fTime_end.QuadPart-fTime_start.QuadPart; 
			dft = (int)(dft/10000);//单位：毫秒
			if(dft >= obj->m_timeout && 0 == obj->m_time_puase && 1 == obj->m_curr_action.state)
			{
				obj->ATS_timeout(obj->m_curr_action);
			}
		}	//while (0 == obj->m_time_puase)
	}	//while(1)

	return 1;
}