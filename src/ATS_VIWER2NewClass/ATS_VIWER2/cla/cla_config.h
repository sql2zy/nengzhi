#ifndef HEADER_CLA_INI_SQL_20131125
#define HEADER_CLA_INI_SQL_20131125

#pragma once

#ifndef MAX_PATH
#define MAX_PATH 260
#endif

class cla_conf
{
public:
	cla_conf(void);
	~cla_conf(void);


public:

	/***
	 * 获取当前路径
	 * @param dst: 容器
	 * @param len: 容器大小
	 * @author: Shi Qingliang
	 * @date: 2012-09-10
	 */
	char Conf_get_curr_path(char* dst, unsigned int len);

	//写入
	int Conf_write(const char* file,const char* section,const char* key, const char* val);
	
	//读取
	unsigned long Conf_read(const char* file,const char* section,const char* key, char* pbuf, unsigned int len);
};

#endif