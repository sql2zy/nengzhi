/************************************************************************/
/*                                                                      */
/************************************************************************/
#ifndef HEADER_CLA_NET_SHIQINGLIANG_20141123
#define HEADER_CLA_NET_SHIQINGLIANG_20141123
#include <iostream>
#include <list>
#include <map>
#include <string>

#include <Winsock2.h>
#include "..\include\comm.h"
#include "cla_list.h"

using namespace std;


//发送缓存大小
#define NET_BUF_SIZE 100

//待发送的数据信息
typedef struct __send_node_buf
{
	net_addr_t nat;
	char  pbuf[NET_BUF_SIZE];
	uint buf_len;

} send_node_buf_t;

//预定义类
class  CBatObject;

//回调函数类型
typedef char(*CNET_CALLBACK)(send_node_buf_t*);

/***
 * 网络通信类
 */
class CNet{

private:

	/***
	 * 构造函数
	 */
	CNet(void);


	/***
	 * 析构函数
	 */
	~CNet(void);


	uint m_remote_port;	//远程端口
	uint m_local_port;	//本地端口

	char flag;			//初始化标志，防止重复初始化
	SOCKET m_sock;
	SOCKADDR_IN m_localAddr;		//本地地址

	HANDLE m_thread_hand;	//线程句柄
	HANDLE m_trhead_send;

	map<string,CBatObject*> m_cb_interface;
	

	CRITICAL_SECTION m_lock;

	CNET_CALLBACK m_special_function;

	/***
	 * 禁用拷贝函数、赋值函数
	 */
	CNet(const CNet&);
	CNet& operator=(const CNet&);

private:

	/***
	 * @brief: 主线程
	 * @param lpVoid:
	 * @return: 
	 */
	static int WINAPI thread_route(LPVOID lpVoid);
	static int WINAPI thread_send(LPVOID lpVoid);


public:

	/***
	 * @brief: 获取实例
	 */
	static CNet* GetInstance(){
		static CNet m_UDP;

		return &m_UDP;
	};


	/***
	 * @brief: 添加回调函数
	 * @param key: 关键字
	 * @param f:   回调函数
	 * @param replace: 1 for 如果已经存在，则替换之
	 * @return 添加成功返回 true，失败 false，
	 */
	bool CN_add_callback(string key, CBatObject* obj, char replace = 1);
	bool CN_add_special_callback(CBatObject *obj,char replace = 1);


	/***
	 * @brief: 删除回调函数
	 * @param key: 关键字
	 */
	bool CN_remove_callback(string key);


	/***
	 * @brief: 检测是否有回调函数
	 * @param key: 关键字
	 * @return: true for exist, false for not.
	 */
	bool CN_check_callback(string key);


	/***
	 * @brief: 开始主线程
	 * @param localport: 本地端口
	 * @return: 
	 */
	bool CN_start(uint localport);


	/***
	 * @brief: 获取本地IP地址
	 * @param nat: 缓存地址
	 * @return: 0 for falied 
	 */
	char CN_get_local_addr(net_addr_t* nat);


	/***
	 * @brief: 获取本地网络名称
	 */
	char CN_get_local_name(char* dst, uint dst_len);


	/***
	 * @brief: 发送数据
	 * @param nat: 远程地址
	 * @param pbuf: 数据
	 * @param len: 数据长度
	 * @return 0 for fail, 1 for OK.
	 */
	char CN_send(net_addr_t* nat, void* pbuf, uint len);


	/***
	 * @brief: 停止
	 */
	char CN_stop();

	/***
	 * @brief: 组合地址网络信息
	 * @param nat: 远程地址
	 * @param soca_in: 网络地址
	 * @return 1 for Success, others for fail
	 */
	char CN_init_sockaddr( net_addr_t* nat, SOCKADDR_IN* soca_in);


	/***
	 * @brief: 获取源地址
	 * @param nat: 远程地址
	 * @param soca_in: 网络地址
	 * @return 1 for Success, others for fail
	 */
	char CN_get_src_ip(SOCKADDR_IN& addr, net_addr_t& nat);


	/***
	 * @brief: 通知地址
	 * @param nat:
	 */
	char CN_tell_addr(net_addr_t* nat, uint port);


	/***
	 * @brief: 发送地址
	 */
	char CN_tell_addrEx(net_addr_t* nat, uint port);
};


class CTimeOut
{
public:	 
	CTimeOut(void);
	~CTimeOut(void);

protected:

	SYSTEMTIME m_StBegin;
	SYSTEMTIME m_StEnd;

	CRITICAL_SECTION m_Lock;

public:


	void TO_Start();

	void TO_Reset();

	BOOL TO_Check(unsigned __int64 diff);
};

#endif //HEADER_CLA_NET_SHIQINGLIANG_20141123