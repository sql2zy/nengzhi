
#ifndef HEADER_CCOBJECT_SHIQINGLIANG_
#define HEADER_CCOBJECT_SHIQINGLIANG_
#include "cla_net.h"
#include <iostream>

#include "..\include\batteryEX.h"
#include "CLA_ATS.h"


/***
 * brick object
 */
class CBatObject:public NZ_ATS
{
public:	 
	CBatObject(void);
	~CBatObject(void);

private:

	ATS_ident_t m_ait;

	char view_flag;

	//sql test
	cla_list<send_node_buf_t> m_recv_list;

	/***
	 * 线程句柄
	 */
	HANDLE m_hand_route;


	/***
	 * @brief: 检测是否满电压
	 * @param mid:
	 * @param uid:
	 * @param bid:
	 * @return 
	 */
	bool obj_check_bat_high(uchar mid, uchar uid, uchar bid);


	/***
	 * @brief: 检测是否低电压
	 * @param mid:
	 * @param uid:
	 * @param bid:
	 * @return 
	 */
	bool obj_check_bat_low(uchar mid, uchar uid, uchar bid);



public:

	ATS_sys_info_t m_asit;

	char m_ReSend;		//消息接收超时之后，是否重发标志


	/***
	 * 初始化数据
	 * @param null
	 * @return null
	 */
	void init_pbuf();


	/***
	 * 接口函数
	 * @param subt:
	 */
	char  obj_Interface(send_node_buf_t* snbt);

	/***
	 * 处理现场
	 */
	static char WINAPI obj_thread_route(LPVOID lpVoid);


	/*** 
	 * 静态对象
	 */
	CBatObject* m_obj;


	/***
	 * @brief: 初始化
	 */
	char obj_inilize();


	/***
	 * @brief: 设置属性
	 * @param mac_id:
	 * @param ups_id:
	 */
	void obj_set_attribution(uchar mac_id, uchar ups_id);


	/***
	 * @brief: 解析信息
	 * @param recved: 收到的信息
	 */
	char obj_parse_message(curr_action_params_t* recved, curr_action_buf_t* action);

	
	/***
	 * @brief: 处理相应消息
	 * @param recved:
	 * @param action
	 */
	char obj_handle_response(curr_action_params_t* recved, curr_action_buf_t* action);

	/***
	 *
	 * @brief: 根据电池单体屏蔽情况计算最大电压
	 * 
	 */
	char obj_calculation_current(range_value_t* rvt, uchar mod_id);

	/***
	 * @brief: 获取最小值
	 */
	uint obj_calculation_min(uint val[4]);

	
	/***
	 * @brief: 计算电压值
	 */
	char obj_calculation_current();


	/***
	 * @brief: 查询能源互联网模式下,电池是否已经充满
	 */
	void obj_check_battery_state(uchar mid);


	/***
	 * @brief: 根据电池单体屏蔽情况计算最大电压
	 */
	char obj_calculation_voltage(range_value_t* rvt,  uchar mod_id);


	/***
	 * @brief: 检测并联电池之间的电压差
	 */
	char obj_check_paralleling_diff(char mid);


	/***
	 * @brief: 计算系统电流电压
	 */
	char obj_calculation_voltage();


	/***
	 * @brief: 设置显示标志
	 */
	void obj_view_flag_set(char flag) {view_flag = flag;};
	char obj_view_flag_get(){return view_flag;};

	
	/***
	 * @brief: 通知界面
	 */
	void obj_send_msg(uint UID, WPARAM wparam = 0, LPARAM lparam = 0);


	/***
	 * @brief: 指令超时提示
	 * @param cab: 超时信息
	 */
	char ATS_timeout(curr_action_buf_t& cab);


	/***
	 * @brief: 有消息正在执行
	 * @param cab  正在执行的消息体
	 * @return char
	 */
	char ATS_current_busy(curr_action_buf_t* cab);


	/***
	 * @brief: 设置充电标志，用来检测是否是故障电池
	 * @param state: 电池状态
	 * @param mid:
	 */
	char obj_battery_charging_state_set(char mid,char state);


	/***
	 * @brief: 模式切换之后，检测是否需要重置电池状态
	 */
	void obj_batter_stich(uchar mid);


	/***
	 * @brief: 在放电时，将满电压状态置为正常
	 */
	void obj_battery_reset2normal(uchar mid);

	/***
	 * @brief:保存数据
	 */
	void obj_Store();

};

#endif //HEADER_CCOBJECT_SHIQINGLIANG_