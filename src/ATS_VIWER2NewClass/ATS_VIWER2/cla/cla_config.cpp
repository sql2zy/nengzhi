//#include "stdafx.h"
#include <libs_version.h>
#include "cla_config.h"
#include <string.h>
#include <Windows.h>


cla_conf::cla_conf(void)
{
}

cla_conf::~cla_conf(void)
{
}


//获取当前路径
char cla_conf::Conf_get_curr_path(char* dst, unsigned int len)
{
	if(!dst) return 0;

	char path[MAX_PATH];

	GetCurrentDirectory(MAX_PATH,path);
	strcat(path,"\\");
	memcpy(dst,path,len);

	return 1;
}

//写入
int cla_conf::Conf_write(const char* file,const char* section,const char* key, const char* val)
{
	if(!file || !section || !key || !val) return 0;

	return WritePrivateProfileString(section,key,val,file);
}

//读取
unsigned long cla_conf::Conf_read(const char* file,const char* section,const char* key, char* pbuf, unsigned int len)
{
	if(!file || !section || !key || !pbuf) return 0;

	return GetPrivateProfileString(section,key,"",pbuf,len,file);
}


