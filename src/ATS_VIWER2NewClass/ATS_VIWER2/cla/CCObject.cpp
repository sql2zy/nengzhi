#include "../stdafx.h"
#include "CCObject.h"
#include "CNotice.h"
#include "cla_log.h"
#include "../CMainLog.h"
#include <cla_prestore.h>
extern void sql_trace(char* format,...);
extern CNetLog m_netlog;
extern CCMainLog *g_main_log;


//过构造函数
CBatObject::CBatObject(void)
{
	m_obj = this;
	init_pbuf();
	m_ReSend = 0;
}


//析构函数
CBatObject::~CBatObject(void)
{
	if(m_hand_route)
	{
		TerminateThread(m_hand_route,0);
		m_hand_route = 0;
	}
}


// 初始化
char CBatObject::obj_inilize()
{
	DWORD id;
	CNet* net = CNet::GetInstance();
	string key;
	char tmp[10] = {0};

	GetPrivateProfileString("general","RESEND","0",tmp,sizeof(tmp),"conf/conf.ini");
	m_ReSend = atoi(tmp);

	memset(tmp,0,sizeof(tmp));
	sprintf_s(tmp,sizeof(tmp),"%d%d",m_mac_id,m_ups_id);
	key = tmp;
	net->CN_add_callback(key,this);
	
	m_recv_list.init();

	m_hand_route  = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)obj_thread_route,this,0,&id);

	return 1;
}


//初始化数据、
void CBatObject::init_pbuf()
{
	memset(&(m_asit),0,sizeof(ATS_sys_info_t));

	m_asit.sitEX.sysEX_bms_mode = 0;
	m_asit.sitEX.sysEX_bms_output_mode = -1;
	m_asit.sitEX.sysEX_state = 0;

	for (int mid=0; mid<MAX_MODUE; mid++)
	{
		m_asit.sit.mod[mid].join_discha = -1;
		for(int uid=0; uid<MAX_UNIT; uid++)
			for (int bid=0; bid<MAX_BAT; bid++){

				m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.isMask = -1;
				m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].batEX_state = -1;
			}
	}
}


// 处理现场
char WINAPI CBatObject::obj_thread_route(LPVOID lpVoid)
{
	SYSTEMTIME st;
	CBatObject* obj = (CBatObject*)lpVoid;
	curr_action_params_t recved;
	curr_action_buf_t action;
	curr_action_buf_t* p = NULL;
	char res1=0, res2=0;
	send_node_buf_t snbt;

	while(1)
	{
		memset(&snbt,0,sizeof(snbt));
	
		if(0 == obj->m_recv_list.GetTail(snbt))
		{
			Sleep(10);
			continue;
		}

		memset(&recved,0,sizeof(curr_action_params_t));

		res1 = obj->NZ_ParseMessage((uchar*)(snbt.pbuf),snbt.buf_len,&recved);

		if(!res1) 
		{
			g_main_log->add_log(LL_NOTICE,obj->m_mac_id,obj->m_ups_id,"NZ_ParseMessage error");
			return 0;
		}

		GetLocalTime(&st);
		
		//自动上传的数据
		if (MSG_UP_MODE_1_BYTE == recved.cmd || MSG_UP_MODE_1_WORD == recved.cmd || MSG_UP_MODE_1_DWORD == recved.cmd)
		{
			
			if(recved.parameter < OBJ_ID_CURRENT_0_EX )
			{
				res2 = obj->ATS_get_action_p(&p);
				if(p)
					memcpy(&action,p,sizeof(curr_action_buf_t));

				m_netlog.add_logEx(snbt.pbuf,snbt.buf_len,DT_READ);
				obj->ATS_action_reset();
			}
			m_netlog.add_logEx(snbt.pbuf,snbt.buf_len,DT_AUTO);
			obj->obj_parse_message(&recved,&action);

			if(1 == obj->obj_view_flag_get())
				obj->obj_send_msg(MSG_UPDATE,1,1);
			
			continue;

		}
		//else
		{

			res2 = obj->ATS_get_action_p(&p);
			if(p)
			{
				memcpy(&action,p,sizeof(curr_action_buf_t));
			}
			obj->ATS_action_reset();

			if(!res2)
			{
				OutputDebugString("here， we jump out.");
				return 0;
			}

			m_netlog.add_logEx(snbt.pbuf,snbt.buf_len,DT_READ);

			//发送的是写指令
			if (MSG_UP_MODE_CMD_STATUS == recved.cmd)
			{
				obj->obj_handle_response(&recved,&action);
			}
			//发送的是读指令
			else if (MSG_UP_MODE_0_READ_BYTE == recved.cmd || MSG_UP_MODE_0_READ_WORD == recved.cmd 
				|| MSG_UP_MODE_0_READ_DWORD == recved.cmd )
			{
					obj->obj_parse_message(&recved,&action);
			}

			obj->obj_send_msg(MSG_UPDATE_LV_UI,1,1);

			if(1 == obj->obj_view_flag_get())
				obj->obj_send_msg(MSG_UPDATE,1,1);
		}
	}

	return 1;
}


//接口函数
char CBatObject::obj_Interface(send_node_buf_t* snbt)
{
	return m_recv_list.AddHead(*snbt);
}


// 设置属性
void CBatObject::obj_set_attribution(uchar mac_id, uchar ups_id)
{ 
	char tmp[10] = {0};
	m_mac_id = mac_id; 
	m_ups_id = ups_id; 
	sprintf_s(tmp,sizeof(tmp),"%d%d",m_mac_id,m_ups_id);
	
	m_key = tmp;
}


//处理写指令
char CBatObject::obj_handle_response(curr_action_params_t* recved, curr_action_buf_t* action)
{
	char tmp[10] = {0};
	char msg[100] = {0};

	if(!recved || !action) return 0;

	if(0x01 != recved->buf[0])
	{
		//输出信息
		char msg[100] = {0};
		sprintf(msg,"command [%x] failed: ",action->action.parameter);
		for (int i=0;i<recved->buf_len; i++)
		{
			sprintf(msg+ strlen(msg),"%02x ",recved->buf[i]);
		}
		OutputDebugString(msg);

		g_main_log->add_log(LL_WARNING,m_mac_id,m_ups_id,"指令[%x]执行失败:%d",action->action.parameter,recved->buf[0]);

		return 1;
	}


	ATS_sys_info_t* asit = &(m_asit);

	//初始化过程
	if(WPARAM_INIT == action->wparam)
	{
		//已经改变了输出模式,并且执行成功
		if(OBJ_ID_DATA_UP_MODE_GFKD == action->action.parameter)
		{

			asit->sitEX.sysEX_bms_output_mode = action->action.buf[0];
			//获取工作状态
			ATS_bms_mode(NULL,NZ_ACT_READ,BMS_MODE_NET_GFKD,NULL,WPARAM_INIT);

		}
	}


	//互联网模式下下,查询充电电流
	if(OBJ_ID_BATTERY_MODULE_ENABLE == action->action.parameter && NET_CALLBACK_QUERY_CHARGING == action->wparam){
		uchar da[4] = {0};
		ATS_charging_current(0,NZ_ACT_READ,da,0,action->wparam);
	}

	//互联网模式下下,设置充电电流
	else if(OBJ_ID_BATTERY_MODULE_ENABLE == action->action.parameter && NET_CALLBACK_SET_CHARGING == action->wparam){

		ATS_charging_current(0,NZ_ACT_WRITE,(uchar*)action->data,0,
				NET_CALLBACK_SET_CHARGING,action->lparam,(char*)action->data);
	}

	 /*不在相应指令的执行结果*/
	else if (OBJ_ID_SYSTEM_CONTROL == action->action.parameter && WPARAM_MODE_SWITCH == action->wparam)
	{
		ATS_bms_mode(NULL,NZ_ACT_WRITE,0);
	}

	//放电时，将满电压置为正常
	else if(OBJ_ID_SYSTEM_CONTROL == action->action.parameter && WPARAM_NET_SYSTEM_CONTROL == action->wparam)
	{
		if(1 == action->lparam )
		{
			obj_battery_reset2normal(0);
		}
		
	}
	 
	//电池模块使能控制
	else if (OBJ_ID_BATTERY_MODULE_ENABLE == action->action.parameter /*&& WPARAM_BATTER_ENABLE ==action->wparam*/)
	{
		for ( uchar i=0; i<4; i++)
		{
			if(action->action.buf[1] & (0x01<<i))
				asit->sit.mod[i].join_discha = 1;
			else 
				asit->sit.mod[i].join_discha = 0;
		}

		if(WPARAM_NET_STOP_CHAR_2 ==action->wparam)
			ATS_system_control_GFKD(NULL,4,NULL,WPARAM_NET_SYSTEM_CONTROL,action->lparam);
	}

	else if (OBJ_ID_CHARGING_CURRENT == action->action.parameter && NET_CALLBACK_SET_CHARGING ==action->wparam)
	{
		uchar tmpd[4] = {0};

		for(int i=0; i<4; i++) tmpd[i] = action->data[i];

		asit->sit.mod[action->lparam].attr.current.charge 	= tmpd[2]*0x100 + tmpd[3];
		asit->sit.mod[action->lparam].attr.voltage.charge 	= tmpd[0]*0x100 + tmpd[1];
	}

	//UPS模式下设置充电电流、电压
	else if (OBJ_ID_CHARGING_CURRENT == action->action.parameter && UPS_CALLBACK_SET_CHARGING == action->wparam)	
	{
		asit->sitEX.sysEX_charge_current = action->action.buf[2]*0x100 + action->action.buf[3];
		asit->sitEX.sysEX_charge_voltage = action->action.buf[0]*0x100 + action->action.buf[1];
	}

	//设置工作模式
	if (OBJ_ID_BMS_MODE == action->action.parameter)
	{
		asit->sitEX.sysEX_bms_mode = action->action.buf[0];
		obj_batter_stich(0);
	}

	//更改消息上传模式
	else if(OBJ_ID_DATA_UP_MODE_GFKD == action->action.parameter)	//
	{
		asit->sitEX.sysEX_bms_output_mode = action->action.buf[0];
	}

	//屏蔽电池
	else if (OBJ_ID_BATTERY_MASK_0 == action->action.parameter || OBJ_ID_BATTERY_MASK_1 == action->action.parameter || 
		OBJ_ID_BATTERY_MASK_2 == action->action.parameter || OBJ_ID_BATTERY_MASK_3 == action->action.parameter )
	{

		char bat_id = action->lparam;
		char id = action->action.parameter - OBJ_ID_BATTERY_MASK_0;

		if(WPARAM_BAT_MASK == action->wparam){
			//电池屏蔽
			asit->sit.mod[id].bat_unit[bat_id/4].battery[bat_id%4].attr.isMask = 1;
		}
		else if (WPARAM_BAT_UNMASK == action->wparam){
			//取消屏蔽
			asit->sit.mod[id].bat_unit[bat_id/4].battery[bat_id%4].attr.isMask = 0;
		}
		else if (WPARAM_BAT_MASK_EX == action->wparam)
		{
			for (int i=0; i< 16; i++)
			{
				if((action->lparam) & (1<<i))	
					asit->sit.mod[id].bat_unit[i/4].battery[i%4].attr.isMask = 0;
				else
					asit->sit.mod[id].bat_unit[i/4].battery[i%4].attr.isMask = 1;

			}//for (int i=0; i< 16; i++)
		}
	}

	//电池已经充满
	else if (OBJ_ID_BATTERY_STATUS_0_EX == action->action.parameter || 
			 OBJ_ID_BATTERY_STATUS_1_EX == action->action.parameter ||
			 OBJ_ID_BATTERY_STATUS_2_EX == action->action.parameter ||
			 OBJ_ID_BATTERY_STATUS_3_EX == action->action.parameter)
	{
		if(WPAPAM_NET_STOP_CHAR_1 == action->wparam)
			ATS_bat_mod_select(NULL,NZ_ACT_WRITE,uchar(1<<(action->lparam)),NULL,WPARAM_NET_STOP_CHAR_2);

		//UPS转换成NET模式，要重置电池状态
		else if (WPARAM_MODE_SWITCH_STATE_CHANGE == action->wparam)
		{
			if(action->lparam < 4)
			{
				obj_batter_stich(action->lparam+1);
			}
		}

		//在放电时，将满电压状态置为正常
		else if (WPARAM_DISCHARGING_STATE_CHANGE == action->wparam)
		{
			if(action->lparam < 4)
			{
				obj_battery_reset2normal(action->lparam+1);
			}
		}
	}

	else if (OBJ_ID_MIN_DISCHARGE_UNIT == action->action.parameter)
	{
		asit->sitEX.sysEX_min_discha_unit = action->action.buf[0];
	}

	//debug模式
	if (OBJ_ID_DATA_UP_MODE_GFKD == action->action.parameter)
	{
		asit->sitEX.sysEX_bms_output_mode = action->action.buf[0];
	}

	return 1;
}


// 解析信息
char CBatObject::obj_parse_message(curr_action_params_t* recved, curr_action_buf_t* action)
{
	ATS_sys_info_t* asit = &m_asit;

	if(!recved || !action)return 0;

	switch(recved->parameter)
	{
	case OBJ_ID_SYS_VERSION:	//获取系统版本号

		memcpy(asit->sitEX.sysEX_version,recved->buf,4);
		break;

	case OBJ_ID_PRODUCT_NAME:	//产品名称

		memcpy(asit->sitEX.sysEX_product_name,recved->buf,4);
		break;

	case OBJ_ID_SW_VERSION:		//系统软件版本号

		memcpy(asit->sitEX.sysEX_soft_version,recved->buf,4);
		break;

	case OBJ_ID_BATTERY_MODULE_ENABLE:	//电池模块使能控制
		{
			uchar sta = recved->buf[1]; 
			
			for ( uchar i=0; i<4; i++)
			{
				if(sta & (0x1<<i)) 
					asit->sit.mod[i].join_discha = 1;

				else 
					asit->sit.mod[i].join_discha = 0;
			}
		}
		break;

	case OBJ_ID_CHARGING_CURRENT:		//设置充电电流电压
		{
			if(BMS_MODE_NET_GFKD == asit->sitEX.sysEX_bms_mode)
			{
				asit->sit.mod[action->wparam].attr.voltage.charge	= (recved->buf[0])*0x100 + recved->buf[1];
				asit->sit.mod[action->wparam].attr.current.charge		= (recved->buf[2])*0x100 + recved->buf[3];
			}
			else{

				asit->sitEX.sysEX_charge_voltage = (recved->buf[0])*0x100 + recved->buf[1];
				asit->sitEX.sysEX_charge_current = (recved->buf[2])*0x100 + recved->buf[3];
			}
		}
		break;

	case OBJ_ID_BMS_MODE:				//系统工作模式

		asit->sitEX.sysEX_bms_mode = recved->buf[0];
		break;

	case OBJ_ID_SYSTEM_CONTROL:	//系统控制
		{
			uchar sta = (recved->buf[3])& 0x07;
			///sql 
			memcpy(asit->sitEX.sysEX_mode_control,recved->buf,sizeof(asit->sitEX.sysEX_mode_control));
		}
		break;

	case OBJ_ID_BATTERY_MASK_0:	//电池屏蔽控制
	case OBJ_ID_BATTERY_MASK_1:
	case OBJ_ID_BATTERY_MASK_2:
	case OBJ_ID_BATTERY_MASK_3:
		{
			uchar id = recved->parameter - OBJ_ID_BATTERY_MASK_0;
			uint tmp = (recved->buf[2])*0x100 + recved->buf[3];
			for (int i=0; i<16; i++)
			{
				if(BMS_MODE_NET_GFKD == asit->sitEX.sysEX_bms_mode)
				{
					if(tmp & (1<<i))
						asit->sit.mod[id].bat_unit[i/4].battery[i%4].attr.isMask = 0;
					else
						asit->sit.mod[id].bat_unit[i/4].battery[i%4].attr.isMask = 1;
				}
				else
				{
					if(tmp & (1<<i))
						asit->sit.mod[id].bat_unit[i/4].battery[i%4].attr.isMask = 1;
					else
						asit->sit.mod[id].bat_unit[i/4].battery[i%4].attr.isMask = 0;
				}

			}

			uchar data[4] = {0};
			//要进行屏蔽
			if (WPARAM_BAT_MASK == action->wparam)
			{
				uint bat_id = action->lparam;
				if(BMS_MODE_NET_GFKD == asit->sitEX.sysEX_bms_mode)	//互联网模式
					tmp = tmp&(~(1<<bat_id));
				else
					tmp = tmp|(1<<bat_id);

				data[2] = (tmp&0xff00)>>8;
				data[3] = tmp & 0x00ff;

				ATS_battery_mask(NULL,NZ_ACT_WRITE,id,data,NULL,WPARAM_BAT_MASK,(uint)bat_id);
			}
			else if (WPARAM_BAT_UNMASK == action->wparam)	//取消屏蔽
			{
				uint bat_id = action->lparam;
				if(BMS_MODE_NET_GFKD == asit->sitEX.sysEX_bms_mode)	//互联网模式
					tmp = tmp|(1<<bat_id);
				else
					tmp = tmp&(~(1<<bat_id));

				data[2] = (tmp&0xff00)>>8;
				data[3] = tmp & 0x00ff;
				
				ATS_battery_mask(NULL,NZ_ACT_WRITE,id,data,NULL,WPARAM_BAT_UNMASK,(uint)bat_id);
			}
			
		}
		
		break;
		
	case OBJ_ID_MIN_DISCHARGE_UNIT:		//最小放电单元
		{
			if(BMS_MODE_UPS_GFKD == asit->sitEX.sysEX_bms_mode)
				asit->sitEX.sysEX_min_discha_unit = recved->buf[0];
		}
		break;
	case OBJ_ID_CURRENT_0_EX:	//电池充、放电电流
	case OBJ_ID_CURRENT_1_EX:				
	case OBJ_ID_CURRENT_2_EX:				
	case OBJ_ID_CURRENT_3_EX:				
	case OBJ_ID_CURRENT_4_EX:				
	case OBJ_ID_CURRENT_5_EX:				
	case OBJ_ID_CURRENT_6_EX:				
	case OBJ_ID_CURRENT_7_EX:				
	case OBJ_ID_CURRENT_8_EX:				
	case OBJ_ID_CURRENT_9_EX:				
	case OBJ_ID_CURRENT_10_EX:			
	case OBJ_ID_CURRENT_11_EX:			
	case OBJ_ID_CURRENT_12_EX:				
	case OBJ_ID_CURRENT_13_EX:				
	case OBJ_ID_CURRENT_14_EX:				
	case OBJ_ID_CURRENT_15_EX:				
	case OBJ_ID_CURRENT_16_EX:				
	case OBJ_ID_CURRENT_17_EX:				
	case OBJ_ID_CURRENT_18_EX:				
	case OBJ_ID_CURRENT_19_EX:				
	case OBJ_ID_CURRENT_20_EX:				
	case OBJ_ID_CURRENT_21_EX:				
	case OBJ_ID_CURRENT_22_EX:				
	case OBJ_ID_CURRENT_23_EX:				
	case OBJ_ID_CURRENT_24_EX:				
	case OBJ_ID_CURRENT_25_EX:				
	case OBJ_ID_CURRENT_26_EX:				
	case OBJ_ID_CURRENT_27_EX:				
	case OBJ_ID_CURRENT_28_EX:				
	case OBJ_ID_CURRENT_29_EX:				
	case OBJ_ID_CURRENT_30_EX:				
	case OBJ_ID_CURRENT_31_EX:				
		{
			uchar id = recved->parameter - OBJ_ID_CURRENT_0_EX;
			uchar modID = id/8;
			uchar batID = (id%8)*2;

			asit->sit.mod[modID].bat_unit[batID/4].battery[batID%4].attr.current.charge = 
				recved->buf[0]*0x100 + recved->buf[1];

			asit->sit.mod[modID].bat_unit[(batID+1)/4].battery[(batID+1)%4].attr.current.charge = 
				recved->buf[2]*0x100 + recved->buf[3];

			if(OBJ_ID_CURRENT_31_EX == recved->parameter)
				obj_calculation_current();

			if(14 == batID)
			{
				obj_check_battery_state(modID);
			}

		}
		break;
	case OBJ_ID_BATTERY_VOLTAGE_0_EX:	//电池单元当前的电压
	case OBJ_ID_BATTERY_VOLTAGE_1_EX:
	case OBJ_ID_BATTERY_VOLTAGE_2_EX:
	case OBJ_ID_BATTERY_VOLTAGE_3_EX:
	case OBJ_ID_BATTERY_VOLTAGE_4_EX:
	case OBJ_ID_BATTERY_VOLTAGE_5_EX:
	case OBJ_ID_BATTERY_VOLTAGE_6_EX:
	case OBJ_ID_BATTERY_VOLTAGE_7_EX:
	case OBJ_ID_BATTERY_VOLTAGE_8_EX:
	case OBJ_ID_BATTERY_VOLTAGE_9_EX:
	case OBJ_ID_BATTERY_VOLTAGE_10_EX:
	case OBJ_ID_BATTERY_VOLTAGE_11_EX:
	case OBJ_ID_BATTERY_VOLTAGE_12_EX:
	case OBJ_ID_BATTERY_VOLTAGE_13_EX:
	case OBJ_ID_BATTERY_VOLTAGE_14_EX:
	case OBJ_ID_BATTERY_VOLTAGE_15_EX:
	case OBJ_ID_BATTERY_VOLTAGE_16_EX:
	case OBJ_ID_BATTERY_VOLTAGE_17_EX:
	case OBJ_ID_BATTERY_VOLTAGE_18_EX:
	case OBJ_ID_BATTERY_VOLTAGE_19_EX:
	case OBJ_ID_BATTERY_VOLTAGE_20_EX:
	case OBJ_ID_BATTERY_VOLTAGE_21_EX:
	case OBJ_ID_BATTERY_VOLTAGE_22_EX:
	case OBJ_ID_BATTERY_VOLTAGE_23_EX:
	case OBJ_ID_BATTERY_VOLTAGE_24_EX:
	case OBJ_ID_BATTERY_VOLTAGE_25_EX:
	case OBJ_ID_BATTERY_VOLTAGE_26_EX:
	case OBJ_ID_BATTERY_VOLTAGE_27_EX:
	case OBJ_ID_BATTERY_VOLTAGE_28_EX:
	case OBJ_ID_BATTERY_VOLTAGE_29_EX:
	case OBJ_ID_BATTERY_VOLTAGE_30_EX:
	case OBJ_ID_BATTERY_VOLTAGE_31_EX:
		{
			uchar id = recved->parameter - OBJ_ID_BATTERY_VOLTAGE_0_EX;
			uchar modID = id/8;
			uchar batID = (id%8)*2;
			asit->sit.mod[modID].bat_unit[batID/4].battery[batID%4].attr.voltage.charge = 
				recved->buf[0]*0x100 + recved->buf[1];

			asit->sit.mod[modID].bat_unit[(batID+1)/4].battery[(batID+1)%4].attr.voltage.charge = 
				recved->buf[2]*0x100 + recved->buf[3];

			if(14 == batID)
			{
				obj_check_battery_state(modID);
			}

			//最后一节电池
			if(OBJ_ID_BATTERY_VOLTAGE_31_EX == recved->parameter)
			{
				obj_calculation_voltage();
			}

			if(OBJ_ID_BATTERY_VOLTAGE_7_EX == recved->parameter || OBJ_ID_BATTERY_VOLTAGE_15_EX == recved->parameter
				||OBJ_ID_BATTERY_VOLTAGE_23_EX == recved->parameter || OBJ_ID_BATTERY_VOLTAGE_31_EX == recved->parameter)
			{

				if(S_EX_STATE_CHARG == asit->sitEX.sysEX_state)
				{	
					obj_battery_charging_state_set(modID,1);
					//obj_battery_charging_state_set(modID,1);
				}
				else
				{
					obj_battery_charging_state_set(modID,0);
					//obj_battery_charging_state_set(modID,0);
				}
			}
		}
		break;
	
	case OBJ_ID_BATTERY_SOC_0_EX:		
	case OBJ_ID_BATTERY_SOC_1_EX:			
	case OBJ_ID_BATTERY_SOC_2_EX:			
	case OBJ_ID_BATTERY_SOC_3_EX:			
	case OBJ_ID_BATTERY_SOC_4_EX:			
	case OBJ_ID_BATTERY_SOC_5_EX:			
	case OBJ_ID_BATTERY_SOC_6_EX:			
	case OBJ_ID_BATTERY_SOC_7_EX:			
	case OBJ_ID_BATTERY_SOC_8_EX:			
	case OBJ_ID_BATTERY_SOC_9_EX:			
	case OBJ_ID_BATTERY_SOC_10_EX:			
	case OBJ_ID_BATTERY_SOC_11_EX:			
	case OBJ_ID_BATTERY_SOC_12_EX:			
	case OBJ_ID_BATTERY_SOC_13_EX:			
	case OBJ_ID_BATTERY_SOC_14_EX:			
	case OBJ_ID_BATTERY_SOC_15_EX:		
		{
			uchar id = recved->parameter - OBJ_ID_BATTERY_SOC_0_EX;
			uchar modID = id/4;
			uchar batID = id%4;
			asit->sitEX.mod[modID].bat_unit[batID].battery[0].vst.soc = recved->buf[0];
			asit->sitEX.mod[modID].bat_unit[batID].battery[1].vst.soc = recved->buf[1];
			asit->sitEX.mod[modID].bat_unit[batID].battery[2].vst.soc = recved->buf[2];
			asit->sitEX.mod[modID].bat_unit[batID].battery[3].vst.soc = recved->buf[3];
		}
		break;
	//电池状态
	case OBJ_ID_BATTERY_STATUS_0_EX:
	case OBJ_ID_BATTERY_STATUS_1_EX:
	case OBJ_ID_BATTERY_STATUS_2_EX:
	case OBJ_ID_BATTERY_STATUS_3_EX:

		{
			uchar id = recved->parameter - OBJ_ID_BATTERY_STATUS_0_EX;

			int sys_code =0;
			for (int i=0; i<4; i++)
			{
				asit->sitEX.mod[id].bat_unit[3-i].battery[0].batEX_state = (recved->buf[i])&0x03;
				asit->sitEX.mod[id].bat_unit[3-i].battery[1].batEX_state = ((recved->buf[i])&0x0C)>>2;
				asit->sitEX.mod[id].bat_unit[3-i].battery[2].batEX_state = ((recved->buf[i])&0x30)>>4;
				asit->sitEX.mod[id].bat_unit[3-i].battery[3].batEX_state = ((recved->buf[i])&0xC0)>>6;
	
			}
			memcpy(asit->sitEX.mod[id].battery_state,recved->buf,4);
		}
		break;
	////电池开关状态
	case OBJ_ID_SWITCH_STATUS_0_EX:
		{
			int tmp = recved->buf[2]*0x100 + recved->buf[3];
			for (int j=0; j<16; j++)
			{
				if(tmp&(1<<j))
					asit->sit.mod[0].bat_unit[j/4].battery[j%4].attr.sw = 1;
				else
					asit->sit.mod[0].bat_unit[j/4].battery[j%4].attr.sw = 0;
			}

			tmp = recved->buf[0]*0x100 + recved->buf[1];
			for (int j=0; j<16; j++)
			{
				if(tmp&(1<<j))
					asit->sit.mod[1].bat_unit[j/4].battery[j%4].attr.sw = 1;
				else
					asit->sit.mod[1].bat_unit[j/4].battery[j%4].attr.sw = 0;		
			}
			
		}
		break;
	case OBJ_ID_SWITCH_STATUS_1_EX:
		{
			int tmp = recved->buf[2]*0x100 + recved->buf[3];
			for (int j=0; j<16; j++)
			{
				if(tmp&(1<<j))
					asit->sit.mod[2].bat_unit[j/4].battery[j%4].attr.sw = 1;
				else
					asit->sit.mod[2].bat_unit[j/4].battery[j%4].attr.sw = 0;
			}

			tmp = recved->buf[0]*0x100 + recved->buf[1];
			for (int j=0; j<16; j++)
			{
				if(tmp&(1<<j))
					asit->sit.mod[3].bat_unit[j/4].battery[j%4].attr.sw = 1;
				else
					asit->sit.mod[3].bat_unit[j/4].battery[j%4].attr.sw = 0;
			}
		}
		break;
	//温度
	case OBJ_ID_MODULE_TEMP_EX:
		{
			asit->sit.mod[0].temperature = recved->buf[0];
			asit->sit.mod[1].temperature = recved->buf[1];
			asit->sit.mod[2].temperature = recved->buf[2];
			asit->sit.mod[3].temperature = recved->buf[3];
		}
		break;
	//系统状态
	case OBJ_ID_SYSTEM_STATUS_EX:
		{
			asit->sitEX.sysEX_state = recved->buf[0];
			if(S_EX_STATE_CHARG != asit->sitEX.sysEX_state)
				for(int i=0; i<4;i++)
				{
					asit->sitEX.mod[i].mod_flag = 0;
					asit->sitEX.mod[i].mod_flag_count = 0;
				}
			obj_Store();
		}
		break;
	case OBJ_ID_SYSTEM_ERROR_EX:
		{
			char err[200] = {0};

			m_asit.sitEX.sysEX_error = recved->buf[0];
			if(m_asit.sitEX.sysEX_error  > 0)
			{
				if(recved->buf[0] &(1<<0))
					strcpy(err,"电池故障");
				if(recved->buf[0] &(1<<1))
					strcat(err+strlen(err),"  低压告警");

				if(recved->buf[0] &(1<<2))
					strcat(err+strlen(err),"  短路告警 [请重启机器]");
				
				if(recved->buf[0] &(1<<3))
					strcat(err+strlen(err),"  高温告警");
				
				if(recved->buf[0] &(1<<4))
					strcat(err+strlen(err),"  过温保护");

				if(recved->buf[0] &(1<<5))
					strcat(err+strlen(err),"  过流保护");

				if(recved->buf[0] &(1<<6))
					strcat(err+strlen(err),"  同步停止");

				if(strlen(err)>0)
					g_main_log->add_log(LL_ERROR,m_mac_id,m_ups_id,"%s",err);
				
			}
		}
		break;

	case OBJ_ID_DATA_PACKET:	//数据包
		sql_trace("数据包\n");
		break;
	default:
		sql_trace("未知的数据类型:%02x\n",recved->parameter);
	}

	return 1;
}

//保存数据
void CBatObject::obj_Store()
{
	CCPreStore cps(m_mac_id,m_ups_id,m_asit.sitEX.sysEX_state);

	for(int i = 0; i<4; i++)
	for(int j = 0; j<4; j++)
	for (int z=0; z<4; z++)
	{
		//asit->sit.mod[modID].bat_unit[batID/4].battery[batID%4].attr.current.charge = 
		//recved->buf[0]*0x100 + recved->buf[1];
		cps.cps_SetBatInfo(i*16+j*4+z,m_asit.sit.mod[i].bat_unit[j].battery[z].attr.current.charge,
			m_asit.sit.mod[i].bat_unit[j].battery[z].attr.voltage.charge,
			m_asit.sitEX.mod[i].bat_unit[j].battery[z].vst.soc,
			m_asit.sit.mod[i].bat_unit[j].battery[z].attr.sw
			);
	}
	cps.cps_Store();
}


// 根据电池单体屏蔽情况计算最大电压
char CBatObject::obj_calculation_current(range_value_t* rvt, uchar mod_id)
{
	uint unit[4] = {0,0,0,0};
	uint ma = 0;
	if(!rvt || mod_id >3)return 0;

	ATS_sys_info_t* asit = &m_asit;
	if(!asit)return 0;

	for (char i=0; i<4; i++)
	{
		for (char j=0; j<4; j++)
		{
			if(0 == asit->sit.mod[mod_id].bat_unit[i].battery[j].attr.isMask)
				unit[i] += 850;
		}//for (char j=0; j<4; j++)
	}
	rvt->mMax = obj_calculation_min(unit);
	//计算最小电流
	for (char i=0; i<4; i++)
	{
		if(ma <= unit[i])
			ma = unit[i];
	}
	if(ma > 0)
	{
		rvt->mMin = 330;
	}

	return 1;
}


// 获取最小值
uint CBatObject::obj_calculation_min(uint val[4])
{
	uint tmp[4] = {0,0,0,0}, res;
	char c = 0, i=0;

	for (i=0; i<4; i++)
	{
		if(0 != val[i])
			tmp[c++] = val[i];
	}
	if(0 == c)return 0;

	res = tmp[0];
	for (i=1; i<c; i++)
		if(res>tmp[i])res = tmp[i];

	return res;

}


 // 计算系统电流电压
char CBatObject::obj_calculation_current()
{

	uint unit[4] = {0,0,0,0};	//四个unit
	uint mod[4] = {0,0,0,0};
	uint tmpsum=0;
	int i=0, j=0;

	ATS_sys_info_t* asit = &m_asit;

	asit->sitEX.sysEX_charge_current  = 0;

	for (i=0; i<4; i++)	//四个模块
	{
		for (j=0; j<4; j++)
		{
			unit[j] = asit->sit.mod[i].bat_unit[j].battery[0].attr.current.charge + 
					  asit->sit.mod[i].bat_unit[j].battery[1].attr.current.charge +
					  asit->sit.mod[i].bat_unit[j].battery[2].attr.current.charge +
					  asit->sit.mod[i].bat_unit[j].battery[3].attr.current.charge ;
		}
		
		//取最小的 不为0的数值
		asit->sitEX.mod[i].modCurrent = (mod[i] = obj_calculation_min(unit));
	}	

	for (i=0; i<4; i++)
		asit->sitEX.sysEX_charge_current += mod[i]; 

	obj_send_msg(MSG_UPS_CURVE_DATA,m_mac_id,m_ups_id);
	return 1;
}


//查询能源互联网模式下,电池是否已经充满
void CBatObject::obj_check_battery_state(uchar mid)
{
	uchar buf[4] = {0};
	unsigned int params = 0x00;
	char flag = 0, stop_flag = 0;


	//获取信息
	ATS_sys_info_t* asit = &m_asit;
	if(BMS_MODE_NET_GFKD == asit->sitEX.sysEX_bms_mode)
	{
		{
			memcpy(buf,asit->sitEX.mod[mid].battery_state,sizeof(buf));

			for(int i = 0; i<4; i++)
			{
				for ( int j=0; j<4; j++)
				{

					if(obj_check_bat_high(mid,i,j) && 
						/*B_EX_STATE_CUR_HI != asit->sitEX.mod[mid].bat_unit[i].battery[j].batEX_state &&*/
						1 == asit->sit.mod[mid].bat_unit[i].battery[j].attr.sw)
					{

						//已经充满
						g_main_log->add_log(LL_NOTICE,m_mac_id,m_ups_id,"模块%d 第%d号电池 已经充满[current: %d  voltage: %d]",
						mid+1,i*4+j+1,asit->sit.mod[mid].bat_unit[i].battery[j].attr.current.charge,
						asit->sit.mod[mid].bat_unit[i].battery[j].attr.voltage.charge);
						int mask = 0xff^(0x03<<(j*2));
						buf[3-i] &= mask;
						buf[3-i] |= (1<<(j*2));


						flag = 1;
						stop_flag = 1;
					}
					else if(obj_check_bat_low(mid,i,j) &&
						/*B_EX_STATE_CUR_LO != asit->sitEX.mod[mid].bat_unit[i].battery[j].batEX_state &&*/
						1 == asit->sit.mod[mid].bat_unit[i].battery[j].attr.sw)
					{

						int mask = 0xff^(0x03<<(j*2));
						buf[3-i] &= mask;
						buf[3-i] |= (2<<(j*2));

						flag = 1;
					}
					else if(S_EX_STATE_NORMAL == asit->sitEX.sysEX_state && 
						(B_EX_STATE_NORMAL !=asit->sitEX.mod[mid].bat_unit[i].battery[j].batEX_state && 
						 B_EX_STATE_ERROR != asit->sitEX.mod[mid].bat_unit[i].battery[j].batEX_state)
						 && 1 == asit->sit.mod[mid].bat_unit[i].battery[j].attr.sw)
					{
						int mask = 0xff^(0x03<<(j*2));
						buf[3-i] &= mask;
			
						flag = 1;
					} //else
				} //for(int i = 0; i<4; i++)
			}
		
			if(flag)
			{
				if(stop_flag)
					ATS_battery_charge_state(NULL,NULL,mid,buf,WPAPAM_NET_STOP_CHAR_1,(uint)mid);
				else
					ATS_battery_charge_state(NULL,NULL,mid,buf);
			}
		}
	} //if(BMS_MODE_NET_GFKD == asit->sitEX.sysEX_bms_mode)
}


// 检测是否满电压
bool CBatObject::obj_check_bat_high(uchar mid, uchar uid, uchar bid)
{
	ATS_sys_info_t* asit = &m_asit;

	//充满电的条件: 电流小于230毫安, 电压大于3550毫伏
	if(((asit->sit.mod[mid].bat_unit[uid].battery[bid].attr.voltage.charge >= 3550 &&
		asit->sit.mod[mid].bat_unit[uid].battery[bid].attr.current.charge <= 230) || 
		asit->sit.mod[mid].bat_unit[uid].battery[bid].attr.voltage.charge >= 3650) && 
		S_EX_STATE_CHARG == asit->sitEX.sysEX_state &&
		1 == asit->sit.mod[mid].bat_unit[uid].battery[bid].attr.sw)
	{
		return true;
	}

	return false;
}


// 检测是否低电压
bool CBatObject::obj_check_bat_low(uchar mid, uchar uid, uchar bid)
{
	ATS_sys_info_t* asit = &m_asit;

	if ((asit->sit.mod[mid].bat_unit[uid].battery[bid].attr.voltage.charge <= 2600 &&
		asit->sit.mod[mid].bat_unit[uid].battery[bid].attr.voltage.charge >0) && 
		S_EX_STATE_DISCHARGE == asit->sitEX.sysEX_state &&
		1 == asit->sit.mod[mid].bat_unit[uid].battery[bid].attr.sw)
	{
		return true;
	}

	return false;
}


//根据电池单体屏蔽情况计算最大电压
char CBatObject::obj_calculation_voltage(range_value_t* rvt, uchar mod_id)
{
	char tmp[100] = {0};
	if(!rvt || mod_id >3)return 0;

	ATS_sys_info_t* asit = &(m_asit);

	rvt->mCurr = rvt->mMin = 0; rvt->mMax = 0;

	for (char i=0; i<4; i++)
	for (char j=0; j<4; j++){

		if(0 == asit->sit.mod[mod_id].bat_unit[i].battery[j].attr.isMask){
			
				rvt->mMin +=3.4*1000;
				rvt->mMax += 3.65*1000;
				break;
		}//if(0 == asit->sit.mod[mod_id].bat_unit[i].battery[j].attr.isMask){
	}//for (char j=0; j<4; j++)

	return 1;
}


// 检测并联电池之间的电压差
char CBatObject::obj_check_paralleling_diff(char mid)
{
	uint m_min = 0, m_max = 0;
	char tmp[100] = {0};
	char min_id = 0, max_id = 0;
	char flag = 0;

	for (int uid = 0; uid<4; uid++)
	{
		flag = 0;
		m_max = m_min = 0;

		for (int i=0; i<4; i++)
		{
			if(1 == m_asit.sit.mod[mid].bat_unit[uid].battery[i].attr.sw || 
				0 == m_asit.sit.mod[mid].bat_unit[uid].battery[i].attr.isMask)
				
				flag++;
		}

		//只选择的一节电池
		if(flag<2) continue;

		for (int i=0; i<4; i++)
		{

			if(1 == m_asit.sit.mod[mid].bat_unit[uid].battery[i].attr.sw || 
				0 == m_asit.sit.mod[mid].bat_unit[uid].battery[i].attr.isMask)
			{

				if(!m_min || m_min > m_asit.sit.mod[mid].bat_unit[uid].battery[i].attr.voltage.charge) 
				{
					min_id = i;
					m_min = m_asit.sit.mod[mid].bat_unit[uid].battery[i].attr.voltage.charge;
				}

				if(!m_max || m_max < m_asit.sit.mod[mid].bat_unit[uid].battery[i].attr.voltage.charge) 
				{
					max_id = i;
					m_max = m_asit.sit.mod[mid].bat_unit[uid].battery[i].attr.voltage.charge;
				}
			}
		}//for (int i=0; i<4; i++)

		//之间的电压差大于300mV,是否继续？
		if(((m_max - m_min) >= 300) && max_id != min_id)
		{
			if(strlen(tmp) > 0)
			{
				sprintf_s(tmp+strlen(tmp)
					,sizeof(tmp) - strlen(tmp),";%d号与%d号电池",uid*4+min_id+1,uid*4+max_id+1);
			}
			else
			{
				sprintf_s(tmp+strlen(tmp)
					,sizeof(tmp) - strlen(tmp),"%d号与%d号电池",uid*4+min_id+1,uid*4+max_id+1);
			}
		}

	}


	if(strlen(tmp) > 0)
	{
		sprintf_s(tmp+strlen(tmp)
			,sizeof(tmp) - strlen(tmp),"之间的电压差大于300mV,有可能对电池造成损坏,是否继续？");

		if(IDYES == ::MessageBox(NULL,tmp,"提示",MB_YESNO|MB_ICONQUESTION))
		{
			return 1;
		}
		else 
			return 0;
	}

	return 1;
}


//计算系统电流电压
char CBatObject::obj_calculation_voltage()
{

	uint unit[4] = {0,0,0,0};	//四个unit
	uint bat[4] = {0,0,0,0};
	uint mod[4] = {0,0,0,0};
	uint tmpsum=0;
	int i=0, j=0, n=0;

	ATS_sys_info_t* asit = &m_asit;

	asit->sitEX.sysEX_charge_voltage = 0;

	for (i=0; i<4; i++)	//四个模块
	{
		for (j=0; j<4; j++)
		{
			bat[0] = bat[1] = bat[2] = bat[3] = 0;
			//1-4是并联的
			for (n=0; n<4; n++)
			{
				if(1 == asit->sit.mod[i].bat_unit[j].battery[n].attr.sw)
					bat[n] = asit->sit.mod[i].bat_unit[j].battery[n].attr.voltage.charge;
			}

			//获取四个电池的最小不为0的电压值
			unit[j] = obj_calculation_min(bat);
		} //for (j=0; j<4; j++)
		
		//获取四个unit的和
		for (j=0; j<4; j++) 
			mod[i] += unit[j];

		asit->sitEX.mod[i].modVoltage = mod[i];
	}//for (i=0; i<4; i++)	//四个模块

	asit->sitEX.sysEX_charge_voltage = obj_calculation_min(mod);

	obj_send_msg(MSG_UPS_CURVE_DATA,m_mac_id,m_ups_id);

	return 1;
}


// 通知界面
void CBatObject::obj_send_msg(uint UID, WPARAM wparam, LPARAM lparam)
{
	CCNotice* n = CCNotice::GetInstance();

	n->sendMSG(UID,m_mac_id,m_ups_id,wparam,lparam);
}


//指令超时提示
char CBatObject::ATS_timeout(curr_action_buf_t& cab)
{
	if(1 == m_ReSend)
	{
		if(1 == m_curr_action.resend)
		{
			g_main_log->add_log(LL_ERROR,m_mac_id,m_ups_id,"指令%x超时",cab.action.parameter);
			ATS_action_reset();
		}
		else
		{
			ATS_action_resend(1);
			mNet->CN_send(&m_nat,m_curr_action.message,m_curr_action.message_len);
		}
	}
	else
	{
		g_main_log->add_log(LL_ERROR,m_mac_id,m_ups_id,"指令%x超时",cab.action.parameter);
		ATS_action_reset();
	}

	return 1;
}


//有指令正在执行
char CBatObject::ATS_current_busy(curr_action_buf_t* cab)
{
	unsigned int cmd = 0;
	char msg[100] = {0};

	if(!cab) return 0;

	sprintf_s(msg,sizeof(msg),"指令[%x]正在执行过程中，请稍后",cab->action.parameter);
	MessageBox(NULL,msg,"提示",MB_OKCANCEL);

	return 1;
}


// 设置充电标志，用来检测是否是故障电池
char CBatObject::obj_battery_charging_state_set(char mid,char state)
{
	uchar buf[4] = {0};
	time_t tn;
	unsigned long interval = 0;

	/*
	sql_trace("设置充电标志，用来检测是否是故障电池 mid: %d bat_state:%d\n",
		mid+1,state);
		*/

	if(m_ups_id == 00 && m_mac_id == 01 && state == 1)
	{

		int d = 0;
	}

	if(0 == state) 
	{
		for(int uid=0; uid<4; uid++)
		for (int bid=0; bid < 4; bid++)
		{
			m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].bat_state		= 0;
			m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].last_current	= 0;
		}

		return 1;
	}

	/* 
	 * 条件判断：
	 * 1、处于能源互联网模式
	 * 2、充电过程
	 * 3、开关闭合
	 * 4、没有处于检测过程中
	 */
	if(BMS_MODE_NET_GFKD == m_asit.sitEX.sysEX_bms_mode)
	{
		int flag = 0;
		memcpy(buf,m_asit.sitEX.mod[mid].battery_state,sizeof(buf));

		for(int uid=0; uid<4; uid++)
		for(int bid=0; bid<4; bid++)
		{

			/*
			sql_trace("1,设置充电标志，用来检测是否是故障电池 mid: %d  id:%d bat_state:%d\n",
		mid+1,uid*4+bid+1,m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].bat_state);
		*/
			//
			if(0 == m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].bat_state)
			{
				if(1 == m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.sw && 
					S_EX_STATE_CHARG == m_asit.sitEX.sysEX_state)
				{
					m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].bat_state = state;
					time(&(m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].charging_start));

					m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].last_current = 
						m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.voltage.charge;

				}
			}
			else
			{
				time(&tn);

				interval = tn - m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].charging_start;

				//////////////////////////////////////////////////////////////////////////
				//检测故障电池

				char tmp[100] = {0};
				sprintf(tmp,"2,开始检测故障电池(mod:%d id:%d) sw: %d\n",mid+1,uid*4+bid+1, m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.sw);
				OutputDebugString(tmp);

				if(interval > (5 * 60))
				{
					sql_trace("3,开始检测故障电池(mod:%d id:%d) sw: %d\n",mid+1,uid*4+bid+1, m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.sw);
				}


				if(interval > (5 * 60) && 1 == m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.sw)
				{
					if(m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.voltage.charge < 3000)
					{
						g_main_log->add_log(LL_WARNING,m_mac_id,m_ups_id,"4,检测到模块%d %02d号电池为故障电池,(充电5min后,电压为:%d)\n",
							mid+1,uid*4+bid+1,m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.voltage.charge);
						if(BAT_STATUS_ERROR != m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.state)
						{
							int mask = 0xff^(0x03<<(bid*2));
							buf[3-uid] &= mask;
							buf[3-uid] |= (0x03<<(bid*2));
							flag |= 2;
						}
					}
					else
					{

						m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].bat_state = 0;

						//小电压充电，一段时间后电压恢复正常
						if(BAT_STATUS_ERROR == m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].batEX_state
							/* m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.state*/)
						{

							int mask = 0xff^(0x03<<(bid*2));
							buf[3-uid] &= mask;
							flag |= 1;

						} //if(BAT_STATUS_ERROR == m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.state)

					}
				}//interval > (5 * 60) && 1 == m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.sw
			}//else
		}//for(int bid=0; bid<4; bid++)

		if(3 == flag || 2 == flag)	//需要关机
		{
			ATS_battery_charge_state(NULL,NULL,mid,buf,WPAPAM_NET_STOP_CHAR_1,(uint)mid);
		}
		else if(1 == flag)
		{
			ATS_battery_charge_state(NULL,NULL,mid,buf);
		}
	}

	return 1;
}	


///
void CBatObject::obj_batter_stich(uchar mid)
{
	bool flag = false;
	uchar buf[4] = {0};
	//超出
	if(mid > 3) return ;

	for(int uid = 0; uid<4; uid++)
	for(int bid = 0; bid<4; bid++)
	{
		m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.sw = 0;
	}

	//只负责能源互联网模式
	if(BMS_MODE_UPS_GFKD == m_asit.sitEX.sysEX_bms_mode) return obj_batter_stich(mid+1);

	memcpy(buf,m_asit.sitEX.mod[mid].battery_state,sizeof(buf));

	for(int uid = 0; uid<4; uid++)
	for(int bid = 0; bid<4; bid++)
	{
		if(B_EX_STATE_CUR_HI == m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].batEX_state ||
		   B_EX_STATE_CUR_LO == m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].batEX_state)
		{
			int mask = 0xff^(0x03<<(bid*2));
			buf[3-uid] &= mask;
			flag = true;
		}
	}

	if(flag)
	{
		ATS_battery_charge_state(NULL,NULL,mid,buf,WPARAM_MODE_SWITCH_STATE_CHANGE,(uint)mid);
	}
	else
	{
		return obj_batter_stich(mid+1);
	}
}


void CBatObject::obj_battery_reset2normal(uchar mid)
{
	char tmp[60] = {0};
	bool flag = false;
	uchar buf[4] = {0};
	//超出
	if(mid > 3) return ;

	

	//只负责能源互联网模式
	if(BMS_MODE_UPS_GFKD == m_asit.sitEX.sysEX_bms_mode) return obj_battery_reset2normal(mid+1);

	if(0 == m_asit.sit.mod[mid].join_discha)
	{
		return obj_battery_reset2normal(mid + 1);
	}

	memcpy(buf,m_asit.sitEX.mod[mid].battery_state,sizeof(buf));

	for(int uid = 0; uid<4; uid++)
	for(int bid = 0; bid<4; bid++)
	{
		if(B_EX_STATE_CUR_HI == m_asit.sitEX.mod[mid].bat_unit[uid].battery[bid].batEX_state &&
			(1 == m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.sw || 
			 0 == m_asit.sit.mod[mid].bat_unit[uid].battery[bid].attr.isMask))
		{
			int mask = 0xff^(0x03<<(bid*2));
			buf[3-uid] &= mask;
			flag = true;
		}
	}

	if(flag)
	{
		//g_main_log->add_log(LL_WARNING,m_mac_id,m_ups_id,"开始放电时，将模块%d切换为空闲",mid);
		ATS_battery_charge_state(NULL,NULL,mid,buf,WPARAM_DISCHARGING_STATE_CHANGE,(uint)mid);
	}
	else
	{
		return obj_battery_reset2normal(mid + 1);
	}
}