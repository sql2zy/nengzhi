#ifndef HEADER_CLA_LIST_SHI_QINGLIANG
#define  HEADER_CLA_LIST_SHI_QINGLIANG

#include <Windows.h>
#include <list>

using namespace std;

template <typename T>
class cla_list
{
public:	
	/***
	 * 构造函数
	 */
	cla_list(void)
	{
		m_eixt = 0;
		if(!flag)
		{
			flag = 1;
			InitializeCriticalSection(&m_lock);
		}
		
	};

	/***
	 * 析构函数
	 */
	~cla_list(void)
	{
		m_list.clear();
		DeleteCriticalSection(&m_lock);
		m_eixt = 1;
	};

public:

	/***
	 * 添加尾部
	 */
	char AddTail(T node);

	/***
	 * 添加头部
	 */
	char AddHead(T node);
	
	/***
	 * 获取尾部
	 * @param	 vct:	容器
	 * @param remove:	是否删除
	 */
	char GetTail(T& vct, char remove = 1);
	
	/***
	 * 获取头部
	 * @param	 vct:	容器
	 * @param remove:	是否删除
	 */
	char GetHead(T& vct, char remove = 1);

	/***
	 * 获取元素个数
	 */
	unsigned int GetCount();

	/***
	 * 初始化
	 */
	void init();

private:

	char m_eixt;

	char flag;

	/***
	 * 锁
	 */
	CRITICAL_SECTION m_lock;

	/***
	 * 链表
	 */
	list<T> m_list;


	/***
	 * 锁定
	 */
	void lock(){if(m_lock.DebugInfo) EnterCriticalSection(&m_lock);};

	/***
	 * 解锁
	 */
	void unlock(){if(m_lock.DebugInfo) LeaveCriticalSection(&m_lock);};
};




//添加头部
template <typename T>
char cla_list<T>::AddHead(T node)
{
	if(1 == m_eixt) return 0;
	lock();
	m_list.push_front(node);
	unlock();

	return 1;
}


//添加尾部
template <typename T>
char cla_list<T>::AddTail(T node)
{
	if(1 == m_eixt) return 0;
	lock();
	m_list.push_back(node);
	unlock();

	return 1;
}


//获取元素数目
template <typename T>
unsigned int cla_list<T>::GetCount()
{
	unsigned int count = 0;

	if(1 == m_eixt) return 0;
	lock();
	count = m_list.size();
	unlock();

	return count;
}


//获取尾部
template <typename T>
char cla_list<T>::GetTail(T& vct, char remove)
{
	char res = 0;

	if(1 == m_eixt) return 0;

	T tmp;
	list<T>::iterator iter;
	lock();
	if (false == m_list.empty())
	{
		iter = m_list.end();
		iter--;
		tmp = *iter;
		memcpy(&vct,&tmp,sizeof(T));

		if (remove)	m_list.pop_back();

		res = 1;
	}
	unlock();

	return res;
}


//获取头部
template <typename T>
char cla_list<T>::GetHead(T& vct, char remove)
{
	char res = 0;

	if(1 == m_eixt) return 0;
	T tmp;
	list<T>::iterator iter;

	lock();
	if (false == m_list.empty())
	{
		iter = m_list.begin();
		tmp = *iter;

		memcpy(&vct,&tmp,sizeof(T));
		if (remove)	m_list.pop_front();

		res = 1;
	}
	unlock();

	return res;
}

//添加尾部
template <typename T>
void cla_list<T>::init()
{
	if(!flag)
	{
		flag = 1;
		InitializeCriticalSection(&m_lock);
	}
}


#endif //HEADER_CLA_LIST_SHI_QINGLIANG