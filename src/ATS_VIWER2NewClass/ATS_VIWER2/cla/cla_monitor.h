#ifndef HEADER_CLA_MONITOR_SHIQINGLIANG
#define HEADER_CLA_MONITOR_SHIQINGLIANG

#include <windows.h>

class cla_monitor
{
public:
	cla_monitor(void);
	~cla_monitor(void);

protected:
	TCHAR m_FilePath[MAX_PATH]; 

	HWND m_hand_Monitor;

public:
	
	//����
	int mon_start();

	//ping
	int mon_ping(HWND hwnd);

	//����
	int mon_reset(HWND hwnd);
};

#endif	//HEADER_CLA_MONITOR_SHIQINGLIANG