#ifndef HEADER_CNOTICE_SHIQINGLIANG_
#define HEADER_CNOTICE_SHIQINGLIANG_
#include <Windows.h>
#include <iostream>
using namespace std;

#ifndef uchar
#define uchar unsigned char 
#endif

#ifndef uint
#define uint unsigned int 
#endif

class CCNotice
{
private:
	CCNotice(void);
	~CCNotice(void);

   /***
	* 禁用拷贝函数、赋值函数
	*/
	CCNotice(const CCNotice&);
	CCNotice& operator=(const CCNotice&);

	//主界面
	HWND m_view_hand;

	/***
	 * 
	 */
	char m_exit;

public:

	/***
	 * 接口函数
	 */
	static CCNotice* GetInstance()
	{
		static CCNotice m_notice;
		return &m_notice;
	}

	/***
	 * 添加
	 */
	void add_hand(HWND mhwnd, char flag = 0);


	/***
	 * 发送
	 */
	void sendMSG(uint UID,uchar ma_id, uchar ups_id, WPARAM wParam, LPARAM lParam);
};

#endif //HEADER_CNOTICE_SHIQINGLIANG_