#ifndef HEADER_ATS_MAIN_SQL_20131125
#define HEADER_ATS_MAIN_SQL_20131125

#pragma once
#include "..\include\comm.h"
#include "CLA_ATS.h"
#include "CCObject.h"

#pragma warning(once:4996)

#define ATS_MACH_MAX	16
#define ATS_UPS_MAX		7
#define ATS_CMD_BUF_LEN 30

//总信息
typedef struct __cluster_info_t
{
	net_addr_t nat;
	uchar machnum;
	uchar type;

}cluster_info_t;


//ups信息
typedef struct __device_t
{
	uchar device_id;
	uchar canaddr;
	uchar upsnum;

}device_t;


typedef enum __BAT_VIEW_STYLE
{
	VS_NOMAL = 0,
	VS_GIF

}BAT_VIEW_STYLE_E;


//主类
class ats_main
{
public:
	ats_main(void);
	~ats_main(void);

protected:

	/***
	 * @brief: 获取配置文件参数
	 * @parma null
	 * @return char
	 */
	char pro_ats_parse_config();
	

public:

	BAT_VIEW_STYLE_E bat_viewStyle;

	/***
	 * @brief: 初始化
	 * @param null
	 * @return int 
	 */
	int ats_inilize();


	/***
	 * @brief: 获取mach信息
	 * @param id
	 * @return device_t pointer 
	 */
	device_t* ats_get_mach_info(uchar id);

	
	/***
	 * @brief: 获取机架数量
	 * @param null
	 * @return uchar 
	 */
	uchar ats_get_mach_num();


	/***
	 * @brief: 开始
	 * @param null
	 * @return char 
	 */
	char ats_start();


	/***
	 * @brief: 重置参数
	 * @param null
	 * @return int 
	 */
	void ats_reset();


	/***
	 * @brief: 获取UPS信息
	 * @param mac_id
	 * @param ups_id
	 * @return int 
	 */
	CBatObject* ats_get_obj(uchar mac_id, uchar ups_id);


	/***
	 * @brief: 系统工作模式
	 */ 
	void ats_set_bms_mode(char mode, action_type_e ate);


	/***
	 * @brief: 消息输出模式
	 *
	 */
	void ats_set_output_mode(char mode, action_type_e ate);


	/***
	 * @brief: 检测当前系统工作模式
	 * @param null
	 */
	int ats_check_sys_mode_ups();
	int ats_check_sys_mode_net();


	/***
	 * @brief: 检测系统输出模式
	 * @param mode: 输出模式
	 * @return int
	 */
	int ats_check_data_output(char mode);


	/***
	 * @brief: UPS模式下，控制
	 * @param ctrl
	 * @return int 
	 */
	int ats_ups_contrl(char ctlr);


	/***
	 * @brief: 获取能量路由器地址
	 * @param null
	 * @return net_addr_t pointer
	 */
	net_addr_t* ats_get_addr();


	/***
	 * @brief: 退出检测
	 */
	bool ats_quit_check();

};

#endif //HEADER_ATS_MAIN_SQL_20131125