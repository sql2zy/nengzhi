#ifndef HEADER_CLA_SOCPARSE_SHIQINGLIANG_20141024
#define HEADER_CLA_SOCPARSE_SHIQINGLIANG_20141024

#include "../include/battery.h"


#define CMD_PARAMTER_SYS	0x438	//系统信息
#define CMD_PARAMTER_SOC	0x440	//SOC信息


/************************************************************************/
/* The error code                                                       */
/************************************************************************/
#define ERROR_EMPTY_INPUT			0x01	//传入数据为空
#define ERROR_WRONG_CMDPARAMETER	0x02	//bad cmd_parameter
#define ERROR_ALLOCATE_FAILED		0x03	//内存错误

// 函数参数
struct __param_t
{
	uchar cmd;

	uint cmd_parameter;

	uchar* msg;

	uint msg_len;

	uint checknum;
};
typedef struct __param_t param_t;

// 返回值
struct __result_t
{
	uchar res;		//0:数据有误，信息见message; 1:返回soc_info_t结构; 2:返回sys_info_t; 3:同时返回两种结构

	soc_info_t* soc;

	sys_info_t* sys;

	uint message;
};
typedef struct __result_t result_t;

// 返回值
struct __result_2t
{
	uchar res;		//0:数据有误，信息见message; 1:返回soc_info_t结构; 2:返回sys_info_t; 3:同时返回两种结构

	soc_info_t soc;

	sys_info_t sys;

	uint message;
};
typedef struct __result_2t result_2t;

struct __run_time
{
	uchar day;

	uchar hour;

	uchar minite;

	uchar second;
};
typedef struct __run_time run_time_t;

class cla_SOCParse
{

public:
	cla_SOCParse();
	~cla_SOCParse();

private:
	sys_info_t mSit;
	soc_info_t mSoc;
	result_t mResult;

	/***
	 * 返回信息 
	 * param res: 返回码
	 * param soc: SOC值
	 * param sys: 系统信息
	 * param message: 附加信息
	 * Author: Shi qingliang 
	 */
	result_t* __return_res(uchar res, soc_info_t* soc, sys_info_t* sys, uint message);


	/***
	 * 获取时间
	 */
	char __get_run_timestamp(uchar* src, uint len, run_time_t* rtt);


	/*** 
	 * 获取位
	 */
	uint __subbit(uint x, uchar start, uchar end);


	/***
	 * 获取四个模块状态及开关状态
	 */
	char __get_module_info(uchar* src, uint len, sys_info_t* sit);


	/***
	 * 获取每个模块的四个储能单元的开关状态
	 */
	char __get_mod_uint_sw_sta(uchar* src, uint len, sys_info_t* sit);


	/***
	 * 获取电压值
	 */
	char __get_battery_voltage(uchar* src, uint len, sys_info_t* sit);


	/***
	 * 获取放电电流和温度
	 */
	char __get_mod_discharge_and_tmp(uchar*src, uint len, sys_info_t* sit);


	/***
	 * 获取充电电流和温度
	 * param refCha: 参考电流
	 */
	char __get_mod_charge_and_tmp(uchar*src, uint len, sys_info_t* sit, uint refCha);


	/***
	 * 获取温度
	 */
	uint __get_tmperature(uchar* src, uint len);


	/***
	 * 获取参考电流
	 */
	uint __get_REF_CHA(uchar*src, int len);

	/***
	 * 获取系统信息
	 * @param	pbuf:	字符串
	 *			len:	字符串长度
	 *			sit:	
	 * @return: 0 for failed, 1 for sucees
	 * @author: 石清良
	 */
	char __get_sys_info(uchar* pbuf, uint len, sys_info_t* sit);


	/***
	 * 获取四个模块的电压
	 */
	uint __get_module_voltage(uchar*pbuf, uint len);

public:
	/***
	 * 解析信息
	 * 使用完返回值之后，请释放soc、sys
	 * 
	 */
	result_t* mParsing(param_t* pt);

	result_2t mParsing2(param_t* pt);

	
	/***
	 * 解析SOC信息
	 * 使用完返回值之后，请释放soc
	 * 
	 */
	result_t* mParsingSOC(param_t* pt);
	
	
	/***
	 * 解析系统信息
	 * 使用完返回值之后，请释放sys
	 * 
	 */
	result_t* mParsingSYS(param_t* pt);


	/***
	 * 释放资源
	 */
	void mfree();
};

#endif