#ifndef HEADER_CLA_ATS_SHIQINGLIANG_NENGZHI_20141125
#define HEADER_CLA_ATS_SHIQINGLIANG_NENGZHI_20141125
#pragma once
#include "../include/comm.h"
#include "cla_net.h"
#include <string>
using namespace std;

#define ACT_MODE_0	0x00	//交互模式
#define ACT_MODE_1	0x01	//自动传输模式
#define MACH_MAX    30

#define MSG_DOWN_NO_DATA		0x01
#define MSG_DOWN_WRITE_BYTE		0x02
#define MSG_DOWN_WIRTE_WORD		0x03
#define MSG_DOWN_WIRTE_DWORD	0x04
#define MSG_DOWN_READ_BYTE		0x05
#define MSG_DOWN_READ_WORD		0x06
#define MSG_DOWN_READ_DWORD		0x07
#define MSG_HOST_ALIVE_NOTIFICATION	0x0B

#define MSG_UP_MODE_CMD_STATUS		0x01
#define MSG_UP_MODE_0_READ_BYTE		0x02
#define MSG_UP_MODE_0_READ_WORD		0x03
#define MSG_UP_MODE_0_READ_DWORD	0x04
#define MSG_UP_MODE_1_BYTE			0x05
#define MSG_UP_MODE_1_WORD			0x06
#define MSG_UP_MODE_1_DWORD			0x07


typedef struct __action_params
	{
		uchar	cmd;
		uint	parameter;
		uchar	buf[5];
		uint	buf_len;

	}curr_action_params_t;

	/***
	 * 当前指令辅助信息
	 */
	typedef struct __cur_action_buf
	{
		uint wparam;
		uint lparam;
		char data[10];
		curr_action_params_t action;
		char state;

		char resend;
		uchar message[128];
		uchar message_len;

	}curr_action_buf_t;


	//读写操作
	typedef enum __action_type{

		NZ_ACT_READ=0,
		NZ_ACT_WRITE

	}action_type_e;

	//工作模式
	typedef enum __mode_e
	{
		NZ_MODE_DS=0,	//分布式UPS模式
		NZ_MODE_EN		//能源互联网模式

	}ATS_MODE;




class NZ_ATS
{

public:
	NZ_ATS(void);
	~NZ_ATS(void);

public:

	uchar m_mac_id;	//所属机架号
	uchar m_ups_id;	//brick号码

protected:

	string m_key;

	CNet* mNet;

	net_addr_t m_nat;

	curr_action_buf_t m_curr_action;
	CRITICAL_SECTION action_lock;


	/***
	 * @brief: 
	 * @param nat:
	 * @param cmd:
	 * @param cmd_param:
	 * @param str
	 * @param str_len:
	 * @param ait
	 * @param wparam:
	 * @param lparam:
	 * @param data:
	 */
	char ATS_build_params(net_addr_t* nat, uchar cmd, uint cmd_param, uchar str[], uint str_len, ATS_ident_t* ait=NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);


	/***
	 * @brief: 初始化头
	 * @param str:
	 * @param len:
	 * @param ait:
	 * @param cmd:
	 * @param cmd_parameter
	 * @return uchar
	 */
	uchar __init_header(uchar* str, uint len, ATS_ident_t* ait, uchar cmd, uint cmd_parameter);

	
	/***
	 * @brief: 生成校验码
	 * @param msg:
	 * @param len
	 * @return char
	 */
	char __make_checksum(uchar* msg, uint len);


	/***
	 * @brief: 检测校验码
	 * @param msg:
	 * @param len
	 * @return char
	 */
	char __check_checksum(uchar* msg, uint len);

	/***
	 * 超时检测线程
	 */
	char m_eixt;
	unsigned int m_timeout;
	char m_time_puase;
	HANDLE m_event_timeout, m_hand_timeout;
	static unsigned WINAPI timeout_route(void* lpvoid);

public:

	/***
	 * @brief: 解析数据
	 * @param snbt:
	 * @param cur_action:
	 * @return char
	 */
	char NZ_ParseMessage(uchar* buf, uint buf_len,  curr_action_params_t* cur_action);

	/***
	 * @brief: 设置IP地址
	 * @param nat:
	 * @return char
	 */
	char NZ_set_addr(net_addr_t* nat);
 

	HWND m_ats_hwnd;

	/***
	 * @brief: 送入消息队列
	 * @param nat:
	 * @param message:
	 * @param len:
	 * @param ait:
	 * @param wparam:
	 * @param lparam:
	 * @param data:
	 * @return char
	 */
	char __push_cmd( net_addr_t* nat, uchar* message, uint len, ATS_ident_t* ait,
		uint wparam = 0, uint lparam = 0, char *data = NULL);


	/***
	 * @brief: 初始化
	 * @param null
	 * @return char
	 */
	char ATS_initlize();


	/***
	 * @brief: 通知地址
	 * @param nat:
	 * @return char
	 */
	char ATS_tell_addr(net_addr_t* nat);

	//添加
	char ATS_add_ups(ups_type_t type, net_addr_t* nat, ATS_ident_t* ait);

	

	//系统版本号
	char ATS_sys_version(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//指令执行状态
	char ATS_command_status(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//主机状态正常通报
	char ATS_host_alive_notification(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);


	/*
	 * //改变输出模式,适用于国防科大
	 */
	char ATS_output_mode_change_GFKD(net_addr_t* nat, uchar mod, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//产品名称
	char ATS_product_name(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//命令执行完成通报
	char ATS_cmd_complete(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//同时停止放电
	char ATS_stop_discharging_all(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//系统软件版本号
	char ATS_sw_version(net_addr_t* nat, action_type_e ate, char ver[4], ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//配置参数写入Flash
	char ATS_flash_save_config(net_addr_t* nat, ATS_ident_t* ait=NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//配置参数Flash读出
	char ATS_flash_read_config(net_addr_t* nat, ATS_ident_t* ait=NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//电池模块使能控制
	char ATS_bat_mod_enable(net_addr_t* nat, action_type_e ate, uchar offset, ATS_ident_t*ait=NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	char ATS_bat_mod_select(net_addr_t* nat, action_type_e ate, uchar offset, ATS_ident_t*ait=NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//设置充电电流电压
	char ATS_charging_current(net_addr_t* nat, action_type_e ate, uchar str[4],ATS_ident_t*ait=NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//系统工作模式
	char ATS_bms_mode(net_addr_t* nat, action_type_e ate, uchar str, ATS_ident_t*ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//系统控制
	char ATS_system_control(net_addr_t* nat, action_type_e ate, uchar str[4], ATS_ident_t*ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);
	char ATS_system_control_GFKD(net_addr_t* nat, uchar str, ATS_ident_t*ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//串口内存访问地址
	//char ATS_mem_address(net_addr_t* nat, action_type_e ate, uchar str[4], ATS_ident_t*ait = NULL);

	//电池屏蔽控制
	char ATS_battery_mask(net_addr_t* nat, action_type_e ate,uchar mod_id, uchar str[4], ATS_ident_t* ait=NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);


	//////////////////////////////////////////////////////////////////////////
	
	// 查询电池单元当前充/放电电流
	char ATS_bat_current_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	// 查询电池单元当前的电压
	char ATS_bat_voltage_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);
	
	//查询电池单元当前的SOC
	char ATS_bat_soc_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//电池状态
	char ATS_bat_status_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//电池开关状态
	char ATS_bat_switch_Ex(net_addr_t* nat, uchar uid, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//温度
	char ATS_bat_module_temperature_Ex(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//系统状态
	char ATS_sys_status_Ex(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	//系统报警
	char ATS_sys_error_Ex(net_addr_t* nat, ATS_ident_t* ait = NULL,
		uint wparam = 0, uint lparam = 0, char *data = NULL);

	/***
	 * 设置电池充电状态
	 */
	char ATS_battery_charge_state(net_addr_t* nat, ATS_ident_t* ait, char id, uchar str[4],
		uint wparam = 0, uint lparam = 0, char *data = NULL);


	/***
	 * 获取缓存指针
	 */
	char ATS_get_action_p(curr_action_buf_t** p);


	/***
	 * 初始化
	 */
	void ATS_action_reset();


	/***
	 * @brief: 置位重发标志
	 * @param flag: 
	 * @return null
	 */
	void ATS_action_resend(char flag = 1);


	/***
	 * 指令超时 
	 */
	virtual char ATS_timeout(curr_action_buf_t& cab) = 0;


	/***
	 * 提示系统繁忙
	 */
	virtual char ATS_current_busy(curr_action_buf_t* cab) = 0;


	/***
	 * 最小放电单元数
	 * @param
	 * @return 
	 */
	char ATS_min_discharge_unit(net_addr_t* nat, ATS_ident_t* ait,action_type_e ate, uchar ct,
		uint wparam = 0, uint lparam = 0, char *data = NULL);


	/***
	 * @brief: 开始计时
	 */
	void ATS_time();
};

#endif //HEADER_CLA_ATS_SHIQINGLIANG_NENGZHI_20141125
