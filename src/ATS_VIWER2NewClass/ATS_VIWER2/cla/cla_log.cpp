#include <libs_version.h>
#include "cla_log.h"
#include <Windows.h>
#include <WinDef.h>
#include <time.h>
#include "cla_list.h"

cla_list<log_buf_t> m_log_list;

//构造函数
CNetLog::CNetLog(void)
{
	SYSTEMTIME st;
	char tmp[10] = {0};
	m_hand = NULL;
	CString file("");

	GetPrivateProfileString("log","send","0",tmp,sizeof(tmp),"conf\\conf.ini");
	flag_send = atoi(tmp);

	memset(tmp,0,sizeof(tmp));
	GetPrivateProfileString("log","recv","0",tmp,sizeof(tmp),"conf\\conf.ini");
	flag_recv = atoi(tmp);

	memset(tmp,0,sizeof(tmp));
	GetPrivateProfileString("log","auto","0",tmp,sizeof(tmp),"conf\\conf.ini");
	flag_auto = atoi(tmp);

	CreateDirectory("log",NULL);
	
	GetLocalTime(&st);
	file.Format("log\\%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);

	CreateDirectory(file,NULL);

	DWORD id;
	HANDLE h = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)log_route,this,0,&id);
}

//析构函数
CNetLog::~CNetLog(void)
{
	if(m_hand)
	{
		fclose(m_hand);
		m_hand = NULL;
	}
}

//打开日志文件
bool CNetLog::log_open(char* file)
{
	if(!file)return false;

	if(m_hand)
	{
		fclose(m_hand);
		m_hand = NULL;
	}

	if(!(m_hand = fopen(file,"a")))
		m_hand = fopen(file,"a");

	if(!m_hand)return false;

	return true;
}

//获取日志文件
void CNetLog::log_get_name(char* dst, int len, char mid, char uid)
{
	SYSTEMTIME st;
	char tmp[100] = {0};
	
	GetLocalTime(&st);

	sprintf_s(dst,len,"log\\%04d-%02d-%02d\\%02d_%02d.txt",
		st.wYear,st.wMonth,st.wDay,mid,uid);
}

//添加日志
void CNetLog::add_log(char* buf, int buf_len, DATA_TYPE_E dte, SYSTEMTIME st)
{

	char file[100] = {0};
	char mid=0, uid=0;
	
	if(buf_len < 3 || !buf)return ;

	mid = buf[0];
	uid = buf[1];
	GetLocalTime(&st);


	if (DT_AUTO == dte && flag_auto)
	{
		log_get_name(file,sizeof(file),mid,uid);
		
		if(false == log_open(file))return ;

		fprintf_s(m_hand,"[%02d:%02d:%02d:%05d] <AUTO-%02d>   ",st.wHour,st.wMinute,st.wSecond,st.wMilliseconds,buf_len);
		for (int i=0; i<buf_len; i++)
		{
			fprintf_s(m_hand,"%02x ",(unsigned char) (buf[i] & 0XFF));
		}
		fprintf_s(m_hand,"\n");
		
		fclose(m_hand);
		m_hand = NULL;
	}

	else if (DT_READ == dte && flag_recv)
	{
		log_get_name(file,sizeof(file),mid,uid);

		if(false == log_open(file))return ;

		fprintf_s(m_hand,"[%02d:%02d:%02d:%05d] <READ-%02d>   ",st.wHour,st.wMinute,st.wSecond,st.wMilliseconds,buf_len);
		for (int i=0; i<buf_len; i++)
		{
			fprintf_s(m_hand,"%02x ",(unsigned char)buf[i]);
		} 
		fprintf_s(m_hand,"\n");

		fclose(m_hand);
		m_hand = NULL;
	}

	else if (DT_WRITE == dte && flag_send)
	{
		log_get_name(file,sizeof(file),mid,uid);

		if(false == log_open(file))return ;

		fprintf_s(m_hand,"[%02d:%02d:%02d:%05d] <WRITE-%02d>  ",st.wHour,st.wMinute,st.wSecond,st.wMilliseconds,buf_len);
		for (int i=0; i<buf_len; i++)
		{
			fprintf_s(m_hand,"%02x ",(unsigned char)buf[i]);
		}
		fprintf_s(m_hand,"\n");

		fclose(m_hand);
		m_hand = NULL;
	}
}

void CNetLog::add_logEx(char* buf, int buf_len, DATA_TYPE_E dte)
{
	log_buf_t lbt;

	if((DT_AUTO == dte && 1 == flag_auto) || (DT_WRITE == dte && 1 == flag_send) || (DT_READ == dte && 1 == flag_recv))
	{
		GetLocalTime(&(lbt.st));
		memset(&lbt,0,sizeof(lbt));
		memcpy(lbt.pbuf,buf,sizeof(lbt.pbuf));
		lbt.buf_len = buf_len;
		lbt.dt = dte;
		m_log_list.AddHead(lbt);
	}

}


/***
 * 判断文件夹是否存在
 */
BOOL CNetLog::FolderExists(CString s)   
{   
	DWORD attr;    
	attr = GetFileAttributes(s);    
	return (attr != (DWORD)(-1) ) &&   
		( attr & FILE_ATTRIBUTE_DIRECTORY);    
} 

BOOL CNetLog::CreateMuliteDirectory(CString P)   
{   
	int len=P.GetLength();   
	if ( len <2 ) return false;    
	if('\\'==P[len-1])  
	{  
		P=P.Left(len-1);  
		len=P.GetLength();  
	}  
	if ( len <=0 ) return false;  
	if (len <=2)   
	{  
		if (FolderExists(P))return true;  
		else return false;   
	}  
	if (FolderExists(P))return true;  
	CString Parent;  
	Parent=P.Left(P.ReverseFind('\\') );   
	if(Parent.GetLength()<=0)return false;    
	BOOL Ret=CreateMuliteDirectory(Parent);    
	if(Ret)    
	{   
		SECURITY_ATTRIBUTES sa;   
		sa.nLength=sizeof(SECURITY_ATTRIBUTES);   
		sa.lpSecurityDescriptor=NULL;   
		sa.bInheritHandle=0;   
		Ret=(CreateDirectory(P,&sa)==TRUE);   
		return Ret;   
	}   
	else  
		return FALSE;   
}

void CNetLog::show_message(char* src, int src_len, char* dst, int dst_len)
{
	int num = 0;
	wchar_t wsrc[30] = L"出现问题";
	wchar_t wdst[20] = L"请注意";

	if(!src || !dst)return ;


	SYSTEMTIME st;
	char file[100] = {0};
	char mid=0, uid=0;

	mid = src[0];
	uid = src[1];


		log_get_name(file,sizeof(file),mid,uid);
		GetLocalTime(&st);

		if(false == log_open(file))return ;

		fprintf_s(m_hand,"[%02d:%02d:%02d:%05d] <ERROR>  发送数据:",st.wHour,st.wMinute,st.wSecond,st.wMilliseconds);
		for (int i=0; i<src_len; i++)
		{
			fprintf_s(m_hand,"%02x ",src[i] & 0XFF);
		}
		fprintf_s(m_hand,"  但是接收到: ");

		for (int i=0; i<dst_len; i++)
		{
			fprintf_s(m_hand,"%02x ",dst[i] & 0XFF);
		}

		fprintf_s(m_hand,"\n");

		fclose(m_hand);
		m_hand = NULL;


	MessageBox(NULL,"出现问题","请注意",MB_OKCANCEL);

}

char WINAPI CNetLog::log_route(LPVOID lpVoid)
{
	log_buf_t lbt;
	CNetLog * log = (CNetLog *)lpVoid; 

	while(1)
	{
		memset(&lbt,0,sizeof(lbt));
		if(m_log_list.GetTail(lbt,1))
		{
			log->add_log(lbt.pbuf,lbt.buf_len,lbt.dt,lbt.st);
		}
		else
		{
			Sleep(10);
			continue;
		}
	}
}