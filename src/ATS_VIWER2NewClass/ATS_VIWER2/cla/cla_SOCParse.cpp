
#include "cla_SOCParse.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#define ATSVAL(var,offset) *((var)+offset)
extern void sql_trace(char* format,...);
//构造函数
cla_SOCParse::cla_SOCParse()
{

}


//析构函数
cla_SOCParse::~cla_SOCParse()
{

}

//解析数据
result_t* cla_SOCParse::mParsing(param_t* pt)
{

	if(!pt)	//error: there is no data in
		return __return_res(0,NULL,NULL,ERROR_EMPTY_INPUT);

	/*
		这里是否需要验证?
	*/

	if (CMD_PARAMTER_SYS == pt->cmd_parameter)
	{
		return mParsingSYS(pt);
	}
	else if (CMD_PARAMTER_SOC == pt->cmd_parameter)
	{
		return mParsingSOC(pt);
	}
	else
		return __return_res(0,NULL,NULL,ERROR_WRONG_CMDPARAMETER);

}

result_2t cla_SOCParse::mParsing2(param_t* pt)
{
	result_2t r2t;
	result_t* rt = NULL;

	rt = mParsing(pt);

	if(rt)
	{
		r2t.res = rt->res;
		r2t.message = rt->message;
		if(rt->soc)
		{
			memcpy(&(r2t.soc),rt->soc,sizeof(soc_info_t));
		}
		if (rt->sys)
		{
			memcpy(&(r2t.sys),rt->sys,sizeof(sys_info_t));
		}
	}
	else
		r2t.res = 0;

	return r2t;
}


//解析SOC信息
result_t* cla_SOCParse::mParsingSOC(param_t* pt)
{
	uchar i=0, j=0, offset=0;
	uint tmp = 0, iFCC=0, iRM=0, l=0;
	unsigned char* src = NULL;

	if(!pt)
		return __return_res(0,NULL,NULL,ERROR_EMPTY_INPUT);

	memset(&mSoc,0,sizeof(mSoc));

	src = pt->msg;

	for(i=0; i<4; i++)
	{

		offset = i*16*4;
		for (j=0; j<16; j++)
		{
			sql_trace("%02x  %02x  %02x  %02x\n",ATSVAL(src,offset+j*4),ATSVAL(src,offset+j*4+1)
				,ATSVAL(src,offset+j*4+2),ATSVAL(src,offset+j*4+3));
			tmp = ATSVAL(src,offset+j*4)*0x100 + ATSVAL(src,offset+j*4+1);

			if((tmp!=0xFFFF)&&((tmp&0x7FFF)<=(2530)))
				iFCC=(tmp);
			else
				iFCC=2300;

			tmp = ATSVAL(src,offset+j*4+2)*0x100 + ATSVAL(src,offset+j*4+3);
			
			if((tmp!=0xFFFF)&&(tmp<=(2530)))
				iRM=tmp;
			else 
				iRM=2300;

			if((iFCC==0)||(iFCC==0x8000))
				l=0;
			else
				l=(iRM*100)/(iFCC&0x7FFF);

			if(l>100)
				l=100;
			sql_trace("j:%d unit:%d   battery:%d\n",j,(j)/4,(j)%4);
			mSoc.soc[i][j/4][j%4].fcc = iFCC;
			mSoc.soc[i][j/4][j%4].rm = iRM;
			mSoc.soc[i][j/4][j%4].soc = l;
		}
	}

	return __return_res(1,&mSoc,NULL,0);

}
	
	
//解析系统信息
result_t* cla_SOCParse::mParsingSYS(param_t* pt)
{
	uint refCha = 0;
	run_time_t rtt;
	memset(&rtt,0,sizeof(rtt));

	memset(&mSit,0,sizeof(mSit));

	//first， we get the timestamp
	__get_run_timestamp(pt->msg,4,&rtt);
	mSit.timestamp[0] = rtt.day;
	mSit.timestamp[1] = rtt.hour;
	mSit.timestamp[2] = rtt.minite;
	mSit.timestamp[3] = rtt.second;

	__get_sys_info((pt->msg)+48*4,16,&mSit);

	//获取四个模块的状态、开关
	__get_module_info((pt->msg)+2*4,4,&mSit);

	//获取每个模块中的四个储能单元的开关状态
	__get_mod_uint_sw_sta((pt->msg)+5*4,4,&mSit);

	//获取电压值
	__get_battery_voltage((pt->msg)+8*4,128,&mSit);

	

	if(SYS_DISCHARGE_STATE == mSit.state)	//放电电流
		__get_mod_discharge_and_tmp((pt->msg)+40*4,8*8, &mSit);

	if (SYS_CHARGE_STATE == mSit.state)	//充电电流
	{
		refCha =__get_REF_CHA((pt->msg)+51,2);	//先获取参考电流
		__get_mod_charge_and_tmp((pt->msg)+40*4,8*8, &mSit,refCha);
	}
	return __return_res(2,NULL, &mSit,0);
}


// 获取系统信息
char cla_SOCParse::__get_sys_info(uchar* pbuf, uint len, sys_info_t* sit)
{
	uint tmp = 0;
	if(!pbuf || !sit)return 0;
	
	sit->T_UP = __subbit(*(pbuf+0 + 1),0,5);	// T[UP]	
	sit->I_DisCha = __subbit(*(pbuf+0+3),0,5);	// I[DisCha]

	sit->T_Scap = __subbit(*(pbuf+1+1),0,5);	// T[Scap]
	sit->T_Piggy = __subbit(*(pbuf+1+3),0,5);	// T[Piggy]

	sit->V_dcbus = __subbit(*(pbuf+2+1),0,5);	// V[dcbus]
	sit->T_Chg = __subbit(*(pbuf+2+3),0,5);		// T[Chg]

	sit->V_REF_CHA = ((*(pbuf+3+0))&0x0f)*0x100 + *(pbuf+3+1);	// V[REF_CHA]
	sit->V_19VPWR = __subbit(*(pbuf+3+3),0,5);	// V[19VPWR]

	return 1;
}

//返回信息
result_t* cla_SOCParse::__return_res(uchar res, soc_info_t* soc, sys_info_t* sys, uint message)
{

	//DEC_INIT_RES(result);
	memset(&mResult,0,sizeof(mResult));
	
		mResult.res = res;
		mResult.soc = soc;
		mResult.sys = sys;
		mResult.message = message;
		return &mResult;
}


// 获取时间
char cla_SOCParse::__get_run_timestamp(uchar* src,uint len, run_time_t* rtt)
{
	if(!src || !rtt)return 0;
	rtt->day = ((*src)<<(7)) + (__subbit(*(src+1),1,7)>>1);
	rtt->hour = (__subbit(src[1],0,0)<<4) + (__subbit(src[2],4,7)>>4);
	rtt->minite = (__subbit(src[2],0,3)<<2) + (__subbit(src[3],6,7)>>6);

	rtt->second = __subbit(*(src+3),0,5);
	return 1;
}

// 获取位
uint cla_SOCParse::__subbit(uint x, uchar start, uchar end)
{
	if(start > end) return 0;
	if(end > 7)	return 0;

	return x & ((0xff<<start)&(0xff>>(7-end)));
}


//获取四个模块状态及开关状态
char cla_SOCParse::__get_module_info(uchar* src, uint len, sys_info_t* sit)
{
	int i = 0;
	uchar d  =0,d1,d2;
	if(!src || !sit) return 0;

	///{discharge_diode_bypass[3:0], charge_enable[3:0], discharge_enable[3:0]}
	d = *(src+3);
	d1 = __subbit(*(src+3),0,3);
	//sql_trace("---> %02x\n",d1);

	//充、放电判断
	if(*(src+3) & 0xf0)
		sit->state = SYS_CHARGE_STATE;

	else if(*(src+3) & 0x0f)
		sit->state =SYS_DISCHARGE_STATE;

	else sit->state = SYS_IDLE_STATE;

	for (i=0; i<4; i++)
	{
		sit->mod[i].attr.state = sit->state;
	}

	d2 = *(src+2);
	for (i=0; i<4; i++)
	{
		if(*(src+2) & (0x01<<i))
			sit->mod[i].attr.dc_diode_bypass = 1;
		else
			sit->mod[i].attr.dc_diode_bypass = 0;
	}
	
	return 1;
}


// 获取每个模块的四个储能单元的旁路开关状态
//{system3_bypass_ctl[3:0], system2_bypass_ctl[3:0],system1_bypass_ctl[3:0], system0_bypass_ctl[3:0]}
char cla_SOCParse::__get_mod_uint_sw_sta(uchar* src, uint len, sys_info_t* sit)
{
	int i = 0, j = 0;
	if(!src || !sit)return 0;

	//先计算system0的开关
	for (i=0; i<8; i++)
	{
		if(*(src+3) &(0x01<<i))
		{
			if(i<4)
				sit->mod[0].bat_unit[i].attribute.bypass_sw = 1;
			else
				sit->mod[1].bat_unit[i-4].attribute.bypass_sw = 1;
		}
		else
		{
			if(i<4)
				sit->mod[0].bat_unit[i].attribute.bypass_sw = 0;
			else
				sit->mod[1].bat_unit[i-4].attribute.bypass_sw = 0;
		}
	}

	//先计算system0的开关
	for (i=0; i<8; i++)
	{
		if(*(src+3) &(0x01<<i))
		{
			if(i<4)
				sit->mod[2].bat_unit[i].attribute.sw = 1;
			else
				sit->mod[3].bat_unit[i-4].attribute.sw = 1;
		}
		else
		{
			if(i<4)
				sit->mod[2].bat_unit[i].attribute.sw = 0;
			else
				sit->mod[3].bat_unit[i-4].attribute.sw = 0;
		}
	}

	return 1;
}


// 获取电压值
char cla_SOCParse::__get_battery_voltage(uchar* src, uint len, sys_info_t* sit)
{
	int i=0, j=0;
	uint tmp = 0, tmp1 = 0, index = 0;
	uchar uint_i = 0, bat_i = 0;

	if(!src || !sit)return 0;

	for (i=0; i<4; i++)
	{
		index = i*(4*8);
		for (j=0; j<8; j++)
		{
			uint_i = (j*2)/4;	//储能单元序号
			bat_i = (j*2)%4;	//储能单元中的电池序号

			tmp = __subbit(*(src+index + j*4),0,3)*0x100 + *(src+index +j*4 + 1);
			//aBatteryVoltage[index][i][j*2+1]=((tmp&0x0fff)*330000)/(4096*51);
			sit->mod[i].bat_unit[uint_i].battery[bat_i+1].attr.voltage.charge = ((tmp&0x0fff)*330000)/(4096*51);

			if((*(src+index + j*4)) & 0x40)
				sit->mod[i].bat_unit[uint_i].battery[bat_i+1].attr.state = B_WORKING_STATE;
			else
				sit->mod[i].bat_unit[uint_i].battery[bat_i+1].attr.state = B_IDLE_STATE;


			tmp = __subbit(*(src+index + j*4 +2),0,3)*0x100 + *(src+index +j*4 + 3);
			sit->mod[i].bat_unit[uint_i].battery[bat_i].attr.voltage.charge = ((tmp&0x0fff)*330000)/(4096*51);

			if((*(src+index + j*4+2)) & 0x40)
				sit->mod[i].bat_unit[uint_i].battery[bat_i].attr.state = B_WORKING_STATE;
			else
				sit->mod[i].bat_unit[uint_i].battery[bat_i].attr.state = B_IDLE_STATE;
		}
	}
	return 1;
}


// 获取放电电流和温度
char cla_SOCParse::__get_mod_discharge_and_tmp(uchar*src, uint len, sys_info_t* sit)
{
	uint i=0;
	uint tmp = 0;
	uchar offset = 2;

	if(!src || !sit) return 0;

	if(SYS_DISCHARGE_STATE == sit->state)	//正在放电
		offset = 2;
	else	//目前处于空闲状态 
		return 0;

	for (i=0; i<4; i++)
	{//(RxBuffer[bptrout][Base+i*8]&0x0f)*0x100+RxBuffer[bptrout][Base+i*8+1];
		tmp = (*(src+i*8+offset) & 0x0f)*0x100 + *(src+i*8+offset+1);
		if(tmp>=4096)
			tmp=0;
		else
		{
			if(tmp<2284)
				tmp=0;
			else
				tmp=(((((tmp&0xfff)*3300)/4096)-1840)*3000)/(20*4);
			//D=(Iswitch*(0.004/3)*20+1840)/3300*4096
		}
		sit->mod[i].attr.current.discharge = tmp;

		sit->mod[i].temperature = __get_tmperature(src+4,2);

		sit->mod[i].attr.voltage.charge = __get_module_voltage(src+i*8,2);
	}
	
	return 1;
}


//获取充电电流和温度
char cla_SOCParse::__get_mod_charge_and_tmp(uchar*src, uint len, sys_info_t* sit, uint refCha)
{
	uint i=0;
	uint tmp = 0;
	uchar offset = 6;

	if(!src || !sit) return 0;

	if(SYS_CHARGE_STATE == sit->state)	//正在放电
		offset = 6;
	else	//目前处于空闲状态 
		return 0;

	for (i=0; i<4; i++)
	{
		tmp = (*(src+i*8+offset) & 0x0f)*0x100 + *(src+i*8+offset+1);
		if(tmp>=4096)
			tmp=0;
		else
		{
			if(tmp<refCha)
				tmp=0;
			else   	      	        
				tmp=((tmp-refCha)*330000)/(4096*20);
		}

		sit->mod[i].attr.current.charge = tmp;

		sit->mod[i].temperature = __get_tmperature(src+4,2);

		sit->mod[i].attr.voltage.charge = __get_module_voltage(src+i*8,2);
	}

	return 1;
}

//获取四个模块的电压
uint cla_SOCParse::__get_module_voltage(uchar*pbuf, uint len)
{
	uint tmp = 0;

	tmp = (*(pbuf) & 0x0f)*0x100 + *(pbuf+1);
	//D=(Vswitch*10/66)/3300*4096
	//sql_trace("[%02x  %02x ]%u<>%u<>\n",pbuf[0],pbuf[1],tmp,((tmp*3300)/4096)*66/10);
	return (((tmp*3300)/4096)*66/10);
}


//获取温度
uint cla_SOCParse::__get_tmperature(uchar* src, uint len)
{
	uint tmp = 0;
	if(!src)return 0;

	tmp = ((*src)&0x0f)*0x100 + *(src+1);
	if(tmp>=4096)
		tmp=0;
	else
	{
		if(tmp<621)
			tmp=0;
		else

			tmp=((((tmp)*3300)/4096)-500)/10;
	}

	return tmp;
}


// 获取参考电流
uint cla_SOCParse::__get_REF_CHA(uchar*src, int len)
{
	if (!src || 2 != len)
	{
		return 0;
	}

	return (0xfff & ((*src)*0x100 + *(src+1)));
}


//释放资源return __return_res(2,NULL,sit,0);
void cla_SOCParse::mfree()
{
	memset(&mSit,0,sizeof(mSit));
	memset(&mSoc,0,sizeof(mSoc));
	memset(&mResult,0,sizeof(mResult));
}