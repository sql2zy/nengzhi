#include "../stdafx.h"
#include "ats_main.h"
#include <string.h>
#include "cla_config.h"
#include <assert.h>
#include "cla_db.h"
#include "../ProgressDlg.h"
#include "Entity.h"


#pragma warning(once:4244)
#pragma warning(once:4996)

extern CProgressDlg *g_progress_dlg;

CList<device_t> g_device_list;
cluster_info_t g_cluster;


//////////////////////////////////////////////////////////////////////////
//数据库
cla_database gDB;

#define OBJ_COUNT 30
ats_main theATS;


// brick object
CBatObject m_obj[OBJ_COUNT];
static int m_count = 0;


//系统化工作模式
typedef struct __SYS_MODE_T
{
	uchar mode_ups;
	uchar mode_net;
}SYS_MODE_T;
SYS_MODE_T gSysMode;

CList<HWND>g_hand_list;


//日志输出
extern void sql_trace(char* format,...);



//构造函数
ats_main::ats_main(void)
{
	ats_reset();
	//pro_ats_parse_config();
}


//重置参数
void ats_main::ats_reset()
{

	for(int i=0; i<m_count; i++)
	{
		m_obj[i].obj_inilize();
	}
	memset(&gSysMode,0,sizeof(gSysMode));
	gSysMode.mode_net = 1;
	gSysMode.mode_ups = 0;
}


// 析构函数
ats_main::~ats_main(void)
{
	g_device_list.RemoveAll();
}



// 初始化
int ats_main::ats_inilize()
{
	pro_ats_parse_config(); 
	
	//初始化数据库
	gDB.init_db();

	return 1;
}


//检测当前系统工作模式
int ats_main::ats_check_sys_mode_ups()
{

	if(BMS_MODE_NET_GFKD == m_obj[0].m_asit.sitEX.sysEX_bms_mode)
		return 0;
	else
		return 1;

}


//检测当前系统工作模式
int ats_main::ats_check_sys_mode_net()
{
	if(__GFKD__)
	{
		if(BMS_MODE_NET_GFKD == m_obj[0].m_asit.sitEX.sysEX_bms_mode)
			return 1;
		else
			return 0;
	}
}


//检测数据上传模式
int ats_main::ats_check_data_output(char mode)
{
	
	if(mode == m_obj[0].m_asit.sitEX.sysEX_bms_output_mode)
		return 1;
	else
		return 0;
}


// 获取配置文件参数
char ats_main::pro_ats_parse_config()
{
	MAC_ENTITY* mac = MAC_ENTITY::GetInstance();
	char pbuf[100] = {0};
	device_t dt;
	cla_conf cfg;
	g_device_list.RemoveAll();

	//获取IP地址
	if((cfg.Conf_read("conf/conf.ini","general","IP",F_PARAM(pbuf))))
	{

		sscanf(pbuf,"%d.%d.%d.%d",&(g_cluster.nat.ip1),&(g_cluster.nat.ip2),&(g_cluster.nat.ip3),&(g_cluster.nat.ip4));
	}

	//获取端口号
	memset(pbuf,0,sizeof(pbuf));
	if((cfg.Conf_read("conf/conf.ini","general","PORT",F_PARAM(pbuf))))
	{

		sscanf(pbuf,"%d",&(g_cluster.nat.port));
	}

	//获取机架个数
	memset(pbuf,0,sizeof(pbuf));
	if((cfg.Conf_read("conf/conf.ini","general","MACHNUM",F_PARAM(pbuf))))
	{

		sscanf(pbuf,"%x",&(g_cluster.machnum));
	}
	
	
	for(uchar i=0; i<g_cluster.machnum; i++)
	{
		memset(&dt,0,sizeof(dt));
		CString sction("");
		sction.Format("MACH%02d",i);
		memset(pbuf,0,sizeof(pbuf));

		if((cfg.Conf_read("conf/conf.ini",sction,"CANADDR",F_PARAM(pbuf))))
		{

			sscanf(pbuf,"%d",&(dt.canaddr));
//			memset(pbuf,0,sizeof(pbuf));

		}

		if((cfg.Conf_read("conf/conf.ini",sction,"UPSNUM",F_PARAM(pbuf))))
		{
			sscanf(pbuf,"%d",&(dt.upsnum));

		}
		dt.device_id = i;
		g_device_list.AddTail(dt);

		//sql add
		mac->mac_Add(i,dt.upsnum);

		for (int j =0; j<dt.upsnum; j++)
		{
			if(m_count < OBJ_COUNT)
			{
				m_obj[m_count].obj_set_attribution(i,j);
				m_obj[m_count].NZ_set_addr(&(g_cluster.nat));
				m_obj[m_count].obj_inilize();
				if(0 == i && 0 == j)
					m_obj[m_count].obj_view_flag_set(1);
				m_count++;
				
			}
		}//for (int j =0; j<dt.upsnum; j++)
	}

	return 0;
}


// 获取mach信息
device_t* ats_main::ats_get_mach_info(uchar id)
{
	int count = g_device_list.GetCount();
	POSITION p=0;
	device_t dt;

	if( id>= count)return NULL;

	for ( int i=0; i<count; i++)
	{
		p = g_device_list.FindIndex(id);
		dt = g_device_list.GetAt(p);

		if(dt.device_id == id) return &(g_device_list.GetAt(p));
	}

	return NULL;
}


//获取机架数量
uchar ats_main::ats_get_mach_num()
{
	return g_device_list.GetCount();
}


//启动
void pthread_start(LPVOID lPvoid)
{
	CNet* net = CNet::GetInstance();
	ats_main*  ma = (ats_main*)lPvoid;

	POSITION p=0;
	int i=0, j=0;
	char tmp[10] = {0};

	GetPrivateProfileString("general","OUTPUT","1",tmp,sizeof(tmp),"conf/conf.ini");

	net->CN_start(6000);	//端口地址6000不变
	net->CN_tell_addr(&(g_cluster.nat),6000);

	Sleep(100);

	for (int i=0; i<m_count; i++)
	{
		m_obj[i].NZ_set_addr(&(g_cluster.nat));
		m_obj[i].ATS_output_mode_change_GFKD(NULL,atoi(tmp),NULL,WPARAM_INIT);
		//for test
		//m_obj[i].obj_Store();
	}
}


// 开始连接
char ats_main::ats_start()
{
	ats_reset();
	DWORD id;
	CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)pthread_start,this,0,&id);

	return 1;
}


// 设置系统工作模式
void __ats_set_bms_mode(LPVOID lpVoid)
{
	int i=0, j=0;
	char mode = (char)lpVoid;

	for ( int i=0; i<m_count; i++)
	{
		if(mode == m_obj[i].m_asit.sitEX.sysEX_bms_mode)	//已经是，则跳过
			continue;

		//在UPS模式下，不处于空闲状态，则先停止
		if(BMS_MODE_UPS_GFKD == m_obj[i].m_asit.sitEX.sysEX_bms_mode)
		{
			if(!(S_EX_STATE_NORMAL == m_obj[i].m_asit.sitEX.sysEX_state ||
				S_EX_STATE_UPS_DOWN == m_obj[i].m_asit.sitEX.sysEX_state))
			{
				m_obj[i].ATS_system_control_GFKD(NULL,1,NULL,WPARAM_MODE_SWITCH);
			}
			else
			{
				m_obj[i].ATS_bms_mode(NULL,NZ_ACT_WRITE,mode,NULL);
			}

		} //if(BMS_MODE_UPS_GFKD == m_obj[i].m_asit.sitEX.sysEX_bms_mode)
		else
		{
			m_obj[i].ATS_bms_mode(NULL,NZ_ACT_WRITE,mode,NULL);
		}
	}
}


// 设置系统工作模式
void ats_main::ats_set_bms_mode(char mode, action_type_e ate)
{
	HANDLE mhand = 0;
	DWORD id;
	char tmp[100] = {0};

	// 先确定已经处于空闲状态
	for (int i=0; i<m_count; i++)
	{
		if(!(S_EX_STATE_NORMAL == m_obj[i].m_asit.sitEX.sysEX_state ||
			S_EX_STATE_UPS_DOWN == m_obj[i].m_asit.sitEX.sysEX_state))
		{
				
			if(BMS_MODE_UPS_GFKD == mode && BMS_MODE_NET_GFKD == m_obj[i].m_asit.sitEX.sysEX_bms_mode)
			{
				sprintf_s(tmp,sizeof(tmp),"机架%d 单元%d 未处于空闲状态，请先切换至“空闲”状态",
				m_obj[i].m_mac_id,m_obj[i].m_ups_id);
				AfxMessageBox(tmp);
				return ;
			}
		}
	}
	mhand = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)__ats_set_bms_mode,(LPVOID)mode,0,&id);
	CloseHandle(mhand);
	return ;
}


//设置系统消息输出模式
void __ats_set_out_mode(LPVOID lPvoid)
{
	int i=0;
	char mode = (char)lPvoid;

	for (int i=0; i<m_count; i++)
	{
		m_obj[i].ATS_output_mode_change_GFKD(NULL,mode,NULL,WPARAM_DEBUG_MODE);
		Sleep(40);
	}
}


//设置系统消息输出模式
void ats_main::ats_set_output_mode(char mode, action_type_e ate)
{
	HANDLE mhand = 0;
	DWORD id;
	mhand = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)__ats_set_out_mode,(LPVOID)mode,0,&id);
	CloseHandle(mhand);
	return ;
}


//获取UPS信息
CBatObject* ats_main::ats_get_obj(uchar mac_id, uchar ups_id)
{
	TRACE("获取UPS信息1: mac_id:%d ups_id:%d\n",mac_id,ups_id);
	if(mac_id >= ATS_MACH_MAX || ups_id >= ATS_UPS_MAX)
		return NULL;

	for (int i=0; i<m_count; i++)
	{
		TRACE("获取UPS信息2: mac_id:%d ups_id:%d\n",m_obj[i].m_mac_id,m_obj[i].m_ups_id);
		if( (mac_id == m_obj[i].m_mac_id) && (ups_id == m_obj[i].m_ups_id))
		{
			return &(m_obj[i]);
		}

			
	}

	return NULL;
}


// UPS控制
void __ats_ups_contrl(LPVOID lPvoid)
{
	int i=0;
	char mode = (char)lPvoid;

	for (int i=0; i<m_count; i++)
	{
		if(BMS_MODE_UPS_GFKD == m_obj[i].m_asit.sitEX.sysEX_bms_mode && 
			S_EX_STATE_UPS_DOWN != m_obj[i].m_asit.sitEX.sysEX_state)
		{
			m_obj[i].ATS_system_control_GFKD(NULL,mode);
			Sleep(40);
		}	
	}
}

// UPS模式下，控制
int ats_main::ats_ups_contrl(char ctlr)
{
	HANDLE mhand = 0;
	DWORD id;
	mhand = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)__ats_ups_contrl,(LPVOID)ctlr,0,&id);
	CloseHandle(mhand);
	return 1;
}


//获取能量路由器地址
net_addr_t* ats_main::ats_get_addr()
{
	return &(g_cluster.nat);
}


//退出检测
bool ats_main::ats_quit_check()
{
	char tmp[100] = {0};
	bool ret = true;
	for ( int i=0; i<m_count; i++)
	{
		if((S_EX_STATE_CHARG     == m_obj[i].m_asit.sitEX.sysEX_state ||
			S_EX_STATE_DISCHARGE == m_obj[i].m_asit.sitEX.sysEX_state) && 
			BMS_MODE_NET_GFKD == m_obj[i].m_asit.sitEX.sysEX_bms_mode)
		{
			sprintf_s(tmp,sizeof(tmp),"机架%d,UPS-%d处于工作状态中，请先停止工作",m_obj[i].m_mac_id,m_obj[i].m_ups_id);
			::MessageBox(NULL,tmp,"提示",MB_OK);
			ret = false;
			break;
		}
	}

	return ret;
}