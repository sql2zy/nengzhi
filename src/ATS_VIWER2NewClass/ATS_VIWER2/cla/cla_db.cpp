//#include "stdafx.h"
#include <libs_version.h>
#include "cla_db.h"
#ifdef _DEBUG
#pragma comment(lib,"lib/lct_mysqld.lib")
#else
#pragma comment(lib,"lib/lct_mysql.lib")
#endif


extern void sql_trace(char* format,...);

void verbse(char* msg);

void verbse(char* msg)
{
	sql_trace(msg);
}
/*
[db]
dbhost=127.0.0.1
dbuser=root
dbpassword=
dbport=3306
dbname=sqltest
*/
cla_database::cla_database(void)
{
	lm_set_verbose(verbse);
	conFlag = 0;
}

cla_database::~cla_database(void)
{
}


/***
 * 初始化
 */
char cla_database::init_db(char dhhost[], char dbuser[], char dbpassword[], char dbname[],unsigned int port)
{
	return 1;
	return (conFlag= lm_initialize(dhhost,dbuser,dbpassword,dbname,port));
}


/***
 * 初始化
 */
char cla_database::init_db()
{
	char dbhost[31] = {0},dbuser[31] = {0},dbpassword[31] = {0},dbname[31] = {0},tmpport[31] = {0};
	unsigned int port =0;

	GetPrivateProfileString("db","dbhost","127.0.0.1",dbhost,sizeof(dbhost),"conf/conf.ini");
	GetPrivateProfileString("db","dbuser","root",dbuser,sizeof(dbuser),"conf/conf.ini");
	GetPrivateProfileString("db","dbpassword","",dbpassword,sizeof(dbpassword),"conf/conf.ini");
	GetPrivateProfileString("db","dbname","sqltest",dbname,sizeof(dbname),"conf/conf.ini");
	GetPrivateProfileString("db","dbport","3306",tmpport,sizeof(tmpport),"conf/conf.ini");

	sscanf(tmpport,"%d",&port);
	return init_db(dbhost,dbuser,dbpassword,dbname,port);
	return 1;
}


/***
 * 插入soc
 */
char cla_database::db_store_soc(uchar macID, uchar upsID,uchar batID,uchar soc)
{
	return db_store("ups_soc",macID,upsID,batID,soc);
}


/***
 * 插入电流
 */
char cla_database::db_store_current(uchar macID, uchar upsID,uchar batID,uint current)
{
	return db_store("ups_current",macID,upsID,batID,current);
}


/***
 * 插入电压
 */
char cla_database::db_store_voltage(uchar macID, uchar upsID,uchar batID,uint voltage)
{
	return db_store("ups_voltage",macID,upsID,batID,voltage);
}


/***
 * 插入温度
 */
char cla_database::db_store_temperature(uchar macID, uchar upsID,uchar batID,uint temperature)
{
	return db_store("ups_temperature",macID,upsID,batID,temperature);
}


//插入数据
char cla_database::db_store(char* dbname,uchar macID, uchar upsID,uchar batID, uint value)
{
	char sqlstr[100] = {0};
	if(!dbname )return 0;
	if(0 == conFlag)return 0;


	sprintf_s(sqlstr,sizeof(sqlstr),"insert into %s(macID,upsID,batID,value) values('%d','%d','%d','%d')",
		dbname,macID,upsID,batID,value);

	return lm_insert_exec(sqlstr,strlen(sqlstr));
	
}