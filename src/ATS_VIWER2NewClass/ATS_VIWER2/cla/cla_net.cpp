#include "../stdafx.h" 
#include "cla_net.h"
#include "CCObject.h"
#include <map>
#include <Entity.h>


#define WIN32_LEAN_AND_MEAN

#ifdef _MSC_VER
#pragma warning(disable:4201)
#endif

#pragma warning(once:4996)
#include <Winsock2.h>
#include "cla_log.h"
#include "../CMainLog.h"


#pragma comment(lib,"WSOCK32.lib") 
#pragma comment(lib,"ws2_32.lib") 
//
CNetLog m_netlog;

extern CCMainLog *g_main_log;

cla_list<send_node_buf_t> m_buf_list;

extern void sql_trace(char* format,...);


static net_addr_t g_remote_addr, g_local_addr;

int g_ReSendInt = 20;

MAC_ENTITY* g_macEntity = NULL;

//////////////////////////////////////////////////////////////////////////

//构造函数
CNet::CNet(void)
{
	InitializeCriticalSection(&m_lock);
	m_special_function = NULL;
}


//析构函数
CNet::~CNet(void)
{
	if(m_sock)
	{
		closesocket(m_sock);
		m_sock = 0;
	}
	DeleteCriticalSection(&m_lock);
}

/***
 * 
 */
char CNet::CN_init_sockaddr( net_addr_t* nat, SOCKADDR_IN* soca_in)
{
	char reip[20] = {0};
	if(!nat || !soca_in)return 0;
	
	sprintf_s(reip,sizeof(reip),"%d.%d.%d.%d",nat->ip1,nat->ip2,nat->ip3,nat->ip4);
	memset((void*)soca_in,0,sizeof(SOCKADDR_IN));
	soca_in->sin_addr.S_un.S_addr = inet_addr(reip);
	soca_in->sin_family =AF_INET;
	soca_in->sin_port = htons(nat->port);
	
	return 1;
}


// 获取本地IP地址
char CNet::CN_get_local_addr(net_addr_t* nat)
{
	if(!nat)return 0;

	char host[255] = {0};  
	if(0 != CN_get_local_name(host,sizeof(host)))  
	{  
		return 0;  
	}   

	struct hostent FAR * lpHostEnt=gethostbyname(host);
	if(host==NULL)
	{
		//产生错误
		nat->ip1=nat->ip2=nat->ip3=nat->ip4=0;
		return 0;
	}
	//获取IP
	LPSTR lpAddr=lpHostEnt->h_addr_list[0];
	if(lpAddr)
	{
		struct in_addr inAddr;
		memmove(&inAddr,lpAddr,4);
		nat->ip1=inAddr.S_un.S_un_b.s_b1;
		nat->ip2=inAddr.S_un.S_un_b.s_b2;
		nat->ip3=inAddr.S_un.S_un_b.s_b3;
		nat->ip4=inAddr.S_un.S_un_b.s_b4;
	}
	return 0;
}


//获取本地名称
char CNet::CN_get_local_name(char* dst, uint dst_len)
{
	char szHostName[256];
	int nRetCode;

	if(!dst) return 0;

	nRetCode=gethostname(szHostName,sizeof(szHostName));
	if(nRetCode!=0)
	{
		//产生错误
		return 0;
	}
	memcpy(dst,szHostName,dst_len);
	return 0;
}


// 获取源地址
char CNet::CN_get_src_ip(SOCKADDR_IN& addr, net_addr_t& nat)
{
	nat.ip1=(addr.sin_addr.S_un.S_addr)&0xff;
	nat.ip2=(addr.sin_addr.S_un.S_addr>>8)&0xff;
	nat.ip3=(addr.sin_addr.S_un.S_addr>>16)&0xff;
	nat.ip4=(addr.sin_addr.S_un.S_addr>>24)&0xff;
	nat.port = ntohs(addr.sin_port);
	return 1;
}


//添加回调函数
bool CNet::CN_add_callback(string key, CBatObject* obj, char replace /*= 1*/)
{
	bool ret = false;

	map<string,CBatObject*>::iterator iter;

	iter = m_cb_interface.find(key);
	if(iter == m_cb_interface.end())
	{
		m_cb_interface.insert(pair<string,CBatObject*>(key,obj));
		ret = true;
	}
	else
	{
		if(replace)
		{
			iter->second = obj;
			ret = true;
		}
	}
	return ret;
}
bool CNet::CN_add_special_callback(CBatObject* obj, char replace /*= 1*/)
{
	return true;
}


// 删除回调函数
bool CNet::CN_remove_callback(string key)
{
	bool ret = false;
	map<string,CBatObject*>::iterator iter;

	iter = m_cb_interface.find(key);
	if(m_cb_interface.end() != iter)
	{
		m_cb_interface.erase(iter);
		ret = true;
	}

	return ret;
}


int WINAPI CNet::thread_send(LPVOID lpVoid)
{
	int res =0;
	CNet* m_net = (CNet*)lpVoid;
	char tmp[21] = {0};
	send_node_buf_t snbt;

	CTimeOut TimeSend, TimePing;

	SOCKADDR_IN remote_addr, tmpAddr;
	int size = sizeof(remote_addr);
	map<string,CBatObject*>::iterator iter;
	string		key;

	GetPrivateProfileString("general","RESENDINT","20",tmp,sizeof(tmp),"conf/conf.ini");
	g_ReSendInt = atoi(tmp);

	//开始计时
	TimeSend.TO_Start();
	TimePing.TO_Start();

	while(1)
	{

		if(TRUE == TimePing.TO_Check(2000))
		{
			m_net->CN_tell_addrEx(&g_remote_addr,g_remote_addr.port);
			TimePing.TO_Reset();
		}

		if(FALSE == TimeSend.TO_Check(g_ReSendInt))
		{
			continue;
		}

		//这里处理发送
		memset(&snbt,0,sizeof(snbt));
		if(1 == m_buf_list.GetTail(snbt,1))
		{	//有数据，则发送
			if (1 == m_net->CN_init_sockaddr(&(snbt.nat),&tmpAddr))
			{
				EnterCriticalSection(&(m_net->m_lock));
				res = sendto(m_net->m_sock,snbt.pbuf,snbt.buf_len,0,(SOCKADDR*)&tmpAddr,sizeof(SOCKADDR));
				LeaveCriticalSection(&(m_net->m_lock));

				memset(tmp,0,sizeof(tmp));
				sprintf_s(tmp,"%d%d",snbt.pbuf[0],snbt.pbuf[1]);
				key = tmp;
				iter = m_net->m_cb_interface.find(key);
				if(m_net->m_cb_interface.end() != iter)
				{
					iter->second->ATS_time();
				}
				else
				{
					if(0xAA != (unsigned char)snbt.pbuf[0])
						g_main_log->add_log(LL_ERROR,(unsigned char)snbt.pbuf[0],(unsigned char)snbt.pbuf[1],
						"注意这个数据2:%s %02x %02x %02x",key.c_str(),
						(unsigned char)snbt.pbuf[0],(unsigned char)snbt.pbuf[1],(unsigned char)snbt.pbuf[2]);
				}

				if(SOCKET_ERROR == res)
				{
					;//这里处理失败情况
				}
				else
				{
					TimeSend.TO_Reset();
					m_netlog.add_logEx(snbt.pbuf,snbt.buf_len,DT_WRITE);
				}
			}
		} //if (1 == m_net->CN_init_sockaddr(&(snbt.nat),&tmpAddr))

		
	} //if(1 == m_buf_list.GetTail(snbt,1))
}


//主线程
int WINAPI CNet::thread_route(LPVOID lpVoid)
{
	CNet* m_net = (CNet*)lpVoid;
	timeval		mTo;		//设置超时时间为1秒
	string		key;
	char		tmp[10] = {0};
	char		pbuf[1024] = {0};
	int			len = 0, res = 0, mret = 0;
	send_node_buf_t snbt;
	fd_set gRfd;  //读描述
	SYSTEMTIME st;

	//判断超时
	int t_flag = 0;
	time_t t_old, t_now;

	SOCKADDR_IN remote_addr;
	int size = sizeof(remote_addr);
	map<string,CBatObject*>::iterator iter;

	while(1)
	{
		//这里处理接收
		mTo.tv_sec = 0;	
		mTo.tv_usec = 1*20;
		FD_ZERO(&gRfd);
		FD_SET(m_net->m_sock,&gRfd);
		mret = select(0,&gRfd,NULL,NULL,&mTo);

		if(0 == mret){
			
			if(t_flag)
			{
				time(&t_now);

				if (difftime(t_now,t_old)>10)
				{
					g_main_log->add_log(LL_ERROR,0,0,"能量路由器无消息达%d秒，请检测能量路由器状态",(int)difftime(t_now,t_old));
					time(&t_old);
				}
			}

		}	//没有数据
		else if(mret >0)	//有数据
		{
			t_flag = 1;
			time(&t_old);

			memset(pbuf,0,sizeof(pbuf));
			EnterCriticalSection(&(m_net->m_lock));
			len = recvfrom(m_net->m_sock,pbuf,sizeof(pbuf),0,(SOCKADDR*)&remote_addr,&size);

			LeaveCriticalSection(&(m_net->m_lock));
			if(len > 0)
			{
				//m_netlog.add_logEx(pbuf,len,DT_READ);

				memset(tmp,0,sizeof(tmp));
				sprintf_s(tmp,"%d%d",pbuf[0],pbuf[1]);
				key = tmp;

				memset(&snbt,0,sizeof(snbt));
				memcpy(snbt.pbuf,pbuf,len);
				snbt.buf_len = len;
				m_net->CN_get_src_ip(remote_addr,snbt.nat);

				/****/
				GetLocalTime(&st);
				int cmd = (pbuf[2] & 0xf0)>>4;
				int parame = ((pbuf[2] & 0x0f)<<8) + pbuf[3];

				iter = m_net->m_cb_interface.find(key);
				if(m_net->m_cb_interface.end() != iter)
				{
					if(0 == iter->second->obj_Interface(&snbt))
					{
						g_main_log->add_log(LL_ERROR,(unsigned char)pbuf[0],(unsigned char)pbuf[1],
							"数据:%s %02x %02x %02x添加失败",key.c_str(),
							(unsigned char)pbuf[0],(unsigned char)pbuf[1],(unsigned char)pbuf[2]);
					}
				}
				else
				{
					if(0xbb != (unsigned char)pbuf[0])
					g_main_log->add_log(LL_ERROR,(unsigned char)pbuf[0],(unsigned char)pbuf[1],
					"注意这个数据:%s %02x %02x %02x",key.c_str(),
						(unsigned char)pbuf[0],(unsigned char)pbuf[1],(unsigned char)pbuf[2]);

					if(m_net->m_special_function)
						m_net->m_special_function(&snbt);
				}
			}//if(len > 0)
		}//else if(mret >0)	//有数据
	}//while(1)
}


// 退出
char CNet::CN_stop()
{
	if(m_thread_hand){
		
		WaitForSingleObject(m_thread_hand,INFINITE);
		closesocket(m_sock);
		m_thread_hand = 0;
	}

	return 1;
}


//发送数据
char CNet::CN_send(net_addr_t* nat, void* pbuf, uint len)
{
	send_node_buf_t snbt;

	if(!pbuf)return 0;

	memcpy(&(snbt.nat),nat,sizeof(net_addr_t));
	memcpy(snbt.pbuf,pbuf,len);
	snbt.buf_len = len;
	m_buf_list.AddHead(snbt);
	return 1;
}


// 通知地址
char CNet::CN_tell_addr(net_addr_t* nat, uint port)
{
	
	//g_remote_addr, g_local_addr
	int ret = 0;
	if(!nat)return 0;

	memset(&g_local_addr,0,sizeof(g_local_addr));
	memset(&g_remote_addr,0,sizeof(g_remote_addr));

	CN_get_local_addr(&g_local_addr);
	g_local_addr.port = port;

	memcpy(&g_remote_addr,nat,sizeof(g_remote_addr));
	g_remote_addr.port = port;


	return CN_tell_addrEx(&g_remote_addr,port);

}


// 发送地址
char CNet::CN_tell_addrEx(net_addr_t* nat, uint port)
{
	char state[5] = {0};
	int ret = 0;
	if(!nat)return 0;

	SOCKADDR_IN  tmpAddr;
	uchar tmp[10] = {0};

	g_macEntity->mac_Get(state,4);

	tmp[0] = 0xAA;
	tmp[1] = 0x01;
	tmp[2] = g_local_addr.ip1;
	tmp[3] = g_local_addr.ip2;
	tmp[4] = g_local_addr.ip3;
	tmp[5] = g_local_addr.ip4;

	sql_trace("发送地址 %d %d %d %d\n",state[0],state[1],state[2],state[3]);

	//sql add 
	tmp[6] = state[0];
	tmp[7] = state[1];
	tmp[8] = state[2];
	tmp[9] = state[3];

	CN_init_sockaddr(nat,&tmpAddr);

	EnterCriticalSection(&m_lock);
	ret = sendto(m_sock,(char*)tmp,10,0,(SOCKADDR*)&tmpAddr,sizeof(SOCKADDR));
	LeaveCriticalSection(&m_lock);

	return 1;
}

bool CNet::CN_start(uint localport)
{
	DWORD id;
	if(1 == flag) return true;
	WSAData data;
	WSAStartup(MAKEWORD(1,1),&data);

	m_sock=socket(AF_INET,SOCK_DGRAM,0);			//基于UDP的socket,用于文本传输
	if(!m_sock){

		return false;
	}

	m_localAddr.sin_addr.S_un.S_addr=htonl(INADDR_ANY);
	m_localAddr.sin_family=AF_INET;
	m_localAddr.sin_port=htons(localport);

	g_macEntity = MAC_ENTITY::GetInstance();

	if(SOCKET_ERROR==bind(m_sock,(SOCKADDR*)&m_localAddr,sizeof(SOCKADDR))){
		return false;
	}

	m_thread_hand = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)thread_route,this,0,&id);
	SetThreadPriority(m_thread_hand,THREAD_PRIORITY_ABOVE_NORMAL);
	m_trhead_send = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)thread_send,this,0,&id);

	flag = 1;
	return true;

}


//////////////////////////////////////////////////////////////////////////

CTimeOut::CTimeOut(void)
{
	InitializeCriticalSection(&m_Lock);
}

CTimeOut::~CTimeOut(void)
{
	DeleteCriticalSection(&m_Lock);
}

void CTimeOut::TO_Start()
{
	EnterCriticalSection(&m_Lock);
	GetLocalTime(&m_StBegin);	
	LeaveCriticalSection(&m_Lock);
}

void CTimeOut::TO_Reset()
{
	TO_Start();
}

BOOL CTimeOut::TO_Check(unsigned __int64 diff)
{

	BOOL ret = FALSE;
	ULARGE_INTEGER m_fTimeBegin;
	ULARGE_INTEGER m_fTimeEnd;

	unsigned __int64 tmp = 0;

	EnterCriticalSection(&m_Lock);
	GetLocalTime(&m_StEnd);

	SystemTimeToFileTime(&m_StBegin,(FILETIME*)&m_fTimeBegin);
	SystemTimeToFileTime(&m_StEnd,(FILETIME*)&m_fTimeEnd); 
	tmp=m_fTimeEnd.QuadPart-m_fTimeBegin.QuadPart; 
	tmp = (int)(tmp/10000);//单位：毫秒
	if(tmp > diff)
	{
		ret = TRUE;
	}
	
	else
		ret = FALSE;

	LeaveCriticalSection(&m_Lock);
	return ret;
}