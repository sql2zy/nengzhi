#ifndef HEADER_CLA_DB_20131125
#define HEADER_CLA_DB_20131125

#pragma once
#include "../include/lct_mysql.h"

#ifndef MAX_PATH
#define MAX_PATH 260
#endif

#ifndef uint
#define uint unsigned int
#endif

#ifndef uchar
#define uchar unsigned char
#endif

typedef struct __database_info_t
{
	char dbhost[31];
	char dbuser[21];
	char dbpwd[31];
	char dbname[31];
	int port;

}db_info_t;

class cla_database
{
public:
	cla_database(void);
	~cla_database(void);

protected:
	char conFlag;

	//插入数据
	char db_store(char* dbname,uchar macID, uchar upsID,uchar batID, uint value);

public:

	//初始化
	char init_db(char dhhost[], char dbuser[], char dbpassword[], char dbname[],unsigned int port);
	char init_db();

	//插入soc
	char db_store_soc(uchar macID, uchar upsID, uchar batID, uchar soc);

	//插入电流
	char db_store_current(uchar macID, uchar upsID, uchar batID, uint current);

	//插入电压
	char db_store_voltage(uchar macID, uchar upsID, uchar batID, uint voltage);

	//插入温度
	char db_store_temperature(uchar macID, uchar upsID, uchar batID,uint temperature);
};

#endif //HEADER_CLA_DB_20131125