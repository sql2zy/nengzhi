#include <libs_version.h>
#include "CNotice.h"
#include <list>
#include "..\include\comm.h"

list<HWND> g_hand_list;

CCNotice::CCNotice(void)
{
	m_exit = 0;
}

//��������
CCNotice::~CCNotice(void)
{
	m_exit = 1;
	g_hand_list.clear();
}

//����
void CCNotice::add_hand(HWND mhwnd, char flag /*= 0*/)
{
	g_hand_list.push_front(mhwnd);
	if(flag)
		m_view_hand = mhwnd;
}


//����
void CCNotice::sendMSG(uint UID,uchar ma_id, uchar ups_id, WPARAM wParam, LPARAM lParam)
{
	HWND tmp;
	list<HWND>::iterator iter;

	ATS_send_msg_t asmt;
	if(1 == m_exit)return ;
	memset(&asmt,0,sizeof(asmt));

	asmt.mac_id = ma_id;
	asmt.ups_id = ups_id;
	asmt.lParam = lParam;
	asmt.wParam = wParam;

	for (iter = g_hand_list.begin(); iter != g_hand_list.end(); iter++)
	{
		SendMessage(*iter,UID,(WPARAM)&asmt,0);
	}
}