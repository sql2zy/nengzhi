
// ATS_VIWER2Doc.cpp : CATS_VIWER2Doc 类的实现
//

#include "stdafx.h"
#include "ATS_VIWER2.h"

#include "ATS_VIWER2Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CATS_VIWER2Doc

IMPLEMENT_DYNCREATE(CATS_VIWER2Doc, CDocument)

BEGIN_MESSAGE_MAP(CATS_VIWER2Doc, CDocument)
END_MESSAGE_MAP()


// CATS_VIWER2Doc 构造/析构

CATS_VIWER2Doc::CATS_VIWER2Doc()
{
	// TODO: 在此添加一次性构造代码

}

CATS_VIWER2Doc::~CATS_VIWER2Doc()
{
}

BOOL CATS_VIWER2Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 在此添加重新初始化代码
	// (SDI 文档将重用该文档)
	return TRUE;
}




// CATS_VIWER2Doc 序列化

void CATS_VIWER2Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}


// CATS_VIWER2Doc 诊断

#ifdef _DEBUG
void CATS_VIWER2Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CATS_VIWER2Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CATS_VIWER2Doc 命令
