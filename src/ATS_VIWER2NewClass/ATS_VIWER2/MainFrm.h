
// MainFrm.h : CMainFrame 类的接口
//

#pragma once
#include "MySplitterWnd.h"
class CATS_VIWER2View;

class CMainFrame : public CFrameWndEx
{
	
protected: // 仅从序列化创建
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 属性
protected:
	CMySplitterWnd m_wndSplitter;
	CMySplitterWnd m_wndSplitterMid;
public:

// 操作
public:

// 重写
public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);

// 实现
public:
	virtual ~CMainFrame();
	CATS_VIWER2View* GetRightPane();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 控件条嵌入成员
	CMFCMenuBar       m_wndMenuBar;
	CMFCToolBar       m_wndToolBar;
	CMFCToolBarImages m_UserImages;
public:
	CMFCStatusBar     m_wndStatusBar;

	CBitmap         m_bmpStatusBarIcon;

// 生成的消息映射函数
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnViewCustomize();
	afx_msg LRESULT OnToolbarCreateNew(WPARAM wp, LPARAM lp);
	afx_msg void OnApplicationLook(UINT id);
	afx_msg void OnUpdateApplicationLook(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()

public:
	virtual void ActivateFrame(int nCmdShow = -1);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMenuModeUps();
	afx_msg void OnMenuModeNet();
	afx_msg void OnUpdateMenuModeUps(CCmdUI *pCmdUI);
	afx_msg void OnUpdateMenuModeNet(CCmdUI *pCmdUI);
	afx_msg void OnMenuStart();
	afx_msg void OnMenuDataAuto();
	afx_msg void OnMenuDataAct();
	afx_msg void OnUpdateMenuDataAuto(CCmdUI *pCmdUI);
	afx_msg void OnUpdateMenuDataAct(CCmdUI *pCmdUI);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	void FullScreen(char state);
	afx_msg void OnMenuPlayGif();
	afx_msg void OnUpsStop();
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnHelp();
	afx_msg void OnClose();
};


