// CurvePlotDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ATS_VIWER2.h"
#include "CurvePlotDlg.h"
#include <math.h>

#define INDEX_CURRENT	0
#define INDEX_VOLTAGE	1
#define INDEX_POWER		2
// CCurvePlotDlg 对话框

IMPLEMENT_DYNAMIC(CCurvePlotDlg, CDialog)

int mSHOW;
CTime mCurrTime;

CRect m_rectBtnExit;
CRect m_rectOldWindow;
CRect m_rectOldPlotWindow;

int m_iBtnExitFromRight;
int m_iBtnExitFromBottom;

//数据类型
DATA_SOURCE_E mDSE;

float m_time;

unsigned char mBatID;
unsigned char mMacID;
unsigned char mUpsID;
CCurvePlotDlg::CCurvePlotDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCurvePlotDlg::IDD, pParent)
{
	m_iBtnExitFromRight = 10;
	m_iBtnExitFromBottom =10;
	mSHOW = 0;
	//默认显示实时数据
	mDSE = CUR_DS_REALTIME;
	m_time	= 0.0f;
	mBatID = 0;
	mMacID = 0;
	mUpsID = 0;

	mCurrTime = CTime::GetCurrentTime();
}

CCurvePlotDlg::~CCurvePlotDlg()
{
	DestroyWindow(); 
}

void CCurvePlotDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STA_PLOT, mPlot);
	DDX_Control(pDX, IDOK, m_btnExit);
}


BEGIN_MESSAGE_MAP(CCurvePlotDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_NCDESTROY()
END_MESSAGE_MAP()


// CCurvePlotDlg 消息处理程序

BOOL CCurvePlotDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetWindowRect(&m_rectOldWindow);
	m_btnExit.GetClientRect(&m_rectBtnExit);
	mPlot.GetWindowRect(&m_rectOldPlotWindow);
	TRACE("m_rectOldPlotWindow.left=%d,m_rectOldPlotWindow.width=%d\n",m_rectOldPlotWindow.left,m_rectOldPlotWindow.Width());
	ScreenToClient(&m_rectOldPlotWindow);
	TRACE("m_rectOldPlotWindow.left=%d,m_rectOldPlotWindow.width=%d\n",m_rectOldPlotWindow.left,m_rectOldPlotWindow.Width());
/*
#define INDEX_CURRENT	0
#define INDEX_VOLTAGE	1
#define INDEX_SOC		2
#define IDNEX_TEMP		3
*/
	mPlot.AddNewLine(_T("电流(A)"),PS_SOLID,RGB(0,255,0));
	mPlot.AddNewLine(_T("电压(V)"),PS_SOLID,RGB(255,0,0));
	mPlot.AddNewLine(_T("功率(W/10)"),PS_SOLID,RGB(218,165,32));

	mPlot.GetAxisX().AxisColor=RGB(0,255,0);
	mPlot.GetAxisY().AxisColor=RGB(0,255,0);

	mSHOW = 0;
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CCurvePlotDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CRect rect;
	GetClientRect(&rect);


	if(m_btnExit.GetSafeHwnd())
	{
		m_btnExit.MoveWindow(rect.right-m_iBtnExitFromRight-m_rectBtnExit.Width(),
			rect.bottom-m_iBtnExitFromBottom-m_rectBtnExit.Height(),
			m_rectBtnExit.Width(),m_rectBtnExit.Height());
	}
	if(mPlot.GetSafeHwnd())
	{
		//TRACE("m_rectOldPlotWindow.left=%d,m_rectOldPlotWindow.width=%d\n",m_rectOldPlotWindow.left,m_rectOldPlotWindow.Width());

		mPlot.MoveWindow(m_rectOldPlotWindow.left,
			m_rectOldPlotWindow.top,
			rect.right-m_iBtnExitFromRight-5-m_rectOldPlotWindow.left,
			rect.bottom-m_iBtnExitFromBottom-m_rectBtnExit.Height()-7-m_rectOldPlotWindow.top);
	}
}


/***
 * 添加数据
 */
void CCurvePlotDlg::PlotAddData(unsigned char macid, unsigned char upsid, DATA_TYPE_E dte, float ttime, float value)
{
	if(macid != mMacID || upsid != mUpsID)return;
	if(0 == mSHOW) return ;
	switch(dte)
	{
	case CUR_CURRENT:
		mPlot.AddNewPoint(ttime,value,INDEX_CURRENT);
		break;

	case CUR_VOLTAGE:
		mPlot.AddNewPoint(ttime,value,INDEX_VOLTAGE);
		break;

	case CUR_POEWER:
		mPlot.AddNewPoint(ttime,value,INDEX_POWER);
		break;
	default:
		break;
	}
}

void CCurvePlotDlg::PlotAddData(unsigned char macid, unsigned char upsid, CTime& ttime, float curr, float volt)
{
	CTimeSpan cts;
	//CTime cur = CTime::GetCurrentTime();
	if(0 == mSHOW) return ;
	cts = ttime - mCurrTime;

	if(macid != mMacID || upsid != mUpsID)return;

	//TRACE("-----> UPS:%d %d  curr:%f  vol:%f power:%f\n",macid,upsid,curr/1000,volt/1000,(curr/1000)*(volt/1000));
	PlotAddData(macid,upsid,CUR_CURRENT,(float)abs((long)(cts.GetTotalSeconds())),curr/1000);
	PlotAddData(macid,upsid,CUR_VOLTAGE,(float)abs((long)(cts.GetTotalSeconds())),volt/1000);
	PlotAddData(macid,upsid,CUR_POEWER,(float)abs((long)(cts.GetTotalSeconds())),(curr/1000)*(volt/1000)/10);
}

void CCurvePlotDlg::PlotAddData(unsigned char macid, unsigned char upsid, DATA_TYPE_E dte, CTime& ttime, float value)
{
	CTimeSpan cts;
	CTime cur = CTime::GetCurrentTime();
	if(0 == mSHOW) return ;
	cts = cur - mCurrTime;

	if(macid != mMacID || upsid != mUpsID)return;
	
	PlotAddData(macid,upsid,dte,(float)abs((long)(cts.GetTotalSeconds())),value);
}


/***
 * 是否是实时数据
 */
BOOL CCurvePlotDlg::IsRealtime()
{
	return (mDSE ==  CUR_DS_REALTIME ? TRUE:FALSE);
}

/***
 *	
 */
void CCurvePlotDlg::mShowWindow(int show, char macid, char upsid,DATA_SOURCE_E dse)
{
	mSHOW = show;
	mCurrTime = CTime::GetCurrentTime();
	mDSE = dse;
	if(show==5)
	{
		mPlot.SetRate(200);//
		mPlot.Start();
	}
	mMacID = macid;
	mUpsID = upsid;
	ShowWindow(show);
}

BOOL CCurvePlotDlg::IsShow()
{
	return mSHOW;
}
void CCurvePlotDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	float  y0 = 500*sin(m_time/5.0f);
	float y1 = 500*cos(m_time/10.0f);
	TRACE("x : %f y: %f\n",m_time,y0);

	mPlot.AddNewPoint(m_time,y0,0);
	mPlot.AddNewPoint(m_time,y1,1);
	m_time += 0.20f;
	CDialog::OnTimer(nIDEvent);
}


void CCurvePlotDlg::cur_set_batID(unsigned char id)
{
	mBatID = id;
}
unsigned char CCurvePlotDlg::cur_get_batID()
{
	return mBatID;
}
void CCurvePlotDlg::OnNcDestroy()
{
	CDialog::OnNcDestroy();

	//delete this;
}
