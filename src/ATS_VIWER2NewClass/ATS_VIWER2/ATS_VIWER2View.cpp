
// ATS_VIWER2View.cpp : CATS_VIWER2View 类的实现
//

#include "stdafx.h"
#include "ATS_VIWER2.h"
#include "ATS_VIWER2Doc.h"
#include "ATS_VIWER2View.h"
#include "cla\ats_main.h"
#include "battery\CBat.h"
#include "MainFrm.h"
#include <assert.h>
#include "CBatSW_Set.h"
#include "MemDC.h"
#include "battery\CBatBallEX.h"
#include "CNameplate.h"

#include "cla\cla_monitor.h"
#include "CPingBaodlg.h"
#include <windef.h>
#include "ProgressDlg.h"
#include "cla\CNotice.h"
#include "CControlPanel.h"
#include "CMinDisUnitDlg.h"
#include "CMainLog.h"

extern cla_monitor g_monitor;

CBrush gBrush(RGB(221,227,228));


//屏保
CCPingBaodlg* g_pb_dlg = NULL;

CCBatteryGIF gBatGIF;

typedef struct __UPS_ERROR_INFO
{
	char index;

	COLORREF colorBK1;
	COLORREF colorTx1;

	COLORREF colorBK2;
	COLORREF colorTx2;

}UPS_ERROR_INFO_T;
UPS_ERROR_INFO_T gUPS_error_info = {0,0,RGB(0,0,0),RGB(255,0,0),RGB(0,0,0)}; 

#define TIMER_UPS_ERROR 1	//UPS错误信息
#define TIMER_BAT_SHOW	2	//电池的动画显示
#define TIMER_MONITOR	3	//监控程序
#define TIMER_DEBUG		5

#define BAT_FRESH_INTERVAL 30


#ifdef _DEBUG
#define new DEBUG_NEW
#endif
extern ats_main theATS;
void sql_trace(char* format,...);
char gSelected = 0;

CCBat mBat[BATTERY_MAX];
CATS_VIWER2View* theVIEWER;

//unsigned char gCurrSelected = 0;

uchar gCurr_mod_id=0;

CCurvePlotDlg *gCurvePlot = NULL;
CProgressDlg *g_progress_dlg = NULL;

extern CCControlPanel * theCtrlPanel;	//控制面板


CCTopHead g_topHead;

extern CCMainLog *g_main_log;

//
CBatObject* g_obj = NULL;


//////////////////////////////////////////////////////////////////////////


void sql_trace(char* format,...)
{
	va_list vl;
	char buf[2048] = {0};

	va_start(vl,format);
	vsprintf_s(buf,format,vl);
	TRACE(buf);

	va_end(vl);
}

// CATS_VIWER2View

IMPLEMENT_DYNCREATE(CATS_VIWER2View, CFormView)

BEGIN_MESSAGE_MAP(CATS_VIWER2View, CFormView)

	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_SIZING()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

// CATS_VIWER2View 构造/析构

CATS_VIWER2View::CATS_VIWER2View()
	: CFormView(CATS_VIWER2View::IDD)
{
	// TODO: 在此处添加构造代码

	mBACK.LoadBitmap(IDB_BMP_BACK);
	m_first_flag = 0;
	m_uid.Format("00-00");
}

CATS_VIWER2View::~CATS_VIWER2View()
{
	if(gCurvePlot)
	{
		gCurvePlot->DestroyWindow();
		delete gCurvePlot;
		gCurvePlot = NULL;
	}

	if(g_pb_dlg)
	{
		g_pb_dlg->DestroyWindow();
		delete g_pb_dlg;
		g_pb_dlg = NULL;
	}

	if (g_progress_dlg)
	{
		g_progress_dlg->DestroyWindow();
		delete g_progress_dlg;
		g_progress_dlg = NULL;
	}
}

void CATS_VIWER2View::DoDataExchange(CDataExchange* pDX)
{

	CFormView::DoDataExchange(pDX);
}

BOOL CATS_VIWER2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式
	

	return CFormView::PreCreateWindow(cs);
}

void CATS_VIWER2View::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//添加界面句柄
	CCNotice* n = CCNotice::GetInstance();
	n->add_hand(this->GetSafeHwnd(),1);

	theVIEWER = this;

	SetTimer(TIMER_MONITOR,4000,NULL);

	m_pDC = GetDC();
	m_pDC->SetBkMode(TRANSPARENT);

/*
	RECT re;

	re.top = 60;
	re.left = 60;
	re.bottom =110;
	re.right = 150;
*/
	//初始化电池
	sql_init_battery();
	//初始化当前UPS信息
	
	g_obj = theATS.ats_get_obj(0,0);
	g_obj->obj_view_flag_set(1);
	

	sql_update_mod_btn(gSelected);

	//曲线图
	gCurvePlot = new CCurvePlotDlg();
	gCurvePlot->Create(CCurvePlotDlg::IDD,this);

	g_pb_dlg = new CCPingBaodlg();
	g_pb_dlg->Create(CCPingBaodlg::IDD,this);

	HBITMAP		hBitmap			= NULL;
	HINSTANCE	hInstResource	= NULL;
	hInstResource = AfxFindResourceHandle(MAKEINTRESOURCE(IDB_BMP_BACK), RT_BITMAP);

	//
	CRect client;
	GetClientRect(client);
	g_topHead.mCreate(WS_CHILD|WS_VISIBLE,CRect(client.left+20,client.top+5,client.right-20,client.top+60),this,10001);

	for (int i=0; i<16; i++)
	{
		mBat[i].ShowWindow(SW_SHOW);
	}

}

void CATS_VIWER2View::OnRButtonUp(UINT nFlags, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CATS_VIWER2View::OnContextMenu(CWnd* pWnd, CPoint point)
{
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
}


// CATS_VIWER2View 诊断

#ifdef _DEBUG
void CATS_VIWER2View::AssertValid() const
{
	CFormView::AssertValid();
}

void CATS_VIWER2View::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CATS_VIWER2Doc* CATS_VIWER2View::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CATS_VIWER2Doc)));
	return (CATS_VIWER2Doc*)m_pDocument;
}
#endif //_DEBUG


// CATS_VIWER2View 消息处理程序
void CATS_VIWER2View::show_message(CBatObject* obj)
{
	ATS_sys_info_t* asit = NULL;
	if(obj)
	{
		if((obj->m_mac_id == g_obj->m_mac_id) && (obj->m_ups_id == g_obj->m_ups_id)) return;
		g_obj->obj_view_flag_set(0);
		g_obj = obj;
		g_obj->obj_view_flag_set(1);
		
		g_topHead.cth_set_id(g_obj->m_mac_id,g_obj->m_ups_id);

		gBatGIF.batEx_set_asit(&(g_obj->m_asit));

		BatteryReset();

		sql_update_ui();
	}	
}


void CATS_VIWER2View::BatteryReset()
{
	for (int i=0; i<BATTERY_MAX; i++)
	{
		mBat[i].bat_init();
	}
}


//改变显示风格
void CATS_VIWER2View::change_view_style()
{
	int sw_type;

	int menable = 0;
	int mvis = 0;
	
	return g_pb_dlg->start();

	if(VS_NOMAL == theATS.bat_viewStyle)
	{
		menable = 1;
		mvis = 1;
		sw_type = SW_SHOW;
	}
	else
	{
		menable = 0;
		mvis = 0;
		sw_type = SW_HIDE;

	}

	for (int i=0; i<16; i++)
	{
		mBat[i].ShowWindow(sw_type);
	}


	if(VS_NOMAL == theATS.bat_viewStyle) 
		KillTimer(TIMER_BAT_SHOW);
	else
	{
		g_pb_dlg->start();
		//SetTimer(TIMER_BAT_SHOW,BAT_FRESH_INTERVAL,NULL);
	}
}


//
void CATS_VIWER2View::ats_show_progress(char flag/* = 1*/, unsigned int timeout/* = 3000*/)
{

	if(!g_progress_dlg)
	{
		g_progress_dlg = new CProgressDlg();
		g_progress_dlg->Create(CProgressDlg::IDD,this);
	}
	g_progress_dlg->CenterWindow();
	g_progress_dlg->m_show(flag,timeout);

}


// 更新按钮风格
void CATS_VIWER2View::sql_update_mod_btn(char selected, ATS_sys_info_t* asit)
{
	sql_update_ui();
}


void CATS_VIWER2View::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CMainFrame* pMF = (CMainFrame *)AfxGetMainWnd();
	//pMF->FullScreen(0);
	CFormView::OnLButtonDown(nFlags, point);
}


void CATS_VIWER2View::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);
	
	int le =0;
	CRect client;
	GetClientRect(client);
	gBatGIF.batEx_set_rect(client,client);

	if(IsWindow(g_topHead.m_hWnd))
	{
		client.left += 20;
		client.top += 5;
		client.bottom = client.top + 60;
		client.right -= 20;
		g_topHead.MoveWindow(client);
		g_topHead.ShowWindow(SW_SHOW);
	}
	sql_init_battery();
}


//初始化电池信息
void CATS_VIWER2View::sql_init_battery()
{
	int index = 0;
	CRect client(0,0,0,0),sub(0,0,0,0);
	DWORD vewStype = 0;
	CWnd* pWnd = GetDlgItem(IDC_BATTERY_VEVT);
	
	if ( IsWindow(this->GetSafeHwnd())){

		//动画
		GetWindowRect(client);
		GetClientRect(sub);
		gBatGIF.batEx_set_rect(sub,sub);
		gBatGIF.batEx_initlize(GetDC());
		if(g_obj)
			gBatGIF.batEx_set_asit(&(g_obj->m_asit));

		if(pWnd)
		{
			//正常模式
			GetWindowRect(client);
			ScreenToClient(client);
			client.top += 60;

			if(VS_NOMAL == theATS.bat_viewStyle)
				vewStype = WS_CHILD|WS_VISIBLE;
			else
				vewStype = WS_CHILD;

			int subW = client.Width()/4;
			int subH = client.Height()/4;

			for (int i=0; i<4; i++)
			{
				int dd = (i)*BAT_HIGHT;
				for (int j=0; j<4; j++)
				{
					sub.SetRect(client.left + subW*j + 40, client.top + i*subH + 15 ,
						client.left + subW*(j+1) - 40, client.top + (i+1)*subH - 15 );

					TRACE("create battery: %d\n",index);
					if(0 == mBat[index].bat_CheckState())
						mBat[index].bat_set_identy(index + 1);
					mBat[index].Create(WS_CHILD|WS_VISIBLE,sub,this,BATTERY_ID0+index);
					index++;
				}
			}
			Invalidate(TRUE);
		}
	}
}


//消息处理
LRESULT CATS_VIWER2View::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 在此添加专用代码和/或调用基类
	ATS_send_msg_t asmt;

	if(MSG_UPDATE == message)
	{
		memset(&asmt,0,sizeof(asmt));
		memcpy(&asmt,(ATS_send_msg_t*)wParam,sizeof(ATS_send_msg_t));

		uchar mac_id = asmt.mac_id;
		uchar ups_id = asmt.ups_id;

		if((g_obj->m_mac_id == g_obj->m_mac_id) && (g_obj->m_ups_id == g_obj->m_ups_id))
			sql_update_ui();
		
	}

	else if (MSG_UI_RESET == message)
	{
		sql_update_mod_btn(gSelected);
	}

	else if (MSG_UPS_CURVE_DATA == message)
	{
		CBatObject* obj = NULL;
		memset(&asmt,0,sizeof(asmt));
		memcpy(&asmt,(ATS_send_msg_t*)wParam,sizeof(ATS_send_msg_t));

		obj = theATS.ats_get_obj(asmt.mac_id,asmt.ups_id);
		ATS_sys_info_t* asit = &(obj->m_asit);

		CTime ct = CTime::GetCurrentTime();
		gCurvePlot->PlotAddData(asmt.mac_id,asmt.ups_id,ct,
			(float)asit->sitEX.sysEX_charge_current,(float)asit->sitEX.sysEX_charge_voltage);

	}
	//电池屏蔽
	else if(MSG_BATTERY_MASK == message)
	{
		uchar id = (uchar)wParam;
		int mask = (int)lParam;

		if(1 == mask)
			g_obj->ATS_battery_mask(NULL,NZ_ACT_READ,gSelected,NULL,NULL,WPARAM_BAT_MASK,(uint)id);
		else
			g_obj->ATS_battery_mask(NULL,NZ_ACT_READ,gSelected,NULL,NULL,WPARAM_BAT_UNMASK,(uint)id);

	}
	//
	else if (MSG_MAC_UPS_CHANGED == message)
	{
		ATS_ident_t ait;
		ait.shelf_id = (int)wParam;
		ait.ups_id = (int)lParam;
		CBatObject* obj = theATS.ats_get_obj(ait.shelf_id,ait.ups_id);

		show_message(obj);
	}

	//电池动作
	else if (MSG_BATTERY_ACT == message)
	{
		sql_battery_action(message,wParam,lParam);		
	}

	//显示曲线图
	else if (MSG_UPS_CURVE_PLOT == message)
	{
		int d = (int)wParam;
		ATS_ident_t ait;
		memset(&ait,0,sizeof(ait));
		memcpy(&ait,(ATS_ident_t*)lParam,sizeof(ait));

		if(1 == 0)
			gCurvePlot->mShowWindow(SW_SHOW,ait.shelf_id,ait.ups_id);
		else
			gCurvePlot->mShowWindow(SW_SHOW,ait.shelf_id,ait.ups_id,CUR_DS_HISTORY);
	}

	//loading
	else if (MSG_SHOW_PROGRESS == message)
	{
		int va1 = (int)wParam;
		int timeout  = (int)lParam;
		//ats_show_progress(va1,timeout);
	}
	
	//设置最小放电单元数
	else if (MSG_MIN_DIS_UNIT_SET == message)
	{
		CBatObject* obj = (CBatObject*)wParam;

		if(obj)
			ats_set_min_dis_unit(obj);
	}
	
	return CFormView::DefWindowProc(message, wParam, lParam);
}


//处理电池的动作
void CATS_VIWER2View::sql_battery_action(uint message, WPARAM wParam, LPARAM lParam)
{
	long action = (long)wParam;
	uchar uid = (uchar)lParam;

	//查询状态
		uint bbid = (gSelected*16+uid)/2;
		if(BAT_MENU_QUERY_STA == action)
		{
			g_obj->ATS_bat_status_Ex(NULL,gSelected);
		}
		//查询电流
		else if (BAT_MENU_QEURY_CURRENT == action)	
		{
			g_obj->ATS_bat_current_Ex(NULL,bbid);
		}
		//查询电压
		else if (BAT_MENU_QUERY_VAL == action)	
		{
			g_obj->ATS_bat_voltage_Ex(NULL,bbid);
		}
		// SOC
		else if (BAT_MENU_QUERY_SOC == action)	
		{
			bbid = (gSelected*16+uid)/4;
			g_obj->ATS_bat_soc_Ex(NULL,bbid);
		}
		//查询温度
		else if (BAT_MENU_QUERY_TEMPRATURE == action)
		{
			g_obj->ATS_bat_module_temperature_Ex(NULL);
		}
		//查询开关
		else if(BAT_MENU_QUERY_SWITCH == action)
		{
			bbid = (gSelected*16+uid)/32;
			g_obj->ATS_bat_switch_Ex(NULL,bbid);
		}
		//查询是否屏蔽
		else if (BAT_MENU_MASK_QUERY == action)
		{
			g_obj->ATS_battery_mask(NULL,NZ_ACT_READ,gSelected,NULL);
		}
}


//更新界面
void CATS_VIWER2View::sql_update_ui()
{
	CString tmp("");
	CString val1(""),val2("");
	ATS_sys_info_t * asit = &(g_obj->m_asit);

	//////////////////////////////////////////////////////////////////////////

	if(BMS_MODE_NET_GFKD == asit->sitEX.sysEX_bms_mode){

		if(S_EX_STATE_CHARG ==  asit->sitEX.sysEX_state || S_EX_STATE_DISCHARGE == asit->sitEX.sysEX_state)
		{
			g_topHead.cth_set_params(asit->sitEX.sysEX_charge_current,asit->sitEX.sysEX_charge_voltage);
		}

		else
		{
			g_topHead.cth_set_params(asit->sit.mod[gSelected].attr.current.charge,
				asit->sit.mod[gSelected].attr.voltage.charge);
		}
	}
	else
	{
		if(S_EX_STATE_CHARG ==  asit->sitEX.sysEX_state || S_EX_STATE_DISCHARGE == asit->sitEX.sysEX_state)

			g_topHead.cth_set_params(asit->sitEX.sysEX_charge_current,asit->sitEX.sysEX_charge_voltage);
		else
			g_topHead.cth_set_params(0,0);
	}
	
	g_topHead.cth_set_bms_mode(asit->sitEX.sysEX_bms_mode); //设置工作模式
	g_topHead.cth_set_status(asit->sitEX.sysEX_state);		//设置系统状态
	g_topHead.cth_set_debug(asit->sitEX.sysEX_bms_output_mode);	//设置系统的debug模式
	g_topHead.cth_set_min_dis(asit->sitEX.sysEX_min_discha_unit);

	//设置四个模块的使能
	theCtrlPanel->ctl_set_enable(0,asit->sit.mod[0].join_discha,false);
	theCtrlPanel->ctl_set_enable(1,asit->sit.mod[1].join_discha,false);
	theCtrlPanel->ctl_set_enable(2,asit->sit.mod[2].join_discha,false);
	theCtrlPanel->ctl_set_enable(3,asit->sit.mod[3].join_discha);

	g_topHead.cth_fresh();

	theCtrlPanel->ctrl_update_btn();
	//////////////////////////////////////////////////////////////////////////
	
	sql_udpate_bat(asit,gSelected);
  
	return ;

}


//修改电池单体
void CATS_VIWER2View::sql_udpate_bat(ATS_sys_info_t* asit, uchar id/* = 0*/)
{
	int i= 0;
	int index = 0;
	for (i =0; i<4; i++)
	{
		for (int j =0; j<4; j++)
		{
			//设置开关
			mBat[index].bat_set_sw(asit->sit.mod[gSelected].bat_unit[i].battery[j].attr.sw);

			//设置电压
			mBat[index].bat_set_voltage(asit->sit.mod[id].bat_unit[i].battery[j].attr.voltage.charge);

			//设置soc
			mBat[index].bat_set_soc(asit->sitEX.mod[id].bat_unit[i].battery[j].vst.soc);

			//电流
			mBat[index].bat_set_current(asit->sit.mod[id].bat_unit[i].battery[j].attr.current.charge);

			//温度
			mBat[index].bat_set_temprature((float)asit->sit.mod[id].temperature);


			//是否参与充放电
			mBat[index].bat_participation(asit->sit.mod[id].join_discha);

			//是否屏蔽
			mBat[index].bat_set_mask(asit->sit.mod[gSelected].bat_unit[i].battery[j].attr.isMask);

			if(asit->sitEX.sysEX_bms_mode == BMS_MODE_NET_GFKD)
				mBat[index].bat_set_mode(CCBat::BAT_MODE_NET);

			else
				mBat[index].bat_set_mode(CCBat::BAT_MODE_UPS);

			if(S_EX_STATE_DISCHARGE == asit->sitEX.sysEX_state)
				mBat[index].bat_set_sys_state(CCBat::S_DISCHARGING);
			else if(S_EX_STATE_CHARG == asit->sitEX.sysEX_state)
				mBat[index].bat_set_sys_state(CCBat::S_CHARGING);
			else if(S_EX_STATE_CJDR_CHARGE == asit->sitEX.sysEX_state)
				mBat[index].bat_set_sys_state(CCBat::S_SUPERCARGING);
			else if(S_EX_STATE_ERROR == asit->sitEX.sysEX_state)
				mBat[index].bat_set_sys_state(CCBat::S_ERROR);
			else if(S_EX_STATE_UPS_WAIT_DOWN == asit->sitEX.sysEX_state)
				mBat[index].bat_set_sys_state(CCBat::S_UPS_WAIT_FOR_STOP);
			else if(S_EX_STATE_UPS_DOWN== asit->sitEX.sysEX_state)
				mBat[index].bat_set_sys_state(CCBat::S_UPS_STOP);
			else
				mBat[index].bat_set_sys_state(CCBat::S_IDLE);


			//电池状态1
			if(B_EX_STATE_NORMAL == asit->sitEX.mod[id].bat_unit[i].battery[j].batEX_state)
				mBat[index].bat_set_state(CCBat::BS_NORMAL);
			else if(B_EX_STATE_CUR_HI == asit->sitEX.mod[id].bat_unit[i].battery[j].batEX_state)
				mBat[index].bat_set_state(CCBat::BS_VOL_HI);
			else if(B_EX_STATE_CUR_LO == asit->sitEX.mod[id].bat_unit[i].battery[j].batEX_state)
				mBat[index].bat_set_state(CCBat::BS_VOL_LO);
			else if(B_EX_STATE_ERROR == asit->sitEX.mod[id].bat_unit[i].battery[j].batEX_state)
				mBat[index].bat_set_state(CCBat::BS_BAD);

			mBat[index].bat_update_ui();/**/
			index++;
		}
	}
}


void CATS_VIWER2View::OnClose()
{
	if(gCurvePlot)
	{
		gCurvePlot->DestroyWindow();
		delete gCurvePlot;
		gCurvePlot = NULL;
	}
	CFormView::OnClose();
}

void CATS_VIWER2View::OnDestroy()
{
	CFormView::OnDestroy();
}


//添加数据
void CATS_VIWER2View::sql_curve_add_data(ATS_ident_t* ait, DATA_TYPE_E dte, float value)
{
	CTime ct = CTime::GetCurrentTime();
	if(gCurvePlot && gCurvePlot->IsShow())
	{
		gCurvePlot->PlotAddData(ait->shelf_id,ait->ups_id,dte,ct,value);
	}
}


// 是否是能源互联网模式
char CATS_VIWER2View::sql_is_net_mode()
{
	if(g_obj->m_asit.sitEX.sysEX_bms_mode == BMS_MODE_NET_GFKD)
		return 1;
	else 
		return 0;
}

//sql test
int soc_step = 0;

void CATS_VIWER2View::OnTimer(UINT_PTR nIDEvent)
{
    if (TIMER_BAT_SHOW == nIDEvent)
	{
		Invalidate();
	}
	else if (TIMER_MONITOR == nIDEvent)
	{
		g_monitor.mon_ping(0);
	}

	else if (TIMER_DEBUG == nIDEvent)
	{
		
		if(soc_step < 100)
		{
			for (int i=0; i< 4; i++)
			{
				for (int j=0; j<4; j++)
				{
					for(int n = 0; n< 4; n++)
					{
						g_obj->m_asit.sitEX.mod[i].bat_unit[j].battery[n].vst.soc = soc_step;
					}//for(int n = 0; n< 4; n++)
				}//for (int j=0; j<4; j++)

			}//for (int i=0; i< 4; i++)

			soc_step ++;
		}
	
	}

	CFormView::OnTimer(nIDEvent);
}


void CATS_VIWER2View::net_system_control(unsigned char str)
{
	//NET
	if(BMS_MODE_NET_GFKD == g_obj->m_asit.sitEX.sysEX_bms_mode){

		g_obj->ATS_system_control_GFKD(NULL,str,NULL,WPARAM_NET_SYSTEM_CONTROL,(uint)str);
	}
}



HBRUSH CATS_VIWER2View::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	pDC->SetBkMode(TRANSPARENT);
	/**/
	if(nCtlColor == CTLCOLOR_STATIC )
	{
		pDC->SetBkMode(TRANSPARENT);//设置背景透明
		pDC->SetTextColor(RGB(0,0,255));//设置字体为黄色
		return (HBRUSH)::GetStockObject(NULL_BRUSH);
	}
	
	return gBrush;
	return hbr;
}


BOOL CATS_VIWER2View::OnEraseBkgnd(CDC* pDC)
{
	if (VS_NOMAL == theATS.bat_viewStyle)
	{
		 //CFormView::OnEraseBkgnd(pDC);
		CRect rect;
		GetWindowRect(rect);
		ScreenToClient(rect);
		 BITMAP bit;

		 mBACK.GetBitmap(&bit);

		 CDC MemDC;
		 MemDC.CreateCompatibleDC(pDC);
		 CBitmap *pOldBMP = (CBitmap*)MemDC.SelectObject(&mBACK);
		 pDC->TransparentBlt(rect.left,rect.top,rect.Width(),rect.Height(),&MemDC,0,0,bit.bmWidth,bit.bmHeight,RGB(0,0,0));
		 
		 if(pOldBMP)
			 MemDC.SelectObject(pOldBMP);
		 MemDC.DeleteDC();
		 return  TRUE;
	}
	else
		return TRUE;

	return TRUE;
}



void CATS_VIWER2View::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 CFormView::OnPaint()

	if(VS_NOMAL == theATS.bat_viewStyle)
	{
		//正常显示
		if (IsWindow(g_topHead.m_hWnd))
			g_topHead.ShowWindow(SW_SHOW);
	}
	else
	{
		CRect rect,client;
		GetClientRect(client);
		GetClientRect(rect);

		if (IsWindow(g_topHead.m_hWnd))
			g_topHead.ShowWindow(SW_HIDE);


		//初始化动画环境
		if(0 == m_first_flag)
		{
			m_memDC.CreateCompatibleDC(NULL);

			m_memBmp.CreateCompatibleBitmap(&dc,rect.Width(),rect.Height());
			m_memDC.SelectObject(m_memBmp);
			m_memDC.SetBkMode(TRANSPARENT);
			m_first_flag = 1;

		}

		gBatGIF.batEx_set_rect(rect,client);
		gBatGIF.batEx_get_memDC(&m_memDC ,rect,&dc);

		dc.BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&m_memDC,0,0,SRCCOPY);
	}

}


void CATS_VIWER2View::OnMouseMove(UINT nFlags, CPoint point)
{
	CMainFrame* pMF = (CMainFrame *)AfxGetMainWnd();
	CFormView::OnMouseMove(nFlags, point);
}


void CATS_VIWER2View::OnRButtonDown(UINT nFlags, CPoint point)
{
	g_pb_dlg->start();
}


//更换当前模块
void CATS_VIWER2View::v_update_moduel_selected(char id)
{
	gSelected = id;
	BatteryReset();
	v_update_module_info(gSelected);
}


//更新模块信息
void CATS_VIWER2View::v_update_module_info(char mid)
{
	sql_udpate_bat(&(g_obj->m_asit),mid);
}


// 获取工作模式
char CATS_VIWER2View::v_get_system_status()
{
	return g_obj->m_asit.sitEX.sysEX_bms_mode;
}


// 设置最小放电单元数
void CATS_VIWER2View::ats_set_min_dis_unit(CBatObject* obj)
{
	/**/
	if(!obj)return ;
	CCMinDisUnitDlg dlg;
	dlg.DoModal();
	
}