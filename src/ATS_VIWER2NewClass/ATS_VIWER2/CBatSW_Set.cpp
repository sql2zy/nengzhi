// CBatSW_Set.cpp : 实现文件
//

#include "stdafx.h"
#include "ATS_VIWER2.h"
#include "CBatSW_Set.h"


// CCBatSW_Set 对话框

IMPLEMENT_DYNAMIC(CCBatSW_Set, CDialog)

CCBatSW_Set::CCBatSW_Set(CWnd* pParent /*=NULL*/)
	: CDialog(CCBatSW_Set::IDD, pParent)
{
	SW_STATE = 0;
}

CCBatSW_Set::~CCBatSW_Set()
{
}

void CCBatSW_Set::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCBatSW_Set, CDialog)
	ON_BN_CLICKED(IDOK, &CCBatSW_Set::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCBatSW_Set::OnBnClickedCancel)
	ON_COMMAND_RANGE(IDC_CK_ROW_1,IDC_CK_ROW_9,OnBoxChanged)
END_MESSAGE_MAP()


// CCBatSW_Set 消息处理程序

void CCBatSW_Set::OnBnClickedOk()
{
	SW_STATE = 0;
	for (int i=IDC_CHECK_BAT_1; i<=IDC_CHECK_BAT_16; i++)
	{
		if(BST_CHECKED == IsDlgButtonChecked(i))
		{
			SW_STATE = SW_STATE |(1<<(i-IDC_CHECK_BAT_1));
		}
	}
	OnOK();
}

void CCBatSW_Set::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	OnCancel();
}


//设置\获取开关
void CCBatSW_Set::cbs_sw_state_set(WORD sw)
{
	SW_STATE = sw;
}


/***
 * 获取
 */
WORD CCBatSW_Set::cbs_sw_state_get()
{
	return SW_STATE;
}


BOOL CCBatSW_Set::OnInitDialog()
{
	CDialog::OnInitDialog();

	for (int i=0; i<16; i++)
	{

		if(SW_STATE & (1<<i))
		{
			((CButton*)GetDlgItem(i+IDC_CHECK_BAT_1))->SetCheck(1);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CCBatSW_Set::OnBoxChanged(UINT uID)
{
	UINT start_id = 0,end_id = 0, step=1;
	int  check = 0;

	if(IsDlgButtonChecked(uID))
		check = 1;

	switch(uID)
	{
	case IDC_CK_ROW_1:
		{
			start_id = IDC_CHECK_BAT_1;
			end_id   = start_id+3;
		}
		break;

	case IDC_CK_ROW_2:
		{
			start_id = IDC_CHECK_BAT_5;
			end_id   = start_id+3;
		}
		break;

	case IDC_CK_ROW_3:
		{
			start_id = IDC_CHECK_BAT_9;
			end_id   = start_id+3;
		}
		break;

	case IDC_CK_ROW_4:
		{
			start_id = IDC_CHECK_BAT_13;
			end_id   = start_id+3;
		}
		break;

	case IDC_CK_ROW_5:
		{
			start_id = IDC_CHECK_BAT_1;
			end_id   = IDC_CHECK_BAT_13;
			step = 4;
		}
		break;
	case IDC_CK_ROW_6:
		{
			start_id = IDC_CHECK_BAT_2;
			end_id   = IDC_CHECK_BAT_14;
			step = 4;
		}
		break;
	case IDC_CK_ROW_7:
		{
			start_id = IDC_CHECK_BAT_3;
			end_id   = IDC_CHECK_BAT_15;
			step = 4;
		}
		break;
	case IDC_CK_ROW_8:
		{
			start_id = IDC_CHECK_BAT_4;
			end_id   = IDC_CHECK_BAT_16;
			step = 4;
		}
		break;
	case IDC_CK_ROW_9:
		{
			start_id = IDC_CHECK_BAT_1;
			end_id   = IDC_CHECK_BAT_16;

			for (UINT i=IDC_CK_ROW_1; i<=IDC_CK_ROW_9;i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(check);
			}
		}
		break;
	}

	for (UINT i=start_id; i<=end_id;i += step)
	{
		 ((CButton*)GetDlgItem(i))->SetCheck(check);
	}
}