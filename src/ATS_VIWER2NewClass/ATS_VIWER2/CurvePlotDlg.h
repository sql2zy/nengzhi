#pragma once
#include "Plot.h"
#include "afxwin.h"


// CCurvePlotDlg 对话框

// 对话框数据


typedef enum __DATA_TYPE{
	CUR_CURRENT=0,
	CUR_VOLTAGE,
	CUR_POEWER,
}DATA_TYPE_E;

typedef enum __DATA_SOURCE
{
	CUR_DS_REALTIME=0,
	CUR_DS_HISTORY

}DATA_SOURCE_E;

class CCurvePlotDlg : public CDialog
{
	DECLARE_DYNAMIC(CCurvePlotDlg)

public:
	CCurvePlotDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCurvePlotDlg();

enum { IDD = IDD_DLG_CURVE_PLOT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

private:



public:


	RealtimeCurve::CPlot mPlot;
	virtual BOOL OnInitDialog();
	CButton m_btnExit;
	afx_msg void OnSize(UINT nType, int cx, int cy);

	void PlotAddData(unsigned char macid, unsigned char upsid, DATA_TYPE_E dte, float ttime, float value);
	void PlotAddData(unsigned char macid, unsigned char upsid, DATA_TYPE_E dte, CTime& ttime, float value);
	void PlotAddData(unsigned char macid, unsigned char upsid, CTime& ttime, float curr, float volt);

	BOOL IsRealtime();
	void mShowWindow(int show,char macid, char upsid, DATA_SOURCE_E dse = CUR_DS_REALTIME);

	BOOL IsShow();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void cur_set_batID(unsigned char id);
	unsigned char cur_get_batID();
	afx_msg void OnNcDestroy();
};
