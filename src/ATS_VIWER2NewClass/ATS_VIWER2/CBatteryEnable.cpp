// CBatteryEnable.cpp : 实现文件
//

#include "stdafx.h"
#include "ATS_VIWER2.h"
#include "CBatteryEnable.h"


// CCBatteryEnable 对话框

IMPLEMENT_DYNAMIC(CCBatteryEnable, CDialog)

CCBatteryEnable::CCBatteryEnable(CWnd* pParent /*=NULL*/)
	: CDialog(CCBatteryEnable::IDD, pParent)
{
	mEnable = 0;
	mResult = 0;
}

CCBatteryEnable::~CCBatteryEnable()
{
}

void CCBatteryEnable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCBatteryEnable, CDialog)
	ON_BN_CLICKED(IDOK, &CCBatteryEnable::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCBatteryEnable::OnBnClickedCancel)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CCBatteryEnable 消息处理程序

void CCBatteryEnable::OnBnClickedOk()
{
	char tmp=0;
	if(((CButton*)(GetDlgItem(IDC_RA_1_YES)))->GetCheck())
		tmp = tmp|(0x01);

	if(((CButton*)(GetDlgItem(IDC_RA_2_YES)))->GetCheck())
		tmp = tmp|(0x02);

	if(((CButton*)(GetDlgItem(IDC_RA_3_YES)))->GetCheck())
		tmp = tmp|(0x04);
	
	if(((CButton*)(GetDlgItem(IDC_RA_4_YES)))->GetCheck())
		tmp = tmp|(0x08);
	mResult = tmp;
	OnOK();
}

void CCBatteryEnable::OnBnClickedCancel()
{
	mResult = mEnable;
	OnCancel();
}


void CCBatteryEnable::cbe_set_enable(char mask)
{
	mEnable = mask;
}

char CCBatteryEnable::cbe_get_eanble()
{
	return mResult;
}

void CCBatteryEnable::cbe_set_text(CString title, CString information, CString yT, CString nT)
{
	mTitle = title;
	mInfo = information;
	mYT = yT;
	mNT = nT;
}

BOOL CCBatteryEnable::OnInitDialog()
{
	CDialog::OnInitDialog();

	if(mEnable & 0x01)
		((CButton*)(GetDlgItem(IDC_RA_1_YES)))->SetCheck(1);
	else
		((CButton*)(GetDlgItem(IDC_RA_1_NO)))->SetCheck(1);

	if(mEnable & 0x02)
		((CButton*)(GetDlgItem(IDC_RA_2_YES)))->SetCheck(1);
	else
		((CButton*)(GetDlgItem(IDC_RA_2_NO)))->SetCheck(1);

	if(mEnable & 0x04)
		((CButton*)(GetDlgItem(IDC_RA_3_YES)))->SetCheck(1);
	else
		((CButton*)(GetDlgItem(IDC_RA_3_NO)))->SetCheck(1);

	if(mEnable & 0x08)
		((CButton*)(GetDlgItem(IDC_RA_4_YES)))->SetCheck(1);
	else
		((CButton*)(GetDlgItem(IDC_RA_4_NO)))->SetCheck(1);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CCBatteryEnable::OnPaint()
{
	CRect rect;
	CPaintDC dc(this); // device context for painting
	char tmp[11] = {0};
	CFont * m_font,* oldfont;

	m_font = new CFont();
	m_font->CreateFont(12,               // 以逻辑单位方式指定字体的高度
		0,                               // 以逻辑单位方式指定字体中字符的平均宽度
		0,                               // 指定偏离垂线和X轴在显示面上的夹角(单位:0.1度)
		0,                               // 指定符串基线和X轴之间的夹角(单位:0.1度)
		FW_NORMAL,                       // 指定字体镑数
		FALSE,                           // 是不是斜体
		FALSE,                           // 加不加下划线
		0,                               // 指定是否是字体字符突出
		ANSI_CHARSET,                    // 指定字体的字符集
		OUT_DEFAULT_PRECIS,              // 指定所需的输出精度
		CLIP_DEFAULT_PRECIS,             // 指定所需的剪贴精度
		DEFAULT_QUALITY,                 // 指示字体的输出质量
		DEFAULT_PITCH | FF_SWISS,        // 指定字体的间距和家族
		_T("宋体")                       // 指定字体字样的名称
		);
	oldfont = (CFont*)dc.SelectObject(m_font);
	GetDlgItem(IDC_STA_TEXT)->GetWindowRect(rect);
	ScreenToClient(rect);
	// 	CBrush brush(RGB(255,255,255)); 
	// 	MemDC.FillRect(rect,&brush);

	dc.SetTextAlign(TA_CENTER|TA_CENTER);
	dc.SetBkMode(TRANSPARENT);
	dc.SetTextColor(RGB(0,127,63));

	dc.TextOut(rect.left+((int)(rect.Width()/2) - (int)(mInfo.GetLength()/2)),rect.top+10,mInfo);
	
	dc.SelectObject(oldfont);
	delete m_font;
}

void CCBatteryEnable::PreInitDialog()
{
	SetWindowText(mTitle);
	GetDlgItem(IDC_RA_1_YES)->SetWindowText(mYT);
	GetDlgItem(IDC_RA_2_YES)->SetWindowText(mYT);
	GetDlgItem(IDC_RA_3_YES)->SetWindowText(mYT);
	GetDlgItem(IDC_RA_4_YES)->SetWindowText(mYT);

	GetDlgItem(IDC_RA_1_NO)->SetWindowText(mNT);
	GetDlgItem(IDC_RA_2_NO)->SetWindowText(mNT);
	GetDlgItem(IDC_RA_3_NO)->SetWindowText(mNT);
	GetDlgItem(IDC_RA_4_NO)->SetWindowText(mNT);

	CDialog::PreInitDialog();
}
